const serviceWorkerVersion = 'v1';
const path = location.protocol + "//" + location.host;
const cache_files = [
    `${path}/offline`,
    `${path}/offline/style.css`,
    `${path}/offline/app.js`,
    `${path}/images/common/logo.svg`,
    `${path}/images/offline/skull.svg`,
    `${path}/images/offline/wifi.svg`,
    `${path}/images/offline/globe.svg`,
    `${path}/images/offline/mood_bad.svg`,
    `${path}/images/offline/mood_frustrated.svg`,
    `${path}/images/offline/phone.svg`,
    `${path}/images/offline/laptop.svg`,
    `${path}/images/offline/moon.svg`,
    `${path}/images/offline/mobiledata_off.svg`,
    'https://cdnjs.cloudflare.com/ajax/libs/bulma/1.0.2/css/bulma.min.css',
];


self.addEventListener('install', (event) => {
    self.skipWaiting();
    event.waitUntil(
        (async () => {
            const cache = await caches.open(serviceWorkerVersion);
            await cache.addAll([...cache_files]);
        })()
    );
    //console.log(`Service Worker ${serviceWorkerVersion} : Installed`);
})

self.addEventListener('activate', (event) => {
    clients.claim();
    event.waitUntil((async () => {
        const keys = await caches.keys();
        await Promise.all(keys.map((key) => {
            if (key !== serviceWorkerVersion) {
                return caches.delete(key);
            }
        }));
    })());
    //console.log(`${serviceWorkerVersion} : activated`);
})
self.addEventListener("fetch", (event) => {
    //console.log(`Service Worker ${serviceWorkerVersion} : Fetching : ` + event.request.url);
    if (event.request.mode === 'navigate') {
        event.respondWith((
            async () => {
                try {
                    const preloadResponse = await event.preloadResponse;
                    if (preloadResponse) {
                        return preloadResponse;
                    }
                    return await fetch(event.request);
                } catch (e) {
                    const cache = await caches.open(serviceWorkerVersion);
                    return await cache.match('/offline');
                }
            })
        ());
    } else if (cache_files.includes(event.request.url)) {
        event.respondWith(
            caches.match(event.request)
        );
    }
});


self.addEventListener("push", async function (event) {
    event.stopImmediatePropagation();
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        console.warn('Notifications are not supported or permission not granted.');
        return;
    }
    const payload = event.data ? event.data.json() : {};
    event.waitUntil(
        self.registration.showNotification(payload.title, payload)
    );
});

self.addEventListener("notificationclick", (event) => {
    event.notification.close();
    event.waitUntil(openUrl(path + '/home'));
});

async function openUrl(url) {
    const windowClients = await self.clients.matchAll({
        type: "window",
        includeUncontrolled: true,
    });
    for (let i = 0; i < windowClients.length; i++) {
        const client = windowClients[i];
        if (client.url === url && "focus" in client) {
            return client.focus();
        }
    }
    if (self.clients.openWindow) {
        return self.clients.openWindow(url);
    }
    return null;
}
