function animateOfflineShapes() {
    let shapesElement = document.querySelector('.shapes');
    if (shapesElement !== null) {
        let html = '';
        for (let i = 1; i <= 50; i++) {
            html += '<div class="shape-container--' + i + ' shape-animation"><div class="random-shape"></div></div>';
        }
        shapesElement.innerHTML += html;
    }
}

document.addEventListener('DOMContentLoaded', animateOfflineShapes);
