<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Beaute du diable is offline</title>
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="https://beautedudiable.fr/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://beautedudiable.fr/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://beautedudiable.fr/favicon-16x16.png">
    <link rel="shortcut icon" href="https://beautedudiable.fr/favicon.ico" type="image/x-icon">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/1.0.2/css/bulma.min.css" rel="stylesheet">
    <link href="https://beautedudiable.fr/offline/style.css" rel="stylesheet">
    <script src="https://beautedudiable.fr/offline/app.js" defer></script>
</head>
<body>
<section class="hero is-fullheight">
    <div class="hero-body m-auto">
        <div class="shapes"></div>
        <div class="columns mx-auto is-centered">
            <div class="column is-12-touch is-8-tablet is-4-desktop m-auto">
                <div class="card">
                    <div class="card-header">
                        <figure class="image is-64x64 mx-auto is-flex mt-3">
                            <img src="https://beautedudiable.fr/images/common/logo.svg" alt="logo" class="is-rounded">
                        </figure>
                    </div>
                    <div class="card-content has-text-centered">
                        <h1 class="title is-size-1-desktop is-size-3-touch my-0">Beauté du diable</h1>
                        <h2 class="subtitle is-size-3-desktop is-size-5-touch">is offline</h2>
                        <a href="https://beautedudiable.fr/home" class="button is-link is-light">Reload</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
