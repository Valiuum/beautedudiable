module.exports = {
    plugins: [
        ...(process.env.NODE_ENV !== 'development' ? [require('@fullhuman/postcss-purgecss')({
            content: [
                './resources/**/*.blade.php',
                './resources/**/*.js',
            ],
            defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
        })] : [])
    ]
}
