<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schedule;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

Schedule::command('telescope:prune --hours=48')->daily();

// Web Notifications
Schedule::command('app:send-web-push-notification')
    ->everyMinute()
    ->emailOutputOnFailure('valentindufour@live.fr');
