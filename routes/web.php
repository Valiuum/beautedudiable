<?php

use App\Http\Controllers\ActivitieController;
use App\Http\Controllers\CycleController;
use App\Http\Controllers\DishController;
use App\Http\Controllers\MealController;
use App\Http\Controllers\NotificationManagerController;
use App\Http\Controllers\WeightController;
use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome');

Route::view('home', 'home')
    ->middleware(['auth', 'verified'])
    ->name('home');

Route::view('calendar', 'calendar.show')
    ->middleware(['auth', 'verified', 'power_user'])
    ->name('calendar.show');

Route::view('bilan', 'followUp.bilan.show')
    ->middleware(['auth', 'verified', 'power_user'])
    ->name('bilan.show');

Route::view('profile', 'profile')
    ->middleware(['auth', 'verified'])
    ->name('profile');

Route::view('ingredients', 'ingredients.ingredients')
    ->middleware(['auth', 'verified', 'power_user'])
    ->name('ingredients');

Route::prefix('activities')->name('activity.')->middleware(['auth', 'verified', 'power_user'])->group(function () {

    Route::get('show', [ActivitieController::class, 'show'])
        ->name('show');

    Route::get('edit/{activity}', [ActivitieController::class, 'edit'])
        ->name('edit');

});

Route::prefix('weights')->name('weight.')->middleware(['auth', 'verified', 'power_user'])->group(function () {

    Route::get('show', [WeightController::class, 'show'])
        ->name('show');

    Route::get('edit/{weight}', [WeightController::class, 'edit'])
        ->name('edit');

});

Route::prefix('cycles')->name('cycle.')->middleware(['auth', 'verified', 'power_user'])->group(function () {

    Route::get('show', [CycleController::class, 'show'])
        ->name('show');

    Route::get('edit/{cycle}', [CycleController::class, 'edit'])
        ->name('edit');

});

Route::prefix('dishes')->name('dish.')->middleware(['auth', 'verified', 'power_user'])->group(function () {

    Route::get('show', [DishController::class, 'show'])
        ->name('show');

    Route::get('create/{type}', [DishController::class, 'create'])
        ->name('create');

    Route::get('edit/{dish}', [DishController::class, 'edit'])
        ->name('edit');

    Route::get('duplicate/{dish}', [DishController::class, 'duplicate'])
        ->name('duplicate');

    Route::delete('delete/{dish}', [DishController::class, 'destroy'])
        ->name('destroy');

});

Route::prefix('meal')->name('meal.')->middleware(['auth', 'verified', 'power_user'])->group(function () {

    Route::get('create/{date}/{type}', [MealController::class, 'create'])
        ->name('create')
        ->middleware(['meal_parameters']);

    Route::get('edit/{meal}', [MealController::class, 'edit'])
        ->name('edit');

    Route::get('share/{meal}/{user}/{shared_by}', [MealController::class, 'share'])
        ->name('share');

    Route::get('duplicate/{meal}', [MealController::class, 'duplicate'])
        ->name('duplicate');

    Route::delete('delete/{meal}', [MealController::class, 'destroy'])
        ->name('destroy');

});

// Web Push Notifications
Route::prefix('notifications')->name('notification.')->middleware('auth')->group(function () {

    Route::post('subscribe', [NotificationManagerController::class, 'subscribe'])
        ->name('subscribe');

    Route::post('unsubscribe', [NotificationManagerController::class, 'unsubscribe'])
        ->name('unsubscribe');

    Route::get('send/{action}/{user?}', [NotificationManagerController::class, 'send'])
        ->name('send');

    Route::get('key', [NotificationManagerController::class, 'key'])
        ->name('key');

    Route::get('check', [NotificationManagerController::class, 'UserWebNotificationsPreferences'])
        ->name('check');

});

// Offline Page
Route::get('/offline', function () {
    setcookie('app_url', env('APP_URL'), time() + (86400 * 30), '/');

    return file_get_contents(public_path('offline/index.php'));
});

require __DIR__.'/auth.php';

// Redirect /register to /login
Route::redirect('/register', '/login');
