<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="Beauté du diable - un site qui vous veut du bien">

    <!-- Favicons -->
    <link rel="shortcut apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">

    <link rel="mask-icon" href="{{asset('safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600|abel:400&display=swap" rel="stylesheet"/>

    <!-- Scripts -->
    @vite(['resources/css/app.scss','resources/js/app.js', 'resources/js/navbar.js', 'resources/js/datepicker.js'])

    <!-- Load the Vite assets by pages -->
    @if(Str::startsWith(Route::currentRouteName(), 'meal'))
        @vite('resources/js/meals.js')
    @endif
    @if(Str::startsWith(Route::currentRouteName(), 'dish'))
        @vite('resources/js/dishes.js')
    @endif
    @if(Str::startsWith(Route::currentRouteName(), 'home'))
        @vite(['resources/js/dailyMeals.js', 'resources/js/rings.js'])
    @endif
    @if(Str::startsWith(Route::currentRouteName(), 'bilan'))
        @vite('resources/js/bilan.js')
    @endif
    @if(Str::startsWith(Route::currentRouteName(), 'calendar'))
        @vite('resources/js/calendar.js')
    @endif
    @if(Str::startsWith(Route::currentRouteName(), 'profile'))
        @vite('resources/js/webPush.js')
    @endif
    @mobile
    @vite('resources/js/mobile.js')
    @endmobile
    <style>
        [x-cloak] {
            display: none !important;
        }
    </style>
</head>
<body>
<livewire:layout.navigation/>
<main id="app" class="mx-0 is-fullheight">
    <x-notifications/>
    <livewire:modals.open-food-facts/>
    <livewire:modals.share-meal-with/>
    <livewire:modals.delete-meal/>
    <section class="columns mx-0">
        <livewire:layout.sidenav/>
        <!-- Page Content -->

        <div id="content"
             class="container-fluid column is-10-desktop is-12-touch mb-5 @desktop is-desktop px-5 @elsedesktop is-mobile @enddesktop">

            <!-- Mobile Menu Page trigger -->
            @mobile
            <a class="navbar-burger has-background-white-ter is-position-absolute" role="button" aria-label="menu"
               aria-expanded="false">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
            {{-- id="notifications-mobile" class="has-text-link is-position-absolute" --}}
            <!-- Mobile Notifications -->
            @livewire('profile.notification-profile')
            @endmobile
            <div class="page-content">
                <!-- Heading Page -->
                @if (isset($header))
                    <div class="box p-0 is-hidden-touch">
                        <figure class="image header-figure is-16by9">
                            <img class="header-image" src="{{ $header->attributes->get('imgPath') }}" alt="Header">
                        </figure>
                    </div>
                @endif
                {{ $slot }}
            </div>
        </div>
    </section>
    <script>
        window.current_route = '{{ Route::currentRouteName() }}';
        window.addEventListener('load', () => {
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/sw.js')
                    .then((registration) => {
                        //console.log('Service Worker registered with scope:', registration.scope);
                    })
                    .catch((error) => {
                        //console.error('Service Worker registration failed:', error);
                    });
            }
        })
    </script>
</main>
</body>
</html>
