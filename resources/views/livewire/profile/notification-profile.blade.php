<?php

use Illuminate\Support\Facades\Auth;
use function Livewire\Volt\state;

state([
    'notifications' => fn() => auth()->user()->unreadNotifications,
]);

$updateNotification = function (string $notificationID) {

    if (!auth()->check()) {
        return;
    }

    switch ($notificationID) {
        case 'all':
            auth()->user()->unreadNotifications->markAsRead();
            auth()->user()->load('unreadNotifications');
            $this->dispatch('notification-updated', notifications: auth()->user()->unreadNotifications);
            break;
        case 'refresh':
            auth()->user()->load('unreadNotifications');
            $this->dispatch('notification-updated', notifications: auth()->user()->unreadNotifications);
            break;
        default:
            if (!auth()->user()->unreadNotifications->where('id', $notificationID)->count()) {
                return;
            } else {
                auth()->user()->unreadNotifications->where('id', $notificationID)->markAsRead();
                auth()->user()->load('unreadNotifications');
                $this->dispatch('notification-updated', notifications: auth()->user()->unreadNotifications);
            }
            break;
    }

};

?>

<div>
    @desktop
    <div id="user-notifications-desktop" class="navbar-item has-dropdown mr-4 is-hoverable"
         x-data="{{ json_encode(['notifications' => auth()->user()->unreadNotifications]) }}"
         x-on:notification-updated.window="notifications = $event.detail.notifications"
         x-on:refresh-notifications.window="$wire.call('updateNotification','refresh')"
    >
        <a class="navbar-link">
        <span id="notification-bell-active"
              class="material-symbols-outlined {{Auth::user()->unreadNotifications->count() === 0 ? 'is-hidden' : ''}}">notifications_active</span>
            <span id="notification-bell"
                  class="material-symbols-outlined {{Auth::user()->unreadNotifications->count() > 0 ? 'is-hidden' : ''}}">notifications</span>
            <span id="notification-count"
                  class="ml-2 is-size-6 tag is-primary is-rounded has-text-white {{Auth::user()->unreadNotifications->count() === 0 ? 'is-hidden' : ''}}">
            {{Auth::user()->unreadNotifications->count()}}
        </span>
        </a>
        <div id="user-notifications-dropdown" class="navbar-dropdown is-right">
            @if(Auth::user()->unreadNotifications->count() > 0)
                <a class="navbar-item" wire:click="updateNotification('all')">
                    <span class="material-symbols-outlined icon has-text-danger-60 mr-2">delete_sweep</span>
                    {{ __('messages.notificationsClearAll') }}
                </a>
            @endif
            @forelse(Auth::user()->unreadNotifications as $notification)
                <a class="navbar-item" wire:key="{{$notification->id}}"
                   wire:click="updateNotification({{json_encode($notification->id)}})">
                    <div class="is-flex is-align-items-center">
                        @if($notification->type === 'App\Notifications\MealSharedNotification')
                            @switch($notification->data['name'])
                                @case ('breakfast')
                                    <span
                                        class="material-symbols-outlined icon has-text-grey-darker mr-2">bakery_dining</span>
                                    @php $name = __('messages.breakfast'); @endphp
                                    @break
                                @case ('lunch')
                                    <span
                                        class="material-symbols-outlined icon has-text-grey-darker mr-2">local_pizza</span>
                                    @php $name = __('messages.lunch'); @endphp
                                    @break
                                @case ('dinner')
                                    <span
                                        class="material-symbols-outlined icon has-text-grey-darker mr-2">lunch_dining</span>
                                    @php $name = __('messages.dinner'); @endphp
                                    @break
                                @case ('snack')
                                    <span
                                        class="material-symbols-outlined icon has-text-grey-darker mr-2">nutrition</span>
                                    @php $name = __('messages.snack'); @endphp
                                    @break
                                @case ('bar')
                                    <span
                                        class="material-symbols-outlined icon has-text-grey-darker mr-2">liquor</span>
                                    @php $name = __('messages.bar'); @endphp
                                    @break
                            @endswitch
                            <span>{{$notification->data['shared_by']}} {{ trans_choice('messages.notificationsSharedMeal', 1, ['name' => $name, 'date' => $notification->data['date']]) }}</span>
                        @elseif($notification->type === 'App\Notifications\BadgeUnlockNotification')
                            <span class="material-symbols-outlined has-text-warning mr-2">award_star</span>
                            <span>{{trans_choice('messages.notification_badge_unlock', 1, ['value' => $notification->data['name']])}}</span>
                        @endif
                    </div>
                </a>
            @empty
                <a class="navbar-item">
                    <div class="is-flex is-align-items-center">
                        <span class="material-symbols-outlined has-text-danger-60 mr-2">dangerous</span>
                        <span>{{ __('messages.notificationsEmpty') }}</span>
                    </div>
                </a>
            @endforelse
        </div>
    </div>
    @elsedesktop
    <div class="is-relative">
        <div
            x-data="{
        open: false,
        toggle() {
            if (this.open) {
                return this.close()
            }
            this.$refs.button.focus()
            this.open = true
            this.updateContentClass()
        },
        close(focusAfter) {
            if (! this.open) return
            this.open = false
            focusAfter && focusAfter.focus()
            this.updateContentClass()
        },
        updateContentClass() {
            const content = document.getElementsByClassName('page-content')[0];
            if (this.open) {
                content.classList.add('mobile-notifications');
            } else {
                content.classList.remove('mobile-notifications');
            }
        },
        ...{{ json_encode(['notifications' => auth()->user()->unreadNotifications]) }}
    }"
            x-on:keydown.escape.prevent.stop="close($refs.button)"
            x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
            x-on:notification-updated.window="notifications = $event.detail.notifications"
            x-on:refresh-notifications.window="$wire.call('updateNotification','refresh')"
            x-id="['dropdown-button']"
            class="is-relative"
        >
            <!-- Button -->
            <a id="user-notifications-dropdown-mobile"
               class="navbar-link navbar-dropdown is-position-absolute"
               x-ref="button"
               x-on:click="toggle()"
               :aria-expanded="open"
               :class="open && 'open'"
            >
        <span
            class="material-symbols-outlined {{Auth::user()->unreadNotifications->count() === 0 ? 'is-hidden' : ''}}">notifications_active</span>
                <span id="notification-bell"
                      class="material-symbols-outlined {{Auth::user()->unreadNotifications->count() > 0 ? 'is-hidden' : ''}}">notifications</span>
                <span id="notification-count"
                      class="ml-2 is-size-6 tag is-primary is-rounded has-text-white {{Auth::user()->unreadNotifications->count() === 0 ? 'is-hidden' : ''}}">
        {{Auth::user()->unreadNotifications->count()}}
        </span>
            </a>

            <!-- Panel -->
            <div
                id="dropdown-button-mobile"
                class="is-position-absolute has-background-white box"
                x-ref="panel"
                x-show="open"
                x-transition.origin.top.right
                x-on:click.outside="close($refs.button)"
                style="display: none;"
            >
                @if(Auth::user()->unreadNotifications->count() > 0)
                    <a class="navbar-item" wire:click="updateNotification('all')" x-on:click="toggle()">
                        <span class="material-symbols-outlined icon has-text-danger mr-2">delete_sweep</span>
                        {{ __('messages.notificationsClearAll') }}
                    </a>
                @endif
                @forelse(Auth::user()->unreadNotifications as $notification)
                    <a class="navbar-item" wire:key="{{$notification->id}}"
                       wire:click="updateNotification({{json_encode($notification->id)}})">
                        <div class="is-flex is-align-items-center">
                            @if($notification->type === 'App\Notifications\MealSharedNotification')
                                @switch($notification->data['name'])
                                    @case ('breakfast')
                                        <span
                                            class="material-symbols-outlined icon has-text-grey-darker mr-2">bakery_dining</span>
                                        @php $name = __('messages.breakfast'); @endphp
                                        @break
                                    @case ('lunch')
                                        <span
                                            class="material-symbols-outlined icon has-text-grey-darker mr-2">local_pizza</span>
                                        @php $name = __('messages.lunch'); @endphp
                                        @break
                                    @case ('dinner')
                                        <span
                                            class="material-symbols-outlined icon has-text-grey-darker mr-2">lunch_dining</span>
                                        @php $name = __('messages.dinner'); @endphp
                                        @break
                                    @case ('snack')
                                        <span
                                            class="material-symbols-outlined icon has-text-grey-darker mr-2">nutrition</span>
                                        @php $name = __('messages.snack'); @endphp
                                        @break
                                    @case ('bar')
                                        <span
                                            class="material-symbols-outlined icon has-text-grey-darker mr-2">liquor</span>
                                        @php $name = __('messages.bar'); @endphp
                                        @break
                                @endswitch
                                <span>{{$notification->data['shared_by']}} {{ trans_choice('messages.notificationsSharedMeal', 1, ['name' => $name, 'date' => $notification->data['date']]) }}</span>
                            @elseif($notification->type === 'App\Notifications\BadgeUnlockNotification')
                                <span class="material-symbols-outlined has-text-warning mr-2">award_star</span>
                                <span>{{trans_choice('messages.notification_badge_unlock', 1, ['value' => $notification->data['name']])}}</span>
                            @endif
                        </div>
                    </a>
                @empty
                    <a class="navbar-item is-flex">
                        <div class="is-flex is-align-items-center">
                            <span class="material-symbols-outlined has-text-danger mr-2">dangerous</span>
                            <span>{{ __('messages.notificationsEmpty') }}</span>
                        </div>
                    </a>
                @endforelse
            </div>
        </div>
    </div>
    @enddesktop
    @script
    <script>
        Echo.private('App.Models.User.' + {{ Auth::id() }})
            .notification((notification) => {
                document.dispatchEvent(new CustomEvent('refresh-notifications', {
                    detail: notification,
                    bubbles: true
                }));
            });
    </script>
    @endscript
</div>

