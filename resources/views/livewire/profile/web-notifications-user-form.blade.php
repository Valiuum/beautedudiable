<?php

use Illuminate\Validation\Rule;
use function Livewire\Volt\state;
use Illuminate\Support\Facades\DB;

$notifications = DB::table('push_subscriptions')
    ->where('subscribable_id', auth()->user()->id)
    ->get('endpoint');

$preferences = auth()->user()->preferences ? collect(json_decode(auth()->user()->preferences, true)) : collect();

state([
    'preferences' => $preferences,
    'notifications' => $notifications,
    'notificationActivate' => $preferences->get('notificationActivate') ?? false,
    'notificationTime' => $preferences->get('notificationTime') ?? null,
    'notificationPeriod' => $preferences->get('notificationPeriod') ?? '0',
    'notificationSharedMeals' => $preferences->get('notificationSharedMeals') ?? null,
    'notificationBadges' => $preferences->get('notificationBadges') ?? null,
]);


$updateProfileNotifications = function () {

    $user = auth()->user();

    $validated = $this->validate([
        'notificationActivate' => ['required', 'boolean'],
        'notificationTime' => ['nullable', 'date_format:H:i'],
        'notificationPeriod' => ['nullable', Rule::in(['0', '1', '2'])],
        'notificationSharedMeals' => ['nullable', 'boolean'],
        'notificationBadges' => ['nullable', 'boolean'],
    ]);

    $preferences = [
        'notificationActivate' => $validated['notificationActivate'],
        'notificationTime' => $validated['notificationTime'],
        'notificationPeriod' => $validated['notificationPeriod'],
        'notificationSharedMeals' => $validated['notificationSharedMeals'],
        'notificationBadges' => $validated['notificationBadges'],
    ];

    $user->preferences = json_encode($preferences);
    $user->save();

    $this->dispatch('notify', ['message' => trans('messages.profilUpdated'), 'type' => 'success']);
}
?>

<section class="box py-5" x-data="{ isNotificationOn: @json($preferences->get('notificationActivate') ?? false) }">
    <header>
        <h2 class="is-hidden-desktop">
            {{ trans('messages.webNotificationsStatus') }}
        </h2>

        @php
            $categories = collect();
            $category_icons = [
                'apple' => 'images/common/safari.svg',
                'mozilla' => 'images/common/firefox.svg',
                'google' => 'images/common/chrome.svg',
            ];
            $browsers = collect();
        @endphp

        @foreach($notifications as $notification)
            @foreach($category_icons as $keyword => $icon_path)
                @if (str_contains($notification->endpoint, $keyword) && !$categories->contains($keyword))
                    @php
                        $categories->push($keyword);
                        $icon = asset($icon_path);
                        $browsers->push("
                            <button class='button is-light' data-tooltip='$keyword'>
                                <figure class='image is-32x32'>
                                    <img src='{$icon}' alt='$keyword'>
                                </figure>
                            </button>
                        ");
                    @endphp
                    @break
                @endif
            @endforeach
        @endforeach

        @if($browsers->count() > 0)
            <p class="mt-1 mb-3">
                {{ trans_choice('messages.webNotificationsList', $browsers->count()) }}
            </p>
            <p class="mt-1 mb-3 mx-2">
                @foreach($browsers as $browser)
                    {!! $browser !!}
                @endforeach
            </p>
        @endif
    </header>

    <p id="buttons-notifications" class="mt-1 mb-3"
       x-data="{ isHidden: false, isSafari: /^((?!chrome|android).)*safari/i.test(navigator.userAgent) }">
        <x-link-button
                class="mr-2"
                x-init="isHidden = (Notification.permission === 'granted')"
                x-bind:class="{ 'is-hidden': isHidden }"
                x-on:click.prevent="$dispatch('ask-for-notifications-permissions')"
                x-show="!isSafari">
            {{ trans('messages.activateWebNotifications') }}
        </x-link-button>

        <x-link-button
                id="askForWebNotifications"
                class="mr-2 mx-2"
                x-init="isHidden = (Notification.permission === 'granted')"
                x-bind:class="{ 'is-hidden': isHidden }"
                x-show="isSafari">
            {{ trans('messages.activateWebNotifications') }}
        </x-link-button>

        @if($browsers->count() > 0)
            <button id="sendWebNotifications"
                    class="button is-link is-light mx-2" x-show="!isSafari">
                <span class="material-symbols-outlined ml-2">mark_chat_unread</span>
                {{ trans('messages.sendNotification') }}
            </button>
            <button id="sendWebNotificationsForSafari"
                    class="button is-link is-light mx-2" x-show="isSafari">
                <span class="material-symbols-outlined ml-2">mark_chat_unread</span>
                {{ trans('messages.sendNotification') }}
            </button>
        @endif
    </p>

    <form id="form-web-notifications"
          wire:submit="updateProfileNotifications" @class(['mt-6', 'is-hidden' => $browsers->count() === 0])>

        <div class="field">
            <input class="is-checkradio" id="notificationActivate" type="checkbox"
                   name="notificationActivate"
                   x-model="isNotificationOn" wire:model="notificationActivate" value="true">
            <label
                    for="notificationActivate">{{ trans('messages.notifications_preferences_reminder_on') }}</label>
            <x-input-error class="mt-2" :messages="$errors->get('notificationActivate')"/>
        </div>

        <div class="field">
            <x-input-label for="notificationTime" :value="trans('messages.notifications_preferences_reminder_time')"/>
            <div class="control has-icons-left">
                <x-input-text id="notificationTime" name="notificationTime" type="time" class="input"
                              x-bind:disabled="!isNotificationOn" wire:model="notificationTime"/>
                <x-input-icon class="icon is-small is-left" icon="schedule"/>
                <x-input-error class="mt-2" :messages="$errors->get('notificationTime')"/>
            </div>
        </div>

        <div class="field">
            <x-input-label :value="trans('messages.notifications_preferences_reminder_period')"/>
            <input class="is-checkradio" id="notificationDisabled" type="radio" name="notificationPeriod"
                   x-bind:disabled="!isNotificationOn" wire:model="notificationPeriod" value="0">
            <label
                    for="notificationDisabled">{{trans_choice('messages.notifications_preferences_period', 0 , ['value' => 0])}}</label>
            <input class="is-checkradio" id="notificationSunday" type="radio" name="notificationPeriod"
                   x-bind:disabled="!isNotificationOn" wire:model="notificationPeriod" value="1">
            <label
                    for="notificationSunday">{{trans_choice('messages.notifications_preferences_period', 1 , ['value' => 1])}}</label>
            <input class="is-checkradio" id="notificationAllDays" type="radio" name="notificationPeriod"
                   x-bind:disabled="!isNotificationOn" wire:model="notificationPeriod" value="2">
            <label
                    for="notificationAllDays">{{trans_choice('messages.notifications_preferences_period', 2 , ['value' => 2])}}</label>
            <x-input-error class="mt-2" :messages="$errors->get('notificationPeriod')"/>
        </div>

        <div class="field">
            <input class="is-checkradio" id="notificationSharedMeals" type="checkbox"
                   name="notificationSharedMeals"
                   wire:model="notificationSharedMeals" x-bind:disabled="!isNotificationOn">
            <label
                    for="notificationSharedMeals">{{ trans('messages.notifications_preferences_shared_meals') }}</label>
            <x-input-error class="mt-2" :messages="$errors->get('notificationsSharedMeals')"/>
        </div>

        <div class="field">
            <input class="is-checkradio" id="notificationBadges" type="checkbox"
                   name="notificationBadges"
                   wire:model="notificationBadges" x-bind:disabled="!isNotificationOn">
            <label
                    for="notificationBadges">{{ trans('messages.notifications_preferences_badges') }}</label>
            <x-input-error class="mt-2" :messages="$errors->get('notificationBadges    ')"/>
        </div>

        <div class="is-flex is-justify-content-flex-end">
            <x-primary-button>{{ trans('messages.save') }}</x-primary-button>
        </div>

    </form>

</section>
