<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

use function Livewire\Volt\state;

state([
    'health_target' => fn(
    ) => auth()->user()->objectives()->latest('created_at')->first()?->health_target ?? 'stabilize',
    'daily_points' => fn() => auth()->user()->objectives()->latest('created_at')->first()?->daily_points ?? 20,
    'select' => fn() => [
        'stabilize' => __('messages.stabilize'),
        'lose' => __('messages.lose'),
        'sport' => __('messages.sport'),
    ],
]);

$updateProfileBeauty = function () {
    $user = Auth::user();

    $validated = $this->validate([
        'health_target' => ['required', Rule::in(['stabilize', 'lose', 'sport'])],
        'daily_points' => ['required', 'int', 'min:10', 'max:40'],
    ]);

    $currentObjective = $user->objectives()->latest('created_at')->first();

    if (!$currentObjective) {
        $user->objectives()->create($validated);
    } else {
        $existingObjective = $user->objectives()
            ->where('health_target', $validated['health_target'])
            ->where('daily_points', $validated['daily_points'])
            ->first();

        if ($existingObjective) {
            $existingObjective->created_at = Carbon\Carbon::now();
            $existingObjective->save();
        } else {
            $newObjective = $currentObjective->replicate();
            $newObjective->fill($validated);
            $newObjective->save();
        }
    }

    $this->dispatch('notify', ['message' => trans('messages.profilUpdated'), 'type' => 'success']);
    $this->dispatch('profile-updated', name: $user->name);
};

$sendVerification = function () {
    $user = Auth::user();

    if ($user->hasVerifiedEmail()) {
        $this->redirectIntended(default: route('dashboard', absolute: false));

        return;
    }

    $user->sendEmailVerificationNotification();

    Session::flash('status', 'verification-link-sent');
};

?>

<section class="box py-5">

    <header>
        <h2 class="is-hidden-desktop">
            {{ __('messages.informationProfil') }}
        </h2>

        <p class="mt-1">
            {{ __("messages.beautyProfilLegend") }}
        </p>
    </header>

    <form wire:submit="updateProfileBeauty" class="mt-6">

        <div id="select-health-target" class="field">
            <x-input-label for="email" :value="__('messages.healthTarget')"/>
            <x-input-select id="health_target"
                            name="health_target"
                            :options="$select"
                            :selected="$health_target"
                            :livewireModel="'health_target'"
                            :icon="'spa'"/>
            @error('health_target')
            <x-input-error :messages="$message" class="mt-2"/>
            @enderror
        </div>

        <div class="field">
            <div x-data="{open: false}" class="column is-full px-0">
                <span x-on:click="open = ! open" class=" button is-info is-light">
                        <span class="material-symbols-outlined mr-2">info</span>
                {{__('messages.dailyPointsButton')}}
                </span>
                <div class="box my-3 mx-0 columns is-vcentered" x-show="open" x-transition:enter.scale.0
                     x-transition:leave.scale.80
                     x-transition:leave.duration.300ms x-transition:enter.duration.100ms>
                    <div
                        class="column is-flex is-justify-content-center is-one-third-touch is-one-fifth-desktop mx-auto">
                        <figure class="image is-96x96 is-flex is-align-items-center">
                            <img alt="icon"
                                 src=" {{asset('images/common/mathematics.svg') }}"/>
                        </figure>
                    </div>
                    <div class="column is-flex-direction-column is-justify-content-center">
                        <h2 class="is-size-5 has-text-centered my-2">Additionne les valeurs qui te correspondent</h2>
                        <h4 class="is-size-5 my-2">Genre</h4>
                        <p>Tu es un Homme : <span class="has-text-weight-bold">15 points</span></p>
                        <p>Tu es une Femme : <span class="has-text-weight-bold">7 points</span></p>
                        <h4 class="is-size-5 my-2">Age</h4>
                        <p>Tu as entre 18 et 20 ans : <span class="has-text-weight-bold">5 points</span></p>
                        <p>Tu as entre 21 et 35 ans : <span class="has-text-weight-bold">4 points</span></p>
                        <p>Tu as entre 36 et 50 ans : <span class="has-text-weight-bold">3 points</span></p>
                        <p>Tu as entre 51 et 65 ans : <span class="has-text-weight-bold">2 point</span></p>
                        <p>Tu as plus de 65 ans : <span class="has-text-weight-bold">1 points</span></p>
                        <h4 class="is-size-5 my-2">Poids</h4>
                        <p>La dizaine de ton poids : <span class="has-text-weight-bold">6 points</span></p>
                        <h4 class="is-size-5 my-2">Taille</h4>
                        <p>Tu mesures < 1m60 : <span class="has-text-weight-bold">1 point</span></p>
                        <p>Tu mesures > 1m60 : <span class="has-text-weight-bold">2 points</span></p>
                        <h4 class="is-size-5 my-2">Activités</h4>
                        <p>Assis mais parfois debout avec quelques déplacements <span class="has-text-weight-bold">2 points</span>
                        </p>
                        <p>Essentiellement debout <span class="has-text-weight-bold">4 points</span></p>
                        <p>Très physique <span class="has-text-weight-bold">6 points</span></p>
                    </div>
                </div>
            </div>
            <x-input-label for="weighing" :value="__('messages.dailyPoints')"/>
            <div class="control has-icons-left">
                <x-input-text
                    wire:model="daily_points"
                    name="daily_points"
                    class="{{ $errors->has('daily_points') ? 'input is-danger' : 'input' }}"
                    type="number"
                    step="1"
                    min="10"
                    max="40"
                />
                <x-input-icon class="icon is-small is-left" icon="pin"/>
                @error('daily_points')
                <x-input-error :messages="$message" class="mt-2"/>
                @enderror
            </div>

            <div class="is-flex is-justify-content-flex-end mt-3">
                <x-primary-button>{{ trans('messages.save') }}</x-primary-button>
            </div>
        </div>
    </form>

</section>
