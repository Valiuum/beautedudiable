<section class="{{!$displayTable ? 'column is-12' : ''}}">
    @if(!$displayTable && !$edit)
        <section class="section">
            <label for="searchActivity"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.activityNewForm')}}</label>
        </section>
    @elseif(!$displayTable && $edit)
        <section class="section">
            <label for="searchActivity"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.activityFormEdit')}}</label>
        </section>
    @endif
    @desktop
    <form wire:submit="save"
            @class(['box' => !$displayTable, 'followUp', 'columns', 'is-multiline', 'is-vcentered', 'is-horizontal', 'mb-5' => !$displayTable, 'm-3' => $displayTable])>
        @elsedesktop
        <form wire:submit="save" class="followUp is-horizontal box mb-5">
            @enddesktop
            <!-- Activity -->
            @if($form->activity)
                @switch($form->activity)
                    @case('hiking') @php $activity_icon = 'hiking'; @endphp @break
                    @case('yoga') @php $activity_icon = 'self_improvement'; @endphp @break
                    @case('running') @php $activity_icon = 'directions_run'; @endphp @break
                    @case('workout') @php $activity_icon = 'fitness_center'; @endphp @break
                    @default @php $activity_icon = 'exercise'; @endphp
                @endswitch
            @else
                @php $activity_icon = 'exercise'; @endphp
            @endif
            <div id="select-activity"
                 class="field column is-full-touch {{$displayTable ? 'is-half-desktop' : 'is-three-fifths-desktop' }}">
                <x-input-label for="activity" :value="__('messages.activity')"/>
                <x-input-select id="activity"
                                name="activity"
                                :options="$select_activities"
                                :selected="$activity"
                                :livewireModel="'activity'"
                                :icon="$activity_icon"/>
                @error('form.activity')
                <x-input-error :messages="$message" class="mt-2"/>
                @enderror
            </div>
            <!-- Date -->
            <div class="field column is-full-touch  {{$displayTable ? 'is-half-desktop' : 'is-two-fifths-desktop' }}">
                <x-input-label for="date" :value="__('messages.datepicker')"/>
                <div class="control">
                    @if(!$displayTable)
                        <livewire:component.datepicker :clearDate="true"/>
                    @else
                        <input for="date" wire:model="form.date" class="input" type="date">
                    @endif
                    @error('form.date')
                    <x-input-error :messages="$message" class="mt-2"/>
                    @enderror
                </div>
            </div>
            <!-- Comment -->
            <div class="field column is-full">
                <x-input-label for="comment" :value="__('messages.comment')"/>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <x-input-textarea wire:model="form.comment"
                                              :placeholder="__('messages.commentPlaceholder')"/>
                        </div>
                    </div>
                </div>
            </div>
            <div
                    class="is-flex field column is-full is-horizontal is-justify-content-end">
                <x-primary-button
                        @class(['is-dark' => $darkMode, 'mr-2 mb-2']) wire:loading.class="is-loading">{{ __('messages.save') }}
                </x-primary-button>
            </div>
        </form>
</section>
