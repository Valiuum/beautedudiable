<div x-data="{selection: $wire.entangle('selection')}">
    <div class="column is-full px-0 mb-3">
        <div class="is-flex is-justify-content-center my-5">
            <label for="search_ingredient"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.searchCycles')}}</label>
        </div>
        <div id="tableSearchCycles"
             class="tableSearch control field is-grouped is-flex is-justify-content-center my-5 mx-auto has-icons-left">
            <x-input-text
                wire:model.live.debounce.250ms="search"
                id="search_cycle"
                class="input"
                name="search_cycle"
                type="text"
            />
            <x-input-icon class="icon is-small is-left" icon="female"/>
        </div>
    </div>
    <x-danger-button
        x-cloak
        x-show="selection.length > 0"
        x-on:click="$wire.deleteSelected">
        <span class="material-symbols-outlined mr-2">delete</span>
        <span>{{ __('messages.delete') }}</span>
    </x-danger-button>
    <div class="table-container">
        <table class="table is-fullwidth is-hoverable">
            <thead>
            <tr>
                <th></th>
                <x-table-header name="duration" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.duration')}}
                </x-table-header>
                <x-table-header name="comment" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.comment')}}
                </x-table-header>
                <x-table-header name="date" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.date')}}
                </x-table-header>
                <th>{{__('messages.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cycles as $cycle)
                <tr>
                    <td>
                        <div class="field">
                            <input id="checkbox-{{$cycle->id}}"
                                   class="is-checkradio is-circle"
                                   x-model="selection"
                                   value="{{$cycle->id}}"
                                   type="checkbox"
                                   name="checkbox-{{$cycle->id}}">
                            <label for="checkbox-{{$cycle->id}}"></label>
                        </div>
                    </td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $cycle->duration) : $cycle->duration !!}</td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $cycle->comment) : $cycle->comment !!}</td>
                    <td>{{\Carbon\Carbon::parse($cycle->date)->translatedFormat('l d M Y')}}</td>
                    <td>
                        <button
                            @class(['is-dark' => $darkMode, 'button', 'is-info is-light']) wire:click="startEdit({{$cycle->id}})">
                            <span class="material-symbols-outlined">more_horiz</span>
                        </button>
                    </td>
                </tr>
                @if($editId === $cycle->id)
                    <tr>
                        <td colspan="5">
                            <livewire:tables.cycles-form
                                :displayTable="true"
                                :darkMode="$darkMode"
                                :edit="false"
                                :cycle="$cycle"
                                :key="$cycle->id"/>
                        </td>
                    </tr>
            @endif
            @endforeach
        </table>
    </div>
    {{$cycles->links()}}
</div>
