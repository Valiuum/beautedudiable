<section class="{{!$displayTable ? 'column is-12' : ''}}">
    @if(!$displayTable && !$edit)
        <section class="section">
            <label for="cycleNewForm"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.cycleNewForm')}}</label>
        </section>
    @elseif(!$displayTable && $edit)
        <section class="section">
            <label for="cycleNewForm"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.cycleFormEdit')}}</label>
        </section>
    @endif
    @desktop
    <form wire:submit="save"
            @class(['box' => !$displayTable, 'followUp', 'columns', 'is-multiline', 'is-vcentered', 'is-horizontal', 'mb-5' => !$displayTable, 'm-3' => $displayTable])>
        @elsedesktop
        <form wire:submit="save" class="followUp is-horizontal box mb-5">
            @enddesktop
            <!-- Duration -->
            <div class="field column is-full-touch is-half-desktop">
                <x-input-label for="duration" :value="__('messages.duration')"/>
                <div class="control has-icons-left">
                    <x-input-text
                            wire:model="form.duration"
                            name="weighing"
                            class="{{ $errors->has('form.duration') ? 'input is-danger' : 'input' }}"
                            type="number"
                            step="1"
                            min="1"
                    />
                    <x-input-icon class="icon is-small is-left" icon="pin"/>

                    @error('form.duration')
                    <x-input-error :messages="$message" class="mt-2"/>
                    @enderror
                </div>
            </div>
            <!-- Date -->
            <div class="field column is-full-touch is-half-desktop">
                <x-input-label for="date" :value="__('messages.datepicker')"/>
                <div class="control">
                    @if(!$displayTable && !$edit)
                        <livewire:component.datepicker :clearDate="true"/>
                    @elseif (!$displayTable && $edit)
                        <livewire:component.datepicker/>
                    @else
                        <input for="date" wire:model="form.date" class="input" type="date">
                    @endif
                    @error('form.date')
                    <x-input-error :messages="$message" class="mt-2"/>
                    @enderror
                </div>
            </div>
            <!-- Comment -->
            <div class="field column is-full">
                <x-input-label for="comment" :value="__('messages.comment')"/>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <x-input-textarea wire:model="form.comment"
                                              :placeholder="__('messages.commentPlaceholder')"/>
                        </div>
                    </div>
                </div>
            </div>
            <div
                    class="is-flex field column is-full is-horizontal is-justify-content-end">
                <x-primary-button
                        @class(['is-dark' => $darkMode, 'mr-2 mb-2']) wire:loading.class="is-loading">{{ __('messages.save') }}
                </x-primary-button>
            </div>
        </form>
</section>
