<div x-data="{selection: $wire.entangle('selection')}">
    <div class="column is-full px-0 mb-3">
        <div class="is-flex is-justify-content-center my-5">
            <label for="search_ingredient"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.searchWeight')}}</label>
        </div>
        <div id="tableSearchWeights"
             class="tableSearch control field is-grouped is-flex is-justify-content-center my-5 mx-auto has-icons-left">
            <x-input-text
                wire:model.live.debounce.250ms="search"
                id="search_weight"
                class="input"
                name="search_weight"
                type="text"
            />
            <x-input-icon class="icon is-small is-left" icon="weight"/>
        </div>
    </div>
    <x-danger-button
        x-cloak
        x-show="selection.length > 0"
        x-on:click="$wire.deleteSelected">
        <span class="material-symbols-outlined mr-2">delete</span>
        <span>{{ __('messages.delete') }}</span>
    </x-danger-button>
    <div class="table-container">
        <table class="table is-fullwidth is-hoverable">
            <thead>
            <tr>
                <th></th>
                <x-table-header name="weighing" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.weighing')}}
                </x-table-header>
                <x-table-header name="comment" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.comment')}}
                </x-table-header>
                <x-table-header name="date" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.date')}}
                </x-table-header>
                <th>{{__('messages.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($weights as $weight)
                <tr>
                    <td>
                        <div class="field">
                            <input id="checkbox-{{$weight->id}}"
                                   class="is-checkradio is-circle"
                                   x-model="selection"
                                   value="{{$weight->id}}"
                                   type="checkbox"
                                   name="checkbox-{{$weight->id}}">
                            <label for="checkbox-{{$weight->id}}"></label>
                        </div>
                    </td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $weight->weighing) : $weight->weighing !!}</td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $weight->comment) : $weight->comment !!}</td>
                    <td>{{\Carbon\Carbon::parse($weight->date)->translatedFormat('l d M Y')}}</td>
                    <td>
                        <button
                            @class(['is-dark' => $darkMode, 'button', 'is-info is-light']) wire:click="startEdit({{$weight->id}})">
                            <span class="material-symbols-outlined">more_horiz</span>
                        </button>
                    </td>
                </tr>
                @if($editId === $weight->id)
                    <tr>
                        <td colspan="5">
                            <livewire:tables.weights-form
                                :displayTable="true"
                                :darkMode="$darkMode"
                                :weight="$weight"
                                :edit="false"
                                :key="$weight->id"/>
                        </td>
                    </tr>
               @endif
            @endforeach
        </table>
    </div>
    {{$weights->links()}}
</div>
