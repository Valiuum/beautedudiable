<section class="{{!$displayTable ? 'column is-12' : ''}}">
    @if(!$displayTable && !$edit)
        <section class="section">
            <label for="weightNewForm"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.weightNewForm')}}</label>
        </section>
    @elseif(!$displayTable && $edit)
        <section class="section">
            <label for="weightEditForm"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.weightFormEdit')}}</label>
        </section>
    @endif
    @desktop
    <form wire:submit="save"
            @class(['box' => !$displayTable, 'followUp', 'columns', 'is-multiline', 'is-vcentered', 'is-horizontal', 'mb-5' => !$displayTable, 'm-3' => $displayTable])>
        @elsedesktop
        <form wire:submit="save" class="followUp is-horizontal box mb-5">
            @enddesktop
            <!-- Weighting -->
            <div class="field column is-full-touch {{$displayTable ? 'is-half-desktop' : 'is-three-fifths-desktop' }}">
                <x-input-label for="weighing" :value="__('messages.weighing')"/>
                <div class="control has-icons-left">
                    @if(!$displayTable)
                        <input id="sliderWithValue"
                               wire:model="form.weighing"
                               class="slider is-fullwidth has-output is-grey is-circle is-medium" step="0.1"
                               min="{{ $edit ? ceil($form->weighing - 5) : (auth()->user()->isWomen() ? '50' : '60') }}"
                               max="{{ $edit ? ceil($form->weighing + 5) : (auth()->user()->isWomen() ? '80' : '90') }}"
                               type=range style="writing-mode: horizontal-tb">
                        <output
                                for="sliderWithValue">{{$form->weighing ? $form->weighing : (auth()->user()->isWomen() ? '65' : '75')}}</output>
                    @else
                        <x-input-text
                                wire:model="form.weighing"
                                name="weighing"
                                class="{{ $errors->has('form.points') ? 'input is-danger' : 'input' }}"
                                type="number"
                                step="0.1"
                                min="{{auth()->user()->isWomen() ? '50' : '60'}}"
                                max="{{auth()->user()->isWomen() ? '80' : '90'}}"
                        />
                        <x-input-icon class="icon is-small is-left" icon="pin"/>
                    @endif
                    @error('form.weighing')
                    <x-input-error :messages="$message" class="mt-2"/>
                    @enderror
                </div>
            </div>
            <!-- Date -->
            <div class="field column is-full-touch  {{$displayTable ? 'is-half-desktop' : 'is-two-fifths-desktop' }}">
                <x-input-label for="date" :value="__('messages.datepicker')"/>
                <div class="control">
                    @if(!$displayTable)
                        <livewire:component.datepicker :clearDate="true"/>
                    @else
                        <input for="date" wire:model="form.date" class="input" type="date">
                    @endif
                    @error('form.date')
                    <x-input-error :messages="$message" class="mt-2"/>
                    @enderror
                </div>
            </div>
            <!-- Comment -->
            <div class="field column is-full">
                <x-input-label for="comment" :value="__('messages.comment')"/>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <x-input-textarea wire:model="form.comment"
                                              :placeholder="__('messages.commentPlaceholder')"/>
                        </div>
                    </div>
                </div>
            </div>

            <div
                    class="is-flex field column is-full is-horizontal is-justify-content-end">
                <x-primary-button
                        @class(['is-dark' => $darkMode, 'mr-2 mb-2']) wire:loading.class="is-loading">{{ __('messages.save') }}
                </x-primary-button>
            </div>
        </form>
</section>
