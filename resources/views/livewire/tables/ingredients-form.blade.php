<section class="{{!$displayTable ? 'column is-12' : ''}}">
    @if(!$displayTable)
        <div x-data="{ isOpen: false }" class="column is-full px-0 mb-3">
            <section class="section">
                <label for="searchOpenFoodFact"
                       class="is-size-4 has-text-weight-semibold">{{__('messages.ingredientNewForm')}}</label>
            </section>
            <div class="field is-grouped is-justify-content-center my-5">
                <div class="control">
                    <input id="searchOpenFoodFact" class="input"
                           wire:model="search"
                           wire:keydown.enter="openFoodFacts"
                           x-on:keydown.enter="isOpen = !isOpen"
                           type="text"
                           placeholder="{{__('messages.searchOpenFoodFacts')}}">
                </div>
                <div class="control">
                <span class="button is-link is-light exclude-closing" @click="isOpen = ! isOpen"
                      wire:click="openFoodFacts"
                      wire:loading.class="is-loading">
                    <span class="material-icons-outlined exclude-closing">{{__('messages.search')}}</span>
                </span>
                </div>
            </div>
            <div id="searchOpenFoodFacts"
                 @class(['box','is-single-result' => $singleResult])
                 x-cloak
                 x-show="isOpen"
                 @click.outside="!$event.target.classList.contains('exclude-closing') ? isOpen = false : isOpen = true"
                 x-transition
                 x-init="$el.scrollTop = 0">
                @if(collect($searchOpenFoodFacts)->get('count') > 0)
                    <x-openfoodfacts-results :data="$searchOpenFoodFacts" :singleResult="$singleResult"
                                             :ingredientPage="true"/>
                    <div class="column is-full is-flex is-justify-content-center" x-data="{ shown: false }"
                         x-intersect.full="$wire.openFoodFactsLoadMore()">
                        <div wire:loading.delay.longer wire:target="openFoodFactsLoadMore">
                            <span class="tag is-light">{{trans('messages.loading')}}</span>
                        </div>
                    </div>
                @elseif (collect($searchOpenFoodFacts)->get('count') === 0)
                    <div id="searchOpenFoodFacts-empty"
                         class="column is-full is-flex is-flex-direction-column is-justify-content-center is-align-content-center is-align-items-center">
                        <h5 class="is-size-5">{{trans('messages.noResults')}}</h5>
                        <figure class="image is-96x96 is-flex is-align-items-center">
                            <img alt="no_result" class="image is-96x96" src="{{asset('images/common/no_data.svg')}}"/>
                        </figure>
                    </div>
                @endif
            </div>
        </div>
    @endif
    @desktop
    <form wire:submit="save"
            @class(['box' => !$displayTable, 'ingredients', 'columns', 'is-multiline', 'is-flex-direction-row', 'is-horizontal', 'mb-5' => !$displayTable, 'm-3' => $displayTable])>
        @elsedesktop
        <form wire:submit="save" class="ingredients is-horizontal box mb-5">
            @enddesktop

            <!-- Infos Points WW -->
            @if(!$displayTable)
                @desktop
                <div x-data="{open: false}" class="column is-full">
                    @elsedesktop
                    <div x-data="{open: false}" class="column is-full px-0">
                        @enddesktop
                        <span x-on:click="open = ! open" class="button is-info is-light">
                        <span class="material-symbols-outlined mr-2">info</span>
                        {{__('messages.ingredientPointsInfo')}}
                    </span>
                        <div class="box my-3 columns is-vcentered" x-show="open" x-transition:enter.scale.0
                             x-transition:leave.scale.80
                             x-transition:leave.duration.300ms x-transition:enter.duration.100ms>
                            <div class="column is-flex is-justify-content-center is-one-third-touch is-one-fifth-desktop mx-auto">
                                <figure class="image is-96x96 is-flex is-align-items-center">
                                    <img alt="icon"
                                         src=" {{asset('images/common/mathematics.svg') }}"/>
                                </figure>
                            </div>
                            <div class="column is-flex-direction-column is-justify-content-center">
                                <p class="is-size-5 has-text-centered has-text-weight-bold mb-2">{{__('messages.ingredientPointsInfoTitle')}}</p>
                                <p>{{__('messages.ingredientPointsDescription')}}</p>
                            </div>
                        </div>
                    </div>
                    @endif

                    <!-- Ingredient name -->
                    @desktop
                    <div class="field is-horizontal column is-half is-align-items-center">
                        @elsedesktop
                        <div class="field is-horizontal">
                            @enddesktop
                            <div class="field-label is-normal pt-0">
                                <x-input-label for="ingredient_description" class="tag is-dark is-light is-flex"
                                               :value="__('messages.ingredientName')"/>
                            </div>
                            <div class="field-body">
                                <div class="control has-icons-left">
                                    <x-input-text
                                            wire:model="form.name"
                                            id="ingredient_name"
                                            name="name"
                                            class="{{ $errors->has('form.name') ? 'input is-danger' : 'input' }}"/>
                                    <x-input-icon class="icon is-small is-left" icon="nutrition"/>
                                    @error('form.name')
                                    <x-input-error :messages="$message" class="mt-2"/>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <!-- Ingredient description -->
                        @desktop
                        <div class="field is-horizontal column is-half is-align-items-center">
                            @elsedesktop
                            <div class="field is-horizontal">
                                @enddesktop
                                <div class="field-label is-normal pt-0">
                                    <x-input-label for="ingredient_description" class="tag is-dark is-light is-flex"
                                                   :value="__('messages.ingredientDescription')"/>
                                </div>
                                <div class="field-body">
                                    <div class="control has-icons-left">
                                        <x-input-text
                                                wire:model="form.description"
                                                id="ingredient_description"
                                                name="description"
                                                class="{{ $errors->has('form.description') ? 'input is-danger' : 'input' }}"/>
                                        <x-input-icon class="icon is-small is-left" icon="checkbook"/>
                                        @error('form.description')
                                        <x-input-error :messages="$message" class="mt-2"/>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <!-- Ingredient point WW -->
                            @desktop
                            <div class="field is-horizontal column is-one-third is-align-items-center">
                                @elsedesktop
                                <div class="field is-horizontal">
                                    @enddesktop
                                    <div class="field-label is-normal pt-0">
                                        <x-input-label for="ingredient_points" class="tag ingredient_points is-flex"
                                                       :value="__('messages.ingredientWW')"/>
                                    </div>
                                    <div class="field-body">
                                        <div class="control has-icons-left">
                                            <x-input-text
                                                    wire:model="form.points"
                                                    id="ingredient_points"
                                                    name="points"
                                                    class="{{ $errors->has('form.points') ? 'input is-danger' : 'input' }}"
                                                    type="number"
                                                    step="0.5"
                                                    min="0"
                                            />
                                            <x-input-icon class="icon is-small is-left" icon="pin"/>
                                            @error('form.points')
                                            <x-input-error :messages="$message" class="mt-2"/>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <!-- Ingredient quantity -->
                                @desktop
                                <div class="field is-horizontal column is-one-third is-align-items-center">
                                    @elsedesktop
                                    <div class="field is-horizontal">
                                        @enddesktop
                                        <div class="field-label is-normal pt-0">
                                            <x-input-label for="ingredient_quantity"
                                                           class="tag is-dark is-light is-flex"
                                                           :value="__('messages.ingredientQuantity')"/>
                                        </div>
                                        <div class="field-body">
                                            <div class="control has-icons-left">
                                                <x-input-text
                                                        wire:model="form.quantity"
                                                        id="ingredient_quantity"
                                                        name="quantity"
                                                        type="number"
                                                        step="0.5"
                                                        min="1"
                                                        class="{{ $errors->has('form.quantity') ? 'input is-danger' : 'input' }}"/>
                                                <x-input-icon class="icon is-small is-left" icon="scale"/>
                                                @error('form.quantity')
                                                <x-input-error :messages="$message" class="mt-2"/>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Ingredient unit -->
                                    @desktop
                                    <div class="field is-horizontal column is-one-third is-align-items-center">
                                        @elsedesktop
                                        <div class="field is-horizontal">
                                            @enddesktop
                                            <div class="field-label is-normal pt-0">
                                                <x-input-label for="ingredient_unit"
                                                               class="tag is-dark is-light is-flex"
                                                               :value="__('messages.ingredientUnit')"/>
                                            </div>
                                            <div class="field-body">
                                                <div class="control has-icons-left">
                                                    <div id="ingredient_unit_container" class="select is-link">
                                                        <x-input-select id="ingredient_unit"
                                                                        name="ingredient_unit"
                                                                        :options="$units"
                                                                        :selected="array_search($this->form->unit, $units)"
                                                                        :livewireModel="'form.unit'"
                                                                        :icon="'abc'"/>
                                                        @error('form.unit')
                                                        <x-input-error :messages="$message" class="mt-2"/>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Ingredient energy -->
                                        @desktop
                                        <div class="field is-horizontal column is-one-third is-align-items-center">
                                            @elsedesktop
                                            <div class="field is-horizontal">
                                                @enddesktop
                                                <div class="field-label is-normal pt-0">
                                                    <x-input-label for="ingredient_energy"
                                                                   class="tag is-primary ingredient_energy is-flex"
                                                                   :value="__('messages.ingredientEnergy')"/>
                                                </div>
                                                <div class="field-body">
                                                    <div class="control has-icons-left">
                                                        <x-input-text
                                                                wire:model="form.energy"
                                                                id="ingredient_energy"
                                                                name="energy"
                                                                class="{{ $errors->has('form.energy') ? 'input is-danger' : 'input' }}"
                                                                type="number"
                                                                step="0.1"
                                                                min="0"
                                                        />
                                                        <x-input-icon class="icon is-small is-left" icon="bolt"/>
                                                        @error('form.energy')
                                                        <x-input-error :messages="$message" class="mt-2"/>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Ingredient proteins -->
                                            @desktop
                                            <div class="field is-horizontal column is-one-third is-align-items-center">
                                                @elsedesktop
                                                <div class="field is-horizontal">
                                                    @enddesktop
                                                    <div class="field-label is-normal pt-0">
                                                        <x-input-label for="ingredient_proteins"
                                                                       class="tag ingredient_proteins is-flex"
                                                                       :value="__('messages.ingredientProteins')"/>
                                                    </div>
                                                    <div class="field-body">
                                                        <div class="control has-icons-left">
                                                            <x-input-text
                                                                    wire:model="form.proteins"
                                                                    id="ingredient_proteins"
                                                                    name="proteins"
                                                                    class="{{ $errors->has('form.energy') ? 'input is-danger' : 'input' }}"
                                                                    type="number"
                                                                    step="0.1"
                                                                    min="0"
                                                            />
                                                            <x-input-icon class="icon is-small is-left" icon="egg"/>
                                                            @error('form.proteins')
                                                            <x-input-error :messages="$message" class="mt-2"/>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Ingredient lipids -->
                                                @desktop
                                                <div
                                                        class="field is-horizontal column is-one-third is-align-items-center">
                                                    @elsedesktop
                                                    <div class="field is-horizontal">
                                                        @enddesktop
                                                        <div class="field-label is-normal pt-0">
                                                            <x-input-label for="ingredient_lipids"
                                                                           class="tag ingredient_lipids is-flex"
                                                                           :value="__('messages.ingredientLipids')"/>
                                                        </div>
                                                        <div class="field-body">
                                                            <div class="control has-icons-left">
                                                                <x-input-text
                                                                        wire:model="form.lipids"
                                                                        id="ingredient_lipids"
                                                                        name="lipids"
                                                                        class="{{ $errors->has('form.lipids') ? 'input is-danger' : 'input' }}"
                                                                        type="number"
                                                                        step="0.1"
                                                                        min="0"
                                                                />
                                                                <x-input-icon class="icon is-small is-left"
                                                                              icon="bakery_dining"/>
                                                                @error('form.lipids')
                                                                <x-input-error :messages="$message" class="mt-2"/>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Ingredient carbohydrates -->
                                                    @desktop
                                                    <div
                                                            class="field is-horizontal column is-align-items-center">
                                                        @elsedesktop
                                                        <div class="field is-horizontal">
                                                            @enddesktop
                                                            <div class="field-label is-normal pt-0">
                                                                <x-input-label for="ingredient_carbohydrates"
                                                                               class="tag ingredient_carbohydrates is-flex"
                                                                               :value="__('messages.ingredientCarbohydrates')"/>
                                                            </div>
                                                            <div class="field-body">
                                                                <div class="control has-icons-left">
                                                                    <x-input-text
                                                                            wire:model="form.carbohydrates"
                                                                            id="ingredient_carbohydrates"
                                                                            name="carbohydrates"
                                                                            class="{{ $errors->has('form.carbohydrates') ? 'input is-danger' : 'input' }}"
                                                                            type="number"
                                                                            step="0.1"
                                                                            min="0"
                                                                    />
                                                                    <x-input-icon class="icon is-small is-left"
                                                                                  icon="icecream"/>
                                                                    @error('form.carbohydrates')
                                                                    <x-input-error :messages="$message" class="mt-2"/>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!-- Ingredient fibers -->
                                                        @desktop
                                                        <div
                                                                class="field is-horizontal column is-align-items-center">
                                                            @elsedesktop
                                                            <div class="field is-horizontal">
                                                                @enddesktop
                                                                <div class="field-label is-normal pt-0">
                                                                    <x-input-label for="ingredient_fibers"
                                                                                   class="tag ingredient_fibers is-flex"
                                                                                   :value="__('messages.ingredientFibers')"/>
                                                                </div>
                                                                <div class="field-body">
                                                                    <div class="control has-icons-left">
                                                                        <x-input-text
                                                                                wire:model="form.fibers"
                                                                                id="ingredient_fibers"
                                                                                name="fibers"
                                                                                class="{{ $errors->has('form.fibers') ? 'input is-danger' : 'input' }}"
                                                                                type="number"
                                                                                step="0.1"
                                                                                min="0"
                                                                        />
                                                                        <x-input-icon class="icon is-small is-left"
                                                                                      icon="breakfast_dining"/>
                                                                        @error('form.fibers')
                                                                        <x-input-error :messages="$message"
                                                                                       class="mt-2"/>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Ingredient open_food_facts_id -->
                                                            @desktop
                                                            <div
                                                                    class="field is-horizontal column is-two-fifths is-align-items-center">
                                                                @elsedesktop
                                                                <div class="field is-horizontal">
                                                                    @enddesktop
                                                                    <div class="field-label is-normal pt-0">
                                                                        <x-input-label
                                                                                for="ingredient_open_food_facts_id"
                                                                                class="tag is-dark is-light is-flex"
                                                                                :value="__('messages.ingredientOpenFoodFacts')"/>
                                                                    </div>
                                                                    <div class="field-body">
                                                                        <div class="control is-flex has-icons-left">
                                                                            <x-input-text
                                                                                    wire:model="form.open_food_facts_id"
                                                                                    id="ingredient_open_food_facts"
                                                                                    name="ingredient_open_food_facts_id"
                                                                                    class="{{ $errors->has('form.open_food_facts_id') ? 'input is-danger' : 'input' }}"/>
                                                                            <x-input-icon class="icon is-small is-left"
                                                                                          icon="app_registration"/>
                                                                            @error('form.open_food_facts_id')
                                                                            <x-input-error :messages="$message"
                                                                                           class="mt-2"/>
                                                                            @enderror
                                                                            @if($displayTable)
                                                                                <span
                                                                                        @class(['button', 'is-link', 'is-light','search-open-food-facts', 'ml-2', 'is-hidden' => $form->open_food_facts_id === '0' || strlen($form->open_food_facts_id) < 2])
                                                                                        x-data="{ isLoading: false }"
                                                                                        @click="isLoading = true; $wire.$parent.searchForOpenFoodFacts({{$form->open_food_facts_id}}).finally(() => { isLoading = false; })"
                                                                                        :class="{'is-loading': isLoading}"
                                                                                        @class(['is-dark' => $darkMode])
                                                                                        wire:click="$parent.searchForOpenFoodFacts({{$form->open_food_facts_id}})">
                        <span class="material-symbols-outlined">search_check</span>
                    </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                        class="is-flex field column is-full is-horizontal is-justify-content-end">
                                                                    <x-primary-button
                                                                            @class(['is-dark' => $darkMode, 'mr-2 mb-2']) wire:loading.class="is-loading">{{ __('messages.save') }}
                                                                    </x-primary-button>
                                                                </div>
        </form>
</section>
