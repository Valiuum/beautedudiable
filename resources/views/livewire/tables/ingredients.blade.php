<div x-data="{selection: $wire.entangle('selection')}">

    <div x-on:close-notification.window="document.getElementsByClassName('banner-notification')[0].remove()"/>
    @if(collect($existing_ingredients_list)->isNotEmpty())
        <x-notification-banner class="animate__animated animate__fadeInRight is-danger banner-notification"
                               :message="'ingredients_exists'"
                               :data='$existing_ingredients_list'
        />
    @endif

    <div class="column is-full px-0 mb-3">
        <div class="is-flex is-justify-content-center my-5">
            <label for="search_ingredient"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.searchIngredient')}}</label>
        </div>
        <div id="tableSearchIngredient"
             class="tableSearch control field is-grouped is-flex is-justify-content-center my-5 mx-auto has-icons-left">
            <x-input-text
                wire:model.live.debounce.250ms="search"
                id="search_ingredient"
                class="input"
                name="search_ingredient"
                type="text"
            />
            <x-input-icon class="icon is-small is-left" icon="nutrition"/>
        </div>
    </div>
    <x-danger-button
        x-cloak
        x-show="selection.length > 0"
        x-on:click="$wire.deleteSelected">
        <span class="material-symbols-outlined mr-2">delete</span>
        <span>{{ __('messages.delete') }}</span>
    </x-danger-button>
    <div class="table-container">
        {{--
        @dump($selection)
        <span x-html="JSON.stringify(selection)"></span>
        --}}
        <table class="table is-fullwidth is-hoverable">
            <thead>
            <tr>
                <th></th>
                <x-table-header name="name" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientName')}}
                </x-table-header>
                <x-table-header name="description" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientDescription')}}
                </x-table-header>
                <x-table-header name="points" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientWW')}}
                </x-table-header>
                <x-table-header name="quantity" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientQuantity')}}
                </x-table-header>
                <x-table-header name="unit" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientUnit')}}
                </x-table-header>
                <x-table-header name="energy" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientEnergy')}}
                </x-table-header>
                <x-table-header name="proteins" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientProteins')}}
                </x-table-header>
                <x-table-header name="lipids" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientLipids')}}
                </x-table-header>
                <x-table-header name="carbohydrates" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientCarbohydrates')}}
                </x-table-header>
                <x-table-header name="fibers" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.ingredientFibers')}}
                </x-table-header>
                <th>{{__('messages.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ingredients as $ingredient)
                <tr>
                    <td>
                        <div class="field">
                            <input id="checkbox-{{$ingredient->id}}"
                                   class="is-checkradio is-circle"
                                   x-model="selection"
                                   value="{{$ingredient->id}}"
                                   type="checkbox"
                                   name="checkbox-{{$ingredient->id}}">
                            <label for="checkbox-{{$ingredient->id}}"></label>
                        </div>
                    </td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $ingredient->name) : $ingredient->name !!}</td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $ingredient->description) : $ingredient->description !!}</td>
                    <td>{{$ingredient->points}}</td>
                    <td>{{$ingredient->quantity}}</td>
                    <td>{{$ingredient->unit}}</td>
                    <td>{{$ingredient->energy}}</td>
                    <td>{{$ingredient->proteins}}</td>
                    <td>{{$ingredient->lipids}}</td>
                    <td>{{$ingredient->carbohydrates}}</td>
                    <td>{{$ingredient->fibers}}</td>
                    <td>
                        <button
                            @class(['is-dark' => $darkMode, 'button', 'is-info is-light']) wire:click="startEdit({{$ingredient->id}})">
                            <span class="material-symbols-outlined">more_horiz</span>
                        </button>
                    </td>
                </tr>
                @if($editId === $ingredient->id)
                    <tr>
                        <td colspan="12">
                            <livewire:tables.ingredients-form
                                :displayTable="true"
                                :darkMode="$darkMode"
                                :isLoading="$isLoading"
                                :ingredient="$ingredient"
                                :key="$ingredient->id"/>
                        </td>
                    </tr>
            @endif
            @endforeach
        </table>
    </div>
    {{$ingredients->links()}}
</div>
