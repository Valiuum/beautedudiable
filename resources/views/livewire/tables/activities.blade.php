<div x-data="{selection: $wire.entangle('selection')}">
    <div class="column is-full px-0 mb-3">
        <div class="is-flex is-justify-content-center my-5">
            <label for="search_ingredient"
                   class="is-size-4 has-text-weight-semibold">{{__('messages.searchActivity')}}</label>
        </div>
        <div id="tableSearchActivities"
             class="tableSearch control field is-grouped is-flex is-justify-content-center my-5 mx-auto has-icons-left">
            <x-input-text
                    wire:model.live.debounce.250ms="search"
                    id="search_activity"
                    class="input"
                    name="search_activity"
                    type="text"
            />
            <x-input-icon class="icon is-small is-left" icon="sports_score"/>
        </div>
    </div>
    <x-danger-button
            x-cloak
            x-show="selection.length > 0"
            x-on:click="$wire.deleteSelected">
        <span class="material-symbols-outlined mr-2">delete</span>
        <span>{{ __('messages.delete') }}</span>
    </x-danger-button>
    <div class="table-container">
        <table class="table is-fullwidth is-hoverable">
            <thead>
            <tr>
                <th></th>
                <x-table-header name="activity" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.activity')}}
                </x-table-header>
                <x-table-header name="comment" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.comment')}}
                </x-table-header>
                <x-table-header name="date" :direction="$orderDirection" :field="$orderField">
                    {{__('messages.date')}}
                </x-table-header>
                <th>{{__('messages.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($activities as $activity)

                @switch($activity->activity)
                    @case('hiking')
                        @php $activity_name = __('messages.hiking'); @endphp
                        @break
                    @case('yoga')
                        @php $activity_name = __('messages.yoga'); @endphp
                        @break
                    @case('running')
                        @php $activity_name = __('messages.running'); @endphp
                        @break
                    @case('workout')
                        @php $activity_name = __('messages.workout'); @endphp
                        @break
                @endswitch

                <tr>
                    <td>
                        <div class="field">
                            <input id="checkbox-{{$activity->id}}"
                                   class="is-checkradio is-circle"
                                   x-model="selection"
                                   value="{{$activity->id}}"
                                   type="checkbox"
                                   name="checkbox-{{$activity->id}}">
                            <label for="checkbox-{{$activity->id}}"></label>
                        </div>
                    </td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $activity_name) : $activity_name !!}</td>
                    <td>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $activity->comment) : $activity->comment !!}</td>
                    <td>{{\Carbon\Carbon::parse($activity->date)->translatedFormat('l d M Y')}}</td>
                    <td>
                        <button
                                @class(['is-dark' => $darkMode, 'button', 'is-info is-light']) wire:click="startEdit({{$activity->id}})">
                            <span class="material-symbols-outlined">more_horiz</span>
                        </button>
                    </td>
                </tr>
                @if($editId === $activity->id)
                    <tr>
                        <td colspan="5">
                            <livewire:tables.activities-form
                                    :displayTable="true"
                                    :darkMode="$darkMode"
                                    :edit="false"
                                    :activity="$activity"
                                    :key="$activity->id"/>
                        </td>
                    </tr>
            @endif
            @endforeach
        </table>
    </div>
    {{$activities->links()}}
</div>
