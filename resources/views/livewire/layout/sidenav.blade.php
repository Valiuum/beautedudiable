@desktop

@if(Str::startsWith(Route::currentRouteName(), 'ingredients') ||
    Str::startsWith(Route::currentRouteName(), 'dish'))

    @php
        $has_parent = true;
        $open_onglet = 1;
    @endphp

@elseif(

    Str::startsWith(Route::currentRouteName(), 'activity') ||
    Str::startsWith(Route::currentRouteName(), 'weight') ||
    Str::startsWith(Route::currentRouteName(), 'bilan') ||
    Str::startsWith(Route::currentRouteName(), 'cycle'))

    @php
        $has_parent = true;
        $open_onglet = 2;
    @endphp

@else

    @php
        $has_parent = false;
        $open_onglet = 0;
    @endphp

@endif

<aside
    id="sidenav" @class(['column','pl-5','pr-3','is-2','is-desktop','is-narrow-mobile','is-fullheight','section','is-hidden-mobile','menu'])>
    <ul class="menu-list">
        <li>
            <x-link-menu-element route="home" link_name="{{ __('messages.home') }}" icon="home"/>
        </li>
        <ul class="my-3">
            <li x-data="{ open: {{ $has_parent && $open_onglet == 1 ? 'true' : 'false' }} }">
                <x-link-menu-element icon="restaurant" is_openable="false" link_name="{{ __('messages.food') }}"/>
                <ul x-show="open">
                    <li class="my-2" x-show="open"
                        x-transition:enter.scale.0
                        x-transition:leave.scale.80
                        x-transition:enter.duration.100ms
                        x-transition:leave.duration.300ms
                        x-cloak>
                        <x-link-menu-element
                            route="ingredients"
                            link_name="{{ __('messages.ingredients') }}"
                            icon="local_pizza"
                        />
                    </li>
                    <li class="my-2" x-show="open"
                        x-transition:enter.scale.0
                        x-transition:leave.scale.80
                        x-transition:enter.duration.200ms
                        x-transition:leave.duration.100ms
                        x-cloak>
                        <x-link-menu-element
                            route="dish.show"
                            is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'dish')}}"
                            link_name="{{ __('messages.dishes') }}"
                            icon="soup_kitchen"
                        />
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="my-3">
            <li x-data="{ open: {{ $has_parent && $open_onglet == 2 ? 'true' : 'false' }} }">
                <x-link-menu-element icon="{{auth()->user()->isWomen() ? 'woman' : 'taunt'}}"
                                     is_openable="false" link_name="{{ __('messages.followUp') }}"/>
                <ul x-show="open">
                    <li class="my-2" x-show="open"
                        x-transition:enter.scale.0
                        x-transition:leave.scale.80
                        x-transition:enter.duration.100ms
                        x-transition:leave.duration.400ms
                        x-cloak>
                        <x-link-menu-element
                            route="weight.show"
                            is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'weight')}}"
                            link_name="{{ __('messages.weight') }}"
                            icon="monitor_weight"
                        />
                    </li>
                    <li class="my-2" x-show="open"
                        x-transition:enter.scale.0
                        x-transition:leave.scale.80
                        x-transition:enter.duration.200ms
                        x-transition:leave.duration.200ms
                        x-cloak>
                        <x-link-menu-element
                            route="activity.show"
                            is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'activity')}}"
                            link_name="{{ __('messages.activities') }}"
                            icon="fitness_center"/>
                    </li>
                    @if(auth()->user()->isWomen())
                        <li class="my-2" x-show="open"
                            x-transition:enter.scale.0
                            x-transition:leave.scale.80
                            x-transition:enter.duration.200ms
                            x-transition:leave.duration.200ms
                            x-cloak>
                            <x-link-menu-element
                                route="cycle.show"
                                is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'cycle')}}"
                                link_name="{{ __('messages.cycle') }}"
                                icon="female"/>
                        </li>
                    @endif
                    <li class="my-2" x-show="open"
                        x-transition:enter.scale.0
                        x-transition:leave.scale.80
                        x-transition:enter.duration.200ms
                        x-transition:leave.duration.200ms
                        x-cloak>
                        <x-link-menu-element route="bilan.show" link_name="{{ __('messages.bilan') }}"
                                             icon="data_usage"/>
                    </li>
                </ul>
            </li>
        </ul>
        <li>
            <x-link-menu-element route="calendar.show" link_name="{{ __('messages.calendar') }}" icon="calendar_month"/>
        </li>
    </ul>
</aside>
@elsedesktop
<?php

use App\Livewire\Actions\Logout;

$logout = function (Logout $logout) {
    $logout();

    $this->redirect('/', navigate: true);
};

?>
<div>
    <nav id="slide-menu">
        <ul>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="home"
                    link_name="{{ __('messages.home') }}"
                    icon="home"
                    is_desktop={{false}}
                />
            </li>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="ingredients"
                    link_name="{{ __('messages.ingredients') }}"
                    icon="local_pizza"
                    is_desktop={{false}}
                />
            </li>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="dish.show"
                    is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'dish')}}"
                    link_name="{{ __('messages.dishes') }}"
                    icon="soup_kitchen"
                    is_desktop={{false}}
                />
            </li>
            @if(auth()->user()->isWomen())
                <li class="my-2 p-2 mobile-link">
                    <x-link-menu-element
                        route="cycle.show"
                        is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'cycle')}}"
                        link_name="{{ __('messages.cycle') }}"
                        is_desktop={{false}}
                    />
                </li>
            @endif
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="activity.show"
                    is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'activity')}}"
                    link_name="{{ __('messages.activities') }}"
                    icon="fitness_center"
                    is_desktop={{false}}
                />
            </li>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="weight.show"
                    is_active="{{(bool) Str::startsWith(Route::currentRouteName(), 'weight')}}"
                    link_name="{{ __('messages.weight') }}"
                    icon="monitor_weight"
                    is_desktop={{false}}
                />
            </li>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="bilan.show"
                    link_name="{{ __('messages.bilan') }}"
                    icon="data_usage"
                    is_desktop={{false}}
                />
            </li>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="calendar.show"
                    link_name="{{ __('messages.calendar') }}"
                    icon="calendar_month"
                    is_desktop={{false}}
                />
            </li>
            <hr class="dropdown-divider has-background-white-ter"/>
            <li class="my-2 p-2 mobile-link">
                <x-link-menu-element
                    route="profile"
                    link_name="{{ __('Profile') }}"
                    icon="account_circle"
                    is_desktop={{false}}
                />
            </li>
            <li class="my-2 p-2 mobile-link">
                <span class="icon-text">
                    <a class="navbar-item is-flex is-align-items-center p-0" wire:click="logout">
                        <span class="material-symbols-outlined has-text-danger">logout</span>
                        {{ __('Log out') }}
                    </a>
                </span>
            </li>
        </ul>
    </nav>
    <nav id="navbar-mobile" class="navbar is-fixed-bottom mx-auto mb-2 is-rounded is-hidden-tablet"
         role="navigation">
        <div class="navbar-brand">
            <a class="navbar-item is-expanded is-flex is-align-content-center is-justify-content-center {{ Route::currentRouteName() == 'activity.show' ? 'is-active-link' : '' }}"
               href="{{ route('activity.show') }}" wire:navigate>
                <i class="material-symbols-outlined">fitness_center</i>
            </a>
            <a class="navbar-item is-expanded is-flex is-align-content-center is-justify-content-center {{ Route::currentRouteName() == 'home' ? 'is-active-link' : '' }}"
               href="{{ route('home') }}" wire:navigate>
                <i class="material-symbols-outlined">home</i>
            </a>
            <a class="navbar-item is-expanded is-flex is-align-content-center is-justify-content-center {{ Route::currentRouteName() == 'weight.show' ? 'is-active-link' : '' }}"
               href="{{ route('weight.show') }}" wire:navigate>
                <i class="material-symbols-outlined">monitor_weight</i>
            </a>
        </div>
    </nav>
</div>
@enddesktop
