<?php

use App\Livewire\Actions\Logout;

$logout = function (Logout $logout) {
    $logout();

    $this->redirect('/', navigate: true);
};

?>

<nav class="navbar is-fixed-top has-shadow is-hidden-mobile" role="navigation" aria-label="main navigation">

    <!-- Primary Navigation Menu -->
    <div class="navbar-brand is-hidden-touch">
        <a href="{{ route('home') }}" wire:navigate class="is-flex is-align-items-center ml-4">
            <x-application-logo/>
        </a>
        <h1 class="navbar-item subtitle is-size-4">
            {{__('messages.website')}}
        </h1>
    </div>

    <!-- Menu Navigation -->
    <div id="navMenu" class="navbar-menu">
        <div class="navbar-end">
            <!-- Notifications -->
            <livewire:profile.notification-profile/>
            <!-- Profile -->
            <div class="navbar-item has-dropdown mr-4 is-hoverable">
                <a class="navbar-link is-size-6">
                    <div x-data="{{ json_encode(['name' => auth()->user()->name]) }}"
                         x-text="name"
                         x-on:profile-updated.window="name = $event.detail.name">
                    </div>
                </a>
                <div class="navbar-dropdown is-right">
                    <a class="navbar-item" href="{{route('profile')}}" wire:navigate>
                        <span class="material-symbols-outlined mr-2">face</span>
                        {{ trans('messages.profile') }}
                    </a>
                    @if(auth()->user()->isAdmin())
                        <a class="navbar-item" href="/pulse">
                            <span class="material-symbols-outlined mr-2">browse_activity</span>
                            {{trans('messages.monitoring') }}
                        </a>
                    @endif
                    <hr class="navbar-divider">
                    <a class="navbar-item" wire:click="logout">
                        <span class="material-symbols-outlined has-text-danger mr-2">logout</span>
                        {{ trans('messages.logout') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
