<div id='calendar' wire:ignore class="column is-full">
    <script>
        window.calendarEvents = @json($calendarEvents);
        window.listDay = @json(__('messages.listDay'));
        window.listWeek = @json(__('messages.listWeek'));
        window.listMonth = @json(__('messages.listMonth'));
        window.listWeekMobile = @json(__('messages.listWeekMobile'));
        window.listMonthMobile = @json(__('messages.listMonthMobile'));
    </script>
</div>
