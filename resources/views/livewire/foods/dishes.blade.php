<div wire:scroll x-cloak x-data="{open: false, dish: dish(), dishData: $wire.entangle('dishData').live}"
     x-init="dish.initDish(dishData)"
     @click.away="open = false">

    @if($existing_dish)
        <x-notification-banner class="animate__animated animate__fadeInRight is-warning banner-notification"
                               :message="'dish_exists'"/>
    @endif

    <div class="columns is-vcentered mx-0">
        <div class="column is-full is-one-third-desktop mx-auto my-5 is-justify-content-center is-align-content-center">
            <div class="control has-icons-left">
                <x-input-text
                        x-model="dish.name"
                        placeholder="{{__('messages.dishName')}}"
                        class="{{ $errors->has('form.name') ? 'input is-danger' : 'input' }}"
                        type="text"
                />
                <x-input-icon class="icon is-small is-left" icon="title"/>
            </div>
            @error('form.name')
            <x-input-error :messages="$message" class="mt-2"/>
            @enderror
        </div>
    </div>

    <div id="intakes-cards-dishes" class="columns is-mobile">
        <template x-for="card in dish.cards" :key="card.id">
            <div class="column is-4-mobile is-4-tablet-only-ta is-2-desktop">
                <x-meal-card x-bind:class="card.id">
                    <x-slot name="icon">
                        <span x-text="card.icon"></span>
                    </x-slot>
                    <x-slot name="title">
                        <span x-text="card.title"></span>
                    </x-slot>
                    <x-slot name="content">
                        <span x-text="card.data"></span>
                        <span x-text="card.unit"></span>
                    </x-slot>
                </x-meal-card>
            </div>
        </template>
    </div>

    <div class="box is-shadowless ingredients-dishes">
        <div id="search_food" class="is-flex dropdown my-4" @class(["is-active" => count($results) > 1])>
            <div class="field mx-auto column is-two-fifths">
                <div class="control has-icons-left">
                    <x-input-text
                            wire:model.live.debounce.500ms="search"
                            id="search_food_input"
                            class="input"
                            name="search_food"
                            type="text"
                            placeholder="{{__('messages.searchIngredient')}}"
                            @click="open = true"
                            @click.away="open = false"
                    />
                    <x-input-icon class="icon is-small is-left" icon="nutrition"/>
                    @if($results)
                        <div class="dropdown-menu column is-full" id="dropdown-menu" role="menu"
                             x-show="open">
                            <div class="dropdown-content">
                                @if(collect($results)->containsOneItem())
                                    <a class="dropdown-item">
                                        <div class="is-flex is-align-items-center">
                                            <span>{{__('messages.searchIngredientsNoResults')}}</span>
                                        </div>
                                    </a>
                                @endif
                                @foreach($results as $result)
                                    {{-- This is ingredient --}}
                                    @if ($result instanceof \App\Models\Ingredient)
                                        <a class="dropdown-item"
                                           @click="dish.addIngredient($el); $wire.set('search', '')"
                                           data-elements="{{json_encode($result)}}"
                                        >
                                            <div class="is-flex is-align-items-center">
                                                <figure class="image is-32x32 is-inline-block mr-2">
                                                    <span class="material-symbols-outlined">grocery</span>
                                                </figure>
                                                <span>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $result->name) : $result->name !!}</span>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <form wire:submit="save">
            <div class="list">
                <template x-for="(ingredient, ingredientIndex) in dish.ingredients" :key="ingredientIndex">
                    <x-dish-ingredient/>
                </template>
            </div>
            <div class="mt-5">
                <div class="columns is-multiline is-mobile is-12">
                    @if($form->action === 'duplicate')
                        <div class="column is-flex is-justify-content-center">
                            <div class="field">
                                <x-input-label for="type" :value="__('messages.duplicateMealSelect')"/>
                                <x-input-select id="type"
                                                name="type"
                                                :options="['breakfast' => __('messages.breakfast'), 'lunch' => __('messages.lunch'), 'dinner' => __('messages.dinner'), 'snack' => __('messages.snack'), 'bar' => __('messages.bar')]"
                                                :selected="$form->type"
                                                :livewireModel="'form.type'"
                                                :icon="'local_dining'"/>
                                @error('form.type')
                                <x-input-error :messages="$message" class="mt-2"/>
                                @enderror
                            </div>
                        </div>
                    @endif
                </div>
                <div class="column is-full">
                    <div class="is-flex is-justify-content-flex-end">
                        <x-primary-button x-bind:class="{'is-dark': dish.darkMode}"
                                          wire:click="saveToDish(dish.name, dish.ingredients)"
                                          wire:loading.class="is-loading"
                                          x-show="dish.ingredients.length > 0"
                        >
                            {{ __('messages.save') }}
                        </x-primary-button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        window.ingredientWW = @json(__('messages.ingredientWW'));
        window.ingredientEnergy = @json(__('messages.ingredientEnergy'));
        window.ingredientProteins = @json(__('messages.ingredientProteins'));
        window.ingredientLipids = @json(__('messages.ingredientLipids'));
        window.ingredientCarbohydrates = @json(__('messages.ingredientCarbohydrates'));
        window.ingredientFibers = @json(__('messages.ingredientFibers'));
        window.ingredientsAlreadyAdded = @json(__('messages.ingredientsAlreadyAdded'));
        window.notAllowed = @json(__('messages.notAllowed'));
    </script>
</div>
