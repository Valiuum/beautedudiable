<div x-cloak x-data="{open: false, items: items(), mealData: $wire.entangle('mealData').live}"
     x-init="items.initMeal(mealData)"
     @click.away="open = false">
    @if($form->warning)
        <x-notification-banner class="animate__animated animate__fadeInRight is-warning banner-notification"
                               :message="'meal_processing'"/>
    @endif
    @if($existing_meal)
        <x-notification-banner class="animate__animated animate__fadeInRight is-warning banner-notification"
                               :message="'meal_exists'"/>
    @endif
    <div id="intakes-cards-meals" class="columns is-mobile">
        <template x-for="card in items.cards" :key="card.id">
            <div class="column is-4-mobile is-4-tablet-only-ta is-2-desktop">
                <x-meal-card x-bind:class="card.id">
                    <x-slot name="icon">
                        <span x-text="card.icon"></span>
                    </x-slot>
                    <x-slot name="title">
                        <span x-text="card.title"></span>
                    </x-slot>
                    <x-slot name="content">
                        <span x-text="card.data"></span>
                        <span x-text="card.unit"></span>
                    </x-slot>
                </x-meal-card>
            </div>
        </template>
    </div>
    <div class="box is-shadowless">
        <div id="search_food" class="is-flex dropdown" @class(["is-active" => count($results) > 1])>
            <div class="field mx-auto column is-two-fifths">
                <div class="control has-icons-left">
                    <x-input-text
                            wire:model.live.debounce.500ms="search"
                            id="search_food_input"
                            class="input"
                            name="search_food"
                            type="text"
                            placeholder="{{__('messages.searchIngredient')}}"
                            @click="open = true"
                            @click.away="open = false"
                    />
                    <x-input-icon class="icon is-small is-left" icon="nutrition"/>
                    @if($results)
                        <div id="dropdown-menu" class="dropdown-menu column is-full" role="menu"
                             x-show="open">
                            <div class="dropdown-content">
                                @if(collect($results)->containsOneItem())
                                    <a class="dropdown-item">
                                        <div class="is-flex is-align-items-center">
                                            <span>{{__('messages.searchIngredientsNoResults')}}</span>
                                        </div>
                                    </a>
                                @endif
                                @foreach($results as $result)
                                    {{-- This is dish --}}
                                    @if($result instanceof \Illuminate\Support\Collection && $result->isNotEmpty())
                                        @foreach($result as $dish)
                                            <a class="dropdown-item" wire:click="addDish({{$dish->id}})">
                                                <div class="is-flex is-align-items-center">
                                                    <figure class="image is-32x32 is-inline-block mr-2">
                                                        <span class="material-symbols-outlined">skillet</span>
                                                    </figure>
                                                    <span>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $dish->name) : $dish->name !!}</span>
                                                </div>
                                            </a>
                                        @endforeach
                                    @elseif ($result instanceof \App\Models\Ingredient)
                                        {{-- This is ingredient --}}
                                        <a class="dropdown-item"
                                           @click="items.addIngredient($el); $wire.set('search', '')"
                                           data-elements="{{json_encode($result)}}"
                                        >
                                            <div class="is-flex is-align-items-center">
                                                <figure class="image is-32x32 is-inline-block mr-2">
                                                    <span class="material-symbols-outlined">grocery</span>
                                                </figure>
                                                <span>{!! !empty($this->search) ? preg_replace('/\b(' . preg_quote($this->search, '/') . '[\w]*)\b/i', '<span class="tag is-info is-light mx-1">$1</span>', $result->name) : $result->name !!}</span>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <form wire:submit="save">
            <div class="field is-grouped is-grouped-multiline my-4">
                @foreach(collect($dishesTags)->take(10) as $tag)
                    @if($form->action !== 'create')
                        @php
                            $exists = collect($mealData['dishes'])->keys()->contains($tag->id);
                        @endphp
                    @else
                        @php
                            $exists = false;
                        @endphp
                    @endif
                    <x-tags class="animate__animated {{$exists ? 'is-hidden' : ''}}" :id="$tag->id" :name="$tag->name"
                            :type="$tag->type"/>
                @endforeach
            </div>
            <div class="list">
                <template x-for="(dish, dishIndex) in items.dishes" :key="dishIndex">
                    <x-meal-dish-ingredient/>
                </template>
                <template x-for="(ingredient, ingredientIndex) in items.ingredients" :key="ingredientIndex">
                    <x-meal-ingredient/>
                </template>
            </div>
            @php
                $hasBonus = collect($mealData['ingredients'])->contains(function ($ingredient) {
                return $ingredient['name'] === 'bonus';
                });
                if ($hasBonus || auth()->user()->bonus?->points <= 0 || $form->action === 'duplicate') {
                    $bonusClass = 'is-hidden';
                } else {
                    $bonusClass = 'column is-flex is-justify-content-center';
                }
            @endphp
            <div class="mt-5">
                <div class="columns is-multiline is-mobile is-12">
                    @if($form->action === 'duplicate')
                        <div class="column is-flex is-justify-content-center">
                            <div class="field">
                                <x-input-label for="type" :value="__('messages.duplicateMealSelect')"/>
                                <x-input-select id="type"
                                                name="type"
                                                :options="['breakfast' => __('messages.breakfast'), 'lunch' => __('messages.lunch'), 'dinner' => __('messages.dinner'), 'snack' => __('messages.snack'), 'bar' => __('messages.bar')]"
                                                :selected="$form->type"
                                                :livewireModel="'form.type'"
                                                :icon="'local_dining'"/>
                                @error('form.type')
                                <x-input-error :messages="$message" class="mt-2"/>
                                @enderror
                            </div>
                        </div>
                    @endif
                    @desktop
                    <div class="{{$bonusClass}}">
                        <div class="field" x-show="items.ingredients.length > 0 || items.dishes.length > 0">
                            @if(auth()->user()->bonus?->points > 0)
                                <x-input-label for="user_bonus"
                                               :value="trans_choice('messages.userBonusPoints', auth()->user()->bonus->points, ['value' => auth()->user()->bonus->points])"/>
                                <x-input-number id="user_bonus" name="user_bonus"
                                                :maxValue="auth()->user()->bonus->points"/>
                            @endif
                        </div>
                    </div>
                    @elsedesktop
                    <div class="{{$bonusClass}} is-12 has-text-centered">
                        <div class="field" x-show="items.ingredients.length > 0 || items.dishes.length > 0">
                            @if(auth()->user()->bonus?->points > 0)
                                <x-input-label for="user_bonus"
                                               :value="trans_choice('messages.userBonusPoints', auth()->user()->bonus->points, ['value' => auth()->user()->bonus->points])"/>
                                <x-input-number id="user_bonus" name="user_bonus"
                                                :maxValue="auth()->user()->bonus->points"/>
                            @endif
                        </div>
                    </div>
                    @enddesktop
                    @if($form->action !== 'create')
                        <div class="column is-flex is-justify-content-center">
                            <div class="field">
                                <x-input-label for="datepicker" :value="__('messages.datepicker')"/>
                                @if($form->action === 'duplicate')
                                    <livewire:component.datepicker :clearDate="true"/>
                                @else
                                    <livewire:component.datepicker/>
                                @endif
                                @error('form.new_date')
                                <x-input-error :messages="$message" class="mt-2"/>
                                @enderror
                            </div>
                        </div>
                    @endif
                    @if($form->action !== 'duplicate')
                        <div class="column is-flex is-justify-content-center">
                            <div class="field" x-show="items.ingredients.length > 0 || items.dishes.length > 0">
                                <x-input-label for="shared_meal_with" :value="__('messages.sharedMealWith')"/>
                                <x-input-select id="shared_meal_with"
                                                name="shared_meal_with"
                                                :options="$shareWith"
                                                :selected="$form->shared_meal_with"
                                                :livewireModel="'shared_meal_with'"
                                                :icon="'share'"/>
                                @error('form.shared_meal_with')
                                <x-input-error :messages="$message" class="mt-2"/>
                                @enderror
                            </div>
                            @endif
                        </div>
                </div>
                <div class="column is-full">
                    <div class="is-flex is-justify-content-flex-end">
                        <x-primary-button :disabled="$form->warning"
                                          x-bind:class="{'is-dark': items.darkMode}"
                                          wire:click="saveToMeal(items.ingredients, items.dishes, items.userBonus.initial)"
                                          wire:loading.class="is-loading"
                                          x-show="items.ingredients.length > 0 || items.dishes.length > 0"
                        >
                            {{ __('messages.save') }}
                        </x-primary-button>
                    </div>
                </div>
            </div>
        </form>
        <div x-on:dish-updated.window="items.addDish($event.detail.data)"></div>
    </div>
</div>
<script>
    window.ingredientWW = @json(__('messages.ingredientWW'));
    window.ingredientEnergy = @json(__('messages.ingredientEnergy'));
    window.ingredientProteins = @json(__('messages.ingredientProteins'));
    window.ingredientLipids = @json(__('messages.ingredientLipids'));
    window.ingredientCarbohydrates = @json(__('messages.ingredientCarbohydrates'));
    window.ingredientFibers = @json(__('messages.ingredientFibers'));
    window.ingredientsAlreadyAdded = @json(__('messages.ingredientsAlreadyAdded'));
    window.dishAlreadyAdded = @json(__('messages.dishAlreadyAdded'));
    window.notAllowed = @json(__('messages.notAllowed'));
    window.minBonusPoints = @json(__('messages.minBonusPoints'));
    window.maxBonusPoints = @json(__('messages.maxBonusPoints'));
</script>
