<div id='bilan' class="column is-full">
    <x-slot name="header" :imgPath="asset('images/header/bilan.svg')"></x-slot>
    <section class="section column is-full">
        <h4 class="is-size-4 has-text-weight-semibold  @mobile has-text-centered @endmobile">{{ __('messages.bilan') }}</h4>
    </section>
    <x-card-bilan class="column is-full bilan-graphs-mobile interactions"
                  :graph_data="['id' => 'graph_meals_follow_up', 'durations' => ['periods' => $durations, 'scales' => $scales, 'default' => collect($default_graphs_display)->get('graph_meals_follow_up')]]"/>
    <div class="columns is-vcentered columns">
        <div class="column is-three-fifths-desktop">
            <div class="column px-0">
                <x-card-bilan class="column bilan-graphs-mobile" :graph_data="['id' => 'graph_meals_by_name']"/>
                <x-card-bilan class="column bilan-graphs-mobile" :graph_data="['id' => 'graph_meals_by_day']"/>
            </div>
        </div>
        <div class="column is-one-fifths-desktop">
            <x-card-bilan class="column is-full bilan-cards-mobile"
                          :card_data="['id' => 'card_ingredients', 'type' => 'ingredients', 'data' => $bilan_data['meals']['ingredients']]"/>
        </div>
    </div>
    <h3 class="is-size-3">{{__('messages.bilan_activities_header')}}</h3>
    <div class="columns bilan-cards-mobile">
        <div class="column is-flex is-one-fifths-desktop">
            <x-card-bilan class="column card is-overflow-scroll @mobile mx-2 @endmobile"
                          :card_data="['id' => 'card_activities_summary', 'type' => 'activities', 'subtype' => 'summary', 'data' =>  $bilan_data['activities']['activities_summary']]">
            </x-card-bilan>
        </div>
        <div class="column is-flex is-one-fifths-desktop">
            <x-card-bilan class="column card is-overflow-scroll @mobile mx-2 @endmobile"
                          :card_data="['id' => 'card_activities_month_average', 'type' => 'activities', 'subtype' => 'average', 'data' => $bilan_data['activities']['activities_months_average']]">
            </x-card-bilan>
        </div>
        <div class="column is-flex is-one-fifths-desktop">
            <x-card-bilan class="column card is-overflow-scroll @mobile mx-2 @endmobile"
                          :card_data="['id' => 'card_activities_tendance', 'type' => 'activities', 'subtype' => 'tendance', 'data' => $bilan_data['activities']['activities_current_month_tendance']]">
            </x-card-bilan>
        </div>
    </div>
    <x-card-bilan class="column bilan-graphs-mobile" :graph_data="['id' => 'graph_activities_follow_up']"/>
    @if(auth()->user()->isWomen())
        <h3 class="is-size-3">{{__('messages.bilan_cycles_header')}}</h3>
        <x-card-bilan class="column is-full bilan-graphs-mobile interactions"
                      :graph_data="['id' => 'graph_cycles_follow_up',  'durations' => ['periods' => $durations, 'scales' => $scales, 'default' => collect($default_graphs_display)->get('graph_cycles_follow_up')]]"/>
    @endif
    <h3 class="is-size-3">{{__('messages.bilan_weights_header')}}</h3>
    <div class="columns bilan-cards-mobile">
        <div class="column is-flex is-one-fifths-desktop">
            <x-card-bilan class="column card is-overflow-scroll @mobile mx-2 @endmobile"
                          :card_data="['id' => 'card_weights', 'type' => 'weights', 'subtype' => 'summary', 'data' => $bilan_data['weights']['weights_summary']]">
            </x-card-bilan>
        </div>
        <div class="column is-flex is-one-fifths-desktop">
            <x-card-bilan class="column card is-overflow-scroll @mobile mx-2 @endmobile"
                          :card_data="['id' => 'card_weights_month_average', 'type' => 'weights', 'subtype' => 'average', 'data' => $bilan_data['weights']['weights_months_average']]">
            </x-card-bilan>
        </div>
        <div class="column is-flex is-one-fifths-desktop">
            <x-card-bilan class="column card is-overflow-scroll is-align-content-center @mobile mx-2 @endmobile"
                          :card_data="['id' => 'card_weights_tendance', 'type' => 'weights', 'subtype' => 'tendance', 'data' => $bilan_data['weights']['weights_months_current_month_tendance']]">
            </x-card-bilan>
        </div>
    </div>
    <x-card-bilan class="column bilan-graphs-mobile interactions"
                  :graph_data="['id' => 'graph_weights_follow_up', 'durations' => ['periods' => $durations, 'scales' => $scales, 'default' => collect($default_graphs_display)->get('graph_weights_follow_up')]]"/>
    <h3 class="is-size-3">{{__('messages.bilan_badges_header')}}</h3>
    <x-card-bilan class="column is-full bilan-cards-mobile bilan-card-badges-container"
                  :card_data="['id' => 'card_badges', 'type' => 'badges', 'data' => $bilan_data['badges']]">
    </x-card-bilan>
    <script>
        window.graphsData = @json($bilan_data);
        window.legendData = @json($legend_data);
        window.isWomen = @json(auth()->user()->isWomen());
    </script>
</div>
