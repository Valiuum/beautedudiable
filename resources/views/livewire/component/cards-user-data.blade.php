<div id="home-cards" wire:ignore>
    @desktop
    @php($desktop = true)
    <div id="home-desktop-cards" class="card is-shadowless columns is-vcentered mx-0 p-4">
        <div id="daily-points-home-desktop-card" class="column is-flex is-three-quarters">
            <div class="card">
                <div class="card-content is-flex">
                    <div
                            x-data="{items: homeCard(), ringsData: $wire.entangle('ringsData').live}"
                            x-init="items.init(ringsData)"
                            id="rings-home-card"
                            class="columns is-flex justify-content-center is-align-items-center m-0">
                        <template x-for="(ring, ringIndex) in items.rings()" :key="ringIndex"></template>
                    </div>
                    <div x-on:ring-and-bonus-updated.window="homeCard().updateRingAndBonus('update', $event.detail.data)"></div>
                </div>
            </div>
        </div>
        <div id="activities-bonus-home-desktop-cards" class="column">
            <div class="card">
                <div class="card-content">
                    <figure class="image is-96x96 mx-auto">
                        <img src="{{ asset('images/activities/activities.svg') }}" alt="Healthy lifestyle">
                    </figure>
                    <h5 class="is-5 has-text-weight-semibold has-text-centered mt-4">{{trans_choice('messages.activitiesHomeCard', $weeklyActivities, ['value' => $weeklyActivities])}}</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <figure @class(['image', 'is-96x96', 'mx-auto','img-home-card-bonus', $weeklyBonusPoints > 0 ? 'glow' : ''])>
                        <img src="{{ asset('images/common/neon-star.svg') }}" alt="Bonus Points">
                    </figure>
                    <h5 class="is-5 has-text-weight-semibold has-text-centered mt-4"><span
                                id="user-bonus-points">{{$weeklyBonusPoints}}</span> {{trans_choice('messages.bonusPointsHomeCard', $weeklyBonusPoints, ['value' => $weeklyBonusPoints])}}
                    </h5>
                </div>
            </div>
        </div>
    </div>
    @elsedesktop
    @php($desktop = false)
    <div id="mobile-desktop-cards" class="columns is-variable is-gap-2 is-mobile mt-4">
        <div class="column is-full-touch">
            <div class="box">
                <div class="card">
                    <div class="card-content">
                        <div
                                id="rings-home-card"
                                class="columns is-flex justify-content-center is-align-items-center m-0 mobile"
                                x-data="{items: homeCard(), ringsData: $wire.entangle('ringsData').live}"
                                x-init="items.init(ringsData)">
                            <template x-for="(ring, ringIndex) in items.rings()" :key="ringIndex"></template>
                        </div>
                        <div x-on:ring-and-bonus-updated.window="homeCard().updateRingAndBonus('update', $event.detail.data)"></div>
                    </div>
                </div>
                <div class="columns column mx-0 px-0 is-variable is-gap-0 is-mobile is-justify-content-space-between">
                    <div class="card home-card-mobile my-0">
                        <div class="card-content">
                            <figure class="image is-96x96 mx-auto">
                                <img src="{{ asset('images/activities/activities.svg') }}" alt="Healthy lifestyle">
                            </figure>
                            <h5 class="is-5 has-text-weight-semibold has-text-centered mt-4">{{trans_choice('messages.activitiesHomeCard', $weeklyActivities, ['value' => $weeklyActivities])}}
                            </h5>
                        </div>
                    </div>
                    <div class="card home-card-mobile my-0">
                        <div class="card-content">
                            <figure @class(['image', 'is-96x96', 'mx-auto','img-home-card-bonus', $weeklyBonusPoints > 0 ? 'glow' : ''])>
                                <img src="{{ asset('images/common/neon-star.svg') }}" alt="Bonus Points">
                            </figure>
                            <h5 class="is-5 has-text-weight-semibold has-text-centered mt-4"><span
                                        id="user-bonus-points">{{$weeklyBonusPoints}}</span> {{trans_choice('messages.bonusPointsHomeCard', $weeklyBonusPoints, ['value' => $weeklyBonusPoints])}}
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @enddesktop
    <script>
        window.monday = @json(__('messages.Monday'));
        window.tuesday = @json(__('messages.Tuesday'));
        window.wednesday = @json(__('messages.Wednesday'));
        window.thursday = @json(__('messages.Thursday'));
        window.friday = @json(__('messages.Friday'));
        window.saturday = @json(__('messages.Saturday'));
        window.sunday = @json(__('messages.Sunday'));
        window.desktop = @json($desktop);
    </script>
</div>
