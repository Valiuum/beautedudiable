<div id="reminder-meals">
    @if(!is_string($weekPoints))
        <div id="weekPoints" class="box">
            <h3 @class(['has-text-weight-semibold', 'has-text-centered', 'is-flex', 'my-5', 'align-items-center', 'is-justify-content-center', $weekPoints > -1 ? 'has-text-success' : 'has-text-warning'])>
                {{trans_choice('messages.weekly_points', $weekPoints, ['value' => abs($weekPoints)])}}
            </h3>
        </div>
    @endif
    @empty($missedMeals)
    @else
        <h3 class="has-text-weight-semibold has-text-centered mt-5 is-size-3">{{trans('messages.missing_meals')}}</h3>
        <section class="section @desktop px-5 @elsedesktop px-2 pb-0 @enddesktop">
            <x-tabs class="has-text-info" :header="$header" :content="$missedMeals" :type="'meals'"/>
        </section>
    @endisset
</div>
