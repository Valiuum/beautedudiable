<div wire:init="initialLoad" x-data="{cards: cards(), date: {{ $date }}}" x-cloak class="list has-hoverable-list-items"
     x-on:update-daily-meals-cards-class.window="cards.updateClassName($event.detail.data)">
    @foreach(collect($cards) as $meal => $content)
        @switch($meal)
            @case ('breakfast')
                @php $name = __('messages.breakfast'); @endphp
                @php $image = 'card_breakfast.svg' @endphp
                @break
            @case ('lunch')
                @php $name = __('messages.lunch'); @endphp
                @php $image = 'card_lunch.svg' @endphp
                @break
            @case ('dinner')
                @php $name = __('messages.dinner'); @endphp
                @php $image = 'card_dinner.svg' @endphp
                @break
            @case ('snack')
                @php $name = __('messages.snack'); @endphp
                @php $image = 'card_snack.svg' @endphp
                @break
            @case ('bar')
                @php $name = __('messages.bar'); @endphp
                @php $image = 'card_bar.svg' @endphp
                @break
        @endswitch
        <div $wire:key="{{$meal}}" class="list-item box list-meal">
            <div class="list-item-image">
                @mobile
                <div class="swipe"></div>
                @endmobile
                <figure class="image is-64x64">
                    <img class="is-rounded" src="{{asset('images/common/' . $image)}}">
                </figure>
            </div>
            <div class="list-item-content">
                <div class="list-item-title">
                    {{$name}}
                    @mobile
                    <div class="list-item-mobile is-invisible">
                        @if(collect($content)->isNotEmpty())
                            <span>{{collect($content->dishes)->count() > 0 ? '🍴' : ''}} {{collect($content->ingredients)->count() > 0 ? '🍽' :'' }}</span>
                        @endif
                    </div>
                    @endmobile
                </div>
                <div class="list-item-description is-invisible">
                    @if(collect($content)->isNotEmpty())
                        @desktop
                        <p>{{$content->description}}
                            @if(collect($content)->get('changed') === 1)
                                <span class="has-text-black-ter meal-process is-inline-flex is-align-items-center mt-2">
                                        <span class="material-symbols-outlined">sync</span>
                                        {{__('messages.mealProcessing')}}
                                    </span>
                            @endif
                        </p>
                        @if(collect($content)->get('changed') === 0)
                            <div class="fixed-grid has-2-cols">
                                <ul class="mt-2 grid">
                                    @foreach(collect($content->ingredients)->unique('name') as $ingredient)
                                        <li class="cell">{{collect($ingredient)->get('name')}}</li>
                                    @endforeach
                                    @foreach(collect($content->dishes)->unique('name') as $ingredient)
                                        <li class="cell is-flex">
                                            <span class="material-symbols-outlined mr-2">menu_book</span>
                                            {{collect($ingredient)->get('name')}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @enddesktop
                    @endif
                </div>
            </div>
            <div class="list-item-controls">
                <div class="buttons is-right">
                    @if(collect($content)->isNotEmpty())
                        <a class="button is-link is-outlined" href="{{route('meal.edit', ['meal' => $content->id])}}"
                           wire:navigate>
                            <span class="material-symbols-outlined">edit</span>
                            <span class="is-hidden-mobile ml-2">{{__('messages.edit')}}</span>
                        </a>
                        <a class="button is-link is-outlined" wire:click.prevent="userMealShare({{$content}})">
                            <span class="material-symbols-outlined">share</span>
                            <span class="is-hidden-mobile ml-2">{{__('messages.share')}}</span>
                        </a>
                        <a class="button is-link is-outlined"
                           href="{{route('meal.duplicate', ['meal' => $content->id])}}">
                            <span class="material-symbols-outlined">content_copy</span>
                            <span class="is-hidden-mobile ml-2">{{__('messages.duplicate')}}</span>
                        </a>
                        <a class="button is-danger is-outlined" wire:click.prevent="userMealDelete({{$content->id}})">
                            <span class="material-symbols-outlined">delete</span>
                        </a>
                    @else
                        <a class="button is-link is-outlined"
                           href="{{route('meal.create', ['date' => $date, 'type' => $meal])}}" wire:navigate>
                            <span class="material-symbols-outlined">add</span>
                            <span class="is-hidden-mobile ml-2">{{__('messages.add')}}</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    <div class="swipe is-hidden"></div>
</div>
