<div>
    @if($isHomePage)
        <div class="columns is-mobile">
            <div
                class="control column is-one-fifth-mobile is-2-tablet is-flex is-justify-content-center">
                <span wire:click="updateCurrentDate('decrease')"
                      class="button has-background-white-ter is-borderless datepicker-selector">
                    <i class="material-symbols-outlined">arrow_back_ios_new</i>
                </span>
            </div>
            <div class="control has-icons-left column is-three-quarters-desktop is-flex is-align-items-center">
                <x-input-text
                    wire:model="selectedDate"
                    id="datepicker"
                    name="datepicker"
                    class="input has-text-centered"
                    type="input"
                    x-ref="datepicker"
                />
                <x-input-icon class="icon datepicker is-small is-left is-hidden-touch" icon="schedule"/>
            </div>
            <div
                class="control column is-one-fifth-mobile is-2-tablet is-flex is-justify-content-center">
                <span wire:click="updateCurrentDate('increase')"
                      class="button has-background-white-ter is-borderless datepicker-selector">
                    <i class="material-symbols-outlined">arrow_forward_ios</i>
                </span>
            </div>
        </div>
    @else
        <div class="control has-icons-left">
            <x-input-text
                wire:model="selectedDate"
                id="datepicker"
                name="datepicker"
                class="input has-text-centered"
                type="input"
                x-ref="datepicker"
            />
            <x-input-icon class="icon is-small is-left is-hidden-touch" icon="schedule"/>
        </div>
    @endif
</div>

