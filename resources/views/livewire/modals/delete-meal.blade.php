<div id="modal-delete-meal" class="modal {{ $isOpen ? 'is-active' : '' }}">
    <div class="modal-background" wire:click="$set('isOpen', false)"></div>
    <div class="modal-content modal-content-share">
        <div class="box">
            <div class="column is-flex is-justify-content-center is-align-items-center">
                <p> {{ __('messages.deleteMeal') }}</p>
            </div>
            <div class="column is-flex is-justify-content-center is-align-items-center">
            <div class="buttons ml-2">
                <button class="button is-link is-outlined"
                        class="{{$darkMode ? 'is-dark' : ''}}"
                        wire:click="$set('isOpen', false)"
                        wire:loading.class="is-loading">{{ __('messages.cancel') }}</button>
                <form method="POST" action="{{ route('meal.destroy', $meal_id) }}">
                    @csrf
                    @method('delete')
                    <button type="submit" class="button is-danger is-light"
                            class="{{$darkMode ? 'is-dark' : ''}}"
                            wire:click="$set('isOpen', false)"
                            wire:loading.class="is-loading">{{ __('messages.delete') }}</button>
                </form>
            </div>
            </div>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>
