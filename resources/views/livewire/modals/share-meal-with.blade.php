<div id="modal-shareWith" class="modal {{ $isOpen ? 'is-active' : '' }}">
    @if($meal)
        @switch($meal['name'])
            @case('breakfast')
                @php $mealName = __('messages.breakfast'); @endphp
                @break
            @case('lunch')
                @php $mealName = __('messages.lunch'); @endphp
                @break
            @case('dinner')
                @php $mealName = __('messages.dinner'); @endphp
                @break
            @case('snack')
                @php $mealName = __('messages.snack'); @endphp
                @break
            @case('bar')
                @php $mealName = __('messages.bar'); @endphp
                @break
        @endswitch
    @else
        @php $mealName = ''; @endphp
    @endif
    <div class="modal-background" wire:click="$set('isOpen', false)"></div>
    <div class="modal-content modal-content-share">
        <div class="box">
            <form wire:submit="save">
                <div class="column is-flex is-justify-content-center">
                    <x-input-label
                        for="shared_meal_with"
                        :value="__('messages.sharedModalMealWith', ['value' => $mealName])"
                    />
                </div>
                <div class="column is-flex is-justify-content-center">
                    <div class="field has-addons">
                        <div class="control is-expanded">
                            <div class="select is-fullwidth">
                                <x-input-select id="shared_meal_with"
                                                name="shared_meal_with"
                                                :options="$shareWith"
                                                :livewireModel="'shared_meal_with'"
                                                :icon="'share'"/>
                            </div>
                        </div>
                        <div class="control">
                            <button type="submit" class="button is-link"
                                    class="{{$darkMode ? 'is-dark' : ''}}"
                                    wire:loading.class="is-loading">{{ __('messages.share') }}</button>
                        </div>
                    </div>
                </div>
                <div class="column is-flex is-justify-content-start">
                    <p> {{ __('messages.sharedModalMealWithConditions') }}</p>
                </div>
            </form>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>
