<div id="modal-openFoodFacts" class="modal {{ $isOpen ? 'is-active' : '' }}">
    <div class="modal-background" wire:click="$set('isOpen', false)"></div>
    <div class="modal-content">
        <div class="box">
            @if(collect($openFoodFactsData)->get('status') === "success")
                <x-openfoodfacts-results :data="$openFoodFactsData" :singleResult="true"/>
            @else
                {{-- Display error --}}
                <p class="has-text-danger is-size-4 has-text-centered">{{collect($openFoodFactsData)->get('reason')}}</p>
            @endif
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>
