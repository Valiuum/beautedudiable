<?php

use App\Livewire\Forms\LoginForm;
use Illuminate\Support\Facades\Session;

use function Livewire\Volt\form;
use function Livewire\Volt\layout;

layout('layouts.guest');

form(LoginForm::class);

$login = function () {

    $this->validate();

    $this->form->authenticate();

    Session::regenerate();

    $this->redirectIntended(default: route('home', absolute: false), navigate: true);
};

?>

<div>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')"/>

    <form wire:submit="login">
        <!-- Email Address -->
        <div class="field">
            <x-input-label for="email" :value="__('Email')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="form.email" id="email" type="email" name="email"
                              class="{{ $errors->get('form.email') ? 'input is-danger' : '' }}"
                              required autofocus autocomplete="username"/>
                <x-input-icon class="icon is-small is-left" icon="mail"/>
                <x-input-error :messages="$errors->get('form.email')"/>
            </div>
        </div>

        <!-- Password -->
        <div class="field mt-4">
            <x-input-label for="password" :value="__('Password')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="form.password" id="password"
                              class="{{ $errors->get('form.password') ? 'input is-danger' : '' }}"
                              type="password"
                              name="password"
                              required autocomplete="current-password"/>
                <x-input-icon class="icon is-small is-left" icon="lock"/>
                <x-input-error :messages="$errors->get('form.password')"/>
            </div>
        </div>

        <!-- Remember Me -->
        <div class="field mt-4">
            <div class="field">
                <input class="is-checkradio has-background-color" wire:model="form.remember" id="remember"
                       type="checkbox" name="remember" checked="checked">
                <label for="remember" class="inline-flex items-center"><span class="ml-2">{{ __('Remember me') }}</span></label>
            </div>
        </div>

        <div id="buttons-login" class="field is-grouped is-flex is-flex-wrap-wrap">
            <div class="control">
                <x-primary-button class="is-light">
                    {{ __('messages.login') }}
                </x-primary-button>
            </div>
            @if (Route::has('password.request'))
                <div class="control">
                    <a class="button is-link is-light" href="{{ route('password.request') }}" wire:navigate>
                        {{ __('messages.forgotYourPassword') }}
                    </a>
                </div>
            @endif
        </div>
    </form>
    <script>
        window.current_route = '{{ Route::currentRouteName() }}';
    </script>
</div>
