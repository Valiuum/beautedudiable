@props(['icon','title','content','unit' => ''])
<div {{$attributes->merge(['class' => 'card intakes is-flex is-justify-content-center is-align-item-center mx-auto']) }}>
    <div class="card-content pt-2">
        <div class="media">
            <div class="media-content">
                <figure class="image is-64x64 has-background-white is-flex is-justify-content-center">
                    <span class="is-flex is-align-self-center material-symbols-outlined">
                        {{ $icon }}
                    </span>
                </figure>
                <p class="title is-7 has-text-centered mt-3">{{$title}}</p>
                <p class="subtitle intake has-text-centered mt-2">{{$content}} {{$unit}}</p>
            </div>
        </div>
    </div>
</div>



