@props(['gauges' => []])
<div
    id="gauges-home-card"
    {{ $attributes->merge(['class' => 'columns is-flex justify-content-center is-align-items-center mx-0']) }}>
    <div class="column is-relative has-text-centered">
        <canvas width=100 height=100 id="canvas-gauge" class="gauges"></canvas>
        <div id="canvas-textfield"
             class="is-size-4 has-text-weight-semibold is-position-absolute textfield-gauges"></div>
    </div>
    <div class="column is-3 has-text-centered">
        <canvas width=200 height=200 class="gauges" id="canvas-gauge-today"></canvas>
        <div id="canvas-textfield-today"
             class="is-size-2 has-text-weight-semibold is-position-absolute textfield-gauges today"></div>
    </div>
    <div class="column is-relative has-text-centered">
        <canvas width=100 height=100 id="canvas-gauge-un" class="gauges"></canvas>
        <div id="canvas-textfield-un"
             class="is-size-4 has-text-weight-semibold is-position-absolute textfield-gauges"></div>
    </div>
    <div class="column is-relative has-text-centered">
        <canvas width=100 height=100 id="canvas-gauge-deux" class="gauges"></canvas>
        <div id="canvas-textfield-deux"
             class="is-size-4 has-text-weight-semibold is-position-absolute textfield-gauges"></div>
    </div>
    <div class="column is-relative has-text-centered">
        <canvas width=100 height=100 id="canvas-gauge-trois" class="gauges"></canvas>
        <div id="canvas-textfield-trois"
             class="is-size-4 has-text-weight-semibold is-position-absolute textfield-gauges"></div>
    </div>
    <div class="column is-relative has-text-centered">
        <canvas width=100 height=100 id="canvas-gauge-quatre" class="gauges"></canvas>
        <div id="canvas-textfield-quatre"
             class="is-size-4 has-text-weight-semibold is-position-absolute textfield-gauges"></div>
    </div>
    <div class="column is-relative has-text-centered">
        <canvas width=100 height=100 id="canvas-gauge-cinq" class="gauges"></canvas>
        <div id="canvas-textfield-cinq"
             class="is-size-4 has-text-weight-semibold is-position-absolute textfield-gauges"></div>
    </div>
</div>
