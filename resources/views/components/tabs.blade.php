@props(['header' => [], 'content' => [], 'type' => '', 'activeTab' => 0])

<div x-data="{ activeTab:  {{$activeTab}} }">
    <div {{$attributes->merge(['class' => 'tabs is-toggle is-fullwidth'])}}>
        <ul>
            @foreach($header as $key =>$tab)
                <li @click="activeTab = {{$key}}" :class="{ 'is-active': activeTab === {{$key}} }">
                    <a>
                        <span>{{ $tab['name'] }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    @if($type === 'dishes')
        @foreach($content as $key => $element)
            @switch($element['slug'])
                @case('breakfast') @php $dish_type =  __('messages.breakfast'); @endphp @break
                @case('lunch') @php $dish_type = __('messages.lunch'); @endphp @break
                @case('dinner') @php $dish_type = __('messages.dinner'); @endphp @break
                @case('snack') @php $dish_type = __('messages.snack'); @endphp @break
                @case('bar') @php $dish_type = __('messages.bar'); @endphp @break
            @endswitch
            <div class="tab-content dishes box mx-1 p-3"
                 :class="{ 'is-active': activeTab === {{$key}} }"
                 x-cloak
                 x-show.transition.in.opacity.duration.600="activeTab === {{$key}}">
                <div class="columns is-variable is-gap-5 p-5 is-multiline is-mobile">
                    @forelse(collect($element['data']) as $dish)
                        @if($loop->first)
                            @desktop
                            <div
                                class="column is-flex is-justify-content-center search-tab is-full is-align-items-center is-align-self-center">
                                <div class="field is-grouped">
                                    <input
                                        class="input search-tab-content column is-full-touch is-one-third-desktop mx-auto my-4"
                                        type="text" placeholder="Search">
                                    <div class="control is-align-self-center">
                                        <a wire:navigate href="{{route('dish.create', ['type' => $element['slug']])}}"
                                           class="button is-link is-light">{{ trans_choice('messages.addNewDish', 1, ['value' => $dish_type]) }}</a>
                                    </div>
                                </div>
                            </div>
                            @elsedesktop
                            <div
                                class="column is-flex is-flex-direction-column search-tab is-full is-align-items-center is-align-self-center">
                                <div class="field is-align-self-center">
                                    <input
                                        class="input search-tab-content column is-full mx-auto"
                                        type="text" placeholder="Search">
                                </div>
                                <div class="field is-align-self-center">
                                    <a wire:navigate href="{{route('dish.create', ['type' => $element['slug']])}}"
                                       class="button is-link is-light">{{ trans_choice('messages.addNewDish', 1, ['value' => $dish_type]) }}</a>
                                </div>
                            </div>
                            @enddesktop
                        @endif
                        <x-dish-card :data="$dish" class="column is-full-mobile is-5-tablet p-0 my-2 mx-auto"/>
                    @empty
                        <div class="column has-text-centered">
                            <figure class="image is-128x128 mx-auto">
                                <img alt="no results" src="{{asset('images/common/no_data.svg')}}">
                            </figure>
                            <p class="title is-4">{{__('messages.noData')}}</p>
                            <a wire:navigate href="{{route('dish.create', ['type' => $element['slug']])}}"
                               class="button is-link is-light">{{ trans_choice('messages.addNewDish', 1, ['value' => $dish_type]) }}</a>
                        </div>
                    @endforelse
                </div>
            </div>
        @endforeach
    @elseif($type === 'meals')
        @foreach($content as $elements)
            <div class="tab-content meals box mx-1 p-3"
                 :class="{ 'is-active': activeTab === {{$elements['key']}} }"
                 x-cloak
                 x-show.transition.in.opacity.duration.600="activeTab === {{$elements['key']}}">
                <div class="columns p-5 is-multiline is-mobile is-flex is-justify-content-space-around">
                    @foreach(collect($elements['meals']) as $meal => $info)
                        <div class="field is-align-self-center my-0 p-5">
                            <a wire:navigate href="{{$info['route']}}"
                               class="button is-link is-light">
                                    <span class="icon mr-1">
                                        @switch($meal)
                                            @case ('breakfast')
                                                🥐
                                                @break
                                            @case ('lunch')
                                                🥗
                                                @break
                                            @case ('dinner')
                                                🍲
                                                @break
                                            @case ('snack')
                                                🍩
                                                @break
                                            @case ('bar')
                                                🍻
                                                @break
                                        @endswitch
                                </span>
                                {{ trans('messages.' . $meal) }}
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    @endif
</div>

