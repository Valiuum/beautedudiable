@props(['dishIngredient' => false])
<div {{ $attributes->merge(['class' => 'list-item box is-success']) }}>
    <div class="is-flex-desktop is-block-mobile is-vcentered is-align-items-center column is-full">
        @desktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-1 p-0 m-2 list-item-image">
            <div class="round-image is-64x64 is-inline-flex">
                <span x-bind:style="'background-color:' + ingredient.backgroundColor,'color:' + ingredient.color"
                      class='image is-64x64'/>
                <div x-bind:style="'color:' + ingredient.color" class="centered-text"
                     x-text="dish.ingredientPicture(ingredient.name)"></div>
            </div>
        </div>
        @elsedesktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-1 p-0 mb-4 list-item-image">
            <div class="round-image is-64x64 is-inline-flex">
                <span x-bind:style="'background-color:' + ingredient.backgroundColor,'color:' + ingredient.color"
                      class='image is-64x64'/>
                <div x-bind:style="'color:' + ingredient.color" class="centered-text"
                     x-text="dish.ingredientPicture(ingredient.name)"></div>
            </div>
        </div>
        @enddesktop
        <div class="is-flex-desktop is-block-mobile is-justify-content-start is-align-items-center column is-5 p-0 m-0">
            <ul class="pl-2">
                <li class="material-symbols-outlined has-text-warning" x-show="ingredient.name === 'bonus'">star</li>
                <li class="ingredient-content" x-text="ingredient.name"></li>
                <li class="ingredient-content" x-text="ingredient.name !== 'bonus' ? ingredient.description : ''"></li>
            </ul>
        </div>
        @desktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-3 p-0 m-0">
            <p class="control mr-1">
                    <span type="button" class="button is-link is-outlined decrement-button"
                          x-bind:class="{'is-dark': dish.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          @click="dish.updateIngredient(ingredient,'decrement')">
                    <span class="material-symbols-outlined">remove</span>
                </span>
            </p>
            <p class="control">
                <input type="text" class="input is-link quantity-input has-text-centered" readonly
                       x-model="ingredient.name === 'bonus' ? Math.abs(ingredient.points) : ingredient.points"/>
            </p>
            <p class="control ml-1">
                    <span type="button"
                          x-bind:class="{'is-dark': dish.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          class="button is-link is-outlined decrement-button"
                          @click="dish.updateIngredient(ingredient,'increment')">
                    <span class="material-symbols-outlined">add</span>
                </span>
            </p>
        </div>
        @elsedesktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-12 p-0 my-4">
            <p class="control mr-1">
                    <span type="button" class="button is-link is-outlined decrement-button"
                          x-bind:class="{'is-dark': dish.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          @click="dish.updateIngredient(ingredient,'decrement')">
                    <span class="material-symbols-outlined">remove</span>
                </span>
            </p>
            <p class="control">
                <input type="text" class="input is-link quantity-input has-text-centered" readonly
                       x-model="ingredient.name === 'bonus' ? Math.abs(ingredient.points) : ingredient.points"/>
            </p>
            <p class="control ml-1">
                    <span type="button"
                          x-bind:class="{'is-dark': dish.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          class="button is-link is-outlined decrement-button"
                          @click="dish.updateIngredient(ingredient,'increment')">
                    <span class="material-symbols-outlined">add</span>
                </span>
            </p>
        </div>
        @enddesktop
        <div class="is-flex is-justify-content-end is-align-items-end column is-3 p-0 m-0">
            <span class="button is-link is-light search-open-food-facts"
                  x-bind:class="{'is-dark': dish.darkMode, 'is-hidden': ingredient.name === 'bonus' || !ingredient.open_food_facts_id}"
                  x-bind:data-open-food-facts-id="ingredient.open_food_facts_id"
                  @click="dish.toggleClass($el, 'is-loading', 'add')"
                  wire:click="searchForOpenFoodFacts(ingredient.open_food_facts_id)">
                <span class="material-symbols-outlined">search_check</span>
            </span>
            <span x-bind:class="{'is-dark': dish.darkMode}" class="button is-danger is-light ml-2"
                  @click="dish.removeIngredient(ingredient)">
                      <span class="material-symbols-outlined">delete</span>
                </span>
        </div>
    </div>
</div>


