<button {{ $attributes->merge(['type' => 'submit', 'class' => 'button is-danger is-light']) }}>
    {{ $slot }}
</button>
