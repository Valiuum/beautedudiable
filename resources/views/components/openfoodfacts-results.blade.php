@props(['singleResult' => false, 'data', 'ingredientPage' => false, 'textLimit' => 60, 'seuil' => 12])

@if(isset($data['status']) && $data['status'] === 'error')

    <div class="columns is-multiline is-mobile">
        <div class="column is-full is-flex is-justify-content-center">
            <figure class="image is-128x128">
                <img class="is-rounded" alt="error" src="{{asset('images/common/no_internet.svg')}}"/>
            </figure>
        </div>
        <div class="column is-full">
            <p class="has-text-centered mt-3">{{$data['reason']}}</p>
        </div>
    </div>

@elseif($singleResult)

    @php
        $element = collect($data)->get('product');
        $ecoScore = collect($element)->get('ecoscore_data');
        $nutriments = collect($element)->get('nutriments');
        $ecoScore = collect($element)->get('ecoscore_data');
        $novaGroup = collect($element)->get('nova_group');
        // 100gr
        $nutrimentsGrammes = collect($nutriments)->filter(function ($value, $key) {
            return str_contains($key, '_100g');
        });
        $nutrimentsQuantity = collect($element)->get('nutrition_data_per');
        $nutrimentsGrammes->put('nutrition_data_per', $nutrimentsQuantity);
        // serving
        $nutrimentsServing = collect($nutriments)->filter(function ($value, $key) {
            return str_contains($key, '_serving');
        });
        $servingQuantity = collect($element)->get('serving_quantity');
        $servingQuantityUnit = collect($element)->get('serving_quantity_unit');
        $nutrimentsServing->put('serving_quantity', $nutrimentsServing . $nutrimentsServing);
    @endphp

    <div class="columns is-multiline is-flex is-justify-content-space-around is-mobile">

        {{-- If there are no values for 100gr and serving, we skip the product --}}
        @if($nutrimentsServing->count() < $seuil && $nutrimentsGrammes->count() < $seuil || collect($element)->isEmpty('_id'))
            <p>{{__('messages.notRecommended')}}</p>
        @endif

        @desktop
        <div @class(['open-food-facts-box', 'is-flex', 'column', 'is-full', 'is-two-fifths-desktop' => $ingredientPage, 'my-3'])>
            @elsedesktop
            <div
                    class="open-food-facts-box is-flex columns is-full-touch mx-1 my-3 is-multiline is-mobile">
                @enddesktop
                <div
                        class="column is-full-touch is-one-third-desktop is-flex is-justify-content-center is-align-items-center">
                    @if(collect($element)->get('image_front_small_url'))
                        <figure class="image">
                            <img alt="product_image"
                                 src="{{ collect($element)->get('image_front_small_url') }}"
                                 onerror="this.onerror=null; this.parentNode.classList.add('is-64x64'); this.src='{{ asset('images/common/no_picture.svg') }}';"/>
                        </figure>
                    @else
                        <figure class="image is-64x64">
                            <img alt="product_image_missing"
                                 src="{{ asset('images/common/no_picture.svg') }}"/>
                        </figure>
                    @endif
                </div>
                <div
                        class="column is-full-touch is-two-third-desktop is-flex is-flex-direction-column is-justify-content-center">
                    <h4 class="has-text-centered has-text-weight-semibold is-size-5">{{Str::limit(collect($element)->get('generic_name_fr'), $textLimit) ? Str::limit(collect($element)->get('generic_name_fr'), $textLimit) : Str::limit(collect($element)->get('generic_name'), $textLimit)}}</h4>
                    @php
                        $brand = isset($element['brands']) ? explode(',', $element['brands'])[0] : '';
                    @endphp
                    @if($brand)
                        <span class="tag is-link is-normal brand mx-auto my-3">{{$brand}}</span>
                    @endif
                    <div class="is-flex is-justify-content-space-around is-align-items-center">
                        @if(collect($ecoScore)->has('grade'))
                            @switch($ecoScore['grade'])
                                @case('a')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.ecoscoreA")}}">
                                        <img class="image openFoodFactsScores" alt="ecoscore"
                                             src="{{asset('images/ingredients/eco-score-a.svg')}}"/>
                                    </figure>
                                    @break
                                @case('b')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.ecoscoreB")}}">
                                        <img class="image openFoodFactsScores" alt="ecoscore"
                                             src="{{asset('images/ingredients/eco-score-b.svg')}}"/>
                                    </figure>
                                    @break
                                @case('c')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.ecoscoreC")}}">
                                        <img class="image openFoodFactsScores" alt="ecoscore"
                                             src="{{asset('images/ingredients/eco-score-c.svg')}}"/>
                                    </figure>
                                    @break
                                @case('d')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.ecoscoreD")}}">
                                        <img class="image openFoodFactsScores" alt="ecoscore"
                                             src="{{asset('images/ingredients/eco-score-d.svg')}}"/>
                                    </figure>
                                    @break
                                @case('e')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.ecoscoreE")}}">
                                        <img class="image openFoodFactsScores" alt="ecoscore"
                                             src="{{asset('images/ingredients/eco-score-e.svg')}}"/>
                                    </figure>
                                    @break
                            @endswitch
                        @endif
                        @if(collect($element)->has('nutrition_grade_fr'))
                            @switch($element['nutrition_grade_fr'])
                                @case('a')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.nutriscoreA")}}">
                                        <img class="image openFoodFactsScores" alt="nutriscore"
                                             src="{{asset('images/ingredients/nutriscore-a.svg')}}"/>
                                    </figure>
                                    @break
                                @case('b')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.nutriscoreB")}}">
                                        <img class="image openFoodFactsScores" alt="nutriscore"
                                             src="{{asset('images/ingredients/nutriscore-b.svg')}}"/>
                                    </figure>
                                    @break
                                @case('c')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.nutriscoreC")}}">
                                        <img class="image openFoodFactsScores" alt="nutriscore"
                                             src="{{asset('images/ingredients/nutriscore-c.svg')}}"/>
                                    </figure>
                                    @break
                                @case('d')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.nutriscoreD")}}">
                                        <img class="image openFoodFactsScores" alt="nutriscore"
                                             src="{{asset('images/ingredients/nutriscore-d.svg')}}"/>
                                    </figure>
                                    @break
                                @case('e')
                                    <figure class="image score has-tooltip-bottom"
                                            data-tooltip="{{__("messages.nutriscoreE")}}">
                                        <img class="image openFoodFactsScores" alt="nutriscore"
                                             src="{{asset('images/ingredients/nutriscore-e.svg')}}"/>
                                    </figure>
                                    @break
                            @endswitch
                        @endif
                        @if($novaGroup)
                            @switch(intval($novaGroup))
                                @case('1')
                                    <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                            data-tooltip="{{__("messages.novascoreOne")}}">
                                        <img class="image openFoodFactsScores" alt="nova"
                                             src="{{asset('images/ingredients/nova-score-1.svg')}}"/>
                                    </figure>
                                    @break
                                @case('2')
                                    <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                            data-tooltip="{{__("messages.novascoreTwo")}}">
                                        <img class="image openFoodFactsScores" alt="nova"
                                             src="{{asset('images/ingredients/nova-score-2.svg')}}"/>
                                    </figure>
                                    @break
                                @case('3')
                                    <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                            data-tooltip="{{__("messages.novascoreThree")}}">
                                        <img class="image openFoodFactsScores" alt="nova"
                                             src="{{asset('images/ingredients/nova-score-3.svg')}}"/>
                                    </figure>
                                    @break
                                @case('4')
                                    <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                            data-tooltip="{{__("messages.novascoreFour")}}">
                                        <img class="image openFoodFactsScores" alt="nova"
                                             src="{{asset('images/ingredients/nova-score-4.svg')}}"/>
                                    </figure>
                                    @break
                            @endswitch
                        @endif
                    </div>
                    @if(!$ingredientPage)
                        <div class="is-flex is-flex-direction-column is-align-items-left mt-3">
                            <p class="my-2">
                                @php
                                    $ingredients_counter = collect($element)->has('ingredients_text_with_allergens_fr') && str_contains(collect($element)->get('ingredients_text_with_allergens_fr'), ',') ? 2 : 1;
                                @endphp
                                <span class="has-text-weight-semibold">
                                {{ trans_choice('messages.ingredients_text_with_allergens_fr', $ingredients_counter, ['ingredients_text_with_allergens_fr' => $element['ingredients_text_with_allergens_fr'] ?? '']) }}
                            </span>
                                {!! $element['ingredients_text_with_allergens_fr'] ?? '' !!}
                            </p>
                            <p class="my-2">
                                <span class="has-text-weight-semibold">{{ trans_choice('messages.additives', isset($element['additives_tags']) ? count($element['additives_tags']) : 0, []) }}</span>
                                @forelse(collect($element)->get('additives_tags', []) as $additif)
                                    @php
                                        $parts = explode(':', $additif);
                                        if(isset($parts[1])) {
                                            $additif_url = "https://fr.wikipedia.org/wiki/" . urlencode($parts[1]);
                                        }
                                    @endphp
                                    <a href="{{ $additif_url }}" target="_blank"
                                       class="tag is-link is-light is-clickable">{{ $parts[1] }}
                                    </a>
                                @empty
                                    <span class="tag is-light additives">{{__('messages.additivesFree')}}</span>
                                @endforelse
                            </p>
                        </div>
                    @endif
                    <div class="is-flex is-flex-direction-column is-align-items-center my-3">
                        @if($nutrimentsServing->count() > 10)
                            <h6 class="my-2 has-text-weight-semibold is-size-6">{{__('messages.servingNutriments')}}</h6>
                            <p class="my-2 has-text-centered">
                                @if(collect($nutriments)->has('energy-kcal_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.energy-kcal')}} : {{ collect($nutriments)->get('energy-kcal_serving') }} {{__('messages.kcal_unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('carbohydrates_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.carbohydrates')}} : {{ collect($nutriments)->get('carbohydrates_serving') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('fat_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.fat')}} : {{ collect($nutriments)->get('fat_serving') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('proteins_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.proteins')}} : {{ collect($nutriments)->get('proteins_serving') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('sugars_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.sugars')}} : {{ collect($nutriments)->get('sugars_serving') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('fiber_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.fiber')}} : {{ collect($nutriments)->get('fiber_serving') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('salt_serving'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.salt')}} : {{ collect($nutriments)->has('salt_serving') }} {{__('messages.unit')}}</span>
                                @endif
                            </p>
                        @elseif ($nutrimentsGrammes->count() > 10)
                            <h6 class="my-2 has-text-weight-semibold is-size-6">{{__('messages.grammesNutriments')}}</h6>
                            <p class="my-2 has-text-centered">
                                @if(collect($nutriments)->has('energy-kcal_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.energy-kcal')}} : {{ collect($nutriments)->get('energy-kcal_100g') }} {{__('messages.kcal_unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('carbohydrates_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.carbohydrates')}} : {{ collect($nutriments)->get('carbohydrates_100g') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('fat_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.fat')}} : {{ collect($nutriments)->get('fat_100g') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('proteins_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.proteins')}} : {{ collect($nutriments)->get('proteins_100g') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('sugars_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.sugars')}} : {{ collect($nutriments)->get('sugars_100g') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('fiber_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.fiber')}} : {{ collect($nutriments)->get('fiber_100g') }} {{__('messages.unit')}}</span>
                                @endif
                                @if(collect($nutriments)->has('salt_100g'))
                                    <span
                                            class="tag is-info is-light my-1">{{__('messages.salt')}} : {{ collect($nutriments)->has('salt_100g') }} {{__('messages.unit')}}</span>
                                @endif
                            </p>
                        @else
                            <p>{{__('messages.notRecommended')}}</p>
                        @endif
                    </div>
                    @if($ingredientPage)
                        <x-primary-button
                                class="is-light openFoodFactsButton mx-auto"
                                wire:click="addToForm({{collect($element)->get('_id')}})"
                                wire:loading.class="is-loading"
                                wire:key="{{collect($element)->get('_id')}}"
                                @click="isOpen = false"
                        >
                            {{ __('messages.useOpenFoodFactProduct') }}
                        </x-primary-button>
                    @endif
                </div>
            </div>
        </div>

        @else

            @php
                $elements = collect($data)->get('products');
            @endphp

            <div class="columns is-multiline is-flex is-justify-content-space-around is-mobile">

                @foreach(collect($elements) as $key => $element)

                    @if(collect($element)->isEmpty('_id'))
                        @continue
                    @endif

                    @php
                        $ecoScore = collect($element)->get('ecoscore_data');
                        $novaGroup = collect($element)->get('nova_group');
                        $nutriments = collect($element)->get('nutriments');
                        // 100gr
                        $nutrimentsGrammes = collect($nutriments)->filter(function ($value, $key) {
                            return str_contains($key, '_100g');
                        });
                        $nutrimentsQuantity = collect($element)->get('nutrition_data_per');
                        $nutrimentsGrammes->put('nutrition_data_per', $nutrimentsQuantity);
                        // serving
                        $nutrimentsServing = collect($nutriments)->filter(function ($value, $key) {
                            return str_contains($key, '_serving');
                        });
                        $servingQuantity = collect($element)->get('serving_quantity');
                        $servingQuantityUnit = collect($element)->get('serving_quantity_unit');
                        $nutrimentsServing->put('serving_quantity', $nutrimentsServing . $nutrimentsServing);
                    @endphp

                    {{-- If there are no values for 100gr and serving, we skip the product --}}
                    @if($nutrimentsServing->count() < $seuil && $nutrimentsGrammes->count() < $seuil)
                        @continue
                    @endif

                    @desktop
                    <div
                            class="box open-food-facts-box is-flex column is-full-touch is-two-fifths-desktop my-3">
                        @elsedesktop
                        <div
                                class="box open-food-facts-box is-flex columns is-full-touch mx-1 my-3 is-multiline is-mobile">
                            @enddesktop
                            <div
                                    class="column is-full-touch is-one-third-desktop is-flex is-justify-content-center is-align-items-center">
                                @if(collect($element)->get('image_front_small_url'))
                                    <figure class="image">
                                        <img alt="product_image"
                                             src="{{ collect($element)->get('image_front_small_url') }}"
                                             onerror="this.onerror=null; this.parentNode.classList.add('is-64x64'); this.src='{{ asset('images/common/no_picture.svg') }}';"/>
                                    </figure>
                                @else
                                    <figure class="image is-64x64">
                                        <img alt="product_image_missing"
                                             src="{{ asset('images/common/no_picture.svg') }}"/>
                                    </figure>
                                @endif
                            </div>
                            <div
                                    class="column is-full-touch is-two-third-desktop is-flex is-flex-direction-column is-justify-content-center">
                                <h4 class="has-text-centered has-text-weight-semibold is-size-5">{{Str::limit(collect($element)->get('generic_name_fr'), $textLimit) ? Str::limit(collect($element)->get('generic_name_fr'), $textLimit) : Str::limit(collect($element)->get('generic_name'), $textLimit)}}</h4>
                                @php
                                    $brand = isset($element['brands']) ? explode(',', $element['brands'])[0] : '';
                                @endphp
                                @if($brand)
                                    <span class="tag is-link is-normal brand mx-auto my-3">{{$brand}}</span>
                                @endif
                                <div class="is-flex is-justify-content-space-around is-align-items-center">
                                    @if(collect($ecoScore)->has('grade'))
                                        @switch($ecoScore['grade'])
                                            @case('a')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.ecoscoreA")}}">
                                                    <img class="image openFoodFactsScores" alt="ecoscore"
                                                         src="{{asset('images/ingredients/eco-score-a.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('b')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.ecoscoreB")}}">
                                                    <img class="image openFoodFactsScores" alt="ecoscore"
                                                         src="{{asset('images/ingredients/eco-score-b.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('c')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.ecoscoreC")}}">
                                                    <img class="image openFoodFactsScores" alt="ecoscore"
                                                         src="{{asset('images/ingredients/eco-score-c.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('d')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.ecoscoreD")}}">
                                                    <img class="image openFoodFactsScores" alt="ecoscore"
                                                         src="{{asset('images/ingredients/eco-score-d.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('e')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.ecoscoreE")}}">
                                                    <img class="image openFoodFactsScores" alt="ecoscore"
                                                         src="{{asset('images/ingredients/eco-score-e.svg')}}"/>
                                                </figure>
                                                @break
                                        @endswitch
                                    @endif
                                    @if(collect($element)->has('nutrition_grade_fr'))
                                        @switch($element['nutrition_grade_fr'])
                                            @case('a')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.nutriscoreA")}}">
                                                    <img class="image openFoodFactsScores" alt="nutriscore"
                                                         src="{{asset('images/ingredients/nutriscore-a.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('b')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.nutriscoreB")}}">
                                                    <img class="image openFoodFactsScores" alt="nutriscore"
                                                         src="{{asset('images/ingredients/nutriscore-b.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('c')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.nutriscoreC")}}">
                                                    <img class="image openFoodFactsScores" alt="nutriscore"
                                                         src="{{asset('images/ingredients/nutriscore-c.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('d')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.nutriscoreD")}}">
                                                    <img class="image openFoodFactsScores" alt="nutriscore"
                                                         src="{{asset('images/ingredients/nutriscore-d.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('e')
                                                <figure class="image score has-tooltip-bottom"
                                                        data-tooltip="{{__("messages.nutriscoreE")}}">
                                                    <img class="image openFoodFactsScores" alt="nutriscore"
                                                         src="{{asset('images/ingredients/nutriscore-e.svg')}}"/>
                                                </figure>
                                                @break
                                        @endswitch
                                    @endif
                                    @if($novaGroup)
                                        @switch(intval($novaGroup))
                                            @case('1')
                                                <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                                        data-tooltip="{{__("messages.novascoreOne")}}">
                                                    <img class="image openFoodFactsScores" alt="nova"
                                                         src="{{asset('images/ingredients/nova-score-1.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('2')
                                                <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                                        data-tooltip="{{__("messages.novascoreTwo")}}">
                                                    <img class="image openFoodFactsScores" alt="nova"
                                                         src="{{asset('images/ingredients/nova-score-2.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('3')
                                                <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                                        data-tooltip="{{__("messages.novascoreThree")}}">
                                                    <img class="image openFoodFactsScores" alt="nova"
                                                         src="{{asset('images/ingredients/nova-score-3.svg')}}"/>
                                                </figure>
                                                @break
                                            @case('4')
                                                <figure class="image score has-tooltip-bottom has-tooltip-ended"
                                                        data-tooltip="{{__("messages.novascoreFour")}}">
                                                    <img class="image openFoodFactsScores" alt="nova"
                                                         src="{{asset('images/ingredients/nova-score-4.svg')}}"/>
                                                </figure>
                                                @break
                                        @endswitch
                                    @endif
                                </div>
                                <div class="is-flex is-flex-direction-column is-align-items-center my-3">
                                    @if($nutrimentsServing->count() > 10)
                                        <h6 class="my-2 has-text-weight-semibold is-size-6">{{__('messages.servingNutriments')}}</h6>
                                        <p class="my-2 has-text-centered">
                                            @if(collect($nutriments)->has('energy-kcal_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.energy-kcal')}} : {{ collect($nutriments)->get('energy-kcal_serving') }} {{__('messages.kcal_unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('carbohydrates_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.carbohydrates')}} : {{ collect($nutriments)->get('carbohydrates_serving') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('fat_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.fat')}} : {{ collect($nutriments)->get('fat_serving') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('proteins_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.proteins')}} : {{ collect($nutriments)->get('proteins_serving') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('sugars_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.sugars')}} : {{ collect($nutriments)->get('sugars_serving') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('fiber_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.fiber')}} : {{ collect($nutriments)->get('fiber_serving') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('salt_serving'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.salt')}} : {{ collect($nutriments)->has('salt_serving') }} {{__('messages.unit')}}</span>
                                            @endif
                                        </p>
                                    @elseif ($nutrimentsGrammes->count() > 10)
                                        <h6 class="my-2 has-text-weight-semibold is-size-6">{{__('messages.grammesNutriments')}}</h6>
                                        <p class="my-2 has-text-centered">
                                            @if(collect($nutriments)->has('energy-kcal_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.energy-kcal')}} : {{ collect($nutriments)->get('energy-kcal_100g') }} {{__('messages.kcal_unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('carbohydrates_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.carbohydrates')}} : {{ collect($nutriments)->get('carbohydrates_100g') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('fat_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.fat')}} : {{ collect($nutriments)->get('fat_100g') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('proteins_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.proteins')}} : {{ collect($nutriments)->get('proteins_100g') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('sugars_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.sugars')}} : {{ collect($nutriments)->get('sugars_100g') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('fiber_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.fiber')}} : {{ collect($nutriments)->get('fiber_100g') }} {{__('messages.unit')}}</span>
                                            @endif
                                            @if(collect($nutriments)->has('salt_100g'))
                                                <span
                                                        class="tag is-info is-light my-1">{{__('messages.salt')}} : {{ collect($nutriments)->has('salt_100g') }} {{__('messages.unit')}}</span>
                                            @endif
                                        </p>
                                    @else
                                        <p>{{__('messages.notRecommended')}}</p>
                                    @endif
                                </div>
                                <x-primary-button
                                        class="is-light openFoodFactsButton mx-auto"
                                        wire:click="addToForm({{collect($element)->get('_id')}})"
                                        wire:loading.class="is-loading"
                                        wire:key="{{$key}}"
                                        @click="isOpen = false"
                                >
                                    {{ __('messages.useOpenFoodFactProduct') }}
                                </x-primary-button>
                            </div>
                        </div>
                        @endforeach
                    </div>

@endif
