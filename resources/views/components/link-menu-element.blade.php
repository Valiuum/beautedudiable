@props([
    'route' => null,
    'link_name' => null,
    'icon' => null,
    'is_openable' => null,
    'is_active' => false,
    'is_desktop' => true,
    ])
@if($is_openable)
    <span class="icon-text">
    <a class="is-flex" @click="open = ! open">
        <span class="material-symbols-outlined">
        {{ $icon }}
        </span>
        <span class='ml-2 is-hidden-touch is-hidden-desktop-only is-hidden-widescreen-only'>
        {{ $link_name }}
        </span>
    </a>
</span>
@else
    <span class="icon-text">
        <a class="is-flex {{ Route::is($route) || $is_active ? 'is-active' : '' }}" href="{{ route($route) }}" wire:navigate>
            <span class="material-symbols-outlined">{{ $icon }}</span>
            @if(!$is_desktop)
                <span class='ml-2'>{{ $link_name }}</span>
            @else
                <span class='ml-2 is-hidden-touch is-hidden-desktop-only is-hidden-widescreen-only'>
                    {{ $link_name }}
                </span>
            @endif
        </a>
    </span>
@endif



