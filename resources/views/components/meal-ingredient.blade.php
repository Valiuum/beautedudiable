@props(['dishIngredient' => false])
<div {{ $attributes->merge(['class' => 'list-item box is-success']) }}>
    <div class="is-flex-desktop is-block-mobile is-vcentered is-align-items-center column is-full">
        <div class="is-flex is-justify-content-center is-align-items-center column is-1 p-0 m-2 list-item-image">
            <div class="round-image is-64x64 is-inline-flex">
                <span x-bind:style="'background-color:' + ingredient.backgroundColor,'color:' + ingredient.color"
                      class='image is-64x64'/>
                <div x-bind:style="'color:' + ingredient.color" class="centered-text"
                     x-text="items.ingredientPicture(ingredient.name)"></div>
            </div>
        </div>
        <div class="is-flex-desktop is-block-mobile is-justify-content-start is-align-items-center column is-5 p-0 m-0">
            <ul class="pl-2">
                <li class="material-symbols-outlined has-text-warning" x-show="ingredient.name === 'bonus'">star</li>
                <li class="ingredient-content" x-text="ingredient.name"></li>
                <li class="ingredient-content" x-text="ingredient.name !== 'bonus' ? ingredient.description : ''"></li>
            </ul>
        </div>
        @desktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-3 p-0 m-0">
            <p class="control mr-1">
                @if($dishIngredient)
                    <span type="button" class="button is-link is-outlined decrement-button" x-bind:class="{'is-dark': items.darkMode}"
                          @click="items.updateDishIngredient(ingredient,'decrement')">
                    <span class="material-symbols-outlined">remove</span>
                </span>
                @else
                    <span type="button" class="button is-link is-outlined decrement-button" x-bind:class="{'is-dark': items.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          @click="items.updateIngredient(ingredient,'decrement')">
                    <span class="material-symbols-outlined">remove</span>
                </span>
                @endif
            </p>
            <p class="control">
                @if($dishIngredient)
                    <input type="text" class="input is-link quantity-input has-text-centered" readonly
                           x-model="ingredient.dish_meal_points ? ingredient.dish_meal_points : ingredient.dish_ingredient_points"/>
                @else
                    <input type="text" class="input is-link quantity-input has-text-centered" readonly
                           x-model="ingredient.name === 'bonus' ? Math.abs(ingredient.points) : ingredient.points"/>
                @endif
            </p>
            <p class="control ml-1">
                @if($dishIngredient)
                    <span type="button" x-bind:class="{'is-dark': items.darkMode}"
                          class="button is-link is-outlined decrement-button"
                          @click="items.updateDishIngredient(ingredient,'increment')">
                    <span class="material-symbols-outlined">add</span>
                </span>
                @else
                    <span type="button" x-bind:class="{'is-dark': items.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          class="button is-link is-outlined decrement-button"
                          @click="items.updateIngredient(ingredient,'increment')">
                    <span class="material-symbols-outlined">add</span>
                </span>
                @endif
            </p>
        </div>
        @elsedesktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-12 p-0 my-4">
            <p class="control mr-1">
                @if($dishIngredient)
                    <span type="button" class="button is-link is-outlined decrement-button" x-bind:class="{'is-dark': items.darkMode}"
                          @click="items.updateDishIngredient(ingredient,'decrement')">
                    <span class="material-symbols-outlined">remove</span>
                </span>
                @else
                    <span type="button" class="button is-link is-outlined decrement-button" x-bind:class="{'is-dark': items.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          @click="items.updateIngredient(ingredient,'decrement')">
                    <span class="material-symbols-outlined">remove</span>
                </span>
                @endif
            </p>
            <p class="control">
                @if($dishIngredient)
                    <input type="text" class="input is-link quantity-input has-text-centered" readonly
                           x-model="ingredient.dish_meal_points ? ingredient.dish_meal_points : ingredient.dish_ingredient_points"/>
                @else
                    <input type="text" class="input is-link quantity-input has-text-centered" readonly
                           x-model="ingredient.name === 'bonus' ? Math.abs(ingredient.points) : ingredient.points"/>
                @endif
            </p>
            <p class="control ml-1">
                @if($dishIngredient)
                    <span type="button" x-bind:class="{'is-dark': items.darkMode}"
                          class="button is-link is-outlined decrement-button"
                          @click="items.updateDishIngredient(ingredient,'increment')">
                    <span class="material-symbols-outlined">add</span>
                </span>
                @else
                    <span type="button" x-bind:class="{'is-dark': items.darkMode, 'is-hidden': ingredient.name === 'bonus'}"
                          class="button is-link is-outlined decrement-button"
                          @click="items.updateIngredient(ingredient,'increment')">
                    <span class="material-symbols-outlined">add</span>
                </span>
                @endif
            </p>
        </div>
        @enddesktop
        <div class="is-flex is-justify-content-end is-align-items-end column is-3 p-0 m-0">
            <span class="button is-link is-light search-open-food-facts"
                  x-bind:class="{'is-dark': items.darkMode, 'is-hidden': ingredient.name === 'bonus' || ingredient.open_food_facts_id === null}"
                  x-bind:data-open-food-facts-id="ingredient.open_food_facts_id"
                  @click="items.toggleClass($el, 'is-loading', 'add')"
                  wire:click="searchForOpenFoodFacts(ingredient.open_food_facts_id)">
                <span class="material-symbols-outlined">search_check</span>
            </span>
            @if(!$dishIngredient)
                <span x-bind:class="{'is-dark': items.darkMode, 'is-hidden': ingredient.name === 'bonus'}" class="button is-danger is-light ml-2"
                      @click="items.removeIngredient(ingredient)">
                      <span class="material-symbols-outlined">delete</span>
                </span>
            @endif
        </div>
    </div>
</div>


