@props(['graph_data' => [], 'card_data' => [], 'type' => '', 'subtype' => ''])
<div {{ $attributes->merge(['class' => 'box']) }}>
    @if($graph_data)
        <div wire:key="graph-{{$graph_data['id']}}"
             @mobile style="position: relative; height:60vh; width:auto;" @endmobile
             class="chart-container column is-full is-position-relative mx-auto"
             @if($graph_data['id'] === 'graph_meals_by_name' || $graph_data['id'] === 'graph_meals_by_day' || $graph_data['id'] === 'graph_activities_follow_up' || $graph_data['id'] === 'card_ingredients' ) wire:ignore @endif>
            @if($graph_data['id'] === 'graph_meals_follow_up' || $graph_data['id'] === 'graph_weights_follow_up' || $graph_data['id'] === 'graph_cycles_follow_up')
                <div class="field is-horizontal graph-buttons">
                    <div class="ml-5 mr-2 is-normal is-flex is-align-items-center">
                        <label class="label">{{trans('messages.period')}}</label>
                    </div>
                    <div class="control has-icons-left">
                        <div class="select">
                            <select wire:change="updateGraphData($event.target.id, $event.target.value)"
                                    id="period_select_{{$graph_data['id']}}">
                                @isset($graph_data['durations'])
                                    @foreach($graph_data['durations']['periods'] as $option)
                                        <option
                                                value="{{$option}}" {{ $graph_data['durations']['default']['period'] == $option ? 'selected' : '' }}>
                                            {{ trans('messages.' . $option) }}
                                        </option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                        <div class="icon is-small is-left">
                            <span class="material-symbols-outlined">history</span>
                        </div>
                    </div>
                    <div class="ml-5 mr-2 is-normal is-flex is-align-items-center">
                        <label class="label">{{trans('messages.scale')}}</label>
                    </div>
                    <div class="control has-icons-left">
                        <div class="select">
                            <select wire:change="updateGraphScale($event.target.id, $event.target.value)"
                                    id="scale_select_{{$graph_data['id']}}">
                                @isset($graph_data['durations']['scales'])
                                    @foreach($graph_data['durations']['scales'] as $option)
                                        <option
                                                value="{{$option}}" {{ $graph_data['durations']['default']['scale'] == $option ? 'selected' : '' }}>
                                            {{ trans('messages.' . $option) }}
                                        </option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                        <div class="icon is-small is-left">
                            <span class="material-symbols-outlined">visibility</span>
                        </div>
                    </div>
                    <div class="ml-5 mr-2 is-normal is-flex is-align-items-center">
                        <button class="button is-light"
                                wire:click="$dispatch('resetZoom',  {{ $graph_data['id'] }} )">
                            <span class="material-symbols-outlined mr-2">reset_image</span>
                            {{trans('messages.reset_zoom')}}
                        </button>
                    </div>
                </div>
            @endif
            <canvas id="{{$graph_data['id']}}"></canvas>
        </div>
    @elseif($card_data)
        @switch($card_data['type'])
            @case('activities')
                @if($card_data['subtype'] === 'summary')
                    <div x-data="{ activeTab: 0}">
                        <div class="tabs is-toggle is-fullwidth">
                            <ul>
                                @foreach($card_data['data'] as $tab)
                                    <li @click="activeTab = {{$tab['key']}}"
                                        :class="{ 'is-active': activeTab === {{$tab['key']}} }">
                                        <a>
                                            <span class="material-symbols-outlined mr-1">{{$tab['icon']}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @foreach($card_data['data'] as $key => $tab)
                            <div class="tab-content mx-1 p-3"
                                 :class="{ 'is-active': activeTab === {{$tab['key']}} }"
                                 x-cloak x-show.transition.in.opacity.duration.600="activeTab === {{$tab['key']}}">
                                <div
                                        class="columns is-multiline bilan-months-activities-container is-block is-justify-content-center is-full pb-5">
                                    <div class="column p-1 is-full box-content">
                                        <h3 class="has-text-centered has-text-weight-semibold has-text-link-70">{{$tab['legend']}}</h3>
                                    </div>
                                    <div class="column p-1 is-full box-content mt-3">
                                        <h4 class="mb-2 has-text-centered has-text-weight-semibold">{{trans_choice('messages.bilan_legend_activities_total', $tab['total'], ['value' => $tab['total']])}}</h4>
                                    </div>
                                    @foreach($tab['activities'] as $activity => $value)
                                        <div class="column p-1 is-full box-content">
                                            @switch($activity)
                                                @case('hiking') @php $activity_icon = 'hiking'; @endphp @break
                                                @case('yoga') @php $activity_icon = 'self_improvement'; @endphp @break
                                                @case('running') @php $activity_icon = 'directions_run'; @endphp @break
                                                @case('workout') @php $activity_icon = 'fitness_center'; @endphp @break
                                                @default @php $activity_icon = 'exercise'; @endphp
                                            @endswitch
                                            <p class="is-flex is-justify-content-center">
                                                    <span
                                                            class="material-symbols-outlined mr-1">{{$activity_icon}}</span>
                                                {{ trans('messages.' . $activity) }}
                                                <span
                                                        class="has-text-weight-semibold ml-1">{{$value['count']}} {{ trans_choice('messages.seance', $value['count']) }}</span>
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                @elseif($card_data['subtype'] === 'average')
                    @foreach($card_data['data'] as $key => $values)
                        @if(is_float($values))
                            <p class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{trans_choice('messages.' . $key, $values, ['value' => $values])}}</p>
                        @else
                            <p class="has-text-centered has-text-weight-semibold">{{trans('messages.' . 'activities_'. $key)}}</p>
                            <div class="bilan-months-activities-container my-5">
                                @foreach($values as $year => $elements)
                                    <article class="message">
                                        <div class="message-header">
                                            <p>{{trans('messages.collapse_legend')}} <a
                                                        href="#collapsible-activities-accordion-{{$year}}"
                                                        data-action="collapse">{{$year}}</a></p>
                                        </div>
                                        <div id="collapsible-activities-accordion-{{$year}}"
                                             class="message-body is-collapsible" data-parent="accordion_first">
                                            <div class="message-body-content">
                                                @foreach($elements as $month => $value)
                                                    <p><span class="has-text-weight-semibold">{{ucfirst(\Carbon\Carbon::parse($month)->isoFormat('MMMM'))}} : </span> {{trans_choice('messages.bilan_seances_by_months', $value, ['value' => $value])}}
                                                    </p>
                                                @endforeach
                                            </div>
                                        </div>
                                    </article>
                                @endforeach

                            </div>
                        @endif
                    @endforeach
                @else

                    @foreach($card_data['data'] as $key => $data)
                        @if($loop->first)
                            <h4 class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{trans_choice('messages.activities_' . $key, $data, ['value' => $data])}}</h4>
                        @elseif($loop->last)
                            <div class="card-content tendance pt-0 is-justify-content-center is-align-content-center has-text-centered">
                                <h4 class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{trans('messages.' . $key)}}
                                </h4>
                                @if($data === 'up')
                                    <span
                                            class="is-align-items-center is-justify-content-center is-align-content-center has-text-centered pagination-link is-rounded has-background-success p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">trending_up</span>
                                </span>
                                @elseif($data === 'down')
                                    <span
                                            class="pagination-link is-justify-content-center is-align-content-center has-text-centered is-rounded has-background-danger p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">trending_down</span>
                                </span>
                                @elseif($data === 'null')
                                    <span
                                            class="pagination-link is-justify-content-center is-align-content-center has-text-centered is-rounded has-background-warning p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">cancel</span>
                                </span>
                                @else
                                    <span
                                            class="pagination-link is-justify-content-center is-align-content-center has-text-centered is-rounded has-background-info p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">trending_flat</span>
                                </span>
                                @endif
                            </div>
                        @else
                            @foreach($data as $activity => $number)
                                @switch($activity)
                                    @case('hiking') @php $activity_icon = 'hiking'; @endphp @break
                                    @case('yoga') @php $activity_icon = 'self_improvement'; @endphp @break
                                    @case('running') @php $activity_icon = 'directions_run'; @endphp @break
                                    @case('workout') @php $activity_icon = 'fitness_center'; @endphp @break
                                    @default @php $activity_icon = 'exercise'; @endphp
                                @endswitch
                                <p class="is-flex is-justify-content-center">
                                    <span class="material-symbols-outlined mr-1">{{$activity_icon}}</span>
                                    {{ trans('messages.' . $activity) }}
                                    <span
                                            class="has-text-weight-semibold ml-1">{{$number}} {{ trans_choice('messages.seance', $number) }}</span>
                                </p>
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @break
            @case('weights')
                @if($card_data['subtype'] === 'summary')
                    <div x-data="{ activeTab: 0}">
                        <div class="tabs is-toggle is-fullwidth">
                            <ul>
                                @foreach($card_data['data'] as $tab)
                                    <li @click="activeTab = {{$tab['key']}}"
                                        :class="{ 'is-active': activeTab === {{$tab['key']}} }">
                                        <a>
                                            <span class="material-symbols-outlined mr-1">{{$tab['icon']}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @foreach($card_data['data'] as $key => $tab)
                            <div class="tab-content mx-1 p-3"
                                 :class="{ 'is-active': activeTab === {{$tab['key']}} }"
                                 x-cloak x-show.transition.in.opacity.duration.600="activeTab === {{$tab['key']}}">
                                <div
                                        class="card-content tendance pt-0 is-justify-content-center is-align-content-center has-text-centered‹‹‹‹‹">
                                    <div class="column p-1 is-full box-content mt-3">
                                        <h3 class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{$tab['legend']}}</h3>
                                    </div>
                                    <div class="column p-1 is-full box-content mt-3">
                                        <h4 class="has-text-centered has-text-weight-semibold my-4">{{trans_choice('messages.bilan_legend_weights_total', $tab['value'], ['value' => $tab['value']])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @elseif($card_data['subtype'] === 'average')
                    @foreach($card_data['data'] as $key => $values)
                        @if(is_float($values))
                            <p class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{trans_choice('messages.' . $key, $values, ['value' => $values])}}</p>
                        @else
                            <p class="has-text-centered has-text-weight-semibold">{{trans('messages.' . $key)}}</p>
                            <div id="bilan-months-weights-container" class="my-5" wire:ignore.self>
                                @foreach($values as $year => $elements)
                                    <article class="message">
                                        <div class="message-header">
                                            <p>
                                                {{trans('messages.collapse_legend')}}
                                                <a href="#collapsible-weights-accordion-{{$year}}"
                                                   data-action="collapse">{{$year}}</a>
                                            </p>
                                        </div>
                                        <div id="collapsible-weights-accordion-{{$year}}"
                                             class="message-body is-collapsible" data-parent="accordion_first">
                                            <div class="message-body-content">
                                                @foreach($elements as $month => $value)
                                                    <p><span class="has-text-weight-semibold">{{ucfirst(\Carbon\Carbon::parse($month)->isoFormat('MMMM'))}} : </span> {{trans_choice('messages.bilan_weights_by_months_without_current', $value, ['value' => $value])}}
                                                    </p>
                                                @endforeach
                                            </div>
                                        </div>
                                    </article>
                                @endforeach
                            </div>
                        @endif
                    @endforeach
                @else
                    @foreach($card_data['data'] as $key => $data)
                        @if($loop->first)
                            <h4 class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{trans_choice('messages.weights_' . $key, $data, ['value' => $data])}}</h4>
                        @elseif($loop->last)
                            <div
                                    class="card-content tendance pt-0 is-justify-content-center is-align-content-center has-text-centered">
                                <h4 class="has-text-centered has-text-weight-semibold has-text-link-70 my-4">{{trans('messages.' . $key)}}</h4>
                                @if($data === 'up')
                                    <span
                                            class="is-align-items-center is-justify-content-center is-align-content-center has-text-centered pagination-link is-rounded has-background-danger p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">trending_up</span>
                                </span>
                                @elseif($data === 'down')
                                    <span
                                            class="pagination-link is-justify-content-center is-align-content-center has-text-centered is-rounded has-background-success p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">trending_down</span>
                                </span>
                                @elseif($data === 'null')
                                    <span
                                            class="pagination-link is-justify-content-center is-align-content-center has-text-centered is-rounded has-background-warning p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">cancel</span>
                                </span>
                                @else
                                    <span
                                            class="pagination-link is-justify-content-center is-align-content-center has-text-centered is-rounded has-background-info p-2 has-text-white has-text-weight-semibold">
                                    <span class="material-symbols-outlined">trending_flat</span>
                                </span>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
                @break
            @case('badges')
                <div class="list has-hoverable-list-items">
                    @foreach(collect($card_data['data']) as $badge)
                        <div class="list-item">
                            <div class="list-item-image">
                                @php
                                    $badgePath = 'images/badges/' . $badge['action_original'] . '_' . $badge['action_count'] . '.svg';
                                    $womenBadgePath = 'images/badges/' . $badge['action_original'] . '_women_' . $badge['action_count'] . '.svg';
                                    $imagePath = auth()->user()->isWomen() && file_exists(public_path($womenBadgePath)) ? $womenBadgePath : $badgePath;
                                @endphp
                                <figure class="image is-96x96">
                                    <img class="is-rounded {{isset($badge['unlock_date']) ? '' : 'locked' }}"
                                         src="{{ asset($imagePath) }}">
                                </figure>
                            </div>
                            <div class="list-item-content">
                                @isset($badge['unlock_date'])
                                    <div class="list-item-title">
                                        @if(auth()->user()->isWomen() && $badge['name_women'])
                                            {{$badge['name_women']}}
                                        @else
                                            {{$badge['name']}}
                                        @endif
                                        <span class="unlock_legend has-text-link-70 ml-1">
                                            {{trans_choice('messages.unlock_badges', 0, [
                                            'value' => $badge['name'],
                                            'unlocked' => $badge['action_count'],
                                            'unit' => trans('messages.badges_'.$badge['action_original'].'_unit'),
                                        ])}}
                                        </span>
                                    </div>
                                    <div
                                            class="list-item-description">
                                        {{trans_choice('messages.unlocked_badges', 1, ['value' => $badge['unlock_date']])}}
                                    </div>
                                @else
                                    <div class="list-item-title">
                                        @if(auth()->user()->isWomen() && $badge['name_women'])
                                            {{$badge['name_women']}}
                                        @else
                                            {{$badge['name']}}
                                        @endif
                                    </div>
                                    <div class="list-item-description">
                                        {{ trans_choice('messages.unlock_badges', 1, [
                                            'value' => auth()->user()->isWomen() && $badge['name_women'] ? $badge['name_women'] : $badge['name'],
                                            'unlocked' => $badge['action_count'],
                                            'unit' => trans('messages.badges_'.$badge['action_original'].'_unit'),
                                        ])}}
                                    </div>
                                @endisset
                            </div>
                        </div>
                    @endforeach
                </div>
                @break
            @case('ingredients')
                <div id="{{$card_data['id']}}" wire:key="card-{{$card_data['id']}}">
                    <h4 class="is-size-4 has-text-centered mt-3 mb-0">{{trans('messages.meals_ingredients_label')}}</h4>
                    <h6 class="is-size-6 has-text-centered my-0">({{trans('messages.current_year')}})</h6>
                    <div
                            class="list has-hoverable-list-items has-visible-pointer-controls is-flex is-justify-content-center">
                        @foreach($card_data['data'] as $ingredient)
                            <div class="list-item is-align-content-center">
                                <div class="list-item-content">
                                    <div class="list-item-title">{{$ingredient['name']}}</div>
                                </div>
                                <div class="list-item-controls">
                                    <span
                                            class="pagination-link is-rounded has-background-warning-70 p-3 has-text-white has-text-weight-semibold">
                                        {{$ingredient['count']}}
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @break
        @endswitch
    @endif
</div>
