@php
    $timeoutDuration = 3000; // Timeout duration in milliseconds
@endphp

{{-- Live Notifications --}}

<div
    x-data="{
        notifications: [],
        notification_show_count: 0,
        notifyCalled: false,
        timeoutDuration: @json($timeoutDuration),
        remove(mid) {
            this.notification_show_count--;
            let m = this.notifications.filter((m) => { return m.id == mid })
            if (m.length) {
                setTimeout(() => {
                    if(this.notification_show_count===0) {
                        this.notifications = [];
                        this.notifyCalled = false;
                    }
                }, 2000)
            }
            $dispatch('close-me', {id: mid})
        },
    }"
    @notify.window="notifyCalled = true;let mid = Date.now();notifications.push({id: mid, msg: $event.detail[0].message, type: $event.detail[0].type});notification_show_count++; setTimeout(() => { remove(mid) }, timeoutDuration)"
    x-init="$watch('notifications', value => { if (value.length === 0) { notifyCalled = false } })"
    :class="{ 'is-hidden': !notifyCalled }"
    id="notifications-container-live"
>
    <template x-for="(notification, notificationIndex) in notifications" :key="notificationIndex">
        <div
            x-data="{ id: notification.id, show: false }"
            x-init="$nextTick(() => { show = true })"
            x-show="show"
            x-transition:enter="animate__fadeInRight"
            x-transition:leave="animate__fadeOutUp"
            @close-me.window="if ($event.detail.id == id) {show=false}"
            :class="`notification animate__animated animate__faster has-background-${notification.type}-light my-2 p-2`"
            x-cloak
        >
            <div class="rounded-lg shadow-lg overflow-hidden">
                <div class="p-4">
                    <div class="is-flex items-start mx-3">
                        <span
                            x-bind:class="{
                                'material-symbols-outlined has-text-success mr-2': notification.type === 'success',
                                'material-symbols-outlined has-text-danger mr-2': notification.type === 'danger',
                                'material-symbols-outlined has-text-warning mr-2': notification.type === 'warning',
                                'material-symbols-outlined has-text-info mr-2': notification.type === 'info'
                            }"
                            x-text="{
                                'success': 'check_circle',
                                'danger': 'error',
                                'warning': 'warning',
                                'info': 'info'
                            }[notification.type]"
                        ></span>
                        <p x-text="notification.msg" class="mx-3"></p>
                        <button @click="remove(notification.id)" class="delete"></button>
                    </div>
                </div>
            </div>
        </div>
    </template>
</div>

{{-- Session Notifications --}}

<div id="notifications-container-session">
    @if(session()->has("success"))
        <div
            x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration), finalClass: false}"
            x-effect="setTimeout(() => { hideNotification = true; setTimeout(() => { finalClass = true }, 500) }, timeoutDuration)"
            x-show="showNotification"
            x-cloak
            :class="{'animate__animated animate__fadeInRight animate__faster': showNotification, 'animate__animated animate__fadeOutUp': hideNotification, 'is-hidden': finalClass}"
            class="notification has-background-success-light {{ session()->has('success') ? '' : 'is-hidden' }}">
            <button @click="hideNotification = true, finalClass = true" class="delete"></button>
            <p class="is-flex">
                <span class="material-symbols-outlined mr-2 has-text-success">check_circle</span>
                {{session("success")}}
            </p>
        </div>
    @endif

    @if(session()->has("error"))
        <div
            x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration), finalClass: false}"
            x-effect="setTimeout(() => { hideNotification = true; setTimeout(() => { finalClass = true }, 500) }, timeoutDuration)"
            x-show="showNotification"
            x-cloak
            :class="{'animate__animated animate__fadeInRight animate__faster': showNotification, 'animate__animated animate__fadeOutUp': hideNotification, 'is-hidden': finalClass}"
            class="notification has-background-danger-light {{ session()->has('error') ? '' : 'is-hidden' }}">
            <button @click="hideNotification = true, finalClass = true" class="delete"></button>
            <p class="is-flex">
                <span class="material-symbols-outlined mr-2 has-text-danger">error</span>
                {{session("error")}}
            </p>
        </div>
    @endif

    @session('warning')
    <div
        x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration), finalClass: false}"
        x-effect="setTimeout(() => { hideNotification = true; setTimeout(() => { finalClass = true }, 500) }, timeoutDuration)"
        x-show="showNotification"
        x-cloak
        :class="{'animate__animated animate__fadeInRight animate__faster': showNotification, 'animate__animated animate__fadeOutUp': hideNotification, 'is-hidden': finalClass}"
        class="notification has-background-warning-light {{ session()->has('warning') ? '' : 'is-hidden' }}">
        <button @click="hideNotification = true, finalClass = true" class="delete"></button>
        <p class="is-flex">
            <span class="material-symbols-outlined has-text-warning mr-2">warning</span>
            {{session("warning")}}
        </p>
    </div>
    @endsession

    @if(session()->has("info"))
        <div
            x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration), finalClass: false}"
            x-effect="setTimeout(() => { hideNotification = true; setTimeout(() => { finalClass = true }, 500) }, timeoutDuration)"
            x-show="showNotification"
            x-cloak
            :class="{'animate__animated animate__fadeInRight animate__faster': showNotification, 'animate__animated animate__fadeOutUp': hideNotification, 'is-hidden': finalClass}"
            class="notification has-background-info-light {{ session()->has('info') ? '' : 'is-hidden' }}">
            <button @click="hideNotification = true, finalClass = true" class="delete"></button>
            <p class="is-flex">
                <span class="material-symbols-outlined has-text-info mr-2">info</span>
                {{session("info")}}
            </p>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Please check the form below for errors</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
</div>

