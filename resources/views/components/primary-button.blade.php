<button {{ $attributes->merge(['type' => 'submit', 'class' => 'button is-primary']) }}>
    {{ $slot }}
</button>
