<button {{ $attributes->merge(['class' => 'button is-link']) }}>
    {{ $slot }}
</button>
