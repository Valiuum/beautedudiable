<div x-data="{ openDish: false}" x-on:click.outside="openDish = false" {{ $attributes->merge(['class' => 'meal-dish box']) }}>
    <div class="is-flex-desktop is-block-mobile is-vcentered is-align-items-center column is-full">
        <div class="is-flex is-justify-content-center is-align-items-center column is-1 p-0 m-2 list-item-image">
            <div class="round-image is-64x64 is-inline-flex">
                <span x-bind:style="'background-color:' + dish.backgroundColor,'color:' + dish.color"
                      class='image is-64x64'/>
                <div
                    x-bind:style="'color:' + dish.color + '; border-radius: 3rem; width: 64px; height: 64px; align-items: center; display: flex; justify-content: center; border: 6px solid ' + dish.border"
                    class="centered-text is-rounded"
                    x-text="items.ingredientPicture(dish.name)"></div>
            </div>
        </div>
        <div class="is-flex is-justify-content-start is-align-items-center column is-3 p-0 m-0">
            <span class="material-symbols-outlined mr-1">skillet</span>
            <span x-text="dish.name"></span>
        </div>
        @desktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-2 p-0 m-0">
            <p class="control">
                <input type="text" class="input is-link quantity-input has-text-centered" readonly
                       x-model="dish.total"/>
            </p>
        </div>
        @elsedesktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-2 p-0 my-4">
            <p class="control">
                <input type="text" class="input is-link quantity-input has-text-centered" readonly
                       x-model="dish.total"/>
            </p>
        </div>
        @enddesktop
        @desktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-3 p-0 m-0">
            <div class="button is-info is-light" x-on:click="openDish = !openDish">
                <span class="material-symbols-outlined dropdown-toggle-ingredients mr-1"
                      x-bind:class="{'rotate-forward': openDish, 'rotate-backward': !openDish }">arrow_drop_down_circle</span>
                <span>{{ __('messages.showIngredients')}}</span>
            </div>
        </div>
        @elsedesktop
        <div class="is-flex is-justify-content-center is-align-items-center column is-3 p-0 m-0">
            <div class="button is-info is-light button-toggle-menu-list" x-on:click="openDish = !openDish">
                <span class="material-symbols-outlined dropdown-toggle-ingredients"
                      x-bind:class="{'rotate-forward': openDish, 'rotate-backward': !openDish }">arrow_drop_down_circle</span>
                <span>{{ __('messages.showIngredients')}}</span>
            </div>
            <span x-bind:class="{'is-dark': items.darkMode}" class="ml-2 button is-danger is-light"
                  @click="items.removeDish(dish.id)">
                <span class="material-symbols-outlined">delete</span>
            </span>
        </div>
        @enddesktop
        @desktop
        <div class="is-flex is-justify-content-end is-align-items-center column is-3 p-0 m-0">
            <span x-bind:class="{'is-dark': items.darkMode}" class="button is-danger is-light"
                  @click="items.removeDish(dish.id)">
                <span class="material-symbols-outlined">delete</span>
            </span>
        </div>
        @enddesktop
    </div>
    @desktop
    <ul x-show="openDish">
        <template x-for="(ingredient, ingredientIndex) in dish.ingredients" :key="ingredientIndex">
            <li x-show="openDish"
                x-transition:enter.scale.50
                x-transition:leave.scale.80
                x-transition:enter.duration.200ms
                x-transition:leave.duration.350ms
                x-cloak>
                <x-meal-ingredient :dishIngredient="true" class="is-shadowless my-2 ml-5"/>
            </li>
        </template>
    </ul>
    @elsedesktop
    <ul x-show="openDish">
        <template x-for="(ingredient, ingredientIndex) in dish.ingredients" :key="ingredientIndex">
            <li x-show="openDish"
                x-transition:enter.scale.50
                x-transition:leave.scale.80
                x-transition:enter.duration.200ms
                x-transition:leave.duration.350ms
                x-cloak>
                <x-meal-ingredient :dishIngredient="true" class="is-shadowless my-2"/>
            </li>
        </template>
    </ul>
    @enddesktop
</div>




