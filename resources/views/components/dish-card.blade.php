@php use App\Models\User as User; @endphp
@props(['header' => [], 'data' => []])
@php
    $points = collect($data->ingredients)->map(function($ingredient) {
        return $ingredient->pivot->points;
    })->sum();
@endphp
<div
    {{$attributes->merge(['class' => 'card dish is-shadow-2xl is-shadow-none is-cursor-pointer transform is-duration-500 hover-translate-y' ]) }} data-name="{{$data->name}}">
    <div class="card-image">
        <img src="{{asset('images/common/'. $data->type .'.svg') }}" alt="Placeholder image"/>
        <div class="meal-points has-background-warning-20 is-flex is-justify-content-center">
            <span class="is-flex is-align-items-center">{{$points}}</span>
        </div>
    </div>
    <div class="card-content @mobile is-open-for-mobile @endmobile">
        <div class="media mb-2">
            <div class="media-left mt-2">
                <span class="icon">
                    @switch($data->type)
                        @case ('breakfast')
                            🥐
                            @break
                        @case ('lunch')
                            🥗
                            @break
                        @case ('dinner')
                            🍲
                            @break
                        @case ('snack')
                            🍩
                            @break
                        @case ('bar')
                            🍻
                            @break
                    @endswitch
                </span>
            </div>
            <div class="media-content">
                <p class="title name is-4">{{Str::limit($data->name, 26, '...')}}</p>
                <p class="subtitle is-6">{{User::where('id', $data->user_id)->pluck('name')[0]}}</p>
            </div>
        </div>
        <div class="content pt-0 px-2 pb-2 mb-2">
            <div class="list">
                @foreach(collect($data->ingredients)->take(3) as $ingredient)
                    <div class="list-item is-borderless py-2">
                        <div class="list-item-description">{{Str::limit($ingredient->name, 30, '...')}}</div>
                    </div>
                @endforeach
                @if(count($data->ingredients) > 2)
                    <div class="list-item py-0">
                        <div class="list-item-description">...</div>
                    </div>
                @endif
            </div>
        </div>
        <div class="actions is-flex columns is-multiline is-justify-content-space-around is-mobile">
            <a wire:navigate href="{{route('dish.edit', ['dish' => $data->id])}}"
               class="column is-full-touch is-3-desktop my-2 button is-flex is-primary is-light has-tooltip-top"
               data-tooltip='{{__('messages.edit')}}'>
                <span class="material-symbols-outlined">edit</span>
            </a>
            <a wire:navigate href="{{route('dish.duplicate', ['dish' => $data->id])}}"
               class="column is-full-touch is-3-desktop my-2 is-flex button is-link is-light has-tooltip-top"
               data-tooltip='{{__('messages.duplicate')}}'>
                <span class="material-symbols-outlined">content_copy</span>
            </a>
            <a @click="$dispatch('dish-delete', {{$data->id}})"
               class="column is-full-touch is-3-desktop my-2 button is-danger is-flex is-light has-tooltip-top"
               data-tooltip='{{__('messages.delete')}}'>
                <span class="material-symbols-outlined">delete</span>
            </a>
        </div>
    </div>
</div>


