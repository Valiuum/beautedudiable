@props(['id','name','type'])
@switch($type)
    @case('breakfast') @php $icon = '🍳'; @endphp @break
    @case('lunch') @php $icon = '🍔'; @endphp @break
    @case('dinner') @php $icon = '🍲'; @endphp @break
    @case('snack') @php $icon = '🍪'; @endphp @break
    @case('bar') @php $icon = '🍪'; @endphp @break
@endswitch
<span id="dish_tag_{{$id}}"
      {{ $attributes->merge(['class' => 'tag dish-tags is-medium is-hoverable is-rounded']) }} wire:click="addDish({{$id}})"
      data-dish="{{json_encode($name)}}"
      x-bind:class="{'is-hidden': items.dishes.some(dish => dish.id === {{$id}})}
    ">
    <figure class="image is-16x16 is-flex is-justify-content-center mr-2">
        <span class="is-flex is-align-self-center">
            {{$icon}}
        </span>
    </figure>
    {{$name}}
</span>
