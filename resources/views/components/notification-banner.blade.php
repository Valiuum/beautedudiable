@props(['message' => '', 'data' => []])

<div {{$attributes->merge(['class' => 'notification is-light']) }}>
    <button class="delete"></button>
    @switch($message)
        @case('meal_processing')
            <p>{{ __('messages.mealProcessingBanner') }}</p>
            @break
        @case('meal_exists')
            <p>{{ __('messages.mealExistsBanner') }}</p>
            @break
        @case('ingredients_exists')
            <h4 class="has-text-centered has-text-weight-bold mb-3">{{__('messages.disabledFeature')}}</h4>
            <p>{{ trans_choice('messages.ingredientsExistsBanner', collect($data)->count() , ['value' => implode(', ', $data)]) }}</p>
            {{--
            <x-danger-button
                class="mt-2 is-inverted"
                wire:click="deleteIngredients">
                <span class="material-symbols-outlined mr-2">delete_forever</span>
                <span>{{ __('messages.confirmDelete') }}</span>
            </x-danger-button>
            --}}
            @break
        @case('dish_exists')
            <p>{{ __('messages.dishExist') }}</p>
            @break
        @case('deleting_dish')
            <p>{{__('messages.dishDeleteConfirm')}}</p>
            <div class="buttons mt-2 is-flex is-justify-content-left">
                <form x-bind:action="'/dishes/delete/' + dishId" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="button is-danger">{{__('messages.confirmDelete')}}</button>
                </form>
                <button @click="dishId = ''; existing_dish = false"
                        class="button is-link is-inverted">{{__('messages.cancel')}}</button>
            </div>
            @break
    @endswitch
</div>
