@props(['icon','text'])
<button class="button">
    <span class="material-symbols-outlined mr-2">
    {{ $icon }}
    </span>
    <span>
        {{ $text }}
    </span>
</button>



