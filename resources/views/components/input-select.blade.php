@props(['options' => [], 'selected' => '', 'icon' => '', 'livewireModel' => ''])
<div class="field">
    <p class="control has-icons-left">
        <span class="select">
            <select wire:model={{$livewireModel}}>
                @if($livewireModel == 'form.unit' )
                    <option selected>{{__('messages.selectIngredientUnit')}}</option>
                    @foreach($options as $option)
                        <option {{ $option === $selected ? 'selected' : '' }}>{{ $option }}</option>
                    @endforeach
                @else
                    @if($livewireModel == 'activity')
                        <option value="none" selected>{{__('messages.selectPlaceholder')}}</option>
                    @else
                        <option value='0' selected disabled>{{ __('messages.selectPlaceholder') }}</option>
                    @endif
                    @foreach($options as $key => $value)
                        <option value="{{ $key }}"
                                @if($key == $selected) selected="selected" @endif>{{ $value }}</option>
                    @endforeach
                @endif
            </select>
        </span>
        <span class="icon is-small is-left">
            <i class="material-symbols-outlined">{{$icon}}</i>
        </span>
    </p>
</div>
