<x-app-layout>
    {{--
    @if(auth()->user()->isPowerUser())
        <p>POWER User</p>
    @endif
    --}}
    @livewire('component.cards-user-data')
    <div class="column is-10-desktop is-10-tablet mx-auto mb-5">
        @livewire('component.reminder-meals')
    </div>
    <div class="column is-6-desktop is-10-tablet mx-auto mb-5">
        @livewire('component.datepicker')
    </div>
    @livewire('component.daily-meals-cards')
</x-app-layout>
