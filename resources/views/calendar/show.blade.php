<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/calendar.svg')"></x-slot>
    <section class="@desktop px-3 @elsedesktop px-5 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <section class="section column is-full">
                <h4 class="is-size-4 has-text-weight-semibold @mobile has-text-centered @endmobile">{{ __('messages.calendar') }}</h4>
            </section>
            <livewire:calendar.calendar/>
        </div>
    </section>
</x-app-layout>
