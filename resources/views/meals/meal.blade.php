<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/' . $meal['type'] .'.svg')"></x-slot>
    <section class="section @desktop px-5 @elsedesktop px-2 @enddesktop">
        <livewire:foods.meals :meal="$meal"/>
    </section>
</x-app-layout>
