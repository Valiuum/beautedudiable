<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/' . $dish['type'] .'.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <section class="section">
            <h4 class="is-size-4 has-text-weight-semibold @mobile has-text-centered @endmobile">{{trans('messages.dishShow')}}</h4>
        </section>
        <livewire:foods.dishes :dish="$dish"/>
    </section>
</x-app-layout>
