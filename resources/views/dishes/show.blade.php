<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/dishes.svg')"></x-slot>
    <div x-data="{ dishId: '' }" @dish-delete.window="dishId = event.detail" x-show="dishId">
        <x-notification-banner class="animate__animated animate__fadeInRight is-danger banner-notification"
                               :message="'deleting_dish'"/>
    </div>
    <section class="section">
        <h4 class="is-size-4 has-text-weight-semibold @mobile has-text-centered @endmobile">{{trans('messages.dishNew')}}</h4>
    </section>
    <section class="@desktop px-5 @elsedesktop px-2 pt-0 @enddesktop section">
        <x-tabs class="has-text-info" :header="$header" :content="$dishes" :type="'dishes'"/>
    </section>
</x-app-layout>
