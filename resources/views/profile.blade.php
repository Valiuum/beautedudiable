<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/profile.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns profils-columns is-multiline is-mobile">

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-size-6 has-text-weight-semibold profile-header p-2 has-background-link-light has-text-link">{{__('messages.beautyProfile')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop profile-update-account">
                <livewire:profile.update-profile-beauty-form/>
            </div>

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-size-6 has-text-weight-semibold profile-header p-2 has-background-link-light has-text-link">{{__('messages.accountProfile')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop">
                <livewire:profile.update-profile-information-form/>
            </div>

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-size-6 has-text-weight-semibold profile-header p-2 has-background-link-light has-text-link">{{__('messages.passwordProfile')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop">
                <livewire:profile.update-password-form/>
            </div>

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-size-6 has-text-weight-semibold profile-header p-2 has-background-link-light has-text-link">{{__('messages.webNotifications')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop">
                <livewire:profile.web-notifications-user-form/>
            </div>

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-size-6 has-text-weight-semibold profile-header p-2 has-background-link-light has-text-link">{{__('messages.deleteProfile')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop profile-delete-account">
                <livewire:profile.delete-user-form/>
            </div>

        </div>

    </section>

</x-app-layout>
