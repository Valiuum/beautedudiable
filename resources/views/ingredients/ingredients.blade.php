<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/ingredients.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <livewire:tables.ingredients-form :displayTable="false" :key="0"/>
            <div class="column is-12 livewire-table-container">
                <livewire:tables.ingredients/>
            </div>
        </div>
    </section>
</x-app-layout>
