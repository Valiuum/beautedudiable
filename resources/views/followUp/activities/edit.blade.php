<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/' . $activity->activity . '.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <livewire:tables.activities-form
                :displayTable="false"
                :activity="$activity"
                :edit="true"
                :key="$activity->id"/>
        </div>
    </section>
</x-app-layout>
