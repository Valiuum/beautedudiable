<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/weights.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <livewire:tables.weights-form
                :displayTable="false"
                :weight="$weight"
                :edit="true"
                :key="$weight->id"/>
        </div>
    </section>
</x-app-layout>
