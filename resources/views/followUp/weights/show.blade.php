<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/weights.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <livewire:tables.weights-form :displayTable="false" :edit="false" :key="0"/>
            <section class="column is-12 livewire-table-container">
                <livewire:tables.weights/>
            </section>
        </div>
    </section>
</x-app-layout>
