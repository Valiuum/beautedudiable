<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/bilan.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-mobile">
            <livewire:bilan.bilan/>
        </div>
    </section>
</x-app-layout>

