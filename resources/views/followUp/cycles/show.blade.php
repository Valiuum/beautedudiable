<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/cycles.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <livewire:tables.cycles-form :displayTable="false" :edit="false" :key="0"/>
            <section class="column is-12 livewire-table-container">
                <livewire:tables.cycles/>
            </section>
        </div>
    </section>
</x-app-layout>
