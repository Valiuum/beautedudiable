<x-app-layout>
    <x-slot name="header" :imgPath="asset('images/header/cycles.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns is-multiline is-mobile">
            <livewire:tables.cycles-form
                :displayTable="false"
                :cycle="$cycle"
                :edit="true"
                :key="$cycle->id"/>
        </div>
    </section>
</x-app-layout>
