import Hammer from 'hammerjs';
import {events} from './app.js';

const initDailyMeals = () => {

    const swipeElements = document.querySelectorAll('.swipe');
    swipeElements.forEach((element) => {
        const swipe = new Hammer(element);
        swipe.on("swipeleft", (ev) => {
            const newDate = new Date(element.date);
            document.dispatchEvent(new CustomEvent('swipe-event', {
                detail: ['increase', {date: newDate}],
                bubbles: true
            }));
        });

        swipe.on("swiperight", (ev) => {
            const newDate = new Date(element.date);
            document.dispatchEvent(new CustomEvent('swipe-event', {
                detail: ['decrease', {date: newDate}],
                bubbles: true
            }));
        });
    });

};


window.cards = () => {

    return {
        updateClassName(element) {
            const listItems = document.querySelectorAll('.list-item');
            let delay = 0;
            listItems.forEach(item => {
                const descriptionDiv = item.querySelector('.list-item-description');
                const ingredientsMobile = item.querySelector('.list-item-mobile');

                // Add new classes with a delay
                setTimeout(() => {
                    if (element.action === 'init') {
                        this.animateCSS(item, ['animate__fast', 'animate__fadeInDown'])
                        descriptionDiv.classList.remove('is-invisible');
                        if (ingredientsMobile) {
                            ingredientsMobile.classList.remove('is-invisible');
                        }
                    } else if (element.action === 'clear') {
                        if (element.userAction === 'increase') {
                            this.animateCSS(item, ['animate__fast', 'animate__fadeOutLeft'])
                        } else if (element.userAction === 'decrease') {
                            this.animateCSS(item, ['animate__fast', 'animate__fadeOutRight'])
                        } else if (element.userAction === 'select') {
                            this.animateCSS(item, ['animate__fast', 'animate__fadeOutDown'])
                        }
                    } else if (element.action === 'reveal') {
                        setTimeout(() => {
                            if (element.userAction === 'increase') {
                                this.animateCSS(item, ['animate__fast', 'animate__fadeInRight'])
                            } else if (element.userAction === 'decrease') {
                                this.animateCSS(item, ['animate__fast', 'animate__fadeInLeft'])
                            } else if (element.userAction === 'select') {
                                this.animateCSS(item, ['animate__fast', 'animate__fadeInDown'])
                            }
                            descriptionDiv.classList.remove('is-invisible');
                            if (ingredientsMobile) {
                                ingredientsMobile.classList.remove('is-invisible');
                            }
                        }, 230);
                    }
                }, delay);
                delay += 50;
            });
        },

        animateCSS(element, animationNames, callback) {
            const node = element;
            animationNames.forEach(animationName => {
                node.classList.add('animate__animated', animationName);
            });

            function handleAnimationEnd() {
                animationNames.forEach(animationName => {
                    node.classList.remove('animate__animated', animationName);
                });
                node.removeEventListener('animationend', handleAnimationEnd);

                if (typeof callback === 'function') callback();
            }

            node.addEventListener('animationend', handleAnimationEnd, {once: true});
        }
    }
}

events.forEach(event => {
    if (event === 'livewire:navigated') {
        window.addEventListener(event, () => {
            initDailyMeals();
        });
    }
});
