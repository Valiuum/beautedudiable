import './bootstrap';
import 'material-symbols';
import bulmaSlider from "bulma-slider/src/js";
import SimpleParallax from "simple-parallax-js/vanilla";

export const events = ['DOMContentLoaded', 'livewire:load', 'livewire:navigated'];

export function isMobile() {
    const userAgent = navigator.userAgent || navigator.vendor || window.opera;
    return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent.toLowerCase());
}

export const darkModePreference = window.matchMedia("(prefers-color-scheme: dark)");
const lightModePreference = window.matchMedia("(prefers-color-scheme: light)");

function activateDarkMode() {
    let buttons = document.querySelectorAll('.button');
    let cardIntakesElements = document.querySelectorAll('.intakes');
    let listItemElements = document.querySelectorAll('.list-item');
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        buttons.forEach(button => {
            if (button.classList.contains('is-light')) {
                button.classList.replace('is-light', 'was-light');
            }
            button.classList.add('is-dark');
        });
        cardIntakesElements.forEach(card => {
            card.classList.add('is-shadowless');
        });
        listItemElements.forEach(list => {
            list.classList.add('is-shadowless');
        });
        window.dispatchEvent(new CustomEvent('dark-mode-update', {detail: ['is-dark'], bubbles: true}))
    } else {
        buttons.forEach(button => {
            button.classList.remove('is-dark');
            if (button.classList.contains('was-light')) {
                button.classList.replace('was-light', 'is-light');
            }
        });
        cardIntakesElements.forEach(card => {
            card.classList.remove('is-shadowless');
        });
        listItemElements.forEach(list => {
            list.classList.remove('is-shadowless');
        });
        window.dispatchEvent(new CustomEvent('dark-mode-update', {detail: ['is-light'], bubbles: true}))
    }
}

function closeNotifications() {
    document.body.addEventListener('click', (event) => {
        if (event.target.matches('.notification .delete')) {
            const $notification = event.target.parentNode;
            $notification.classList.add('animate__fadeOutLeft');
            setTimeout(() => {
                $notification.remove();
            }, 600);
        }
    });
}

function homeCardsDesktop() {

    if (typeof current_route === 'undefined' || current_route !== 'home' || isMobile()) {
        return;
    }

    const dailyPointsCard = document.getElementById('daily-points-home-desktop-card');
    const activitiesBonusCard = document.getElementById('activities-bonus-home-desktop-cards');

    if (dailyPointsCard && activitiesBonusCard) {
        const activitiesHeight = activitiesBonusCard.offsetHeight;
        dailyPointsCard.style.height = `${activitiesHeight}px`;
    }
}

export function bilanCardsDesktop() {

    if (typeof current_route === 'undefined' && current_route !== 'bilan.show' || isMobile()) {
        return
    }

    const cardIngredients = document.getElementById('card_ingredients') ?? null;
    if (!cardIngredients) {
        return
    }

    const listElement = cardIngredients.querySelector('div.list.has-hoverable-list-items');
    const parentCard = cardIngredients.closest('.is-one-fifths-desktop');
    const graphMeals = document.getElementById('graph_meals_by_name');
    const parentGraph = graphMeals.closest('.is-three-fifths-desktop');

    if (parentCard && parentGraph && listElement) {
        const graphHeight = parentGraph.offsetHeight;
        const height = Math.round(graphHeight * 0.84)
        listElement.style.setProperty('max-height', `${height}px`);
    }

}


function searchTabs() {
    const inputs = document.querySelectorAll('.search-tab-content');
    if (inputs.length === 0) {
        return
    }
    inputs.forEach(input => {
        input.addEventListener('input', function (e) {
            let filter = e.target.value.toLowerCase();
            let inputContainer = e.target.parentNode.parentNode;
            inputContainer.parentNode.querySelectorAll('.card').forEach(function (card) {
                let name = card.getAttribute('data-name').toLowerCase();
                if (name.includes(filter)) {
                    card.style.display = '';
                } else {
                    card.style.display = 'none';
                }
            });
        });
    });
}

const modalManager = {
    openModal: function ($el) {
        $el.classList.add('is-active');
    },

    closeModal: function ($el) {
        $el.classList.remove('is-active');
    },

    closeAllModals: function () {
        (document.querySelectorAll('.modal') || []).forEach(($modal) => {
            this.closeModal($modal);
        });
    },

    init: function () {
        // Add a click event on buttons to open a specific modal
        (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
            const modal = $trigger.dataset.target;
            const $target = document.getElementById(modal);

            $trigger.addEventListener('click', () => {
                this.openModal($target);
            });
        });

        // Add a click event on various child elements to close the parent modal
        (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
            const $target = $close.closest('.modal');

            $close.addEventListener('click', () => {
                this.closeModal($target);
            });
        });

        // Add a keyboard event to close all modals
        document.addEventListener('keydown', (event) => {
            if (event.key === "Escape") {
                this.closeAllModals();
            }
        });
    }
}

function imagesParallax() {
    let images = document.querySelectorAll('figure.header-figure');
    if (images.length) {
        new SimpleParallax(images, {
            delay: .2,
            scale: 1.3,
            transition: 'cubic-bezier(0,0,0,1)'
        });
    }
}

function tabs() {
    const tabs = document.querySelectorAll('.tabs.is-toggle li');
    tabs.forEach(tab => {
        tab.addEventListener('click', (event) => {
            event.preventDefault();
            event.stopPropagation(); // Stop the event from propagating to parent elements
            const currentScrollPosition = window.scrollY; // Store the current scroll position
            setTimeout(() => {
                window.scrollTo(0, currentScrollPosition); // Set the scroll position back to the stored position
            }, 0); // Use a timeout to ensure the scroll position is set after the click event
        });
    });
}

function initializeSliders() {
    bulmaSlider.attach();
}

function handleFunctions() {
    activateDarkMode()
    closeNotifications()
    searchTabs()
    homeCardsDesktop()
    tabs()
    initializeSliders()
    imagesParallax()
    //bilanCardsDesktop()
    modalManager.init()
}

darkModePreference.addEventListener("change", e => e.matches && activateDarkMode());
lightModePreference.addEventListener("change", e => e.matches && activateDarkMode());
events.forEach(event => {
    document.addEventListener(event, handleFunctions);
});







