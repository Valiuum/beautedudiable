document.addEventListener('livewire:navigated', () => {
    (function () {
        const $body = document.body;
        const $menu_trigger = $body.getElementsByClassName('navbar-burger')[0];
        const $sidenav_links = document.querySelectorAll('#sidenav a');

        function toggleMenu(event) {
            event.stopPropagation();
            $body.className = ($body.className == 'menu-active') ? '' : 'menu-active';
        }

        function closeMenu(event) {
            if (!$menu_trigger.contains(event.target) && $body.className == 'menu-active') {
                $body.className = '';
                $menu_trigger.classList.remove('is-active');
            }
        }

        function closeMenuOnLinkClick() {
            $body.className = '';
            $menu_trigger.className = 'navbar-burger'; // Restore to original state
        }

        function attachEventListeners() {
            if (typeof $menu_trigger !== 'undefined') {
                $menu_trigger.addEventListener('click', toggleMenu);
                document.addEventListener('click', closeMenu);
                $sidenav_links.forEach(function (link) {
                    link.addEventListener('click', closeMenuOnLinkClick);
                });
            }
        }

        attachEventListeners();
    }).call(this);
});
