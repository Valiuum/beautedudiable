import {events} from './app.js';
import * as FullCalendar from 'fullcalendar'
import interactionPlugin from '@fullcalendar/interaction'
import dayGridPlugin from '@fullcalendar/daygrid'
import listPlugin from '@fullcalendar/list'
import frLocale from '@fullcalendar/core/locales/fr'
import momentPlugin from '@fullcalendar/moment'

// Define initial Calendar view
const isMobileDevice = /Mobi/i.test(window.navigator.userAgent)
const initialView = isMobileDevice ? 'listWeek' : 'dayGridMonth';
const element = document.getElementById('calendar');

const titleFormat = isMobileDevice ? {
    month: 'short',
    year: 'numeric',
    day: 'numeric',
    weekday: 'short'
} : {
    month: 'long',
    year: 'numeric',
    day: 'numeric',
    weekday: 'long'
};

const views = isMobileDevice ? {
    listWeek: {buttonText: window.listWeekMobile},
    listMonth: {buttonText: window.listMonthMobile}
} : {
    listDay: {buttonText: window.listDay},
    listWeek: {buttonText: window.listWeek},
    listMonth: {buttonText: window.listMonth}
};

const headerToolbar = isMobileDevice ? {
    left: 'prev,next today',
    right: 'listWeek,listMonth'
} : {
    left: 'prev,next today',
    center: 'title',
    right: 'dayGridMonth,listWeek,listMonth'
};

function initCalendar() {
    const calendarEl = document.getElementById('calendar');
    if (!calendarEl) return;
    let calendar = new FullCalendar.Calendar(calendarEl, {
        locale: frLocale,
        plugins: [
            interactionPlugin,
            dayGridPlugin,
            listPlugin,
            momentPlugin,
        ],
        initialView: initialView,
        titleFormat: titleFormat,
        views: views,
        headerToolbar: headerToolbar,
        editable: true,
        selectable: true,
        height: 'auto',
        events: JSON.parse(window.calendarEvents),
        eventResize: function (eventResizeInfo) {
            let endDelta = null;
            Object.entries(eventResizeInfo.endDelta).forEach(([key, value]) => {
                if (value !== 0) {
                    endDelta = {[key]: value};
                }
            });
            window.dispatchEvent(new CustomEvent('event-update', {
                detail: [{
                    eventData: eventResizeInfo.event._def,
                    eventDates: eventResizeInfo.event._instance.range,
                    eventType: 'cycle',
                    eventDeltaEnd: endDelta,
                    action: 'resize',
                }],
                bubbles: true
            }))
            /* Event is trigger multiple time */
            Livewire.on('cycle-updated', (response) => {
                const result = response.data;
                calendar.refetchEvents()
                if (result.success) {
                    const notify = {type: 'success', message: result.success.message}
                    element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                } else {
                    Object.entries(result.errors).forEach(([key, value]) => {
                        let notify = {type: 'danger', message: value}
                        element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                    })
                    eventResizeInfo.revert();
                }
            });
        },
        eventDrop: function (eventDropInfo) {
            window.dispatchEvent(new CustomEvent('event-update', {
                detail: [{
                    eventData: eventDropInfo.event._def,
                    eventDates: eventDropInfo.event._instance.range,
                    eventDelta: eventDropInfo.delta,
                    action: 'drop',
                }],
                bubbles: true
            }))
            Livewire.on('event-updated', (response) => {
                const result = response.data;
                calendar.refetchEvents()
                if (result.success) {
                    const notify = {type: 'success', message: result.success.message}
                    dispatchNotifyEvent(notify);
                } else {
                    Object.entries(result.errors).forEach(([key, value]) => {
                        let notify = {type: 'danger', message: value}
                        dispatchNotifyEvent(notify);
                    })
                    eventDropInfo.revert();
                }
            });
            calendar.refetchEvents()
        },
    });
    calendar.render();
}

let lastDispatchTime = 0;
const dispatchNotifyEvent = (notify) => {
    const currentTime = Date.now();
    if (currentTime - lastDispatchTime > 1000) { // 1 second interval
        element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}));
        lastDispatchTime = currentTime;
    }
}

events.forEach(event => {
    document.addEventListener(event, initCalendar);
});
