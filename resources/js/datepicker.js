import {events} from './app.js';
import MCDatepicker from 'mc-datepicker';

// Declare a global variable for the datepicker
window.datepickerInstance = null;

const customWeekDays = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
const customMonths = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']

// Function to initialize the datepicker
function initDatepicker() {

    // Check if the global datepicker instance exists
    if (window.datepickerInstance) {
        // Destroy the existing datepicker instance
        window.datepickerInstance.destroy();
    }

    const datepicker = MCDatepicker.create({
        el: '#datepicker',
        context: document.body,
        dateFormat: 'dddd dd mmmm yyyy',
        autoClose: true,
        closeOnBlur: true,
        customClearBTN: 'Effacer',
        customCancelBTN: 'Annuler',
        firstWeekday: 1,
        showCalendarDisplay: false,
        customWeekDays: customWeekDays,
        customMonths: customMonths,
        theme: {
            theme_color: '#4258ff',
            main_background: '#f5f5f6',
            active_text_color: 'rgb(0, 0, 0)',
            inactive_text_color: 'rgba(0, 0, 0, 0.2)',
            display: {
                foreground: 'rgba(255, 255, 255, 0.8)',
                background: '#38ada9'
            },
            picker: {
                foreground: 'rgb(0, 0, 0)',
                background: '#f5f5f6'
            },
            picker_header: {
                active: '#818181',
                inactive: 'rgba(0, 0, 0, 0.2)'
            },
            weekday: {
                foreground: '#38ada9'
            },
            button: {
                success: {
                    foreground: '#38ada9'
                },
                danger: {
                    foreground: '#e65151'
                }
            },
        }
    });

    datepicker.onSelect((date) => {

        const utcDate = date ? new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())) : new Date()
        const wDay = date ? date.getDay() : new Date().getDay();
        if (wDay >= 0 && wDay <= 6) {
            window.dispatchEvent(new CustomEvent('datepicker-update', {
                detail: [{
                    date: utcDate,
                    action: 'select',
                    weekday: customWeekDays[wDay]
                }],
                bubbles: true
            }));
        }
    });

    datepicker.onOpen(() => {

        const datepickerInput = document.querySelector('#datepicker');
        if (datepickerInput) {
            datepickerInput.blur(); // Prevent focus on the input field
        }

        const datepickerValue = document.querySelector('#datepicker').value;
        if (datepickerValue && typeof datepickerValue === 'string') {
            markMissingMeals(datepicker, current_route);
        }

        // Create a button element
        const button = document.createElement('button');
        button.textContent = 'Aujourd\'hui';
        button.classList.add('mc-btn');
        button.classList.add('mc-btn--today');

        // Append the button to the datepicker's footer
        const datepickerFooter = document.querySelector('.mc-footer__section.mc-footer__section--primary');
        datepickerFooter.appendChild(button);

        const btnCancel = document.getElementById('mc-btn__cancel');
        btnCancel.classList.remove('mc-btn--success');
        btnCancel.classList.add('mc-btn--danger');

        // Add a click event listener to the button
        button.addEventListener('click', () => {
            datepicker.setFullDate(new Date());
            datepicker.close();
            window.dispatchEvent(new CustomEvent('datepicker-update', {
                detail: [{
                    date: new Date(),
                    action: 'select'
                }],
                bubbles: true
            }))
        });

        // Create an overlay element
        const overlay = document.createElement('div');
        overlay.style.position = 'fixed';
        overlay.style.top = '0';
        overlay.style.left = '0';
        overlay.style.width = '100%';
        overlay.style.height = '100%';
        overlay.style.zIndex = '1000'; // Ensure the overlay is above other elements
        overlay.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; // Semi-transparent black
        overlay.id = 'datepicker-overlay'; // Add an ID for easy removal

        // Append the overlay to the body
        document.body.appendChild(overlay);

    });

    datepicker.onClose(() => {
        // Remove the button when the datepicker is closed
        const button = document.querySelector('.mc-btn--today');
        if (button) {
            button.remove();
        }

        // Remove the overlay when the datepicker is closed
        const overlay = document.querySelector('#datepicker-overlay');
        if (overlay) {
            overlay.remove();
        }
    });

    markMissingMeals(datepicker, current_route);

    // Store the datepicker instance in the global variable
    window.datepickerInstance = datepicker;
}

function markMissingMeals(datepicker, current_route) {
    if (current_route === 'home') {
        requestMissingMeals().then((response) => {
            response.data.forEach((items) => {
                for (let i = 0; i < Object.values(items).length; i++) {
                    const item = Object.values(items)[i];
                    const itemDate = new Date(item.date);
                    if (item.meals.length > 0) {
                        datepicker.markDatesCustom((date) => (date.getDate() === itemDate.getDate() && date.getMonth() === itemDate.getMonth() && date.getFullYear() === itemDate.getFullYear()));
                        let markedDateElement = document.querySelectorAll(`.mc-date--marked`);
                        markedDateElement.forEach(element => {
                            itemDate.setHours(0, 0, 0, 0); // Set hour to 00:00:00
                            if (element.getAttribute('data-val-date') === itemDate.toString()) {
                                if (item.meals.includes('lunch') && item.meals.includes('dinner')) {
                                    element.classList.add('lunch-dinner-missing');
                                } else if (item.meals.includes('lunch')) {
                                    element.classList.add('lunch-missing');
                                } else if (item.meals.includes('dinner')) {
                                    element.classList.add('dinner-missing');
                                }
                            }
                        });
                    }
                }
            });
        });
    }
}

function convertFrenchDateToJSDate(frenchDate) {
    // Map of French month names to English month names
    const monthNames = {
        'janv.': 'January',
        'févr.': 'February',
        'mars': 'March',
        'avr.': 'April',
        'mai': 'May',
        'juin': 'June',
        'juil.': 'July',
        'août': 'August',
        'sept.': 'September',
        'oct.': 'October',
        'nov.': 'November',
        'déc.': 'December'
    };

    // Split the French date into parts
    const parts = frenchDate.split(' ');

    // Translate the French month name to English
    parts[2] = monthNames[parts[2].toLowerCase()];

    // Join the parts back together and create a new Date object
    const englishDate = parts.join(' ');
    return new Date(englishDate);
}

function requestMissingMeals() {
    return new Promise((resolve) => {
        const handler = (event) => {
            window.removeEventListener('datepicker-request-missing-meals-response', handler);
            resolve(event.detail);
        };
        window.addEventListener('datepicker-request-missing-meals-response', handler);

        window.dispatchEvent(new CustomEvent('datepicker-request-missing-meals', {
            detail: [{
                date: new Date(),
            }],
            bubbles: true
        }));
    });
}


// Listen for all events from app.js
events.forEach(event => {
    if (event === 'livewire:navigated') {
        window.addEventListener(event, initDatepicker);
    }
});


