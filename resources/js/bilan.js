// bilan.js
import {events} from './app.js'
import {isMobile} from "./app.js";
import Chart from 'chart.js/auto'
import 'chartjs-adapter-date-fns'
import zoomPlugin from 'chartjs-plugin-zoom';

Chart.register(zoomPlugin);
import {fr} from 'date-fns/locale'
import bulmaCollapsible from '@creativebulma/bulma-collapsible'


// Define the chart instances
let graph_meals_follow_up_instance = null
let graph_meals_by_name_instance = null
let graph_meals_by_day_instance = null
let graph_activities_follow_up_instance = null
let graph_cycles_follow_up_instance = null
let graph_weights_follow_up_instance = null


const user_daily_points = (graphsData.objectives && graphsData.objectives[0]) ? graphsData.objectives[0].daily_points : 0;
const user_daily_points_up = "#af5c5c"
const user_daily_points_down = "#4bc0c0"

const colorsBubbleMealsName = ['#f5d3b7', '#c5dcc6', '#b4c9de', '#d66d62', '#43463e']
const colorsBubbleMealsNameHover = ['#f0bc91', '#abccac', '#98b5d2', '#d05649', '#3c3f38']

const colorsBubbleMealsDay = ['#981B68', '#BF0D48', '#DE687E', '#FB8B24', '#CE570E', '#795838', '#0F4C5C']
const colorsBubbleMealsDayHover = ['#791553', '#990B3A', '#D74761', '#F07605', '#AC490C', '#62472D', '#0B3A46']

let dataLength = ''
let colorBarCyclesBackgroundColors = ''
let colorBarCyclesBorderColors = ''


if (isWomen) {
    dataLength = graphsData.cycles.cycles_follow_up.length;
    colorBarCyclesBackgroundColors = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(201, 203, 207, 0.2)'
    ];
    colorBarCyclesBorderColors = [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(153, 102, 255)',
        'rgb(201, 203, 207)'
    ];
}

const checkGraphsInstances = () => {

    if (graph_meals_follow_up_instance !== null) {
        graph_meals_follow_up_instance.destroy();
    }

    if (graph_meals_by_name_instance !== null) {
        graph_meals_by_name_instance.destroy();
    }

    if (graph_meals_by_day_instance !== null) {
        graph_meals_by_day_instance.destroy();
    }

    if (graph_activities_follow_up_instance !== null) {
        graph_activities_follow_up_instance.destroy();
    }

    if (isWomen) {
        if (graph_cycles_follow_up_instance !== null) {
            graph_cycles_follow_up_instance.destroy();
        }
    }

    if (graph_weights_follow_up_instance !== null) {
        graph_weights_follow_up_instance.destroy();
    }

}

function initChart() {

    if (current_route !== 'bilan.show') {
        return
    }

    checkGraphsInstances()

    const chartResponsiveOptions = {
        responsive: true,
        maintainAspectRatio: isMobile() ? false : true,
    };
    const {
        minDateMeals,
        maxDateMeals,
        minYMeals,
        maxYMeals,
    } = computeGraphScales(graphsData.meals.meals_follow_up, 'Meals')
    let graph_meals_follow_up = document.getElementById('graph_meals_follow_up').getContext('2d');
    graph_meals_follow_up_instance = new Chart(graph_meals_follow_up, {
        type: 'line',
        data: {
            datasets: [{
                data: graphsData.meals.meals_follow_up,
                label: graphsData.meals.meals_follow_up_label,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.4,
                borderWidth: 4,
                segment: {
                    borderColor: ctx => {
                        if (ctx.p0.parsed.y > user_daily_points) {
                            return user_daily_points_up;
                        } else {
                            return user_daily_points_down;
                        }
                    }
                },
            }]
        },
        options: {
            ...chartResponsiveOptions,
            transitions: {
                zoom: {
                    animation: {
                        duration: 1000,
                        easing: 'easeOutCubic'
                    }
                }
            },
            scales: {
                x: {
                    type: 'time',
                    distribution: 'linear',
                    min: new Date(minDateMeals).valueOf(),
                    max: new Date(maxDateMeals).valueOf(),
                    time: {
                        unit: 'week',
                        tooltipFormat: 'eee dd MMM yy',
                        displayFormats: {
                            'day': 'eee dd MMM yy',
                            'week': 'eee dd MMM yyyy',
                            'month': 'eee dd MMM yyyy',
                            'quarter': 'eee dd MMM yyyy',
                            'year': 'eee dd MMM yyyy'
                        },
                    },
                    adapters: {
                        date: {
                            locale: fr
                        }
                    },
                },
                y: {
                    beginAtZero: false,
                    min: Math.max(1, Math.floor(minYMeals * 0.9)) > 0 ? Math.max(1, Math.floor(minYMeals * 0.9)) : 0,
                    max: Math.ceil(maxYMeals * 1.1),
                },
            },
            plugins: {
                legend: {
                    labels: {
                        //color: GraphLegendColor
                    }
                },
                tooltip: {
                    callbacks: {
                        title: function (context) {
                            return context.label;
                        },
                        label: function (context) {
                            const pointsLabel = context.parsed.y === 2
                                ? legendData.graph_meals_follow_up.points.slice(0, -1)
                                : legendData.graph_meals_follow_up.points;
                            return ' ' + context.parsed.y + ' ' + pointsLabel;
                        },
                        footer: function (context) {
                            const pointsLabel = context[0].raw.average === 1
                                ? legendData.graph_meals_follow_up.points.slice(0, -1)
                                : legendData.graph_meals_follow_up.points;
                            let footer = [legendData.graph_meals_follow_up.number + ': ' + context[0].raw.number];
                            footer.push(legendData.graph_meals_follow_up.average + ': ' + context[0].raw.average + ' ' + pointsLabel);
                            return footer;
                        }
                    }
                },
                zoom: {
                    pan: {
                        enabled: true,
                        mode: 'xy',
                    },
                    zoom: {
                        wheel: {
                            enabled: true,
                        },
                        pinch: {
                            enabled: true,
                        },
                        mode: 'xy',
                    }
                }
            },
        }
    });
    graph_meals_follow_up_instance.render()

    let graph_meals_by_name = document.getElementById('graph_meals_by_name').getContext('2d');
    graph_meals_by_name_instance = new Chart(graph_meals_by_name, {
        type: 'bubble',
        data: {
            datasets: [{
                data: graphsData.meals.meals_by_name,
                label: graphsData.meals.meals_by_name_label,
                backgroundColor: colorsBubbleMealsName,
                hoverBackgroundColor: colorsBubbleMealsNameHover
            }]
        },
        options: {
            ...chartResponsiveOptions,
            plugins: {
                legend: {
                    labels: {
                        //color: GraphLegendColor
                    }
                },
                tooltip: {
                    callbacks: {
                        label: function (context) {
                            return context.raw.label
                        },
                        footer: function (context) {
                            const pointsLabel = context[0].raw.number === 1
                                ? legendData.graph_meals_follow_up.points.slice(0, -1)
                                : legendData.graph_meals_follow_up.points;
                            let footer = [legendData.graph_meals_by_name.number + ': ' + context[0].raw.number];
                            footer.push(legendData.graph_meals_by_name.average + ': ' + context[0].raw.average + ' ' + pointsLabel);
                            return footer;
                        }
                    }
                },
            },
        }
    });

    let graph_meals_by_day = document.getElementById('graph_meals_by_day').getContext('2d');
    graph_meals_by_day_instance = new Chart(graph_meals_by_day, {
        type: 'bubble',
        data: {
            datasets: [{
                data: graphsData.meals.meals_by_day,
                label: graphsData.meals.meals_by_day_label,
                backgroundColor: colorsBubbleMealsDay,
                hoverBackgroundColor: colorsBubbleMealsDayHover
            }]
        },
        options: {
            ...chartResponsiveOptions,
            plugins: {
                legend: {
                    labels: {
                        //color: GraphLegendColor
                    }
                },
                tooltip: {
                    callbacks: {
                        label: function (context) {
                            return context.raw.label
                        },
                        footer: function (context) {
                            const pointsLabel = context[0].raw.number === 1
                                ? legendData.graph_meals_follow_up.points.slice(0, -1)
                                : legendData.graph_meals_follow_up.points;
                            let footer = [legendData.graph_meals_by_day.number + ': ' + context[0].raw.number];
                            footer.push(legendData.graph_meals_by_day.average + ': ' + context[0].raw.average + ' ' + pointsLabel);
                            return footer;
                        }
                    }
                },
            },
        }
    });

    let graph_activities_follow_up = document.getElementById('graph_activities_follow_up').getContext('2d');
    graph_activities_follow_up_instance = new Chart(graph_activities_follow_up, {
        type: 'doughnut',
        data: {
            labels: graphsData.activities.activities_follow_up.labels,
            datasets: [{
                label: 'Activities Follow Up',
                data: graphsData.activities.activities_follow_up.data,
                backgroundColor: graphsData.activities.activities_follow_up.colors,
                hoverOffset: 20,
                cutout: '70%',
            }]
        },
        options: {
            ...chartResponsiveOptions,
            plugins: {
                legend: {
                    position: 'top', // Set legend position to bottom
                    labels: {
                        //color: GraphLegendColor
                    }
                },
                tooltip: {
                    callbacks: {
                        title: function (context) {
                            return context[0].dataset.label;
                        },
                        label: function (context) {
                            const pointsLabel = context.raw.value === 1
                                ? legendData.graph_activities_follow_up.seances.slice(0, -1)
                                : legendData.graph_activities_follow_up.seances;
                            if (context.raw.value > 1) {
                                return ' ' + context.raw.label + ' : ' + context.raw.value + ' ' + pointsLabel
                            } else {
                                return ' ' + context.raw.label + ' : ' + context.raw.value + ' ' + pointsLabel
                            }
                        },
                        footer: function (context) {
                            let sum = 0;
                            let dataArr = context[0].dataset.data;
                            dataArr.map(data => {
                                sum += Number(data.value);
                            });

                            let percentage = (context[0].parsed * 100 / sum).toFixed(0) + '%';
                            return `${percentage} ${legendData.graph_activities_follow_up.percentage}`;
                        }
                    }
                },
            },
        }
    });
    graph_activities_follow_up_instance.render();

    if (isWomen) {

        const {
            minDateCycles,
            maxDateCycles,
            minYCycles,
            maxYCycles,
        } = computeGraphScales(graphsData.cycles.cycles_follow_up, 'Cycles')
        const backgroundColorsCycles = Array.from({length: dataLength}, (_, i) => colorBarCyclesBackgroundColors[i % colorBarCyclesBackgroundColors.length]);
        const borderColorsCycles = Array.from({length: dataLength}, (_, i) => colorBarCyclesBorderColors[i % colorBarCyclesBorderColors.length]);
        let graph_cycles_follow_up = document.getElementById('graph_cycles_follow_up').getContext('2d');
        graph_cycles_follow_up_instance = new Chart(graph_cycles_follow_up, {
            type: 'bar', // Change from 'line' to 'bar'
            data: {
                datasets: [{
                    data: graphsData.cycles.cycles_follow_up,
                    label: legendData.graph_cycles_follow_up.label,
                    backgroundColor: backgroundColorsCycles,
                    borderColor: borderColorsCycles,
                    borderWidth: 1
                }]
            },
            options: {
                ...chartResponsiveOptions,
                scales: {
                    x: {
                        type: 'time',
                        distribution: 'linear',
                        min: new Date(minDateCycles).valueOf(),
                        max: new Date(maxDateCycles).valueOf(),
                        time: {
                            unit: 'week', // 'day', 'week', 'month', 'quarter', 'year'
                            tooltipFormat: 'eee dd MMM yy',
                            displayFormats: {
                                'day': 'eee dd MMM yy',
                                'week': 'eee dd MMM yyyy',
                                'month': 'eee dd MMM yyyy',
                                'quarter': 'eee dd MMM yyyy',
                                'year': 'eee dd MMM yyyy'
                            },
                        },
                        adapters: {
                            date: {
                                locale: fr
                            }
                        },
                    },
                    y: {
                        beginAtZero: false,
                        min: Math.max(1, Math.floor(minYCycles * 0.9)) > 0 ? Math.max(1, Math.floor(minYCycles * 0.9)) : 0,
                        max: Math.ceil(maxYCycles * 1.1),
                    },
                },
                plugins: {
                    legend: {
                        labels: {
                            //color: GraphLegendColor
                        }
                    },
                    tooltip: {
                        callbacks: {
                            title: function (context) {
                                return context.label;
                            },
                            label: function (context) {
                                const pointsLabel = context.parsed.y === 1
                                    ? legendData.graph_cycles_follow_up.days.slice(0, -1)
                                    : legendData.graph_cycles_follow_up.days;
                                return ' ' + context.parsed.y + ' ' + pointsLabel;
                            }
                        }
                    },
                },
            }
        });
        graph_cycles_follow_up_instance.render()

    }

    const {
        minDateWeights,
        maxDateWeights,
        minYWeights,
        maxYWeights
    } = computeGraphScales(graphsData.weights.weights_follow_up, 'Weights')

    let graph_weights_follow_up = document.getElementById('graph_weights_follow_up').getContext('2d');
    graph_weights_follow_up_instance = new Chart(graph_weights_follow_up, {
        type: 'line',
        data: {
            datasets: [{
                data: graphsData.weights.weights_follow_up,
                label: legendData.graph_weights_follow_up.label,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.4,
                borderWidth: 4,
            }]
        },
        options: {
            ...chartResponsiveOptions,
            transitions: {
                zoom: {
                    animation: {
                        duration: 1000,
                        easing: 'easeOutCubic'
                    }
                }
            },
            scales: {
                x: {
                    type: 'time',
                    distribution: 'linear',
                    min: new Date(minDateWeights).valueOf(),
                    max: new Date(maxDateWeights).valueOf(),
                    time: {
                        unit: 'week', // 'day', 'week', 'month', 'quarter', 'year'
                        tooltipFormat: 'eee dd MMM yy',
                        displayFormats: {
                            'day': 'eee dd MMM yy',
                            'week': 'eee dd MMM yyyy',
                            'month': 'eee dd MMM yyyy',
                            'quarter': 'eee dd MMM yyyy',
                            'year': 'eee dd MMM yyyy'
                        },
                    },
                    adapters: {
                        date: {
                            locale: fr
                        }
                    },
                },
                y: {
                    beginAtZero: false,
                    min: Math.max(1, Math.floor(minYWeights * 0.9)) > 0 ? Math.max(1, Math.floor(minYWeights * 0.9)) : 0,
                    max: Math.ceil(maxYWeights * 1.1),
                },
            },
            plugins: {
                legend: {
                    labels: {
                        //color: GraphLegendColor
                    }
                },
                tooltip: {
                    callbacks: {
                        title: function (context) {
                            return context.label;
                        },
                        label: function (context) {
                            return ' ' + context.parsed.y + ' ' + legendData.graph_weights_follow_up.unit;
                        },
                    }
                },
                zoom: {
                    pan: {
                        enabled: true,
                        mode: 'xy',
                    },
                    zoom: {
                        wheel: {
                            enabled: true,
                        },
                        pinch: {
                            enabled: true,
                        },
                        mode: 'xy',
                    }
                }
            },
        }
    });
    graph_weights_follow_up_instance.render()

}

function computeGraphScales(data, legendData) {

    let result = {}

    if (data.length > 0) {
        const firstXValue = data[0].x;
        const lastXValue = data[data.length - 1].x;
        const maxYValue = Math.max(...data.map(data => data.y));
        const minYValue = Math.min(...data.map(data => data.y));
        const roundedMaxYValue = Math.ceil(maxYValue / 10 + 1) * 10;
        const roundedMinYValue = Math.floor(minYValue / 10 - 1) * 10;

        result['minDate' + legendData] = firstXValue;
        result['maxDate' + legendData] = lastXValue;
        result['minY' + legendData] = roundedMinYValue;
        result['maxY' + legendData] = roundedMaxYValue;

    } else {
        result['minDate' + legendData] = null;
        result['maxDate' + legendData] = null;
        result['minY' + legendData] = 0;
        result['maxY' + legendData] = 0;
    }

    return result;

}

Livewire.on('updateGraphScale', (data) => {

    const response = JSON.parse(data)

    switch (response.graph) {
        case 'graph_meals_follow_up':
            updateGraphScale(graph_meals_follow_up_instance, response.scale);
            break;
        case 'graph_activities_follow_up':
            updateGraphScale(graph_activities_follow_up_instance, response.scale);
            break;
        case 'graph_cycles_follow_up':
            updateGraphScale(graph_cycles_follow_up_instance, response.scale);
            break;
        case 'graph_weights_follow_up':
            updateGraphScale(graph_weights_follow_up_instance, response.scale);
            break;
    }

});

Livewire.on('updateGraphData', (data) => {

    const response = JSON.parse(data)

    if (!response.data) {
        return
    }

    switch (response.graph) {
        case 'graph_meals_follow_up':
            updateGraph(graph_meals_follow_up_instance, response.data);
            break;
        case 'graph_activities_follow_up':
            updateGraph(graph_ativities_follow_up_instance, response.data);
            break;
        case 'graph_cycles_follow_up':
            updateGraph(graph_cycles_follow_up_instance, response.data);
            break;
        case 'graph_weights_follow_up':
            updateGraph(graph_weights_follow_up_instance, response.data);
            break;
    }

});
Livewire.on('resetZoom', (chart) => {
    const chartId = chart.id;
    const chartInstances = {
        'graph_meals_follow_up': graph_meals_follow_up_instance,
        'graph_weights_follow_up': graph_weights_follow_up_instance,
    };
    if (chartInstances[chartId]) {
        chartInstances[chartId].resetZoom();
    }
});

function updateGraphScale(instance, scale) {
    instance.options.scales.x.time.unit = scale;
    instance.update();
    instance.resize();
}

function updateGraph(instance, data) {
    const {minDate, maxDate, minY, maxY} = computeGraphScales(data);
    instance.data.datasets[0].data = data;
    instance.options.scales.x.min = new Date(minDate).valueOf();
    instance.options.scales.x.max = new Date(maxDate).valueOf();
    instance.options.scales.y.min = minY;
    instance.options.scales.y.max = maxY;
    instance.update();
    instance.resize();
    instance.resetZoom();
}

function initCollapsibles() {
    const bulmaCollapsibleInstances = bulmaCollapsible.attach('.is-collapsible');
    bulmaCollapsibleInstances.forEach(bulmaCollapsibleInstance => {
        bulmaCollapsibleInstance.collapse(); // Close by default

        let currentScrollPosition = window.scrollY;

        bulmaCollapsibleInstance.on('before:expand', () => {
            currentScrollPosition = window.scrollY;
        });

        bulmaCollapsibleInstance.on('after:expand', () => {
            window.scrollTo(0, currentScrollPosition);
        });

        bulmaCollapsibleInstance.on('before:collapse', () => {
            currentScrollPosition = window.scrollY;
        });

        bulmaCollapsibleInstance.on('after:collapse', () => {
            window.scrollTo(0, currentScrollPosition);
        });
    });
}


// Listen for the livewire:navigated event
events.forEach(event => {
    document.addEventListener(event, () => {
        initChart();
        if (event === 'livewire:navigated') {
            initCollapsibles();
        }
    });
});


