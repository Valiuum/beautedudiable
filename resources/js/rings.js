const animateValue = (element, endValue, duration, action = 'init', className = null) => {
    let startValue = 0;
    let run = true;
    let startTime = null;
    const svg = element.closest('div.ring-value').previousElementSibling;
    const circle = svg.querySelector('circle:not(.background)');
    const ring = svg.querySelector('g.ring');
    const nextElement = element.nextElementSibling;
    const initClass = 'init';
    const hiddenClass = 'is-hidden';
    const completedClass = 'completed';

    // Simulate ease-in-out effect
    const easeInOutQuad = (t) => t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;

    const step = (timestamp) => {
        if (!startTime) startTime = timestamp;
        const elapsed = timestamp - startTime;
        const progress = Math.min(elapsed / duration, 1);
        const easedProgress = easeInOutQuad(progress);
        const currentValue = Math.round(easedProgress * (endValue - startValue) + startValue);

        if (nextElement.classList.contains(initClass)) {
            nextElement.classList.add(hiddenClass);
        }
        element.classList.remove(hiddenClass);
        element.textContent = currentValue;

        if (progress < 1) {
            requestAnimationFrame(step);
            if (action === 'update' && run && easedProgress > 0.1) {
                circle.classList.add(completedClass);
                ring.classList.add(className);
                run = false;
            }
        } else {
            element.textContent = endValue; // Ensure it ends on the exact value, mimicking 'forwards'
        }
    };
    requestAnimationFrame(step);
}

const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

window.homeCard = () => {
    return {
        ringsData: [],
        init(data) {
            this.ringsData = data.data.map(item => {
                let day = item.day
                let frenchDays = item.day_fr
                return {...item, day_fr: frenchDays, day: day, ringName: 'daily-points-' + day.toLowerCase()};
            });
            this.ringsData.forEach((item, index) => {
                this.buildRings(item, index)
            });
        },
        buildRings(ring) {
            const isToday = ring.isToday ? ring.day.toLowerCase() + ' today' : ''
            const className = isToday + ' ' + ring.day.toLowerCase()
            const circleClass = ring.value > 100 ? 'fail' : 'success'
            const svg = `
            <svg class="rings ${className} is-position-relative"  viewBox='0 0 37 37'>
                <g class="ring ${circleClass}" style="transform: scale(1) rotate(-90deg);">
                    <circle stroke-width="3" r="15.915" cx="50%" cy="50%" class="background" />
                    <circle stroke-width="3" r="15.915" cx="50%" cy="50%" stroke-dasharray="0, 100" />
                </g>
            </svg>`
            const svgValue = `
            <div class="is-flex is-justify-content-center is-align-content-center is-align-items-center is-position-relative has-text-centered ring-value ${ring.day.toLowerCase()} ${isToday}">
                <span class="has-text-weight-semibold is-size-5 is-flex is-justify-content-center is-align-content-center is-align-items-center is-hidden">${ring.points}</span>
                <span class="has-text-weight-semibold is-size-5 init"></span>
            </div>
            `
            const ringsContainer = document.getElementById('rings-home-card');
            if (!ringsContainer) return;

            let ringContainer = document.createElement('div');
            ringContainer.className = `column is-flex is-justify-content-center is-align-content-center is-align-items-center has-text-centered ring-container ${ring.day.toLowerCase()} ${isToday}`;
            ringContainer.innerHTML = svg;
            ringContainer.innerHTML += svgValue;
            ringsContainer.appendChild(ringContainer);
            ringContainer.insertAdjacentHTML('beforebegin', `<p class="is-size-5 is-hidden-touch ring-day has-text-weight-semibold">${capitalizeFirstLetter(ring.day_fr)}</p>`)
        },
        rings() {
            this.updateRingAndBonus('init')
        },
        updateRingAndBonus: function (action, element = null) {
            if (action === 'init') {
                const svgs = document.querySelectorAll('.rings');
                svgs.forEach((svg, index) => {
                    const dayClass = svg.classList[1];
                    const circle = svg.querySelector('circle:not(.background)');
                    const ringData = this.ringsData.find(ring => ring.day.toLowerCase() === dayClass);
                    if (ringData) {
                        setTimeout(() => {
                            const textRing = svg.nextElementSibling.querySelector('span')
                            const endValue = parseFloat(textRing.textContent);
                            animateValue(textRing, endValue, 800);
                            const newDashArray = `${ringData.value}, 100`;
                            circle.setAttribute('stroke-dasharray', newDashArray);
                            if (ringData.value !== 0) {
                                circle.classList.add('completed');
                            }
                        }, 150 * index);
                    }
                });
            } else if (action === 'update') {
                const svg = document.querySelector(`.rings.${element.day.toLowerCase()}`)
                const gRing = svg.querySelector('g.ring')
                const circle = svg.querySelector('circle:not(.background)')
                const newDashArray = `${element.value}, 100`
                const textRing = svg.nextElementSibling.querySelector('span')
                const endValue = parseFloat(element.points);
                circle.classList.remove('completed')
                gRing.classList.remove('fail', 'success')
                circle.setAttribute('stroke-dasharray', newDashArray)
                const className = element.value > 100 ? 'fail' : 'success'
                animateValue(textRing, endValue, 2000, 'update', className);
                const bonus = document.getElementById('user-bonus-points')
                bonus.textContent = element.bonus
            }
            setTimeout(() => {
                if (current_route === 'home') {
                    if (desktop) {
                        const ringsHomeCard = document.getElementById('rings-home-card');
                        const todayElement = ringsHomeCard.querySelector('.ring-container.today');
                        if (todayElement) {
                            todayElement.scrollIntoView({behavior: 'smooth', block: 'nearest'});
                        }
                    } else {
                        document.querySelectorAll('.ring-container').forEach(element => {
                            if (element.classList.contains('today')) {
                                const rect = element.getBoundingClientRect();
                                const isInView = (
                                    rect.top >= 0 &&
                                    rect.left >= 0 &&
                                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
                                );

                                if (!isInView) {
                                    element.scrollIntoView({behavior: 'smooth', inline: 'center', block: 'nearest'});
                                }
                            }
                        });
                    }
                }
            }, 300)
        },
    }
}
