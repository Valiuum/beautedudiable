import {darkModePreference} from "./app.js";

window.items = () => {

    return {
        ingredients: [],
        ingredientsOriginals: [],
        dishes: [],
        userBonus: {initial: 0, maxValue: 0},
        cards: [
            {id: 'points', data: 0, icon: '123', title: ingredientWW, unit: ''},
            {id: 'energy', data: 0, icon: 'bolt', title: ingredientEnergy, unit: ''},
            {id: 'proteins', data: 0, icon: 'egg', title: ingredientProteins, unit: 'g'},
            {id: 'lipids', data: 0, icon: 'bakery_dining', title: ingredientLipids, unit: 'g'},
            {id: 'carbohydrates', data: 0, icon: 'icecream', title: ingredientCarbohydrates, unit: 'g'},
            {id: 'fibers', data: 0, icon: 'breakfast_dining', title: ingredientFibers, unit: 'g'},
        ],
        darkMode: false,

        updateBonusPoints(action) {
            if (action === 'increment') {
                if (this.userBonus.initial < this.userBonus.maxValue) {
                    this.userBonus.initial += 1;
                    let pointsCard = this.cards.find(card => card.id === 'points');
                    if (pointsCard && pointsCard.data >= 1) {
                        pointsCard.data -= 1;
                    } else {
                        this.userBonus.initial -= 1;
                        let element = document.getElementById('search_food');
                        const notify = {type: 'warning', message: minBonusPoints}
                        element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                    }
                } else {
                    let element = document.getElementById('search_food');
                    const notify = {type: 'warning', message: maxBonusPoints}
                    element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                }
            } else if (action === 'decrement') {
                if (this.userBonus.initial > 0) {
                    this.userBonus.initial -= 1;
                    let pointsCard = this.cards.find(card => card.id === 'points');
                    if (pointsCard) {
                        pointsCard.data += 1;
                    }
                } else {
                    let element = document.getElementById('search_food');
                    const notify = {type: 'warning', message: minBonusPoints}
                    element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                }
            }
        },

        updateIngredient(item, action) {

            let ingredient = this.ingredients.find(ingredient => ingredient.id === item.id);

            if (!ingredient) {
                return false;
            }

            const updatePoint = 0.5;

            // Calculate original points for the ingredient
            let originalPoints = ingredient.points;

            if (action === 'increment' && ingredient.points !== 0) {
                // Calculate proportion based on original points
                let proportion = originalPoints !== 0 ? (originalPoints + updatePoint) / originalPoints : 1;
                // Update the ingredient and card data using the calculated proportion
                this.updateProportionally(ingredient, updatePoint, action, proportion);
            } else if (action === 'increment' && ingredient.points === 0) {
                let ingredientOriginal = this.ingredientsOriginals.find(ingredientOriginal => ingredientOriginal.id === item.id);
                let proportion = (ingredientOriginal.points) / updatePoint;
                this.updateProportionally(ingredient, updatePoint, action, proportion);
            } else if (action === 'decrement' && ingredient.points !== 0 && this.cards.find(card => card.id === 'points').data > 0) {
                // Calculate proportion based on original points
                let proportion = originalPoints !== 0 ? (originalPoints - updatePoint) / originalPoints : 1;
                // Update the ingredient and card data using the calculated proportion
                this.updateProportionally(ingredient, -updatePoint, action, proportion);
            }
        },

        updateDishIngredient(item, action) {

            let ingredient = null;
            let dishId = item.dish_id;
            let dish = this.dishes.find(dish => dish.id === dishId);
            if (dish) {
                ingredient = dish.ingredients.find(ingredient => ingredient.id === item.id);
            }

            if (!ingredient) {
                return false;
            }

            const updatePoint = 0.5;

            // Calculate original points for the ingredient
            const originalPoints = ingredient.dish_meal_points ? ingredient.dish_meal_points : ingredient.dish_ingredient_points;

            if (action === 'increment' && originalPoints !== 0) {
                // Calculate proportion based on original points
                let proportion = originalPoints !== 0 ? (originalPoints + updatePoint) / originalPoints : 1;
                // Update the ingredient and card data using the calculated proportion
                dish.total += updatePoint;
                this.updateDishProportionally(ingredient, updatePoint, action, proportion);
            } else if (
                (action === 'increment' && originalPoints === 0) ||
                (action === 'decrement' && originalPoints === 0) ||
                (action === 'decrement' && ingredient.dish_meal_points === 0.5) ||
                (action === 'decrement' && ingredient.dish_ingredient_points === 0.5)) {
                let element = document.getElementById('search_food');
                const notify = {type: 'warning', message: notAllowed}
                element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
            } else if (action === 'decrement' && originalPoints !== 0) {
                // Calculate proportion based on original points
                dish.total -= updatePoint;
                let proportion = originalPoints !== 0 ? (originalPoints - updatePoint) / originalPoints : 1;
                // Update the ingredient and card data using the calculated proportion
                this.updateDishProportionally(ingredient, -updatePoint, action, proportion);
            }

        },

        updateDishProportionally(ingredient, updatePoint, action, proportion) {

            if (ingredient.hasOwnProperty('dish_meal_points')) {
                ingredient.dish_meal_points += updatePoint;
                // Update other fields proportionally
                for (let key in ingredient) {
                    /*if (key !== 'points' && ingredient.hasOwnProperty(key) && (ingredient[key] === null || ingredient[key] === 0)) {
                        proportion = 1;
                    }*/
                    switch (key) {
                        case 'quantity':
                        case 'energy':
                        case 'proteins':
                        case 'lipids':
                        case 'carbohydrates':
                        case 'fibers':
                            ingredient[key] *= proportion;
                            break;
                    }
                }
            } else {
                ingredient.dish_ingredient_points += updatePoint;
                // Update other fields proportionally
                for (let key in ingredient) {
                    /*if (key !== 'points' && ingredient.hasOwnProperty(key) && (ingredient[key] === null || ingredient[key] === 0)) {
                        proportion = 1;
                    }*/
                    switch (key) {
                        case 'quantity':
                        case 'energy':
                        case 'proteins':
                        case 'lipids':
                        case 'carbohydrates':
                        case 'fibers':
                            ingredient[key] *= proportion;
                            break;
                    }
                }
            }

            // Update card data proportionally
            this.updateCardData(action, ingredient, proportion);
        },

        updateProportionally(ingredient, updatePoint, action, proportion) {

            const ingredientOriginal = this.ingredientsOriginals.find(ingredientOriginal => ingredientOriginal.id === ingredient.id);

            if (ingredient.points === 0 && action === 'increment') {
                ingredient.points += updatePoint;

                if (ingredientOriginal.points !== 0) {
                    // Update other fields proportionally
                    for (let key in ingredient) {
                        /*if (key !== 'points' && ingredient.hasOwnProperty(key) && (ingredient[key] === null || ingredient[key] === 0)) {
                            proportion = 1;
                        }*/
                        switch (key) {
                            case 'quantity':
                            case 'energy':
                            case 'proteins':
                            case 'lipids':
                            case 'carbohydrates':
                            case 'fibers':
                                ingredient[key] = Math.round(ingredientOriginal[key] / proportion * 10) / 10;
                                break;
                        }
                    }
                } else {
                    const pointsCard = this.cards.find(card => card.id === 'points');
                    if (action === 'increment') {
                        pointsCard.data += 0.5;
                    } else {
                        pointsCard.data -= 0.5;
                    }
                    return
                }
            } else {
                // check original ingredients points and update proportion
                ingredient.points += updatePoint;
                // Update other fields proportionally
                if (ingredientOriginal.points !== 0) {
                    for (let key in ingredient) {
                        switch (key) {
                            case 'quantity':
                            case 'energy':
                            case 'proteins':
                            case 'lipids':
                            case 'carbohydrates':
                            case 'fibers':
                                ingredient[key] = Math.round(ingredientOriginal[key] / proportion * 10) / 10;
                                break;
                        }
                    }
                } else {
                    const pointsCard = this.cards.find(card => card.id === 'points');
                    if (action === 'increment') {
                        pointsCard.data += 0.5;
                    } else {
                        pointsCard.data -= 0.5;
                    }
                    return
                }
            }

            // Update card data proportionally
            this.updateCardData(action, ingredient, proportion);
        },

        updateCardData(action, item, proportion = null) {
            Object.entries(item).forEach(([key, value]) => {
                let card = this.cards.find(card => card.id === key);
                if (card) {
                    if (action === 'add' || action === 'increment' || action === 'addFromDish') {
                        if (proportion !== null) {
                            // Update other fields proportionally
                            if (card.id === 'points') {
                                card.data += 0.5;
                            } else {
                                for (let key in card) {
                                    if (key === 'data') {
                                        if (card[key] === 0) {
                                            for (let key in item) {
                                                if (key === card.id) {
                                                    card.data = value;
                                                }
                                            }
                                        } else {
                                            let value = Math.round(card[key] * proportion * 10) / 10; // Round to nearest 0.1
                                            card[key] = value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (action === 'addFromDish') {
                                // Use meal_dish_points if it exists, otherwise use meal_ingredient_points
                                const points = item.dish_meal_points ? item.dish_meal_points : item.dish_ingredient_points;
                                if (key === 'points') {
                                    value = Math.round(points * 2) / 2; // Round to nearest 0.5
                                }
                            } else {
                                if (key === 'points') {
                                    value = Math.round(value * 2) / 2; // Round to nearest 0.5
                                }
                            }
                            card.data += Math.round(value * 10) / 10;
                        }
                    } else if (action === 'decrement') {
                        if (proportion !== null) {
                            // Update other fields proportionally
                            if (card.id === 'points') {
                                card.data -= 0.5;
                            } else {
                                for (let key in card) {
                                    if (key === 'data') {
                                        let value = Math.round(card[key] * proportion * 10) / 10; // Round to nearest 0.1
                                        card[key] = value;
                                    }
                                }
                            }
                        } else {
                            if (key === 'points') {
                                value = Math.round(value * 2) / 2
                            }
                            card.data -= value;
                        }
                    } else if (action === 'remove' || action === 'removeFromDish') {
                        if (this.ingredients.length === 0) {
                            this.cards.forEach(card => {
                                card.data = 0;
                            })
                        } else {
                            if (action === 'removeFromDish') {
                                const points = item.dish_meal_points ? item.dish_meal_points : item.dish_ingredient_points;
                                card.data = Math.round((card.data - points) * 10) / 10;
                            } else {
                                card.data = Math.round((card.data - value) * 10) / 10;
                            }
                        }
                    }
                    card.data = parseFloat(card.data.toFixed(1));
                    if (isNaN(card.data) || !isFinite(card.data)) {
                        card.data = 0;
                    }
                }
            })
        },

        initMeal(items) {
            if (!Array.isArray(items)) {
                Object.entries(items.dishes).reverse().forEach(([key, values]) => {
                    this.addDish({id: parseInt(key), ingredients: values});
                });
                items.ingredients.reverse().forEach(item => {
                    this.addIngredient({dataset: {elements: JSON.stringify(item)}});
                });
                if (items.hasOwnProperty('user_bonus')) {
                    this.userBonus.maxValue = items.user_bonus;
                }
            }

            // set local storage
            this.colorScheme();
        },

        addIngredient(element) {

            let item = JSON.parse(element.dataset.elements);

            // Check if the ingredient is already present in the ingredients array
            let ingredientExists = this.ingredients.some(ingredient => ingredient.id === item.id);
            // Check if the ingredient is already present in the dishes array
            let ingredientExistsInDishes = this.dishes.some(dish => {
                return dish.ingredients.some(ingredient => ingredient.id === item.id);
            });

            if (ingredientExists || ingredientExistsInDishes) {
                let element = document.getElementById('search_food');
                const message = this.translate('ingredientsAlreadyAdded', 1, {values: item.name});
                const notify = {type: 'warning', message: message}
                element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                return;
            }

            if (item.hasOwnProperty('meal_current_points')) {
                let proportion = item.name === "bonus" ? 1 : item.points / item.meal_current_points;
                /*if (proportion === 0) {
                    proportion = 1;
                }*/
                for (let key in item) {
                    switch (key) {
                        case 'id':
                        case 'name':
                        case 'description':
                        case 'open_food_facts_id':
                        case 'quantity':
                        case 'unit':
                            continue;
                        case 'points':
                            item[key] = item.meal_current_points;
                            break;
                        default:
                            item[key] = Math.round((item[key] / proportion) * 10) / 10;
                            break;
                    }
                }
                delete item.meal_current_points;
            }

            const colors = this.generateRandomColor();
            item.backgroundColor = colors.backgroundColor;
            item.color = colors.color;
            this.ingredients.unshift(item);
            this.ingredientsOriginals = JSON.parse(JSON.stringify(this.ingredients));
            this.updateCardData('add', item, null);
        },

        addDish: function (element) {

            const dish_id = element.id;
            const dish_ingredients = element.ingredients;
            const dish_ingredients_sum = dish_ingredients.reduce((total, ingredient) => {
                return total + (ingredient.dish_meal_points ? ingredient.dish_meal_points : ingredient.dish_ingredient_points);
            }, 0);

            // Check if the dish is already present in the dishes array
            const dishExists = this.dishes.some(dish => dish.id === dish_id);
            if (dishExists) {
                let element = document.getElementById('search_food');
                const message = dishAlreadyAdded;
                const notify = {type: 'warning', message: message}
                element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                return;
            }

            // Check if the ingredients are already present in the ingredients array
            let ingredientsAlreadyPresents = [];
            const dishIngredientIds = dish_ingredients.map(ingredient => ingredient.id);
            this.ingredients.forEach(ingredient => {
                if (dishIngredientIds.includes(ingredient.id)) {
                    ingredientsAlreadyPresents.push(ingredient.name);
                }
            });

            if (ingredientsAlreadyPresents.length > 0) {
                let element = document.getElementById('search_food');
                const message = this.translate('ingredientsAlreadyAdded', ingredientsAlreadyPresents.length, {values: ingredientsAlreadyPresents.join(', ')});
                const notify = {type: 'warning', message: message}
                element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
                return;
            }

            // Check if the ingredients are already present in other dishes
            let ingredientExistsInOtherDishes = this.dishes.some(dish => {
                return dish.id !== dish_id && dish.ingredients.some(ingredient => dishIngredientIds.includes(ingredient.id));
            });

            if (ingredientExistsInOtherDishes) {
                let duplicateIngredients = [];
                dish_ingredients.forEach(ingredient => {
                    this.dishes.forEach(dish => {
                        if (dish.ingredients.some(dishIngredient => dishIngredient.id === ingredient.id)) {
                            duplicateIngredients.push(ingredient.name);
                        }
                    });
                });
                let element = document.getElementById('search_food');
                const message = this.translate('ingredientsAlreadyAdded', duplicateIngredients.length, {values: duplicateIngredients.join(', ')});
                const notify = {type: 'warning', message: message}
                element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))

            } else {

                const tag = document.getElementById('dish_tag_' + dish_id);
                let dish_name = '';
                if (tag) {
                    dish_name = JSON.parse(tag.dataset.dish);
                } else {
                    dish_name = element.dish_name ?? element.ingredients[0].dish_name;
                }

                dish_ingredients.forEach(ingredient => {
                    const colors = this.generateRandomColor();
                    ingredient.backgroundColor = colors.backgroundColor;
                    ingredient.color = colors.color;
                })
                const colors = this.generateRandomColor();
                this.dishes.unshift({
                    id: dish_id,
                    name: dish_name,
                    backgroundColor: colors.backgroundColor,
                    color: colors.color,
                    border: this.generateHarmoniousColor(colors.backgroundColor),
                    total: dish_ingredients_sum,
                    ingredients: dish_ingredients
                });
                let dish = this.dishes.filter(dish => dish.id === dish_id)
                let from_dish = dish[0].ingredients
                from_dish.forEach(ingredient => {
                    this.updateCardData('addFromDish', ingredient, null);
                })
            }
        },

        tags(e, action, duration = 500) {
            if (action === 'show') {
                e.classList.remove('is-hidden');
                //e.classList.add('animate__fadeIn');
            } else if (action === 'hide') {
                setInterval(() => {
                    e.classList.add('is-hidden');
                }, duration);
            }
        },

        removeIngredient(item) {
            this.ingredients = this.ingredients.filter(ingredient => ingredient.id !== item.id);
            this.ingredientsOriginals = this.ingredientsOriginals.filter(ingredientOriginal => ingredientOriginal.id !== item.id);
            this.updateCardData('remove', item, null);
        },

        removeDish(dishId) {
            let dish = this.dishes.find(dish => dish.id === dishId);
            if (dish) {
                dish.ingredients.forEach(ingredient => {
                    this.updateCardData('removeFromDish', ingredient, null);
                });
            }
            const tag = document.getElementById('dish_tag_' + dishId);
            if (tag) {
                this.tags(tag, 'show')
            }
            this.dishes = this.dishes.filter(dish => dish.id !== dishId);
        },

        clearIngredients() {
            this.ingredients = [];
        },

        clearIngredientsOriginals() {
            this.ingredientsOriginals = [];
        },

        clearDishes() {
            this.dishes = [];
        },

        clearCards() {
            this.cards.forEach(card => {
                card.data = 0;
            });
        },

        toggleClass(item, className, action) {
            if (action !== 'add') {
                item.classList.remove(className);
            } else {
                item.classList.add(className);
            }
        },

        ingredientPicture(item) {
            return item.includes(' ')
                ? item.split(' ').slice(0, 2).map(word => word[0]).join('').toUpperCase()
                : item.slice(0, 2).toUpperCase();
        },

        clearAll() {
            this.clearIngredients();
            this.clearIngredientsOriginals();
            this.clearDishes();
            this.clearCards();
        },

        all() {
            return {ingredients: this.ingredients, dishes: this.dishes}
        },

        generateRandomColor() {
            let color = Math.floor(Math.random() * 16777215).toString(16);

            while (color.length < 6) {
                color = Math.floor(Math.random() * 16777215).toString(16);
            }

            // Convert to RGB
            let red = parseInt(color.substring(0, 2), 16);
            let green = parseInt(color.substring(2, 4), 16);
            let blue = parseInt(color.substring(4, 6), 16);

            // Mix with white to create pastel color
            red = Math.round((red + 255) / 2);
            green = Math.round((green + 255) / 2);
            blue = Math.round((blue + 255) / 2);

            // Convert back to hex
            let pastelColor = ((red << 16) | (green << 8) | blue).toString(16);

            // Add leading zeros if necessary
            while (pastelColor.length < 6) {
                pastelColor = '0' + pastelColor;
            }

            let brightness = red * 0.299 + green * 0.587 + blue * 0.114;
            if (brightness > 180) {
                return {
                    backgroundColor: '#' + pastelColor,
                    color: '#000000'
                }
            } else {
                return {
                    backgroundColor: '#' + pastelColor,
                    color: '#ffffff'
                }
            }
        },

        generateHarmoniousColor(hexColor) {
            // Convert hex to RGB
            let r = parseInt(hexColor.slice(1, 3), 16);
            let g = parseInt(hexColor.slice(3, 5), 16);
            let b = parseInt(hexColor.slice(5, 7), 16);

            // Convert RGB to HSL
            r /= 255, g /= 255, b /= 255;
            let max = Math.max(r, g, b), min = Math.min(r, g, b);
            let h, s, l = (max + min) / 2;

            if (max === min) {
                h = s = 0; // achromatic
            } else {
                let d = max - min;
                s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                switch (max) {
                    case r:
                        h = (g - b) / d + (g < b ? 6 : 0);
                        break;
                    case g:
                        h = (b - r) / d + 2;
                        break;
                    case b:
                        h = (r - g) / d + 4;
                        break;
                }
                h /= 6;
            }

            // Change hue value by 30 degrees (or 0.083 in HSL model)
            h = (h + 0.083) % 1;

            // Convert HSL back to RGB
            let r1, g1, b1;
            if (s === 0) {
                r1 = g1 = b1 = l; // achromatic
            } else {
                let hue2rgb = function hue2rgb(p, q, t) {
                    if (t < 0) t += 1;
                    if (t > 1) t -= 1;
                    if (t < 1 / 6) return p + (q - p) * 6 * t;
                    if (t < 1 / 2) return q;
                    if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                    return p;
                }
                let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                let p = 2 * l - q;
                r1 = hue2rgb(p, q, h + 1 / 3);
                g1 = hue2rgb(p, q, h);
                b1 = hue2rgb(p, q, h - 1 / 3);
            }

            // Convert RGB back to hex
            let toHex = function (x) {
                let hex = Math.round(x * 255).toString(16);
                return hex.length === 1 ? "0" + hex : hex;
            };
            return "#" + toHex(r1) + toHex(g1) + toHex(b1);
        },

        colorScheme() {
            if (darkModePreference.matches) {
                this.darkMode = true;
            }
        },

        translate(key, count, replacements) {

            let translation = ingredientsAlreadyAdded;

            if (count === 1) {
                // Extract the part of the string after {1} until |
                let match = translation.match(/\{1\}([^|]*)/);
                if (match) {
                    translation = match[1].trim();
                }
            } else {
                // Extract the part of the string after {2,*} until the end of the sentence
                let match = translation.match(/\[2,\*\](.*)/);
                if (match) {
                    translation = match[1].trim();
                }
            }

            // Replace other placeholders
            for (let placeholder in replacements) {
                translation = translation.replace(':' + placeholder, replacements[placeholder]);
            }

            return translation;
        }

    }
}
