import {events} from "./app";

document.getElementById('askForWebNotifications').addEventListener('click', async () => {
    if (Notification.permission === 'default') {
        await window.Notification.requestPermission().then(async permission => {
            if (permission === 'granted') {
                await registerServiceWorker();
            }
        });
    }
})

if (navigator.userAgent.includes('Safari') && !navigator.userAgent.includes('Chrome')) {
    document.getElementById('sendWebNotificationsForSafari').addEventListener('click', async () => {
        await sendNotification('notifications/send/test/');
    });
}

const notificationTest = document.getElementById('sendWebNotifications');

if (notificationTest) {
    document.getElementById('sendWebNotifications').addEventListener('click', async function () {
        await sendNotification('notifications/send/test/');
    });
}

async function sendNotification(url) {
    try {
        const response = await fetch(url, {
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
                'Content-Type': 'application/json',
                Accept: 'application/json'
            }
        });
        if (!response.ok) {
            console.error('Failed to send notification:', response.statusText);
        }
    } catch (error) {
        console.error('Error sending notification:', error);
    }
}

function webNotify() {

    if (current_route !== 'profile' || !('Notification' in window) || !('serviceWorker' in navigator)) {
        return
    }

    document.addEventListener('ask-for-notifications-permissions', async function () {
        if (Notification.permission === 'default') {
            Notification.requestPermission().then(async permission => {
                if (permission === 'granted') {
                    await registerServiceWorker();
                }
            });
        }
    });

}

async function registerServiceWorker() {
    const registration = await navigator.serviceWorker.register('/sw.js')
    let subscription = await registration.pushManager.getSubscription();
    if (!subscription) {
        subscription = await registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: await getPublicKey()
        });
    }
    await saveSubscription(subscription);
}

async function getPublicKey() {
    const {key} = await fetch('/notifications/key', {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json());
    return key;
}

async function saveSubscription(subscription) {
    await fetch('/notifications/subscribe', {
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify(subscription)
    });
}

events.forEach(event => {
    document.addEventListener(event, () => {
        webNotify();
    });
});
