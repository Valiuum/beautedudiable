import {defineConfig} from 'vite';
import laravel from 'laravel-vite-plugin';
import postcss from '@vituum/vite-plugin-postcss'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/js/app.js',
                'resources/css/app.scss',
                'resources/js/datepicker.js',
                'resources/js/shapes.js',
                'resources/js/navbar.js',
                'resources/js/meals.js',
                'resources/js/dishes.js',
                'resources/js/webPush.js',
                'resources/js/dailyMeals.js',
                'resources/js/echo.js',
                'resources/js/bilan.js',
                'resources/js/calendar.js',
                'resources/js/rings.js',
                'resources/js/mobile.js',
            ],
            refresh: true,
        }),
        postcss(),
    ],
    css: {
        devSourcemap: true,
    },
});
