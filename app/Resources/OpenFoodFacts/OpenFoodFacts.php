<?php

namespace App\Resources\OpenFoodFacts;

use App\Interfaces\OpenFoodFacts\OpenFoodFactsInterface;
use Illuminate\Support\Facades\Http;

class OpenFoodFacts implements OpenFoodFactsInterface
{
    /**
     * Create a new class instance.
     */
    public function __construct()
    {
        self::setServer();
    }

    private static string $server;

    private static string $apiVersion = 'api/v3';

    private static string $lang = 'fr';

    private static array $filters = [
        '_id',
        'product_name',
        'product_name_fr',
        'generic_name',
        'generic_name_fr',
        'product_quantity',
        'product_quantity_unit',
        'quantity',
        'ecoscore_grade',
        'ecoscore_tags',
        'food_groups_tags',
        'nutrient_levels',
        'pnns_groups_1',
        'serving_quantity',
        'serving_quantity_unit',
        'brands',
        'categories',
        'ecoscore_tags',
        'nova_groups_tags',
        'nutrient_levels_tags',
        'ecoscore_data',
        'ingredients_text_with_allergens_fr',
        'ingredients_text_fr',
        'packaging_text_fr',
        'generic_name_fr',
        'abbreviated_product_name_fr',
        'image_front_small_url',
        'image_front_url',
        'image_nutrition_url',
        'image_url',
        'ecoscore_data',
        'environment_impact_level_tags',
        'environment_impact_level',
        'additives_tags',
        'allergens_lc',
        'ingredients',
        'ingredients_analysis',
        'ingredients_lc',
        'ingredients_text_with_allergens',
        'nutriments',
        'nutriscore_data',
        'nutrition_grade_fr',
        'nutrition_score_beverage',
        'carbon_footprint_percent_of_known_ingredients',
        'conservation_conditions',
        'nova_groups_markers',
        'origin',
        // 'packagings'
        // 'serving_size',
        // 'allergens_from_ingredients',
    ];

    private static array $searchFilters = [
        'sort_by=unique_scans_n',
        'page_size=10',
        'countries=France',
        'json=true',
    ];

    public function search(string $elem, int $page = 1)
    {

        try {
            $response = Http::get(self::$server.'/cgi/search.pl?action=process&search_terms='.$elem.'&'.'page='.$page.'&'.implode('&',
                self::$searchFilters));
            if ($response->successful()) {
                return $response->json();
            } else {
                return [
                    'status' => 'error',
                    'code' => $response->status(),
                    'reason' => $response->reason(),
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status' => 'error',
                'code' => 400,
                'reason' => $exception->getMessage(),
            ];
        }
    }

    public function barcode(string $elem)
    {

        try {
            $response = Http::get(self::$server.'/'.self::$apiVersion.'/product/'.$elem.'?cc='.
                self::$lang.'&lc='.self::$lang.'&fields='.implode(',', self::$filters)
            );
            if ($response->successful()) {
                $product = $response->json();
                $product['count'] = 1;

                return $product;
            } else {
                return [
                    'status' => 'error',
                    'code' => $response->status(),
                    'count' => 0,
                    'reason' => $response->reason(),
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status' => 'error',
                'code' => 400,
                'reason' => $exception->getMessage(),
            ];
        }
    }

    private static function setServer(): string
    {
        return self::$server = app()->environment('local') ? 'https://world.openfoodfacts.net' : 'https://world.openfoodfacts.org';
    }
}
