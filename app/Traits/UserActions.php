<?php

namespace App\Traits;

use App\Models\Meal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait UserActions
{
    public function shareWithList()
    {
        // Based on current user, check others Power Users to share with
        return User::where('role', 'power')->where('id', '!=', auth()->id())->pluck('name', 'id')->toArray();
    }

    public function addMeal($data, $shared_user = false): void
    {
        $current_data = $data->data;

        if ($current_data['action'] === 'delete') {
            return;
        }

        $meal = Meal::find($data->meal_id);
        $meal_new_date = $current_data['new_date'] !== '' ? $current_data['new_date'] : null;

        $ingredients = collect($current_data['ingredients']);
        if ($current_data['action'] === 'duplicate' || $shared_user) {
            $ingredients = $ingredients->reject(function ($ingredient) {
                return $ingredient['name'] === 'bonus';
            });
        }

        if ($shared_user) {
            $current_data['action'] = 'create';
            if ($current_data['new_date']) {
                $meal->created_at = Carbon::parse($current_data['new_date'])->setTimeFrom(Carbon::now());
            }
        } elseif ($current_data['user_bonus'] > 0 && $current_data['action'] !== 'duplicate') {
            $this->handleBonusPoints($meal->user_id, -$current_data['user_bonus']);
        }

        $meal_points = $ingredients->sum('points');

        $sum_points_dishes = $current_data['action'] === 'create' ? collect($current_data['dishes'])->map(function (
            $dish
        ) {
            return collect($dish['ingredients'])->sum('dish_ingredient_points');
        })->sum() : collect($current_data['dishes'])->map(function ($dish) {
            return collect($dish)->flatten(1)->sum('dish_meal_points');
        })->sum();

        $meal_points += $sum_points_dishes;
        $meal_previous_points = $current_data['meal_previous_points'];
        $userID = $shared_user ? $shared_user : $meal->user_id;

        switch ($current_data['action']) {
            case 'create':
            case 'duplicate':
                $this->updatePoints($userID, $meal->created_at, $meal_points, 'add');
                $meal->changed = 0;
                $meal->save();
                break;
            case 'edit':
                $this->updatePoints($userID, $meal->created_at, $meal_previous_points, 'subtract');
                if ($meal_new_date) {
                    $this->updatePoints($userID, $meal_new_date, $meal_points, 'add');
                    $meal->created_at = $meal->updated_at;
                    $meal->changed = 0;
                    $meal->save();
                } else {
                    $this->updatePoints($userID, $meal->created_at, $meal_points, 'add');
                }
                break;
        }
    }

    public function deleteMeal($user_id, $meal_id): void
    {

        $user = User::find($user_id);

        $meal_to_delete = $user->meals()
            ->where('id', $meal_id)
            ->with('ingredients', 'dishes')
            ->first();

        $ingredients_to_delete = collect($meal_to_delete->ingredients);

        $bonus_ingredient_deleted = $ingredients_to_delete->firstWhere('name', 'bonus');

        if (Carbon::parse($meal_to_delete->created_at)->isSameWeek(Carbon::now()) && $bonus_ingredient_deleted) {
            $this->handleBonusPoints($user_id, abs($bonus_ingredient_deleted->pivot->points));
        }

        $meal_to_delete_points = $ingredients_to_delete->sum('pivot.points');

        $sum_points_dishes = collect($meal_to_delete->dishes)->map(function ($dish) {
            return $dish->pivot->points;
        })->sum();

        $meal_to_delete_points += $sum_points_dishes;
        $this->updatePoints($user_id, $meal_to_delete->created_at, $meal_to_delete_points, 'subtract');
        $meal_to_delete->delete();
    }

    public function updatePoints($userID, $date, $points, $operation): void
    {
        switch ($operation) {
            case 'add':
                $this->userDailyPoints($userID, Carbon::parse($date)->format('Y-m-d'),
                    ['points' => $points, 'meals' => 1]);
                $this->userWeeklyPoints($userID, Carbon::parse($date)->startOfWeek()->format('Y-m-d'),
                    ['points' => $points]);
                break;
            case 'subtract':
                $this->userDailyPoints($userID, Carbon::parse($date)->format('Y-m-d'),
                    ['points' => -$points, 'meals' => -1]);
                $this->userWeeklyPoints($userID, Carbon::parse($date)->startOfWeek()->format('Y-m-d'),
                    ['points' => -$points]);
                break;
        }
    }

    public function userDailyPoints($userID, $date, $data = null): void
    {

        $user = User::find($userID);
        $daily_points = $user->dailyPoints()->where('date', $date)->first();

        if ($daily_points) {
            $daily_points->points += $data['points'];
            $daily_points->meals += $data['meals'];
            if ($daily_points->points <= 0 || $daily_points->meals <= 0) {
                $daily_points->delete();
            } else {
                $daily_points->save();
            }
        } else {

            if ($data['points'] < 0) {
                return;
            }
            // If the record does not exist, create a new one
            $user->dailyPoints()->create([
                'user_id' => $userID,
                'date' => $date,
                'points' => $data['points'],
                'meals' => $data['meals'],
            ]);

        }

    }

    public function userWeeklyPoints($userID, $date, $data = null): void
    {
        // Retrieve the existing record
        $user = User::find($userID);
        $weekly_points = $user->weeklyPoints()->where('date', $date)->first();

        $currentObjective = $user->objectives()->latest('created_at')->first();
        $user_weekly_points = $currentObjective->daily_points !== 0 ? $currentObjective->daily_points * 7 : 0;

        // If the record exists, add or subtract the new points to the existing ones
        if ($weekly_points) {
            $weekly_points->points += $data['points'];
            if ($weekly_points->points <= 0) {
                $weekly_points->delete();
            } elseif ($weekly_points->points <= $user_weekly_points) {
                $weekly_points->status = 1;
            } else {
                $weekly_points->status = 0;
            }
            $weekly_points->save();
        } else {

            if ($data['points'] < 0) {
                return;
            }

            // If the record does not exist, create a new one
            $weekly_points = $user->weeklyPoints()->create([
                'user_id' => $userID,
                'date' => $date,
                'points' => $data['points'],
            ]);

            if ($weekly_points->points <= $user_weekly_points || $data['points'] <= $user_weekly_points) {
                $weekly_points->status = 1;
            } else {
                $weekly_points->status = 0;
            }
            $weekly_points->save();
        }
    }

    public function handleBonusPoints($userID, $points): void
    {
        $user = User::find($userID);
        $user_bonus = $user->bonus()->first();
        if ($user_bonus) {
            $user_bonus->points += $points;
            if ($user_bonus->points <= 0) {
                $user_bonus->delete();
            } else {
                $user_bonus->save();
            }
        } else {
            if ($points > 0) {
                $user->bonus()->create([
                    'points' => $points,
                ]);
            }
        }
    }

    public function duplicateMeal($meal, $user, $date, $ingredients, $dishes): void
    {
        $new_meal = $meal->replicate();
        $new_meal->user_id = $user->id;
        $new_meal->created_at = Carbon::parse($date)->setTimeFrom(Carbon::now());
        $new_meal->updated_at = Carbon::parse($date)->setTimeFrom(Carbon::now());
        $new_meal->save();

        $shared_meal_ingredients = collect($ingredients)->map(function ($ingredient) use (
            $new_meal
        ) {
            return [
                'meal_id' => $new_meal->id,
                'ingredient_id' => $ingredient['id'],
                'points' => $ingredient['points'],
            ];
        })->toArray();

        $shared_meal_dishes = collect($dishes)->map(function ($ingredients) use ($new_meal) {
            $dish_id = $ingredients['id'];

            return collect($ingredients['ingredients'])->map(function ($ingredient) use (
                $new_meal,
                $dish_id
            ) {
                return [
                    'dish_id' => $dish_id,
                    'meal_id' => $new_meal->id,
                    'ingredient_id' => $ingredient['id'],
                    'points' => $ingredient['dish_meal_points'] ?? $ingredient['dish_ingredient_points'],
                ];
            });
        })->flatten(1)->toArray();

        $new_meal->ingredients()->sync($shared_meal_ingredients);
        $new_meal->dishes()->sync($shared_meal_dishes);

    }

    public function missedMeals()
    {

        if (! Carbon::now()->isSunday()) {
            return [];
        }

        $startOfWeek = Carbon::now()->startOfWeek();
        $endOfWeek = Carbon::now()->endOfWeek();
        $required_meals = ['lunch', 'dinner'];
        $daysOfWeek = collect(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']);
        $mealsSchedule = $daysOfWeek->mapWithKeys(function ($day, $index) use (
            $daysOfWeek,
            $required_meals,
            $startOfWeek
        ) {
            $date = $startOfWeek->copy()->addDays(array_search($day, $daysOfWeek->toArray()))->format('Y-m-d');

            return [
                $day => [
                    'slug' => strtolower(Carbon::parse($date)->format('l')),
                    'date' => $date,
                    'key' => $index,
                    'meals' => collect($required_meals)->mapWithKeys(function ($meal) use ($date) {
                        return [
                            $meal => [
                                'route' => route('meal.create', ['date' => $date, 'type' => $meal]),
                            ],
                        ];
                    }),
                ],
            ];
        });

        $userWeekMeals = Auth::user()->meals()
            ->whereBetween('created_at', [$startOfWeek, $endOfWeek])
            ->get()
            ->groupBy(function ($meal) {
                return Carbon::parse($meal->created_at)->format('l');
            });

        $mealsSchedule = $mealsSchedule->transform(function ($daySchedule, $day) use ($userWeekMeals) {
            if (isset($userWeekMeals[$day])) {
                $daySchedule['meals'] = $daySchedule['meals']->reject(function ($meal, $mealName) use (
                    $userWeekMeals,
                    $day
                ) {
                    return $userWeekMeals[$day]->contains('name', $mealName);
                });
            }

            return $daySchedule['meals']->isEmpty() ? null : $daySchedule;
        })->filter();

        return $mealsSchedule->toArray();

    }

    public function pointsToReachObjective()
    {

        if (! Carbon::now()->isSunday()) {
            return 'none';
        }

        $userObjectives = Auth::user()->objectives()->latest('created_at')->first();
        $weekPointsObjectives = $userObjectives->daily_points !== 0 ? $userObjectives->daily_points * 7 : 0;
        $weekPoints = Auth::user()->weeklyPoints()->whereDate('date',
            Carbon::now()->startOfWeek()->format('Y-m-d'))->first() ?? null;
        if ($weekPoints) {
            return $weekPointsObjectives - $weekPoints->points;
        } else {
            return $weekPointsObjectives;
        }
    }
}
