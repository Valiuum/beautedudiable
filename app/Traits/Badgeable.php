<?php

namespace App\Traits;

trait Badgeable
{
    public function badges(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Badge');
    }
}
