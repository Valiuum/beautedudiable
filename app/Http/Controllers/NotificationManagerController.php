<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\WebPushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class NotificationManagerController extends Controller
{
    public function subscribe(Request $request)
    {

        Validator::make($request->all(),
            [
                'endpoint' => ['required'],
                'keys.auth' => ['required'],
                'keys.p256dh' => ['required'],
            ]
        )->validate();

        $user = Auth::user();
        $endpoint = $request->endpoint;
        $token = $request->keys['auth'];
        $key = $request->keys['p256dh'];
        $user->updatePushSubscription($endpoint, $key, $token);

        return response()->json(['message' => 'Subscribed!']);
    }

    public function unsubscribe(Request $request)
    {
        $user = Auth::user();
        $user->deletePushSubscription($request->post('endpoint'));

        // Remove User Notifications Preferences
        return response()->json(['message' => 'Unsubscribed!']);
    }

    public function key(): array
    {
        return [
            'key' => env('VAPID_PUBLIC_KEY'),
        ];
    }

    public function UserWebNotificationsPreferences()
    {
        $user = Auth::user();
        if ($user->preferences) {
            $preferences = collect(json_decode($user->preferences, true))[0];
            if (isset($preferences->ios_notifs) && $preferences->ios_notifs == 'true') {
                $preferences->ios_notifs = 'false';
                $user->update(['preferences' => [$preferences]]);

                return [
                    'user_preferences' => 'updated',
                ];
            } else {
                return [
                    'user_preferences' => 'undefined',
                ];
            }
        } else {
            return [
                'user_preferences' => 'unchanged',
            ];
        }
    }

    public function send($action, $user_id = null)
    {

        $user = $user_id ? User::find($user_id) : Auth::user();

        if ($user) {

            try {

                if (Auth::user()->id !== $user->id) {

                    $user->notify(new WebPushNotification($action, Auth::user()->name));

                } else {

                    $user->notify(new WebPushNotification($action));

                }

                return response()->json(['notification_status' => 'sent to '.$user->name]);

            } catch (\Throwable $e) {
                return redirect('home')->with('error', $e->getMessage());
            }

        } else {

            $user->notify(new WebPushNotification($action));

            return response()->json(['notification_status' => 'sent']);

        }

        return redirect('home')->with('error', 'Nothing was sent');
    }
}
