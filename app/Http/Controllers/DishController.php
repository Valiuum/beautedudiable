<?php

namespace App\Http\Controllers;

use App\Models\Dish;

class DishController extends Controller
{
    public function show()
    {
        $data = Dish::with('ingredients')->orderBy('name')->get()
            ->groupBy('type');
        $header = [
            ['name' => __('messages.breakfast'), 'slug' => 'breakfast'],
            ['name' => __('messages.lunch'), 'slug' => 'lunch'],
            ['name' => __('messages.snack'), 'slug' => 'snack'],
            ['name' => __('messages.dinner'), 'slug' => 'dinner'],
            ['name' => __('messages.bar'), 'slug' => 'bar'],
        ];

        $dishes = collect($header)->map(function ($item) use ($data) {
            return [
                'slug' => $item['slug'],
                'data' => $data[$item['slug']] ?? [],
            ];
        });

        return view('dishes.show', compact('header', 'dishes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create($type)
    {
        if (! ($type === 'breakfast' || $type === 'lunch' || $type === 'dinner' || $type === 'snack' || $type === 'bar')) {
            return redirect()->back()->with('error', __('messages.dishNotExist'));
        }

        $dish = [
            'action' => 'create',
            'type' => $type,
            'data' => [
                'dish' => new Dish,
            ],
        ];

        return view('dishes.create', compact('dish'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {

        $data = Dish::with('ingredients')->find($id);

        if (! $data) {
            return redirect()->route('dish.show')->with('error', __('messages.dishNotFound'));
        }

        $ingredients_pivot = $data->ingredients->map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
                'points' => $item->pivot->points,
                'dish_current_points' => $item->pivot->points,
                'unit' => $item->unit,
                'energy' => $item->energy,
                'lipids' => $item->lipids,
                'proteins' => $item->proteins,
                'carbohydrates' => $item->carbohydrates,
                'fibers' => $item->fibers,
                'open_food_facts_id' => $item->open_food_facts_id ?? null,
            ];
        });

        $dish = [
            'action' => 'edit',
            'type' => $data->type,
            'data' => [
                'dish' => [
                    'id' => $data->id,
                    'user_id' => $data->user_id,
                    'name' => $data->name,
                    'type' => $data->type,
                    'ingredients' => $ingredients_pivot,
                ],
            ],
        ];

        return view('dishes.create', compact('dish'));

    }

    public function duplicate(string $id)
    {

        $data = Dish::with('ingredients')->find($id);

        if (! $data) {
            return redirect()->route('dish.show')->with('error', __('messages.dishNotFound'));
        }

        $ingredients_pivot = $data->ingredients->map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
                'points' => $item->pivot->points,
            ];
        });

        $dish = [
            'action' => 'duplicate',
            'type' => $data->type,
            'data' => [
                'dish' => [
                    'id' => $data->id,
                    'user_id' => $data->user_id,
                    'name' => $data->name,
                    'type' => $data->type,
                    'ingredients' => $ingredients_pivot,
                ],
            ],
        ];

        return view('dishes.create', compact('dish'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        if (Dish::find($id)->meals->count() > 0) {
            return redirect()->route('dish.show')->with('error', __('messages.dishInUse'));
        } else {
            Dish::destroy($id);

            return redirect()->route('dish.show')->with('success', __('messages.dishDeleted'));
        }

    }
}
