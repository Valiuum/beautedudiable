<?php

namespace App\Http\Controllers;

use App\Events\MealProcessed;
use App\Models\Dish;
use App\Models\Meal;
use App\Models\User;
use App\Notifications\MealSharedNotification;
use App\Notifications\WebPushNotification;
use App\Traits\UserActions;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MealController extends Controller
{
    use UserActions;

    /**
     * Show the form for creating a new resource.
     */
    public function create(string $date, string $type)
    {
        $meal_exists = auth()->user()->meals()
            ->whereDate('created_at', Carbon::createFromFormat('Y-m-d', $date))
            ->where('name', $type)
            ->exists();

        $dishes_tags = $this->dishTags($type);

        $meal = [
            'action' => 'create',
            'date' => $date,
            'type' => $type,
            'data' => [
                'dishes_tags' => $dishes_tags,
                'user_bonus' => auth()->user()?->bonus->points ?? 0,
            ],
        ];

        return $meal_exists ? redirect('home')->with('error', __('messages.alreadyExist')) : view('meals.meal',
            compact('meal'));

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {

        $meal_edit = Meal::with('dishes.ingredients')->find($id);

        if (! $meal_edit) {
            return redirect('home')->with('error', __('messages.mealNotFound'));
        }

        if ($meal_edit->user_id !== auth()->id()) {
            return redirect('home')->with('error', __('messages.notAllowed'));
        }

        // Add ingredients to the meal
        $data['ingredients'] = $meal_edit->ingredients->map(function ($ingredient) {
            return [
                'id' => $ingredient->id,
                'name' => $ingredient->name,
                'description' => $ingredient->description,
                'points' => $ingredient->points,
                'meal_current_points' => $ingredient->pivot->points,
                'quantity' => $ingredient->quantity,
                'unit' => $ingredient->unit,
                'energy' => $ingredient->energy,
                'lipids' => $ingredient->lipids,
                'proteins' => $ingredient->proteins,
                'carbohydrates' => $ingredient->carbohydrates,
                'fibers' => $ingredient->fibers,
                'open_food_facts_id' => $ingredient->open_food_facts_id,
            ];
        })->toArray();

        $dish_meal_data = DB::table('dish_meal')->where('meal_id', $meal_edit->id)->get();

        // Add dishes to the meal
        $data['dishes'] = $meal_edit->dishes->mapWithKeys(function ($dish) use ($dish_meal_data) {
            $content = $dish->ingredients->map(function ($ingredient) use ($dish, $dish_meal_data) {
                // Access the pivot data for the ingredient in the context of the dish
                $ingredient_pivot = $ingredient->pivot;
                $dish_ingredient = $ingredient_pivot ? $ingredient_pivot->points : null;
                // Access the pivot data for the dish in the context of the meal
                $dish_pivot = $dish_meal_data->where('ingredient_id', $ingredient->id)
                    ->where('dish_id', $dish->id)
                    ->first();
                $dish_meal_points = $dish_pivot ? $dish_pivot->points : null;

                return [
                    'id' => $ingredient->id,
                    'dish_id' => $dish->id,
                    'dish_name' => $dish->name,
                    'name' => $ingredient->name,
                    'description' => $ingredient->description,
                    'points' => $ingredient->points,
                    'dish_ingredient_points' => $dish_ingredient,
                    'dish_meal_points' => $dish_meal_points,
                    'quantity' => $ingredient->quantity,
                    'unit' => $ingredient->unit,
                    'energy' => $ingredient->energy,
                    'lipids' => $ingredient->lipids,
                    'proteins' => $ingredient->proteins,
                    'carbohydrates' => $ingredient->carbohydrates,
                    'fibers' => $ingredient->fibers,
                    'open_food_facts_id' => $ingredient->open_food_facts_id,
                ];
            })->sortBy('name')->values()->all();

            return [$dish->id => $content];
        })->toArray();

        // Calculate the sum of points for the meal
        $sum_points = collect($data['ingredients'])->sum('meal_current_points') + collect($data['dishes'])->flatten(1)->sum('dish_meal_points');

        $dishes_tags = $this->dishTags($meal_edit->name);

        // Prepare the meal data
        $meal = [
            'action' => 'edit',
            'id' => $id,
            'date' => Carbon::parse($meal_edit->created_at)->format('Y-m-d'),
            'warning' => $meal_edit->changed,
            'type' => $meal_edit->name,
            'data' => [
                'ingredients' => $data['ingredients'],
                'dishes' => $data['dishes'] ?? [],
                'dishes_tags' => $dishes_tags,
                'user_bonus' => auth()->user()?->bonus->points ?? 0,
                'meal_previous_points' => $sum_points,
            ],
        ];

        return view('meals.meal', compact('meal'));
    }

    public function share(int $mealID, int $userID, int $shared_by)
    {

        $meal = Meal::where('id', $mealID)->with('ingredients', 'dishes')->first();
        $user = User::find($userID);

        if (! $meal || ! $user) {
            return redirect('home')->with('error', __('messages.mealNotFound'));
        }

        $mealShared = $meal->replicate();
        $mealShared->user_id = $user->id;
        $mealShared->changed = 1;
        $mealShared->created_at = Carbon::now();
        $mealShared->save();

        $mealShared_ingredients = collect($meal->ingredients)
            ->reject(function ($ingredient) {
                return $ingredient->name === 'bonus';
            })
            ->map(function ($ingredient) use ($mealShared) {
                return [
                    'meal_id' => $mealShared->id,
                    'ingredient_id' => $ingredient['id'],
                    'points' => $ingredient->pivot->points,
                ];
            })
            ->toArray();

        $mealShared_dishes = collect($meal->dishes)->map(function ($dish) use ($mealShared) {
            return [
                'dish_id' => $dish->id,
                'meal_id' => $mealShared->id,
                'ingredient_id' => $dish->pivot->ingredient_id,
                'points' => $dish->pivot->points,
            ];
        })->toArray();

        $mealShared->ingredients()->sync($mealShared_ingredients);
        $mealShared->dishes()->sync($mealShared_dishes);

        $data = [
            'ingredients' => $mealShared_ingredients,
            'dishes' => [['ingredients' => $mealShared_dishes]],
            'date' => Carbon::now()->format('Y-m-d'),
            'warning' => false,
            'new_date' => '',
            'type' => $mealShared->name,
            'shared_meal_with' => 0,
            'meal_id' => $mealShared->id,
            'action' => 'create',
            'user_bonus' => 0.0,
            'meal_previous_points' => 0,
            'meal_to_delete' => [],
        ];

        MealProcessed::dispatch($mealShared->id, $data);

        // Notify the user

        $user->notify(new MealSharedNotification($mealShared->id, $mealShared->user_id));
        // WebPushNotification
        $preferences = $user->preferences ? collect(json_decode($user->preferences, true)) : collect();
        if (! is_null($preferences) && $preferences->get('notificationActivate') && $preferences->get('notificationSharedMeals')) {
            $user_shared_by = User::find($shared_by);
            $user->notify(new WebPushNotification('shared_meal', $user_shared_by->name));
        }

        return redirect('home')->with('success', __('messages.mealShared'));
    }

    public function duplicate(string $id)
    {

        $meal_duplicate = Meal::with('dishes.ingredients')->find($id);

        if (! $meal_duplicate) {
            return redirect('home')->with('error', __('messages.mealNotFound'));
        }

        if ($meal_duplicate->user_id !== auth()->id()) {
            return redirect('home')->with('error', __('messages.notAllowed'));
        }

        // Add ingredients to the meal
        $data['ingredients'] = $meal_duplicate->ingredients->map(function ($ingredient) {
            return [
                'id' => $ingredient->id,
                'name' => $ingredient->name,
                'description' => $ingredient->description,
                'points' => $ingredient->points,
                'meal_current_points' => $ingredient->pivot->points,
                'quantity' => $ingredient->quantity,
                'unit' => $ingredient->unit,
                'energy' => $ingredient->energy,
                'lipids' => $ingredient->lipids,
                'proteins' => $ingredient->proteins,
                'carbohydrates' => $ingredient->carbohydrates,
                'fibers' => $ingredient->fibers,
                'open_food_facts_id' => $ingredient->open_food_facts_id,
            ];
        })->toArray();

        $dish_meal_data = DB::table('dish_meal')->where('meal_id', $meal_duplicate->id)->get();

        // Add dishes to the meal
        $data['dishes'] = $meal_duplicate->dishes->mapWithKeys(function ($dish) use ($dish_meal_data) {
            $content = $dish->ingredients->map(function ($ingredient) use ($dish, $dish_meal_data) {
                // Access the pivot data for the ingredient in the context of the dish
                $ingredient_pivot = $ingredient->pivot;
                $dish_ingredient = $ingredient_pivot ? $ingredient_pivot->points : null;
                // Access the pivot data for the dish in the context of the meal
                $dish_pivot = $dish_meal_data->where('ingredient_id', $ingredient->id)
                    ->where('dish_id', $dish->id)
                    ->first();
                $dish_meal_points = $dish_pivot ? $dish_pivot->points : null;

                return [
                    'id' => $ingredient->id,
                    'dish_id' => $dish->id,
                    'dish_name' => $dish->name,
                    'name' => $ingredient->name,
                    'description' => $ingredient->description,
                    'points' => $ingredient->points,
                    'dish_ingredient_points' => $dish_ingredient,
                    'dish_meal_points' => $dish_meal_points,
                    'quantity' => $ingredient->quantity,
                    'unit' => $ingredient->unit,
                    'energy' => $ingredient->energy,
                    'lipids' => $ingredient->lipids,
                    'proteins' => $ingredient->proteins,
                    'carbohydrates' => $ingredient->carbohydrates,
                    'fibers' => $ingredient->fibers,
                    'open_food_facts_id' => $ingredient->open_food_facts_id,
                ];
            })->sortBy('name')->values()->all();

            return [$dish->id => $content];
        })->toArray();

        // Calculate the sum of points for the meal
        $sum_points = collect($data['ingredients'])->sum('meal_current_points') + collect($data['dishes'])->flatten(1)->sum('dish_meal_points');

        $dishes_tags = $this->dishTags($meal_duplicate->name);

        // Prepare the meal data
        $meal = [
            'action' => 'duplicate',
            'id' => $id,
            'date' => '',
            'warning' => $meal_duplicate->changed,
            'type' => $meal_duplicate->name,
            'data' => [
                'ingredients' => $data['ingredients'],
                'dishes' => $data['dishes'] ?? [],
                'dishes_tags' => $dishes_tags,
                'user_bonus' => auth()->user()?->bonus->points ?? 0,
                'meal_previous_points' => $sum_points,
            ],
        ];

        return view('meals.meal', compact('meal'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        $meal = Meal::find($id);
        $this->deleteMeal($meal->user_id, $meal->id);

        return redirect('home')->with('success', __('messages.mealDeleted'));
    }

    private function dishTags($type)
    {

        // Popular dishes associate with the meal type for tags
        $dish_search_type = match ($type) {
            'lunch', 'dinner' => ['lunch', 'dinner'],
            default => [$type],
        };

        $dishes_tags = Dish::whereIn('type', $dish_search_type)->get(['id', 'name', 'type']);

        if (! $dishes_tags->isEmpty()) {

            $dishes_raw = DB::table('dish_meal')->distinct('meal_id')->get()->groupBy('dish_id');
            $popular_dishes = $dishes_raw->map(function ($group) {
                return $group->count();
            })->sortByDesc(function ($count) {
                return $count;
            });

            $dishes_tags = $dishes_tags->sortBy(function ($dish) use ($popular_dishes) {
                return $popular_dishes[$dish->id] ?? null;
            })->reverse()->values();

            return $dishes_tags;
        } else {
            return [];
        }
    }
}
