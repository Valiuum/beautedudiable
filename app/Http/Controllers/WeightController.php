<?php

namespace App\Http\Controllers;

use App\Models\Weight;
use Illuminate\Support\Facades\Auth;

class WeightController extends Controller
{
    public function show()
    {
        return view('followUp.weights.show');
    }

    public function edit(string $id)
    {
        $weight = Weight::find($id);
        if ($weight && $weight->user_id === Auth::id()) {
            return view('followUp.weights.edit', compact('weight'));
        } else {
            return redirect()->back()->with('error', __('messages.forbidden'));
        }
    }
}
