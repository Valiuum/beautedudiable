<?php

namespace App\Http\Controllers;

use App\Models\Cycle;
use Illuminate\Support\Facades\Auth;

class CycleController extends Controller
{
    public function show()
    {
        return view('followUp.cycles.show');
    }

    public function edit(string $id)
    {
        $cycle = Cycle::find($id);
        if ($cycle && $cycle->user_id === Auth::id()) {
            return view('followUp.cycles.edit', compact('cycle'));
        } else {
            return redirect()->back()->with('error', __('messages.forbidden'));
        }
    }
}
