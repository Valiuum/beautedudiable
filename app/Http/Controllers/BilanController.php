<?php

namespace App\Http\Controllers;

use App\Models\Badge;
use App\Models\BadgeUnlock;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class BilanController extends Controller
{
    /**
     * @var array|string[]
     */
    private array $queries_duration = [
        'week', '2weeks', 'month', '2months', '6months', 'year', 'all',
    ];

    public function show()
    {
        $bilan_graphs = $this->bilanData();

        // Extract from $bilan_graphs unneeded data for JS
        // $bilan_graphs = collect($bilan_graphs)->except('meals')->toArray();

        return view('followUp.bilan', compact('bilan_graphs'));
    }

    public function update($graph, $duration)
    {
        if (! Str::contains($graph, 'graph_') || ! in_array($duration, $this->queries_duration)) {
            return response()->json(['error' => 'Invalid parameters'], 400);
        }

        switch ($graph) {
            case 'graph_meals_follow_up':
                $data_querie = collect($this->userMeals($duration));

                return $this->buildMealsFollowUpGraphData($data_querie);
            case 'graph_meals_by_name':
                $this->buildMealsByNameGraphData($duration);
                break;
            case 'graph_meals_by_day':
                $this->buildMealsByDayGraphData($duration);
                break;
            default:
                return response()->json(['error' => 'Invalid graph'], 400);
        }
    }

    private function bilanData(): array
    {

        // User's meals data
        $meals_query = $this->userMeals('all');
        $meals_data = $this->handleMealsData($meals_query);
        $meals_graphs_and_cards = $this->buildGraphAndCards('meals_graphs_and_cards', $meals_data);

        // User's objectives
        $objectives = auth()->user()->objectives()->select('health_target', 'daily_points', 'created_at')
            ->orderBy('created_at', 'desc')
            ->get();

        // User's activities data
        $activities_query = $this->userActivities('all');
        $activities_data = $this->handleActivitiesData($activities_query);
        $activities_graphs_and_cards = $this->buildGraphAndCards('activities_graphs_and_cards', $activities_data);

        // User's cycle data
        if (auth()->user()->isWomen()) {
            $cycles_query = collect($this->userCycles('6months'));
            $cycles_graphs = $this->buildGraphAndCards('cycles_graph', $cycles_query);
        }

        // User's weights data
        $weights_query = $this->userWeights('all');
        $weights_data = $this->handleWeightData($weights_query);
        $weights_graphs_and_cards = $this->buildGraphAndCards('weights_graphs_and_cards', $weights_data);

        // User's badges
        $badges_querie = Badge::all();
        $badges_users_unlocks_querie = BadgeUnlock::where('user_id', auth()->id())->get();

        $badges = $badges_querie->map(function ($badge) use ($badges_users_unlocks_querie) {
            $badge_unlocked = $badges_users_unlocks_querie->contains('badge_id', $badge->id);
            $badge_array = $badge->toArray();
            $badge_array['action'] = trans('messages.'.$badge_array['action']);
            if ($badge_unlocked) {
                $badge_unlock = $badges_users_unlocks_querie->firstWhere('badge_id', $badge->id);

                return collect($badge_array + ['unlock_date' => Carbon::parse($badge_unlock->created_at)->translatedFormat('l d M Y à H:i:s')]);
            } else {
                return collect($badge_array);
            }
        });

        return [
            'meals' => $meals_graphs_and_cards,
            'objectives' => $objectives,
            'activities' => $activities_graphs_and_cards,
            'cycles' => $cycles_graphs ?? [],
            'weights' => $weights_graphs_and_cards,
            'badges' => $badges,
        ];
    }

    private function handleDate($duration): array
    {

        if (! in_array($duration, $this->queries_duration)) {
            return response()->json(['error' => 'Invalid duration'], 400);
        }

        $end_date = Carbon::createFromFormat('Y-m-d', now()->format('Y-m-d'))->endOfDay();

        switch ($duration) {
            case 'week':
                $start_date = Carbon::createFromFormat('Y-m-d', now()->subWeek()->format('Y-m-d'))->startOfDay();
                break;
            case '2weeks':
                $start_date = Carbon::createFromFormat('Y-m-d', now()->subWeeks(2)->format('Y-m-d'))->startOfDay();
                break;
            case 'month':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subMonthWithoutOverflow()->startOfMonth()->format('Y-m-d'))->startOfDay();
                break;
            case '2months':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subMonthsWithoutOverflow(2)->startOfMonth()->format('Y-m-d'))->startOfDay();
                break;
            case '6months':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subMonthsWithoutOverflow(6)->startOfMonth()->format('Y-m-d'))->startOfDay();
                break;
            case 'year':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subYearWithoutOverflow()->format('Y-m-d'))->startOfDay();
                break;
            case 'all':
                $start_date = Carbon::createFromFormat('Y-m-d', '2020-01-01')->startOfDay();
                break;
            default:
                if (preg_match('/^(\d+)years$/', $duration, $matches)) {
                    $years = intval($matches[1]);
                    $start_date = Carbon::createFromFormat('Y-m-d',
                        now()->subYearsWithoutOverflow($years)->startOfYear()->format('Y-m-d'))->startOfDay();
                }
                break;

        }

        return [
            'start' => $start_date,
            'end' => $end_date,
        ];

    }

    public function userMeals($duration)
    {

        $dates = $this->handleDate($duration);

        return auth()->user()->meals()
            ->with('ingredients', 'dishes')
            ->whereBetween('created_at', [$dates['start'], $dates['end']])
            ->orderBy('created_at', 'asc')
            ->get();

    }

    public function userActivities($duration)
    {

        $dates = $this->handleDate($duration);

        return auth()->user()->activities()
            ->whereBetween('date', [$dates['start'], $dates['end']])
            ->orderBy('date', 'asc')
            ->select('activity', 'date')->get();

    }

    public function userWeights($duration)
    {

        $dates = $this->handleDate($duration);

        return auth()->user()->weights()
            ->whereBetween('date', [$dates['start'], $dates['end']])
            ->orderBy('date', 'asc')
            ->select('weighing', 'date')->get();

    }

    public function userCycles($duration)
    {

        if (! auth()->user()->isWomen()) {
            return [];
        }

        $dates = $this->handleDate($duration);

        return auth()->user()->cycles()
            ->whereBetween('date', [$dates['start'], $dates['end']])
            ->orderBy('date', 'asc')
            ->select('duration', 'date')->get();

    }

    public function handleMealsData($data): array
    {

        // User's meals (2 last weeks)
        $meals_data_two_weeks = collect(
            $data->whereBetween('created_at', [
                Carbon::createFromFormat('Y-m-d', now()->subWeeks(2)->format('Y-m-d'))->startOfDay(),
                Carbon::createFromFormat('Y-m-d', now()->format('Y-m-d'))->endOfDay(),
            ]
            ));

        // User's meals (all)
        $meals_data_all = collect($data);

        return [
            'two_weeks' => $meals_data_two_weeks,
            'all' => $meals_data_all,
        ];
    }

    public function handleWeightData($data): array
    {

        // User's weights (current month)
        $weights_data_month = collect(
            $data->whereBetween('date', [now()->subMonth()->startOfMonth()->format('Y-m-d'), now()->format('Y-m-d')]));

        // User's weights (all)
        $weights_data_all = collect($data);

        return [
            'month' => $weights_data_month,
            'all' => $weights_data_all,
        ];

    }

    public function handleActivitiesData($data): array
    {
        // User's meals (2 last weeks)
        $activities_current_month = collect(
            $data->whereBetween('date', [now()->subMonth()->startOfMonth()->format('Y-m-d'), now()->format('Y-m-d')]
            ));

        // User's meals (all)
        $activities_data_all = collect($data);

        return [
            'current_month' => $activities_current_month,
            'all' => $activities_data_all,
        ];
    }

    private function buildGraphAndCards($type, $data)
    {
        return match ($type) {
            'meals_graphs_and_cards' => $this->buildMealsGraphsAndCards($data),
            'activities_graphs_and_cards' => $this->buildActivitiesGraphsAndCards($data),
            'cycles_graph' => $this->buildCyclesGraph($data),
            'weights_graphs_and_cards' => $this->buildWeightsGraphsAndCards($data),
            default => response()->json(['error' => 'Invalid type'], 400),
        };
    }

    private function buildMealsGraphsAndCards($data)
    {

        // Graph meals_follow_up
        $meals_follow_up = $this->buildMealsFollowUpGraphData($data['two_weeks']);

        // Graph meals_by_name
        $meals_by_name = $this->buildMealsByNameGraphData($data['all']);

        // Graph meals_by_day
        $meals_by_day = $this->buildMealsByDayGraphData($data['all']);

        // Ingredients
        $ingredients = $this->buildIngredientsCards($data['all']);

        return collect([
            'meals_follow_up' => $meals_follow_up,
            'meals_by_name' => $meals_by_name,
            'meals_by_day' => $meals_by_day,
            'ingredients' => $ingredients,
        ])->toArray();

    }

    private function buildActivitiesGraphsAndCards($data)
    {
        // Graph activities_follow_up
        $activities_follow_up = $this->buildActivitiesFollowUpGraphData($data['all']);

        // Card activities_summary
        $activities_summary = $this->buildActivitiesSummaryCardData($data['all']);

        // Card activities_months_average
        $activities_months_average = $this->buildActivitiesMonthsAverageCardData($data['all']);

        // Card activities_current_month_tendance
        $activities_months_current_month_tendance = $this->buildActivitiesCurrentMonthTendanceCardData($data['all'],
            $activities_months_average['average_activities_per_month_without_current']);

        return collect([
            'activities_follow_up' => $activities_follow_up,
            'activities_summary' => $activities_summary,
            'activities_months_average' => $activities_months_average,
            'activities_months_current_month_tendance' => $activities_months_current_month_tendance,
        ])->toArray();
    }

    private function buildWeightsGraphsAndCards($data)
    {

        // Graph weights_follow_up
        $weights_follow_up = $this->buildWeightsFollowUpGraphData($data['month']);

        // Card weights_summary
        $weights_summary = $this->buildWeightsSummaryCardData($data['all']);

        // Card weights_months_average
        $weights_months_average = $this->buildWeightsMonthsAverageCardData($data['all']);

        // Card weights_current_month_tendance
        $weights_months_current_month_tendance = $this->buildWeightsCurrentMonthTendanceCardData($data['all'],
            $weights_months_average['average_weights_per_month_without_current']);

        return collect([
            'weights_follow_up' => $weights_follow_up,
            'weights_summary' => $weights_summary,
            'weights_months_average' => $weights_months_average,
            'weights_months_current_month_tendance' => $weights_months_current_month_tendance,
        ])->toArray();
    }

    private function buildCyclesGraph($data)
    {

        // Graph cycles_follow_up
        $cycles_follow_up = $this->buildCyclesFollowUpGraphData($data);

        return collect([
            'cycles_follow_up' => $cycles_follow_up,
        ])->toArray();
    }

    private function buildMealsFollowUpGraphData($data)
    {

        $grouped_meals = $data->groupBy(function ($item) {
            return Carbon::parse($item->created_at)->format('Y-m-d');
        });

        $result = $grouped_meals->map(function ($group, $date) {
            $ingredients_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->ingredients->reduce(function ($ingredientCarry, $ingredient) {
                    return $ingredientCarry + $ingredient->pivot->points;
                }, 0);
            }, 0);

            $dishes_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->dishes->reduce(function ($ingredientCarry, $dish) {
                    return $ingredientCarry + $dish->pivot->points;
                }, 0);
            }, 0);

            return [
                'x' => $date,
                'y' => $ingredients_points + $dishes_points,
            ];
        })->values();

        return $result->toArray();

    }

    private function buildMealsByNameGraphData($data)
    {

        $order = [
            'breakfast' => trans('messages.breakfast'),
            'lunch' => trans('messages.lunch'),
            'snack' => trans('messages.snack'),
            'dinner' => trans('messages.dinner'),
            'bar' => trans('messages.bar'),
        ];

        $grouped_data = $data->groupBy('name')->sortKeysUsing(function ($name1, $name2) use ($order) {
            return array_search($name1, array_keys($order)) <=> array_search($name2, array_keys($order));
        })->mapWithKeys(function ($group, $name) use ($order) {
            return [$order[$name] => $group];
        });

        $result = $grouped_data->map(function ($group, $name) {
            $total_ingredients_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->ingredients->reduce(function ($ingredientCarry, $ingredient) {
                    return $ingredientCarry + $ingredient->pivot->points;
                }, 0);
            }, 0);

            $total_dishes_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->dishes->reduce(function ($ingredientCarry, $dish) {
                    return $ingredientCarry + $dish->pivot->points;
                }, 0);
            }, 0);

            $group_size = $group->count();

            $average_ingredients_points = $group_size > 0 ? $total_ingredients_points / $group_size : 0;
            $average_dishes_points = $group_size > 0 ? $total_dishes_points / $group_size : 0;

            return [
                'name' => $name,
                'points' => round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2, 1),
            ];
        })->values();

        return $result->toArray();

    }

    private function buildMealsByDayGraphData($data)
    {

        $grouped_meals = $data->groupBy(function ($item) {
            return Carbon::parse($item->created_at)->translatedFormat('l');
        })->sortKeysUsing(function ($day1, $day2) {
            $order = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];

            return array_search($day1, $order) <=> array_search($day2, $order);
        });

        $result = $grouped_meals->map(function ($group, $day) {
            $total_ingredients_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->ingredients->reduce(function ($ingredientCarry, $ingredient) {
                    return $ingredientCarry + $ingredient->pivot->points;
                }, 0);
            }, 0);

            $total_dishes_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->dishes->reduce(function ($ingredientCarry, $dish) {
                    return $ingredientCarry + $dish->pivot->points;
                }, 0);
            }, 0);

            $group_size = $group->count();

            $average_ingredients_points = $group_size > 0 ? $total_ingredients_points / $group_size : 0;
            $average_dishes_points = $group_size > 0 ? $total_dishes_points / $group_size : 0;

            return [
                'day' => $day,
                'points' => round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2, 1),
            ];
        })->values();

        return $result->toArray();

    }

    private function buildIngredientsCards($data)
    {

        $ingredients = $data->pluck('ingredients')
            ->flatten()
            ->filter(function ($ingredient) {
                return $ingredient->name !== 'bonus';
            })
            ->map(function ($ingredient) {
                return [
                    'id' => $ingredient->id,
                    'name' => $ingredient->name,
                ];
            });
        $dishes_ingredients = $data->pluck('dishes.*.ingredients')->flatten()->select('id', 'name');
        $all_ingredients = $ingredients->merge($dishes_ingredients);

        $grouped_ingredients = $all_ingredients->groupBy('id')
            ->map(function ($group) {
                return [
                    'name' => $group->first()['name'],
                    'count' => $group->count(),
                ];
            })->sortByDesc('count')->take(10);

        return $grouped_ingredients->toArray();

    }

    private function buildActivitiesFollowUpGraphData($data)
    {

        $titles = [
            'yoga' => trans('messages.yoga'),
            'workout' => trans('messages.workout'),
            'running' => trans('messages.running'),
            'hiking' => trans('messages.hiking'),
        ];

        $grouped_activities = $data->groupBy('activity')->mapWithKeys(function ($group, $activity) use ($titles) {
            $title = $titles[$activity] ?? $activity;

            return [$title => $group];
        });

        $result = $grouped_activities->map(function ($group, $title) {
            return [
                'x' => $group->count(),
                'y' => $title,
            ];
        })->values();

        return $result->toArray();

    }

    private function buildActivitiesSummaryCardData($data)
    {

        $currentYear = now()->year;
        $previousYear = now()->subYear()->year;
        $previousMonth = now()->subMonth()->month;

        // Current year

        $grouped_activities_by_year = $data->groupBy('activity')->map(function ($group) use ($currentYear) {

            $currentYearActivities = $group->filter(function ($item) use ($currentYear) {
                return Carbon::parse($item->date)->year === $currentYear;
            });

            return [
                'count' => $currentYearActivities->count(),
            ];
        });

        $current_year = [
            'total' => $grouped_activities_by_year->sum('count'),
            'activities' => $grouped_activities_by_year->toArray(),
        ];

        // Previous year

        $grouped_activities_previous_year = $data->groupBy('activity')->map(function ($group) use ($previousYear) {

            $previousYearActivities = $group->filter(function ($item) use ($previousYear) {
                return Carbon::parse($item->date)->year === $previousYear;
            });

            return [
                'count' => $previousYearActivities->count(),
            ];
        });

        $previous_year = [
            'total' => $grouped_activities_previous_year->sum('count'),
            'activities' => $grouped_activities_previous_year->toArray(),
        ];

        // Previous month

        $grouped_activities_previous_month = $data->groupBy('activity')->map(function ($group) use ($previousMonth) {

            $previousYearActivities = $group->filter(function ($item) use ($previousMonth) {
                return Carbon::parse($item->date)->month === $previousMonth;
            });

            return [
                'count' => $previousYearActivities->count(),
            ];
        });

        $previous_month = [
            'total' => $grouped_activities_previous_month->sum('count'),
            'activities' => $grouped_activities_previous_month->toArray(),
        ];

        return [
            'current_year' => $current_year,
            'previous_year' => $previous_year,
            'previous_month' => $previous_month,
        ];

    }

    private function buildWeightsSummaryCardData($data)
    {

        $currentYear = now()->year;
        $previousYear = now()->subYear()->year;
        $previousMonth = now()->subMonth()->month;

        // Sum of all weighing values
        $total_weighing_all = $data->avg('weighing');

        // Sum of weighing values for current year
        $total_weighing_year = $data->filter(function ($item) use ($currentYear) {
            return Carbon::parse($item->date)->year === $currentYear;
        })->avg('weighing');

        // Sum of weighing values for the last year
        $total_weighing_last_year = $data->filter(function ($item) use ($previousYear) {
            return Carbon::parse($item->date)->year === $previousYear;
        })->avg('weighing');

        // Sum of weighing values for the previous month
        $total_weighing_previous_month = $data->filter(function ($item) use ($previousMonth) {
            return Carbon::parse($item->date)->month === $previousMonth;
        })->avg('weighing');

        return [
            'weighing_all' => round($total_weighing_all, 1),
            'weighing_current_year' => round($total_weighing_year, 1),
            'weighing_last_year' => round($total_weighing_last_year, 1),
            'weighing_previous_month' => round($total_weighing_previous_month, 1),
        ];

    }

    private function buildActivitiesMonthsAverageCardData($data)
    {

        // Group activities by month and year
        $grouped_activities = $data->groupBy(function ($item) {
            return Carbon::parse($item->date)->format('Y-m');
        });

        // Exclude the current year and month
        $currentYearMonth = now()->format('Y-m');
        $filtered_activities = $grouped_activities->except($currentYearMonth);

        // Calculate total activities for each month
        $monthly_totals = $filtered_activities->map(function ($group) {
            return $group->count();
        });

        // Calculate the average number of activities per month
        $average_activities_per_month = $monthly_totals->avg();

        return [
            'average_activities_per_month_without_current' => round(ceil(($average_activities_per_month) * 2) / 2,
                1) ?? 0,
            'monthly_totals_without_current' => $monthly_totals,
        ];

    }

    private function buildWeightsMonthsAverageCardData($data)
    {

        // Group activities by month and year
        $grouped_weights = $data->groupBy(function ($item) {
            return Carbon::parse($item->date)->format('Y-m');
        });

        // Exclude the current year and month
        $currentYearMonth = now()->format('Y-m');
        $filtered_weights = $grouped_weights->except($currentYearMonth);

        // Calculate total weights for each month
        $monthly_totals = $filtered_weights->map(function ($group) {
            return $group->avg('weighing');
        });

        // Calculate the average number of weights per month
        $average_weights_per_month = $monthly_totals->avg();

        return [
            'average_weights_per_month_without_current' => round($average_weights_per_month, 2) ?? 0,
            'monthly_totals_without_current' => $average_weights_per_month,
        ];

    }

    private function buildActivitiesCurrentMonthTendanceCardData($data, $avg)
    {

        $currentYearMonth = now()->format('Y-m');

        $current_month = $data->filter(function ($item) use ($currentYearMonth) {
            return Carbon::parse($item->date)->format('Y-m') === $currentYearMonth;
        });

        $current_month_total = $current_month->count();

        $tendance = $current_month_total > $avg ? 'up' : ($current_month_total < $avg ? 'down' : 'equal');

        return [
            'current_month_total' => $current_month_total,
            'tendance' => $tendance,
        ];
    }

    private function buildWeightsCurrentMonthTendanceCardData($data, $avg)
    {
        $currentYearMonth = now()->format('Y-m');

        $current_month = $data->filter(function ($item) use ($currentYearMonth) {
            return Carbon::parse($item->date)->format('Y-m') === $currentYearMonth;
        });

        $current_month_total = $current_month->avg('weighing');

        $tendance = $current_month_total > $avg ? 'up' : ($current_month_total < $avg ? 'down' : 'equal');

        return [
            'current_month_total' => $current_month_total,
            'tendance' => $tendance,
        ];
    }

    public function buildCyclesFollowUpGraphData($data)
    {

        $result = $data->map(function ($item) {
            return [
                'x' => Carbon::parse($item->date)->format('Y-m-d'),
                'y' => $item->duration,
            ];
        });

        return $result->toArray();

    }

    private function buildWeightsFollowUpGraphData($data)
    {

        $result = $data->map(function ($item) {
            return [
                'x' => Carbon::parse($item->date)->format('Y-m-d'),
                'y' => $item->weighing,
            ];
        })->values();

        return $result->toArray();
    }
}
