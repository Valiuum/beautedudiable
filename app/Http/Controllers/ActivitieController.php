<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Support\Facades\Auth;

class ActivitieController extends Controller
{
    public function show()
    {
        return view('followUp.activities.show');
    }

    public function edit(string $id)
    {
        $activity = Activity::find($id);
        if ($activity && $activity->user_id === Auth::id()) {
            return view('followUp.activities.edit', compact('activity'));
        } else {
            return redirect()->back()->with('error', __('messages.forbidden'));
        }
    }
}
