<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserRole
{
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->user() && auth()->user()->isPowerUser() && auth()->user()->objectives()->count()) {
            return $next($request);
        }

        if (! auth()->user()->objectives()->count()) {
            return redirect('home')->with('warning', trans('messages.no_objectives'));
        } else {
            return redirect('home')->with('error', trans('messages.not_power_user'));
        }

    }
}
