<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MealParameters
{
    public function handle(Request $request, Closure $next): Response
    {
        $date = $request->route('date');
        $type = $request->route('type');

        return ! preg_match('/\d{4}-\d{2}-\d{2}/', $date) || ! in_array($type, [
            'breakfast', 'lunch', 'dinner', 'snack', 'bar',
        ]) ? redirect('home')->with('error', __('messages.mealInvalidParameters')) : $next($request);

    }
}
