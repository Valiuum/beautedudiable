<?php

namespace App\Providers;

use App\Interfaces\OpenFoodFacts\OpenFoodFactsInterface;
use App\Listeners\BadgeEventSubscriber;
use App\Listeners\MealEventSubscriber;
use App\Models\User;
use App\Resources\OpenFoodFacts\OpenFoodFacts;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
        $this->app->singleton(OpenFoodFactsInterface::class, OpenFoodFacts::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Event::subscribe(MealEventSubscriber::class);
        Event::subscribe(BadgeEventSubscriber::class);
        Gate::define('viewPulse', function (User $user) {
            return $user->isAdmin();
        });
    }
}
