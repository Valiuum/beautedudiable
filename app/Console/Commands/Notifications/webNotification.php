<?php

namespace App\Console\Commands\Notifications;

use App\Models\User;
use App\Notifications\WebPushNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class webNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-web-push-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send web push notification to users';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Web Notifications
        // Only for User's remember meal

        $users = User::all();

        foreach ($users as $notify_user) {

            if ($notify_user->role !== 'power') {
                continue;
            }

            if ($notify_user->preferences) {
                $preferences = collect(json_decode($notify_user->preferences, true));
                // $this->info('Checking user: '.$notify_user->name);
                if (! is_null($preferences) && $preferences->get('notificationActivate') && $preferences->get('notificationPeriod') !== '0') {
                    $ref_time = Carbon::now()->timezone('Europe/Paris')->format('H:i');
                    $ref_day = Carbon::now()->timezone('Europe/Paris')->format('N');
                    $notify_time = $preferences->get('notificationTime');
                    // $this->info('Reference time: '.$ref_time);
                    // $this->info('Notification time: '.$notify_time);
                    // $this->info('Notification period: '.$preferences->get('notificationPeriod'));
                    // $this->info('Notification day: '.$ref_day);
                    $allow_notify = false;
                    switch ($preferences->get('notificationPeriod')) {
                        case '0':
                            $this->info('Notification deactivated');
                            break;
                        case '1':
                            $this->info('Sunday notification');
                            if ($ref_day == 7) {
                                $allow_notify = true;
                            }
                            break;
                        case '2':
                            $this->info('Daily notification');
                            $allow_notify = true;
                            break;
                    }

                    if ($ref_time == $notify_time && $allow_notify) {
                        $notify_user->notify(new WebPushNotification('reminder_meal'));
                        $this->info('Web Notifications successfully sent to '.$notify_user->name);
                    }
                }
            }
        }

        $this->info('Web Notifications successfully ended');

        // Only for test purpose
        /*
        $user = User::find(1);
        $user->notify(new WebPushNotification('reminder_meal'));
        */
    }
}
