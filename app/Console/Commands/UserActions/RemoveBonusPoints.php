<?php

namespace App\Console\Commands\UserActions;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveBonusPoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:remove-bonus-points';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete bonus points from the database';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Removing unused bonus points...');

        // Your logic here
        if (Carbon::now()->isMonday()) {

            $users = User::where('role', 'power')->get();
            $this->info('Removing bonus points from '.$users->count().' users...');
            foreach ($users as $user) {
                if ($user->bonus()->exists()) {
                    $user->bonus()->delete();
                    $this->info($user->bonus()->count().' bonus points removed from '.$user->name);
                }
            }

        }

        $this->info('Bonus points removed successfully.');
    }
}
