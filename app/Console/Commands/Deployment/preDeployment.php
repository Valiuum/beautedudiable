<?php

namespace App\Console\Commands\Deployment;

use Illuminate\Console\Command;

class preDeployment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:pre-deployment';

    private $progress_bar;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'execute pre-deployment tasks';

    private function runTask(string $taskName, string $command, int $progress): void
    {
        $this->line("$taskName is starting...");
        $this->newLine(2);
        $output = shell_exec($command);
        $this->info($output);
        $this->newLine(2);
        $this->progress_bar->advance($progress);
        $this->newLine(2);
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->newLine();
        $this->progress_bar = $this->output->createProgressBar(100);
        $this->progress_bar->start();
        $this->newLine(2);
        $this->info('Starting pre-deployment tasks');
        $this->newLine();
        $this->runTask('Laravel Pint', './vendor/bin/pint -v', 80);
        $this->progress_bar->finish();
        $this->newLine(2);
        $this->info('Pre-deployment tasks completed successfully 🎉');
    }
}
