<?php

namespace App\Console\Commands\Deployment;

use Illuminate\Console\Command;

class postDeployment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:post-deployment';

    private $progress_bar;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'execute post-deployment tasks';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $current_user = trim(shell_exec('id -un'));
        if ($current_user !== 'root') {
            $this->error('root user is required to run post-deployment tasks');
            $this->line("You are currently log as $current_user");

            return;
        }

        // check if app is up

        $update = $this->choice(
            'Do you want to update dependencies ?',
            ['No', 'Yes'],
            'Yes'
        );

        $this->newLine(1);
        $this->info('Starting post-deployment tasks');
        $this->newLine(1);
        $this->progress_bar = $this->output->createProgressBar(100);
        $this->progress_bar->start();
        $this->newLine(1);

        if ($update === 'Yes') {
            $this->info('Updating dependencies...');
            $this->runTask('Composer update', 'sudo -u Valentin php82 composer.phar update -o', 10);
            $this->runTask('npm update', 'npm update', 20);
            $this->info('Dependencies updated successfully');
        }

        $this->runTask('npm build', 'npm run build', 30);

        $this->runTask('clear old files', 'php82 artisan optimize:clear', 40);
        $this->runTask('cache files', 'php82 artisan optimize', 50);
        $this->runTask('remove cache from env', 'php82 artisan config:clear', 52);

        $this->runTask('bootstrap folder permissions', 'chmod 775 -R bootstrap/cache', 55);
        $this->runTask('bootstrap folder owner', 'chown Valentin:http -R bootstrap/cache', 60);
        shell_exec('chown root:root bootstrap/cache/.gitignore');
        shell_exec('rm /bootstrap/cache/config');

        $this->runTask('storage folder permissions', 'chmod 775 -R storage', 65);
        $this->runTask('storage folder owner', 'chown Valentin:http -R storage', 70);
        shell_exec('chown root:root storage/.DS_Store');
        shell_exec('chown Valentin:http -R app/Console/Commands/Deployment');
        shell_exec('chmod 775 -R app/Console/Commands/Deployment');
        $this->progress_bar->advance(75);

        $process_queue = shell_exec('ps aux | grep "artisan queue:work" | grep -v grep | wc -l');
        if ($process_queue < 1) {
            $this->info("Laravel Queue isn't started...");
            // $this->runTask('Queue','php82 artisan queue:work --daemon --queue=default --memory=128 --timeout=60 --sleep=3 --tries=3 &',80);
        }
        $this->progress_bar->advance(80);

        $process_reverb = shell_exec('ps aux | grep "artisan reverb:start" | grep -v grep | wc -l');
        if ($process_reverb < 1) {
            $this->info("Laravel Reverb isn't started...");
            // $this->runTask('Reverb', 'php82 artisan reverb:start', 95);
        }
        $this->progress_bar->advance(95);
        $this->progress_bar->finish();
        $this->info('Post-deployment tasks completed successfully 🎉');
    }

    private function runTask(string $taskName, string $command, int $progress): void
    {
        $this->line("$taskName is starting...");
        $this->newLine(1);
        $output = shell_exec($command);
        $this->info($output);
        $this->newLine(1);
        $this->progress_bar->advance($progress);
        $this->newLine(2);
    }
}
