<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointWeekly extends Model
{
    use HasFactory;

    public $table = 'points_weekly';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'points',
        'status',
        'date',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
