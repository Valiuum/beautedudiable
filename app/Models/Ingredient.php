<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'points',
        'quantity',
        'unit',
        'energy',
        'proteins',
        'lipids',
        'carbohydrates',
        'fibers',
        'open_food_facts_id',
    ];

    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
        ];
    }

    public function scopeSearch($query, $value)
    {
        return self::where('name', '!=', 'bonus')
            ->whereRaw('MATCH(name, description) AGAINST(? IN NATURAL LANGUAGE MODE)', [$value]);

    }

    public function scopeSearchMysql($query, $value)
    {

        return $query->whereAny([
            'name',
            'description',
            'points',
        ], 'like', "%{$value}%")
            ->orderByRaw('LOCATE(?, name)', [$value]);

    }

    public function meals(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Meal')
            ->withPivot('points');
    }

    public function dishes(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Dish')
            ->withPivot('meal_id', 'dish_id', 'ingredient_id', 'points');
    }
}
