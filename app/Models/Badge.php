<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function unlockActionFor(User $user, string $action, int $count)
    {
        $badge = $this->where('action', $action)
            ->where('action_count', $count)
            ->first();

        if ($badge && ! $badge->isUnlockedFor($user)) {
            $user->badges()->attach($badge, [
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            return $badge;
        }

    }

    public function isUnlockedFor(User $user): bool
    {
        return $this->unlocks()
            ->where('user_id', $user->id)
            ->exists();
    }

    public function unlocks(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(BadgeUnlock::class);
    }
}
