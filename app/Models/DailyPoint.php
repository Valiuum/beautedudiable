<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyPoint extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'points',
        'meals',
        'date',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
