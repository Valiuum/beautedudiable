<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'type',
    ];

    public function ingredients()
    {
        return $this->belongsToMany('App\Models\Ingredient')
            ->withPivot('points');
    }

    public function meals()
    {
        return $this->belongsToMany('App\Models\Meal')
            ->withPivot('dish_id', 'meal_id', 'ingredient_id', 'points');
    }
}
