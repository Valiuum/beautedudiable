<?php

namespace App\Interfaces\OpenFoodFacts;

interface OpenFoodFactsInterface
{
    public function search(string $elem, int $page = 1);

    public function barcode(string $elem);
}
