<?php

namespace App\Livewire\Calendar;

use App\Livewire\Forms\ActivityForm;
use App\Livewire\Forms\CycleForm;
use App\Livewire\Forms\WeightForm;
use Carbon\Carbon;
use Livewire\Attributes\On;
use Livewire\Component;

class Calendar extends Component
{
    public object $user;

    public string $calendarEvents;

    public CycleForm $cycleForm;

    public ActivityForm $activityForm;

    public WeightForm $weightForm;

    public function mount()
    {
        $this->user = Auth()->user();
        $this->getEvents();
    }

    #[On('event-update')]
    public function updateCalendarEvent($data)
    {

        if ($data['action'] === 'resize') {

            try {
                // Get the cycle
                $cycle = $this->user->cycles()->where('id', $data['eventData']['publicId'])->first();
                // Set the cycleForm properties based on the $data
                $this->cycleForm->id = $data['eventData']['publicId'];
                $this->cycleForm->date = Carbon::parse($data['eventDates']['start'])->format('Y-m-d');
                $this->cycleForm->duration = $cycle->duration + $data['eventDeltaEnd']['days'];

                // Validate the cycleForm
                $this->cycleForm->validate();
                $cycle->duration = $this->cycleForm->duration;
                $cycle->save();
                $this->dispatch('cycle-updated', data: ['success' => ['message' => __('messages.cycleEdited')]]);
            } catch (\Illuminate\Validation\ValidationException $e) {
                $this->dispatch('cycle-updated', data: ['errors' => $e->validator->errors()]);
            }

        }

        if ($data['action'] === 'drop') {

            switch ($data['eventData']['extendedProps']['event_type']) {
                case 'cycle':
                    try {
                        // Get the cycle
                        $cycle = $this->user->cycles()->where('id', $data['eventData']['publicId'])->first();
                        // Set the cycleForm properties based on the $data
                        $this->cycleForm->id = $data['eventData']['publicId'];
                        $this->cycleForm->date = Carbon::parse($data['eventDates']['start'])->format('Y-m-d');

                        // Validate the cycleForm
                        $this->cycleForm->validate();
                        $cycle->date = $this->cycleForm->date;
                        $cycle->save();
                        $this->dispatch('event-updated',
                            data: ['success' => ['message' => __('messages.cycleEdited')]]);
                    } catch (\Illuminate\Validation\ValidationException $e) {
                        $this->dispatch('event-updated', data: ['errors' => $e->validator->errors()]);
                    }
                    break;
                case 'activity':
                    try {
                        // Get activity
                        $activity = $this->user->activities()->where('id', $data['eventData']['publicId'])->first();
                        // Set the cycleForm properties based on the $data
                        $this->activityForm->id = $data['eventData']['publicId'];
                        $this->activityForm->activity = $activity->activity;
                        $this->activityForm->date = Carbon::parse($data['eventDates']['start'])->format('Y-m-d');

                        // Validate the activityForm
                        $this->activityForm->validate();
                        $activity->date = $this->activityForm->date;
                        $activity->save();
                        $this->dispatch('event-updated',
                            data: ['success' => ['message' => __('messages.activityEdited')]]);
                    } catch (\Illuminate\Validation\ValidationException $e) {
                        $this->dispatch('event-updated', data: ['errors' => $e->validator->errors()]);
                    }
                    break;
                case 'weight':
                    try {
                        // Get weight
                        $weight = $this->user->weights()->where('id', $data['eventData']['publicId'])->first();
                        // Set the cycleForm properties based on the $data
                        $this->weightForm->id = $data['eventData']['publicId'];
                        $this->weightForm->weighing = $weight->weighing;
                        $this->weightForm->date = Carbon::parse($data['eventDates']['start'])->format('Y-m-d');

                        // Validate the weightForm
                        $this->weightForm->validate();
                        $weight->date = $this->weightForm->date;
                        $weight->save();
                        $this->dispatch('event-updated',
                            data: ['success' => ['message' => __('messages.weighingEdited')]]);
                    } catch (\Illuminate\Validation\ValidationException $e) {
                        $this->dispatch('event-updated', data: ['errors' => $e->validator->errors()]);
                    }
                    break;
            }

        }

    }

    public function getEvents(): void
    {
        // Collect User Events
        $data = collect();

        // Daily Points
        $dailyPoints = $this->user->dailyPoints()->get();
        foreach ($dailyPoints as $dailyPoint) {
            $data->push([
                'id' => $dailyPoint->id,
                'title' => trans_choice('messages.pointsDisplay', $dailyPoint->points,
                    ['value' => $dailyPoint->points]),
                'start' => Carbon::parse($dailyPoint->date)->format('Y-m-d'),
                'end' => Carbon::parse($dailyPoint->date)->format('Y-m-d'),
                'editable' => false,
                'classNames' => ['daily-point-event', 'has-text-weight-semibold', 'pl-1'],
                'extendedProps' => [
                    'event_type' => 'dailyPoint',
                ],
                'color' => '#f4a261',
                'textColor' => '#fff',
            ]);
        }

        // Cycles
        if ($this->user->gender === 'F') {
            $cycles = $this->user->cycles()->get();
            foreach ($cycles as $cycle) {
                $data->push([
                    'id' => $cycle->id,
                    'title' => __('messages.cycle'),
                    'start' => Carbon::parse($cycle->date)->format('Y-m-d'),
                    'end' => Carbon::parse($cycle->date)->addDays($cycle->duration)->format('Y-m-d'),
                    'durationEditable' => true,
                    'color' => '#d88c9a',
                    'textColor' => '#fff',
                    'classNames' => ['cycle-event', 'has-text-weight-semibold', 'pl-1'],
                    'extendedProps' => [
                        'event_type' => 'cycle',
                    ],
                    'url' => route('cycle.edit', ['cycle' => $cycle->id]),
                ]);
            }
        }

        // Activities
        $activities = $this->user->activities()->get();

        foreach ($activities as $activity) {
            switch ($activity->activity) {
                case 'workout':
                    $data->push([
                        'id' => $activity->id,
                        'title' => __('messages.workout'),
                        'start' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'end' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'durationEditable' => false,
                        'classNames' => ['activity-event', 'has-text-weight-semibold', 'pl-1'],
                        'extendedProps' => [
                            'event_type' => 'activity',
                            'event_subtype' => 'workout',
                        ],
                        'color' => '#848dce',
                        'textColor' => '#fff',
                        'url' => route('activity.edit', ['activity' => $activity->id]),
                    ]);
                    break;

                case 'running':
                    $data->push([
                        'id' => $activity->id,
                        'title' => __('messages.running'),
                        'start' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'end' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'durationEditable' => false,
                        'classNames' => ['activity-event', 'has-text-weight-semibold', 'pl-1'],
                        'extendedProps' => [
                            'event_type' => 'activity',
                            'event_subtype' => 'running',
                        ],
                        'color' => '#99c1b9',
                        'textColor' => '#fff',
                        'url' => route('activity.edit', ['activity' => $activity->id]),
                    ]);
                    break;

                case 'hiking':
                    $data->push([
                        'id' => $activity->id,
                        'title' => __('messages.hiking'),
                        'start' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'end' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'durationEditable' => false,
                        'classNames' => ['activity-event', 'has-text-weight-semibold', 'pl-1'],
                        'extendedProps' => [
                            'event_type' => 'activity',
                            'event_subtype' => 'hiking',
                        ],
                        'color' => '#c5d2c6',
                        'textColor' => '#fff',
                        'url' => route('activity.edit', ['activity' => $activity->id]),
                    ]);
                    break;

                case 'yoga':
                    $data->push([
                        'id' => $activity->id,
                        'title' => __('messages.yoga'),
                        'start' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'end' => Carbon::parse($activity->date)->format('Y-m-d'),
                        'durationEditable' => false,
                        'classNames' => ['activity-event', 'has-text-weight-semibold', 'pl-1'],
                        'extendedProps' => [
                            'event_type' => 'activity',
                            'event_subtype' => 'yoga',
                        ],
                        'color' => '#643256',
                        'textColor' => '#fff',
                        'url' => route('activity.edit', ['activity' => $activity->id]),
                    ]);
                    break;
            }
        }

        // Weights
        $weights = $this->user->weights()->get();
        foreach ($weights as $weight) {
            $data->push([
                'id' => $weight->id,
                'title' => __('messages.weighing'),
                'start' => Carbon::parse($weight->date)->format('Y-m-d'),
                'end' => Carbon::parse($weight->date)->format('Y-m-d'),
                'durationEditable' => false,
                'classNames' => ['weight-event', 'has-text-weight-semibold', 'pl-1'],
                'extendedProps' => [
                    'event_type' => 'weight',
                ],
                'color' => '#73d2de',
                'textColor' => '#fff',
                'url' => route('weight.edit', ['weight' => $weight->id]),
            ]);
        }

        $this->calendarEvents = $data->toJson();
    }

    public function render()
    {
        return view('livewire.calendar.calendar');
    }
}
