<?php

namespace App\Livewire\Foods;

use App\Events\MealProcessed;
use App\Interfaces\OpenFoodFacts\OpenFoodFactsInterface;
use App\Livewire\Forms\MealsForm;
use App\Models\Dish;
use App\Models\Ingredient;
use App\Models\Meal;
use App\Traits\UserActions;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Attributes\On;
use Livewire\Component;

class Meals extends Component
{
    use UserActions;

    public MealsForm $form;

    public string $search = '';

    public string $type = '';

    public array $mealData = [];

    public $dishesTags;

    public $dishes;

    public array $shareWith = [];

    public int $shared_meal_with = 0;

    public $existing_meal = false;

    private OpenFoodFactsInterface $openFoodFacts;

    public function boot(OpenFoodFactsInterface $openFoodFacts)
    {
        $this->openFoodFacts = $openFoodFacts;
    }

    public function mount(array $meal): void
    {
        foreach ($meal as $key => $value) {
            if (property_exists($this->form, $key)) {
                $this->form->$key = $value;
            }
            if ($key === 'action' && $value !== 'create') {
                $meals_data = collect($meal['data']);
                $this->form->meal_id = $meal['id'];
                $this->mealData['ingredients'] = collect($meals_data['ingredients'])->sortBy('name')->values()->all();
                $this->mealData['dishes'] = collect($meals_data['dishes']);
                $this->dishesTags = collect($meals_data['dishes_tags']);
                $this->form->meal_previous_points = $meals_data['meal_previous_points'];
            } elseif ($key === 'action' && $value === 'create') {
                $meals_data = collect($meal['data']);
                $this->dishesTags = collect($meals_data['dishes_tags']);
                $this->mealData['ingredients'] = [];
                $this->mealData['dishes'] = [];
                $this->form->meal_id = 0;
            }

            if ($key === 'action' && $value === 'duplicate') {
                $this->form->meal_id = 0;
            }

        }

        $this->mealData['user_bonus'] = $meal['data']['user_bonus'];

    }

    public function searchForOpenFoodFacts($id)
    {
        if (empty($id) || ! ctype_digit((string) $id)) {
            return;
        } else {
            $element = $this->openFoodFacts->barcode($id);
            $this->dispatch('open-food-facts', $element);
        }
    }

    #[On('datepicker-update')]
    public function updateMealDate($data)
    {
        $this->form->new_date = Carbon::parse($data['date'])->format('Y-m-d');

        // Check if the meal already exists
        $meal = auth()->user()->meals()
            ->whereDate('created_at', Carbon::parse($this->form->new_date)->format('Y-m-d'))
            ->where('name', $this->form->type)
            ->where('id', '!=', $this->form->meal_id)
            ->first();

        if ($meal) {
            $this->existing_meal = $meal;
        } else {
            $this->existing_meal = false;
        }

    }

    public function addDish($id): void
    {
        $dish = Dish::find($id)->ingredients;
        // Add the dish to the meal
        $dish_meal_data = DB::table('dish_meal')
            ->where('dish_id', $id)
            ->where('meal_id', $this->form->meal_id)
            ->get();

        $dish_ingredients = $dish->map(function ($item) use ($id, $dish_meal_data) {
            if (collect($dish_meal_data)->where('ingredient_id', $item->id)->first()) {
                $item['dish_meal_points'] = collect($dish_meal_data)->where('ingredient_id',
                    $item->id)->first()->points;
            }
            $item['dish_id'] = $id;
            $item['dish_ingredient_points'] = $item->pivot->points;

            return $item;
        })->sortBy('name')->values()->all();

        // Dispatch the event
        $this->dispatch('dish-updated', data: [
            'id' => $id,
            'dish_name' => Dish::find($id)->name,
            'ingredients' => $dish_ingredients,
        ]);
    }

    public function saveToMeal($ingredients, $dishes, $bonus): void
    {
        $this->form->ingredients = $ingredients;
        $this->form->dishes = $dishes;
        $this->form->user_bonus = $bonus;
    }

    public function save(): void
    {

        if ($this->shared_meal_with) {
            $this->form->shared_meal_with = $this->shared_meal_with;
        }

        if ($this->type) {
            $this->form->type = $this->type;
        }

        $this->validate();

        if ($this->existing_meal) {
            $this->form->meal_to_delete = collect($this->existing_meal)->toArray();
        }

        if ($this->form->user_bonus) {
            $bonus_ingredient = Ingredient::where('name', 'bonus')->first();
            $bonus_ingredient->points = $this->form->user_bonus * -1;
            $this->form->ingredients[] = collect($bonus_ingredient)->toArray();
        }

        if ($this->form->action === 'duplicate') {
            $ingredients = collect($this->form->ingredients)->reject(function ($ingredient) {
                return $ingredient['name'] === 'bonus';
            })->values()->all();
        } else {
            $ingredients = $this->form->ingredients;
        }

        $dishes_ingredients = $this->form->dishes;

        if ($this->form->meal_id !== 0) {
            $meal = Meal::find($this->form->meal_id);
            $meal->name = $this->form->type;
            // Check if the meal date has been changed
            if ($this->form->new_date) {
                $meal->updated_at = Carbon::parse($this->form->new_date)->setTimeFrom(Carbon::now());
                $meal->changed = 1;
            }
            $meal->save();
        } else {
            $meal = new Meal;
            $meal->name = $this->form->type;
            $meal->user_id = auth()->id();
            $meal->created_at = $this->form->new_date !== '' ? Carbon::parse($this->form->new_date)->setTimeFrom(Carbon::now()) : Carbon::parse($this->form->date)->setTimeFrom(Carbon::now());
            $meal->changed = 1;
            $meal->save();
        }

        $meal_ingredients = collect($ingredients)->map(function ($ingredient) use ($meal) {
            return [
                'meal_id' => $meal->id,
                'ingredient_id' => $ingredient['id'],
                'points' => $ingredient['points'],
            ];
        })->toArray();

        $dish_meal = collect($dishes_ingredients)->map(function ($ingredients) use ($meal) {
            $dish_id = $ingredients['id'];

            return collect($ingredients['ingredients'])->map(function ($ingredient) use ($meal, $dish_id) {
                return [
                    'dish_id' => $dish_id,
                    'meal_id' => $meal->id,
                    'ingredient_id' => $ingredient['id'],
                    'points' => $ingredient['dish_meal_points'] ?? $ingredient['dish_ingredient_points'],
                ];
            });
        })->flatten(1)->toArray();

        if ($this->form->meal_id !== 0) {
            $meal->ingredients()->detach();
            $meal->ingredients()->sync($meal_ingredients);
            $meal->dishes()->detach();
            $meal->dishes()->sync($dish_meal);
        } else {
            $meal->ingredients()->sync($meal_ingredients);
            $meal->dishes()->sync($dish_meal);
        }

        MealProcessed::dispatch($meal->id, $this->form->toArray());

        switch ($this->form->action) {
            case 'create':
                session()->flash('success', __('messages.mealSaved'));
                break;
            case 'edit':
                session()->flash('success', __('messages.mealEdited'));
                break;
            case 'duplicate':
                session()->flash('success', __('messages.mealDuplicated'));
                break;
        }
        $this->redirectIntended('/home');
    }

    public function render()
    {

        $ingredients = Ingredient::query();
        $results = [];

        if (! empty($this->search) && strlen($this->search) >= 2) {
            $ingredients = $ingredients
                ->where('name', '!=', 'bonus')
                ->search($this->search)
                ->limit(10)
                ->select([
                    'id',
                    'name',
                    'description',
                    'points',
                    'quantity',
                    'unit',
                    'energy',
                    'lipids',
                    'proteins',
                    'carbohydrates',
                    'fibers',
                    'open_food_facts_id',
                ])
                ->orderBy('name', 'ASC')
                ->get();

            $results = $ingredients;

            // If the search does not return any results, perform another search
            if ($ingredients->count() == 0) {
                $ingredients = Ingredient::query()
                    ->where('name', '!=', 'bonus')
                    ->select([
                        'id',
                        'name',
                        'description',
                        'points',
                        'quantity',
                        'unit',
                        'energy',
                        'proteins',
                        'lipids',
                        'carbohydrates',
                        'fibers',
                        'open_food_facts_id',
                    ])
                    ->searchMysql($this->search)
                    ->limit(60)
                    ->orderBy('name', 'ASC')
                    ->get();
                $results = $ingredients;
            }

            $results = collect($results)->add($this->dishesTags);
        }

        $this->shareWith = $this->shareWithList();

        return view('livewire.foods.meals', ['results' => $results]);
    }
}
