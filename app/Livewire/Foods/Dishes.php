<?php

namespace App\Livewire\Foods;

use App\Interfaces\OpenFoodFacts\OpenFoodFactsInterface;
use App\Livewire\Forms\DishForm;
use App\Models\Dish;
use App\Models\Ingredient;
use Livewire\Component;

class Dishes extends Component
{
    public DishForm $form;

    public string $search = '';

    public $existing_dish = false;

    public $existing_dish_name = false;

    public array $dishData = [];

    private OpenFoodFactsInterface $openFoodFacts;

    public function boot(OpenFoodFactsInterface $openFoodFacts)
    {
        $this->openFoodFacts = $openFoodFacts;
    }

    public function mount($dish): void
    {
        foreach ($dish as $key => $value) {

            if (property_exists($this->form, $key)) {
                $this->form->$key = $value;
            }

            if ($key === 'action' && $value === 'create') {
                $this->dishData['ingredients'] = [];
            } elseif ($key === 'action' && $value !== 'create') {
                $this->dishData['ingredients'] = collect($dish['data']['dish']['ingredients'])->sortBy('name')->values()->all();
                $this->dishData['name'] = $dish['data']['dish']['name'];
                $this->form->dish_id = $dish['data']['dish']['id'];
            }

        }
    }

    public function saveToDish($name, $ingredients)
    {
        $this->form->name = $name;
        $this->form->ingredients = $ingredients;

    }

    public function searchForOpenFoodFacts($id)
    {
        if (empty($id) || ! ctype_digit((string) $id)) {
            return;
        } else {
            $element = $this->openFoodFacts->barcode($id);
            $this->dispatch('open-food-facts', $element);
        }
    }

    public function save()
    {

        $this->validate();

        $this->existing_dish = Dish::where('name', $this->form->name)
            ->whereNot('id', $this->form->dish_id)
            ->exists();

        if (! $this->existing_dish) {

            if ($this->form->dish_id !== 0 && $this->form->action === 'edit') {

                $dish = Dish::find($this->form->dish_id);
                $dish->update([
                    'name' => $this->form->name,
                ]);

                $dish_ingredients = collect($this->form->ingredients)->map(function ($ingredient) use ($dish) {
                    return [
                        'dish_id' => $dish->id,
                        'ingredient_id' => $ingredient['id'],
                        'points' => $ingredient['points'],
                    ];
                })->toArray();

                $dish->ingredients()->detach();

            } elseif ($this->form->action === 'duplicate' || $this->form->action === 'create') {

                $dish = Dish::create([
                    'user_id' => auth()->id(),
                    'name' => $this->form->name,
                    'type' => $this->form->type,
                ]);

                $dish_ingredients = collect($this->form->ingredients)->map(function ($ingredient) use ($dish) {
                    return [
                        'dish_id' => $dish->id,
                        'ingredient_id' => $ingredient['id'],
                        'points' => $ingredient['points'],
                    ];
                })->toArray();

            }
            $dish->ingredients()->sync($dish_ingredients);

            $this->form->reset();

            $this->redirect('/dishes/show');

        }

    }

    public function render()
    {

        $ingredients = Ingredient::query();
        $results = [];

        if (! empty($this->search) && strlen($this->search) >= 2) {
            $ingredients = $ingredients
                ->where('name', '!=', 'bonus')
                ->search($this->search)
                ->limit(10)
                ->select([
                    'id',
                    'name',
                    'description',
                    'points',
                    'quantity',
                    'unit',
                    'energy',
                    'lipids',
                    'proteins',
                    'carbohydrates',
                    'fibers',
                    'open_food_facts_id',
                ])
                ->orderBy('name', 'ASC')
                ->get();

            $results = $ingredients;

            // If the search does not return any results, perform another search
            if ($ingredients->count() == 0) {
                $ingredients = Ingredient::query()
                    ->where('name', '!=', 'bonus')
                    ->select([
                        'id',
                        'name',
                        'description',
                        'points',
                        'quantity',
                        'unit',
                        'energy',
                        'proteins',
                        'lipids',
                        'carbohydrates',
                        'fibers',
                        'open_food_facts_id',
                    ])
                    ->searchMysql($this->search)
                    ->limit(60)
                    ->orderBy('name', 'ASC')
                    ->get();
                $results = $ingredients;
            }

        }

        return view('livewire.foods.dishes', ['results' => $results]);
    }
}
