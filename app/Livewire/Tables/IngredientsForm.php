<?php

namespace App\Livewire\Tables;

use App\Interfaces\OpenFoodFacts\OpenFoodFactsInterface;
use App\Livewire\Forms\IngredientForm;
use App\Models\Ingredient;
use Livewire\Attributes\On;
use Livewire\Component;

class IngredientsForm extends Component
{
    public IngredientForm $form;

    private OpenFoodFactsInterface $openFoodFacts;

    public array $searchOpenFoodFacts = [];

    public array $openFoodFactsProduct = [];

    // 8712566328208
    public string $search = '';

    public string $currentSearch = '';

    public bool $displayTable;

    public bool $singleResult = false;

    public int $page = 1;

    public $darkMode = false;

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark';
    }

    public function boot(OpenFoodFactsInterface $openFoodFacts)
    {
        $this->openFoodFacts = $openFoodFacts;
    }

    public function mount(bool $displayTable, ?Ingredient $ingredient = null): void
    {
        if ($ingredient) {
            foreach ($ingredient->getAttributes() as $key => $value) {
                if (property_exists($this->form, $key)) {
                    if ($key === 'open_food_facts_id' && $value === null) {
                        $value = '';
                    } else {
                        $this->form->$key = $value === null ? 0.0 : $value;
                    }
                }
            }
        }
        $this->displayTable = $displayTable;
    }

    public function openFoodFacts(): void
    {
        if ($this->currentSearch === $this->search) {
            return;
        }

        if (ctype_digit((string) $this->search)) {
            $this->searchOpenFoodFacts = $this->openFoodFacts->barcode($this->search);
            $this->singleResult = true;
            $this->currentSearch = $this->search;
        } elseif (is_string($this->search)) {
            $this->page = 1;
            $this->searchOpenFoodFacts = $this->openFoodFacts->search($this->search);
            $this->currentSearch = $this->search;
            $this->singleResult = false;
        }

    }

    public function openFoodFactsLoadMore(): void
    {
        if ($this->singleResult || isset($this->searchOpenFoodFacts['status']) && $this->searchOpenFoodFacts['status'] === 'error') {
            return;
        }
        $totalPages = ceil($this->searchOpenFoodFacts['count'] / $this->searchOpenFoodFacts['page_size']);
        if ($this->page < $totalPages) {
            $this->page++;
            $data = $this->openFoodFacts->search($this->search, $this->page);
            if (isset($data['products']) && $data['page'] === $this->page) {
                $this->searchOpenFoodFacts['products'] = array_merge($this->searchOpenFoodFacts['products'],
                    $data['products']);
            }
        }
    }

    public function addToForm($productID)
    {
        if ($this->singleResult) {
            $this->openFoodFactsProduct = $this->searchOpenFoodFacts['product'];
        } else {
            $products = collect($this->searchOpenFoodFacts['products']);
            $this->openFoodFactsProduct = $products->firstWhere('_id', $productID);
        }
        $this->calculateFormValues();
    }

    private function calculateFormValues(): void
    {
        $product = $this->openFoodFactsProduct;
        $nutriments = $product['nutriments'] ?? [];

        $this->form->fill([
            'name' => $product['product_name'] ?? '',
            'description' => $product['generic_name'] ?? '',
            'points' => $this->ingredientPointsWW() ?? 0,
            'quantity' => $product['product_quantity'] ?? 0,
            'unit' => $product['product_quantity_unit'] ?? '',
            'energy' => $nutriments['energy-kcal_100g'] ?? 0,
            'proteins' => $nutriments['proteins_100g'] ?? 0,
            'lipids' => $nutriments['fat_100g'] ?? 0,
            'carbohydrates' => $nutriments['carbohydrates_100g'] ?? 0,
            'fibers' => $nutriments['fiber_100g'] ?? 0,
            'open_food_facts_id' => $product['_id'] ?? '',
        ]);
    }

    private function ingredientPointsWW(): float
    {

        $seuil = 10;

        $nutriments_serving = collect($this->openFoodFactsProduct['nutriments'])->filter(function ($value, $key) {
            return str_contains($key, '_serving');
        });

        $nutriments_grammes = collect($this->openFoodFactsProduct['nutriments'])->filter(function ($value, $key) {
            return str_contains($key, '_100g');
        });

        $nutriments = $nutriments_serving->count() > $seuil ? $nutriments_serving : $nutriments_grammes;

        $kcal = $nutriments_serving->count() > $seuil ? $nutriments['energy-kcal_serving'] ?? 0 : $nutriments['energy-kcal_100g'] ?? 0;
        $lipides = $nutriments_serving->count() > $seuil ? $nutriments['fat_serving'] ?? 0 : $nutriments['fat_100g'] ?? 0;

        // Points WW = (kcal / 60) + (lipides / 9)
        $points = ($kcal / 60) + ($lipides / 9);

        return round($points * 2) / 2;
    }

    public function save()
    {
        $this->validate();
        Ingredient::updateOrCreate(
            ['id' => $this->form->id ?? null],
            $this->form->toArray()
        );
        $this->dispatch('ingredientUpdated');
        $this->form->reset([
            'name',
            'description',
            'points',
            'quantity',
            'unit',
            'energy',
            'proteins',
            'lipids',
            'carbohydrates',
            'fibers',
            'open_food_facts_id',
        ]);
    }

    public function render()
    {
        $units = $this->form->units;
        sort($units);

        return view('livewire.tables.ingredients-form', [
            'units' => $units,
        ]);
    }
}
