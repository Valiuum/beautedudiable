<?php

namespace App\Livewire\Tables;

use App\Livewire\Component\Datepicker;
use App\Livewire\Forms\ActivityForm;
use App\Models\Activity;
use App\Traits\UserActions;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\On;
use Livewire\Component;

class ActivitiesForm extends Component
{
    use UserActions;

    public ActivityForm $form;

    public bool $displayTable;

    public string $search = '';

    public string $activity = '';

    public array $select_activities = [];

    public bool $edit = false;

    public string $previous_date_picker_date = '';

    public $darkMode = false;

    public function mount(bool $displayTable, bool $edit, ?Activity $activity = null): void
    {
        if ($activity) {
            foreach ($activity->getAttributes() as $key => $value) {
                if (property_exists($this->form, $key)) {
                    $this->form->$key = $value === null ? 0.0 : $value;
                    if ($key === 'date') {
                        $this->form->date = Carbon::parse($value)->format('Y-m-d');
                    }
                }
            }
            $this->activity = $this->form->activity;
            if ($edit) {
                $this->edit = true;
            }
        }

        foreach ($this->form->activities as $activity) {
            $this->select_activities[$activity] = __("messages.$activity");
        }

        $this->form->user_id = Auth::id();

        $this->displayTable = $displayTable;
    }

    public function eventDateUpdate($action): void
    {
        if (! $this->edit) {
            return;
        }

        if ($action === 'init') {
            // Store the previous date
            $this->previous_date_picker_date = session('selectedDate', now()->format('Y-m-d'));
            // Dispatch the event
            $this->dispatch('datepicker-update', ['date' => $this->form->date])->to(Datepicker::class);
        }

        if ($action === 'restore') {
            if ($this->previous_date_picker_date) {
                session(['selectedDate' => $this->previous_date_picker_date]);
            }
        }

    }

    public function addUserActivityPoints($activity)
    {

        $points = 0;
        switch ($activity) {

            case 'hiking':
            case 'running':
                $points = 2;
                break;
            case 'yoga':
            case 'workout':
                $points = 1;
                break;

        }

        $this->handleBonusPoints(Auth::id(), $points);

    }

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark';
    }

    #[On('datepicker-update')]
    public function updateDate($data)
    {
        $this->form->date = Carbon::parse($data['date'])->format('Y-m-d');
    }

    public function save()
    {
        if ($this->activity) {
            $this->form->activity = $this->activity;
        }

        $this->validate();

        if ($this->edit) {
            $this->eventDateUpdate('restore');
        }

        if ($this->form->id === 0) {
            $activity = new Activity;
            $activity->activity = $this->form->activity;
            $activity->comment = $this->form->comment;
            $activity->user_id = $this->form->user_id;
            $this->addUserActivityPoints($this->form->activity);
        } else {
            $activity = Activity::find($this->form->id);
            $activity->activity = $this->form->activity;
            $activity->comment = $this->form->comment;
        }
        $activity->date = Carbon::parse($this->form->date)->format('Y-m-d');
        $activity->save();

        if ($this->form->id === 0) {
            session()->flash('success', __('messages.activitySaved'));
        } else {
            session()->flash('success', __('messages.activityEdited'));
        }

        $this->redirectIntended('/home');

    }

    public function render()
    {
        if ($this->edit) {
            $this->eventDateUpdate('init');
        }

        return view('livewire.tables.activities-form');
    }
}
