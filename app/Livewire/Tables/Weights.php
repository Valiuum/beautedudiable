<?php

namespace App\Livewire\Tables;

use App\Models\Weight;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithPagination;

class Weights extends Component
{
    use withPagination;

    public string $search = '';

    public string $orderField = 'date';

    public string $orderDirection = 'DESC';

    public int $paginate = 10;

    public int $editId = 0;

    public array $selection = [];

    public $darkMode = false;

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark';
    }

    protected array $queryString = [
        'search' => ['except' => ''],
        'orderField' => ['except' => 'name'],
        'orderDirection' => ['except' => 'ASC'],
    ];

    public function startEdit(int $id): void
    {
        $this->editId = $id;
    }

    public function updating($field, $value): void
    {
        if ($field === 'search') {
            $this->resetPage();
        }
    }

    public function paginationView()
    {
        return 'livewire.tables.pagination';
    }

    public function setOrderField(string $field)
    {
        if ($this->orderField === $field) {
            $this->orderDirection = $this->orderDirection === 'ASC' ? 'DESC' : 'ASC';
        } else {
            $this->orderField = $field;
            $this->reset('orderDirection');
        }
    }

    public function deleteSelected(): void
    {
        Weight::destroy($this->selection);
        $this->notifyDelete();
    }

    public function notifyDelete(): void
    {
        $this->dispatch('notify',
            ['message' => trans_choice('messages.weighingDeleted', count($this->selection)), 'type' => 'success']);
        $this->selection = [];
    }

    public function render()
    {

        $weights = Auth::user()->weights();

        if (strlen($this->search) > 2) {
            try {
                Carbon::setLocale('fr');
                $date = Carbon::parse($this->search)->format('Y-m-d');
                $weights = $weights->whereDate('date', $date);
            } catch (\Exception $e) {
                $weights = $weights->where('comment', 'like', '%'.$this->search.'%');
            }
        } elseif (ctype_digit($this->search)) {
            // If the search term is a number, search in the 'weighing' field
            $weights = $weights->where('weighing', 'like', '%'.$this->search.'%');
        }

        $weights = $weights->orderBy($this->orderField, $this->orderDirection)
            ->paginate($this->paginate);

        return view('livewire.tables.weights', ['weights' => $weights]);
    }
}
