<?php

namespace App\Livewire\Tables;

use App\Models\Activity;
use App\Traits\UserActions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithPagination;

class Activities extends Component
{
    use UserActions;
    use withPagination;

    public string $search = '';

    public string $orderField = 'date';

    public string $orderDirection = 'DESC';

    public int $paginate = 10;

    public int $editId = 0;

    public array $selection = [];

    public $darkMode = false;

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark';
    }

    protected array $queryString = [
        'search' => ['except' => ''],
        'orderField' => ['except' => 'name'],
        'orderDirection' => ['except' => 'ASC'],
    ];

    public function startEdit(int $id): void
    {
        $this->editId = $id;
    }

    public function updating($field, $value): void
    {
        if ($field === 'search') {
            $this->resetPage();
        }
    }

    public function paginationView()
    {
        return 'livewire.tables.pagination';
    }

    public function setOrderField(string $field)
    {
        if ($this->orderField === $field) {
            $this->orderDirection = $this->orderDirection === 'ASC' ? 'DESC' : 'ASC';
        } else {
            $this->orderField = $field;
            $this->reset('orderDirection');
        }
    }

    public function deleteSelected(): void
    {

        foreach ($this->selection as $id) {
            $activity = Activity::find($id);
            if ($activity) {
                $points = 0;
                switch ($activity->activity) {
                    case 'hiking':
                    case 'running':
                        $points = -2;
                        break;
                    case 'yoga':
                    case 'workout':
                        $points = -1;
                        break;
                }
                $this->handleBonusPoints(Auth::id(), $points);
            }
        }

        Activity::destroy($this->selection);
        $this->notifyDelete();
    }

    public function notifyDelete(): void
    {
        $this->dispatch('notify',
            ['message' => trans_choice('messages.activityDeleted', count($this->selection)), 'type' => 'success']);
        $this->selection = [];
    }

    public function render()
    {
        $activities = Auth::user()->activities();

        if (strlen($this->search) > 2) {

            $translations = trans('messages');
            $matching_value = [];

            foreach ($translations as $key => $value) {
                if (Str::startsWith($key, 'searchFrenchActivity_')) {
                    if (Str::contains(strtolower($value), strtolower($this->search))) {
                        $matching_value[] = $value;
                        break;
                    }
                }
            }

            if (count($matching_value) > 0) {
                $activities = $activities->where('activity',
                    $translations['searchEnglishActivity_translate_'.$matching_value[0]]);
            } else {
                $activities = $activities->where('activity', 'like', '%'.$this->search.'%');
            }

        }

        $activities = $activities->orderBy($this->orderField, $this->orderDirection)
            ->paginate($this->paginate);

        return view('livewire.tables.activities', ['activities' => $activities]);
    }
}
