<?php

namespace App\Livewire\Tables;

use App\Livewire\Component\Datepicker;
use App\Livewire\Forms\WeightForm;
use App\Models\Weight;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\On;
use Livewire\Component;

class WeightsForm extends Component
{
    public WeightForm $form;

    public bool $displayTable;

    public string $search = '';

    public bool $edit = false;

    public string $previous_date_picker_date = '';

    public $darkMode = false;

    public function mount(bool $displayTable, bool $edit, ?Weight $weight = null): void
    {
        if ($weight) {
            foreach ($weight->getAttributes() as $key => $value) {
                if (property_exists($this->form, $key)) {
                    $this->form->$key = $value === null ? 0.0 : $value;
                    if ($key === 'date') {
                        $this->form->date = Carbon::parse($value)->format('Y-m-d');
                    }
                }
            }
            if ($edit) {
                $this->edit = true;
            }
        }

        if ($this->form->weighing === 0.0) {
            if (Auth::user()->gender === 'M') {
                $this->form->weighing = 75.0;
            } else {
                $this->form->weighing = 65.0;
            }
        }

        $this->form->user_id = Auth::id();

        $this->displayTable = $displayTable;

    }

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark';
    }

    #[On('datepicker-update')]
    public function updateDate($data)
    {
        $this->form->date = Carbon::parse($data['date'])->format('Y-m-d');
    }

    public function eventDateUpdate($action): void
    {
        if (! $this->edit) {
            return;
        }

        if ($action === 'init') {
            // Store the previous date
            $this->previous_date_picker_date = session('selectedDate', now()->format('Y-m-d'));
            // Dispatch the event
            $this->dispatch('datepicker-update', ['date' => $this->form->date])->to(Datepicker::class);
        }

        if ($action === 'restore') {
            if ($this->previous_date_picker_date) {
                session(['selectedDate' => $this->previous_date_picker_date]);
            }
        }

    }

    public function save()
    {
        $this->validate();

        if ($this->edit) {
            $this->eventDateUpdate('restore');
        }

        if ($this->form->id === 0) {
            $weight = new Weight;
            $weight->weighing = $this->form->weighing;
            $weight->comment = $this->form->comment;
            $weight->user_id = $this->form->user_id;
        } else {
            $weight = Weight::find($this->form->id);
            $weight->weighing = $this->form->weighing;
            $weight->comment = $this->form->comment;
        }
        $weight->date = Carbon::parse($this->form->date)->format('Y-m-d');
        $weight->save();

        if ($this->form->id === 0) {
            session()->flash('success', __('messages.weighingSaved'));
        } else {
            session()->flash('success', __('messages.weighingEdited'));
        }

        $this->redirectIntended('/home');

    }

    public function render()
    {

        if ($this->edit) {
            $this->eventDateUpdate('init');
        }

        return view('livewire.tables.weights-form');
    }
}
