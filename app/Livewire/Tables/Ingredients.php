<?php

namespace App\Livewire\Tables;

use App\Interfaces\OpenFoodFacts\OpenFoodFactsInterface;
use App\Models\Ingredient;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithPagination;

class Ingredients extends Component
{
    use withPagination;

    private OpenFoodFactsInterface $openFoodFacts;

    public string $search = '';

    public string $orderField = 'name';

    public string $orderDirection = 'ASC';

    public int $paginate = 20;

    public int $editId = 0;

    public array $selection = [];

    public $darkMode = false;

    public bool $isLoading = false;

    public array $existing_ingredients_list = [];

    public function boot(OpenFoodFactsInterface $openFoodFacts)
    {
        $this->openFoodFacts = $openFoodFacts;
    }

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark' ? true : false;
    }

    protected array $queryString = [
        'search' => ['except' => ''],
        'orderField' => ['except' => 'name'],
        'orderDirection' => ['except' => 'ASC'],
    ];

    public function startEdit(int $id)
    {
        $this->editId = $id;
    }

    public function deleteSelected()
    {

        $this->existing_ingredients_list = [];
        $delete_warning = false;
        foreach ($this->selection as $id) {
            $ingredient = Ingredient::find($id);
            if ($ingredient->meals()->exists() || $ingredient->dishes()->exists()) {
                $delete_warning = true;
                $this->existing_ingredients_list[] = $ingredient->name;
            }
        }

        if (! $delete_warning) {
            $this->handleDelete();
        }
    }

    public function handleDelete()
    {
        Ingredient::destroy($this->selection);
        $this->closeNotification();
        $this->notifyDelete();
        $this->existing_ingredients_list = [];
        $this->selection = [];
        $this->search = '';

        // Event calcul new points for all users

    }

    public function deleteIngredients()
    {
        $this->handleDelete();
    }

    public function closeNotification()
    {
        $this->dispatch('close-notification');
    }

    public function notifyDelete()
    {
        $this->dispatch('notify',
            ['message' => trans_choice('messages.ingredientDeleted', count($this->selection)), 'type' => 'success']);
    }

    public function paginationView()
    {
        return 'livewire.tables.pagination';
    }

    public function setOrderField(string $field)
    {
        if ($this->orderField === $field) {
            $this->orderDirection = $this->orderDirection === 'ASC' ? 'DESC' : 'ASC';
        } else {
            $this->orderField = $field;
            $this->reset('orderDirection');
        }
    }

    public function searchForOpenFoodFacts($id)
    {
        if (empty($id) || ! ctype_digit((string) $id)) {
            return;
        } else {
            $element = $this->openFoodFacts->barcode($id);
            $this->dispatch('open-food-facts', $element);
        }
    }

    #[On('ingredientUpdated')]
    public function onIngredientUpdated()
    {
        $this->dispatch('notify',
            ['message' => trans('messages.ingredientAdded'), 'type' => 'success']);
        $this->reset('editId');
    }

    public function updating($field, $value)
    {
        if ($field === 'search') {
            $this->resetPage();
        }
    }

    public function render()
    {
        $ingredients = Ingredient::query();

        if (! empty($this->search) && strlen($this->search) >= 2) {
            $ingredients = $ingredients->search($this->search);

            // If the search does not return any results, perform another search
            if ($ingredients->count() == 0) {
                $ingredients = Ingredient::query()->searchMysql($this->search);
            }
        }

        $ingredients = $ingredients->orderBy($this->orderField, $this->orderDirection)
            ->paginate($this->paginate);

        // Get the collection from the paginator
        $collection = $ingredients->getCollection();

        // Perform the reject operation on the collection
        $updatedCollection = $collection->reject(function ($ingredient) {
            return strtolower(trim($ingredient->name)) === 'bonus'; // Reject the ingredient if its name is 'bonus'
        });

        // Create a new paginator with the updated collection
        $ingredients->setCollection($updatedCollection);

        return view('livewire.tables.ingredients', ['ingredients' => $ingredients]);
    }
}
