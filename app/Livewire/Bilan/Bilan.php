<?php

namespace App\Livewire\Bilan;

use App\Models\Badge;
use App\Models\BadgeUnlock;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class Bilan extends Component
{
    public array $bilan_data = [];

    public array $legend_data = [];

    public array $durations = [
        'week', '2weeks', 'month', '2months', '6months', 'year', 'all',
    ];

    public array $scales = [
        'day', 'week', 'month', 'quarter', 'year',
    ];

    /**
     * @var array
     *            Default graphs to display - manually set
     */
    public array $default_graphs_display = [
        'graph_meals_follow_up' => ['period' => '2weeks', 'scale' => 'week'],
        'graph_meals_by_name' => ['period' => 'all', 'scale' => 'year'],
        'graph_meals_by_day' => ['period' => 'all', 'scale' => 'year'],
        'graph_activities_follow_up' => ['period' => 'all', 'scale' => 'year'],
        'graph_weights_follow_up' => ['period' => 'month', 'scale' => 'month'],
        'graph_cycles_follow_up' => ['period' => '6months', 'scale' => 'quarter'],
    ];

    public function mount()
    {
        $this->bilan_data = $this->bilanData();
        $this->legend_data = $this->legendData();
    }

    public function updateGraphData($graphId, $selectedValue)
    {
        $graph_name = str_replace('period_select_', '', $graphId);
        switch ($graph_name) {
            case 'graph_meals_follow_up':
                $graph_querie = $this->userMeals($selectedValue, 'ignore');
                $graph_data = $this->buildMealsFollowUpGraphData($graph_querie);
                $this->dispatch('updateGraphData',
                    json_encode(['graph' => $graph_name, 'data' => $graph_data, 'selectedValue' => $selectedValue]));
                break;
            case 'graph_activities_follow_up':
                $graph_querie = $this->userActivities($selectedValue);
                $graph_data = $this->buildActivitiesFollowUpGraphData($graph_querie);
                $this->dispatch('updateGraphData',
                    json_encode(['graph' => $graph_name, 'data' => $graph_data, 'selectedValue' => $selectedValue]));
                break;
            case 'graph_weights_follow_up':
                $graph_querie = $this->userWeights($selectedValue);
                $graph_data = $this->buildWeightsFollowUpGraphData($graph_querie);
                $this->dispatch('updateGraphData',
                    json_encode(['graph' => $graph_name, 'data' => $graph_data, 'selectedValue' => $selectedValue]));
                break;
            case 'graph_cycles_follow_up':
                $graph_querie = $this->userCycles($selectedValue);
                $graph_data = $this->buildCyclesFollowUpGraphData($graph_querie);
                $this->dispatch('updateGraphData',
                    json_encode(['graph' => $graph_name, 'data' => $graph_data, 'selectedValue' => $selectedValue]));
                break;
            default:
                return response()->json(['error' => 'Invalid graph'], 400);
        }
    }

    public function updateGraphScale($graphId, $selectedValue)
    {
        if (! in_array($selectedValue, $this->scales)) {
            return response()->json(['error' => 'Invalid scale'], 400);
        } else {
            $graph_name = str_replace('scale_select_', '', $graphId);
            $this->dispatch('updateGraphScale', json_encode(['graph' => $graph_name, 'scale' => $selectedValue]));
        }
    }

    public function legendData()
    {
        return [
            'graph_meals_by_name' => [
                'number' => trans('messages.meals_by_name_legend_number'),
                'average' => trans('messages.meals_by_name_legend_average'),
            ],
            'graph_meals_by_day' => [
                'number' => trans('messages.meals_by_day_legend_number'),
                'average' => trans('messages.meals_by_day_legend_average'),
            ],
            'graph_meals_follow_up' => [
                'number' => trans('messages.meals_follow_up_legend_number'),
                'average' => trans('messages.meals_follow_up_legend_average'),
                'points' => trans('messages.meals_follow_up_legend_points'),
            ],
            'graph_weights_follow_up' => [
                'number' => trans('messages.weights_follow_up_legend_number'),
                'average' => trans('messages.weights_follow_up_legend_average'),
                'unit' => trans('messages.legend_weights_unit'),
                'label' => trans('messages.weights_follow_up_legend_label'),
            ],
            'graph_activities_follow_up' => [
                'percentage' => trans('messages.activities_follow_up_legend_percentage'),
                'average' => trans('messages.activities_follow_up_legend_average'),
                'points' => trans('messages.activities_follow_up_legend_points'),
                'seances' => trans('messages.activities_follow_up_legend_seances'),
            ],
            'graph_cycles_follow_up' => [
                'percentage' => trans('messages.cycles_follow_up_legend_percentage'),
                'average' => trans('messages.cycles_follow_up_legend_average'),
                'days' => trans('messages.legend_day'),
                'label' => trans('messages.cycles_follow_up_legend_label'),
            ],
        ];
    }

    public function bilanData()
    {

        // User's meals data
        $meals_query = $this->userMeals('year');
        $meals_data = $this->handleMealsData($meals_query);
        $meals_graphs_and_cards = $this->buildGraphAndCards('meals_graphs_and_cards', $meals_data);

        // User's objectives
        $objectives = auth()->user()->objectives()->select('health_target', 'daily_points', 'created_at')
            ->orderBy('created_at', 'desc')
            ->get();

        // User's activities data
        $activities_query = $this->userActivities('all');
        $activities_data = $this->handleActivitiesData($activities_query);
        $activities_graphs_and_cards = $this->buildGraphAndCards('activities_graphs_and_cards', $activities_data);

        // User's cycle data
        if (auth()->user()->isWomen()) {
            $cycles_query = collect($this->userCycles('6months'));
            $cycles_graphs = $this->buildGraphAndCards('cycles_graph', $cycles_query);
        }

        // User's weights data
        $weights_query = $this->userWeights('all');
        $weights_data = $this->handleWeightData($weights_query);
        $weights_graphs_and_cards = $this->buildGraphAndCards('weights_graphs_and_cards', $weights_data);

        // User's badges
        $badges_querie = Badge::all()
            ->sortBy('display_order')
            ->groupBy('display_order')
            ->map(function ($group) {
                return $group->sortBy('action_count');
            })->flatten();

        $badges_users_unlocks_querie = BadgeUnlock::where('user_id', auth()->id())->get();

        $badges = $badges_querie->map(function ($badge) use ($badges_users_unlocks_querie) {
            $badge_unlocked = $badges_users_unlocks_querie->contains('badge_id', $badge->id);
            $badge_array = $badge->toArray();
            $badge_array['action_original'] = $badge_array['action'];
            $badge_array['action'] = trans('messages.'.$badge_array['action']);
            if ($badge_unlocked) {
                $badge_unlock = $badges_users_unlocks_querie->firstWhere('badge_id', $badge->id);

                return collect($badge_array + ['unlock_date' => Carbon::parse($badge_unlock->created_at)->translatedFormat('l d M Y à H:i:s')]);
            } else {
                return collect($badge_array);
            }
        });

        return [
            'meals' => $meals_graphs_and_cards,
            'objectives' => $objectives,
            'activities' => $activities_graphs_and_cards,
            'cycles' => $cycles_graphs ?? [],
            'weights' => $weights_graphs_and_cards,
            'badges' => $badges,
        ];

    }

    private function handleDate($duration): array
    {

        if (! in_array($duration, $this->durations)) {
            return response()->json(['error' => 'Invalid duration'], 400);
        }

        $end_date = Carbon::createFromFormat('Y-m-d', now()->format('Y-m-d'))->endOfDay();

        switch ($duration) {
            case 'week':
                $start_date = Carbon::createFromFormat('Y-m-d', now()->subWeek()->format('Y-m-d'))->startOfDay();
                break;
            case '2weeks':
                $start_date = Carbon::createFromFormat('Y-m-d', now()->subWeeks(2)->format('Y-m-d'))->startOfDay();
                break;
            case 'month':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subMonthWithoutOverflow()->startOfMonth()->format('Y-m-d'))->startOfDay();
                break;
            case '2months':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subMonthsWithoutOverflow(2)->startOfMonth()->format('Y-m-d'))->startOfDay();
                break;
            case '6months':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subMonthsWithoutOverflow(6)->startOfMonth()->format('Y-m-d'))->startOfDay();
                break;
            case 'year':
                $start_date = Carbon::createFromFormat('Y-m-d',
                    now()->subYearWithoutOverflow()->format('Y-m-d'))->startOfDay();
                break;
            case 'all':
                $start_date = Carbon::createFromFormat('Y-m-d', '2020-01-01')->startOfDay();
                break;
            default:
                if (preg_match('/^(\d+)years$/', $duration, $matches)) {
                    $years = intval($matches[1]);
                    $start_date = Carbon::createFromFormat('Y-m-d',
                        now()->subYearsWithoutOverflow($years)->startOfYear()->format('Y-m-d'))->startOfDay();
                }
                break;

        }

        return [
            'start' => $start_date,
            'end' => $end_date,
        ];

    }

    public function userMeals($duration, $cache = 'remember')
    {

        $dates = $this->handleDate($duration);

        if ($cache === 'remember') {
            $meals = Cache::remember('user_'.Auth::user()->id.'_bilan_meals', 60, function () use ($dates) {
                return auth()->user()->meals()
                    ->with('ingredients', 'dishes')
                    ->whereBetween('created_at', [$dates['start'], $dates['end']])
                    ->orderBy('created_at', 'asc')
                    ->get();
            });
        } else {
            $meals = auth()->user()->meals()
                ->with('ingredients', 'dishes')
                ->whereBetween('created_at', [$dates['start'], $dates['end']])
                ->orderBy('created_at', 'asc')
                ->get();
        }

        return $meals;

    }

    public function userActivities($duration, $cache = 'remember')
    {

        $dates = $this->handleDate($duration);

        if ($cache === 'remember') {

            $activities = Cache::remember('user_'.Auth::user()->id.'_bilan_activities', 60, function () use ($dates) {
                return auth()->user()->activities()
                    ->whereBetween('date', [$dates['start'], $dates['end']])
                    ->orderBy('date', 'asc')
                    ->select('activity', 'date')->get();
            });

        } else {

            $activities = auth()->user()->activities()
                ->whereBetween('date', [$dates['start'], $dates['end']])
                ->orderBy('date', 'asc')
                ->select('activity', 'date')
                ->get();
        }

        return $activities;

    }

    public function userWeights($duration)
    {

        $dates = $this->handleDate($duration);

        return auth()->user()->weights()
            ->whereBetween('date', [$dates['start'], $dates['end']])
            ->orderBy('date', 'asc')
            ->select('weighing', 'date')->get();

    }

    public function userCycles($duration)
    {

        if (! auth()->user()->isWomen()) {
            return [];
        }

        $dates = $this->handleDate($duration);

        return auth()->user()->cycles()
            ->whereBetween('date', [$dates['start'], $dates['end']])
            ->orderBy('date', 'asc')
            ->select('duration', 'date')->get();

    }

    public function handleMealsData($data): array
    {
        // User's meals (2 last weeks)
        // User's meals (all)
        $meals_data_year = collect($data);

        return [
            'two_weeks' => collect(
                $data->whereBetween('created_at', [
                    Carbon::createFromFormat('Y-m-d', now()->subWeeks(6)->format('Y-m-d'))->startOfDay(),
                    Carbon::createFromFormat('Y-m-d', now()->format('Y-m-d'))->endOfDay(),
                ]
                )),
            'year' => $meals_data_year,
        ];
    }

    public function handleWeightData($data): array
    {

        // User's weights (current month)
        // User's weights (all)
        return [
            'month' => collect(
                $data->whereBetween('date',
                    [now()->subMonth()->startOfMonth()->format('Y-m-d'), now()->format('Y-m-d')])),
            'all' => collect($data),
        ];

    }

    public function handleActivitiesData($data): array
    {
        // User's meals (2 last weeks)
        // User's meals (all)
        return [
            'current_month' => collect(
                $data->whereBetween('date', [now()->subMonth()->startOfMonth()->format('Y-m-d'), now()->format('Y-m-d')]
                )),
            'all' => collect($data),
        ];
    }

    private function buildGraphAndCards($type, $data)
    {
        return match ($type) {
            'meals_graphs_and_cards' => $this->buildMealsGraphsAndCards($data),
            'activities_graphs_and_cards' => $this->buildActivitiesGraphsAndCards($data),
            'cycles_graph' => $this->buildCyclesGraph($data),
            'weights_graphs_and_cards' => $this->buildWeightsGraphsAndCards($data),
            default => response()->json(['error' => 'Invalid type'], 400),
        };
    }

    private function buildMealsGraphsAndCards($data)
    {

        // Graph meals_follow_up
        $meals_follow_up = $this->buildMealsFollowUpGraphData($data['two_weeks']);

        // Graph meals_by_name
        $meals_by_name = $this->buildMealsByNameGraphData($data['year']);

        // Graph meals_by_day
        $meals_by_day = $this->buildMealsByDayGraphData($data['year']);

        // Ingredients
        $ingredients = $this->buildIngredientsCards($data['year']);

        return collect([
            'meals_follow_up' => $meals_follow_up,
            'meals_by_name' => $meals_by_name,
            'meals_by_day' => $meals_by_day,
            'ingredients' => $ingredients,
            'meals_follow_up_label' => trans('messages.meals_follow_up_label'),
            'meals_by_name_label' => trans('messages.meals_by_name_label'),
            'meals_by_day_label' => trans('messages.meals_by_day_label'),
            'ingredients_label' => trans('messages.ingredients_label'),

        ])->toArray();

    }

    private function buildActivitiesGraphsAndCards($data)
    {

        // Graph activities_follow_up
        $activities_follow_up = $this->buildActivitiesFollowUpGraphData($data['all']);

        // Card activities_summary
        $activities_summary = $this->buildActivitiesSummaryCardData($data['all']);

        // Card activities_months_average
        $activities_months_average = $this->buildActivitiesMonthsAverageCardData($data['all']);

        // Card activities_current_month_tendance
        $activities_months_current_month_tendance = $this->buildActivitiesCurrentMonthTendanceCardData($data['all'],
            $activities_months_average['average_activities_per_month_without_current']);

        return collect([
            'activities_follow_up' => $activities_follow_up,
            'activities_summary' => $activities_summary,
            'activities_months_average' => $activities_months_average,
            'activities_current_month_tendance' => $activities_months_current_month_tendance,
        ])->toArray();
    }

    private function buildWeightsGraphsAndCards($data)
    {

        // Graph weights_follow_up
        $weights_follow_up = $this->buildWeightsFollowUpGraphData($data['month']);

        // Card weights_summary
        $weights_summary = $this->buildWeightsSummaryCardData($data['all']);

        // Card weights_months_average
        $weights_months_average = $this->buildWeightsMonthsAverageCardData($data['all']);

        // Card weights_current_month_tendance
        $weights_months_current_month_tendance = $this->buildWeightsCurrentMonthTendanceCardData($data['all'],
            $weights_months_average['average_weights_per_month_without_current']);

        return collect([
            'weights_follow_up' => $weights_follow_up,
            'weights_follow_up_legend' => trans('messages.weights_follow_up_legend'),
            'weights_summary' => $weights_summary,
            'weights_months_average' => $weights_months_average,
            'weights_months_current_month_tendance' => $weights_months_current_month_tendance,
        ])->toArray();
    }

    private function buildCyclesGraph($data)
    {

        // Graph cycles_follow_up
        $cycles_follow_up = $this->buildCyclesFollowUpGraphData($data);

        return collect([
            'cycles_follow_up' => $cycles_follow_up,
        ])->toArray();
    }

    private function buildMealsFollowUpGraphData($data)
    {

        $grouped_meals = $data->groupBy(function ($item) {
            return Carbon::parse($item->created_at)->format('Y-m-d');
        });

        $result = $grouped_meals->map(function ($group, $date) {
            $ingredients_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->ingredients->reduce(function ($ingredientCarry, $ingredient) {
                    return $ingredientCarry + $ingredient->pivot->points;
                }, 0);
            }, 0);

            $dishes_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->dishes->reduce(function ($ingredientCarry, $dish) {
                    return $ingredientCarry + $dish->pivot->points;
                }, 0);
            }, 0);

            $group_size = $group->count();
            $average_ingredients_points = $group_size > 0 ? $ingredients_points / $group_size : 0;
            $average_dishes_points = $group_size > 0 ? $dishes_points / $group_size : 0;

            return [
                'label' => $group,
                'number' => $group_size,
                'average' => $group_size > 0 ? round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2,
                    1) : 0,
                'x' => $date,
                'y' => $ingredients_points + $dishes_points,
            ];
        })->values();

        return $result->toArray();

    }

    private function buildMealsByNameGraphData($data)
    {

        $order = [
            'breakfast' => trans('messages.breakfast'),
            'lunch' => trans('messages.lunch'),
            'snack' => trans('messages.snack'),
            'dinner' => trans('messages.dinner'),
            'bar' => trans('messages.bar'),
        ];

        $grouped_data = $data->groupBy('name')->sortKeysUsing(function ($name1, $name2) use ($order) {
            return array_search($name1, array_keys($order)) <=> array_search($name2, array_keys($order));
        })->mapWithKeys(function ($group, $name) use ($order) {
            return [$order[$name] => $group];
        });

        return $grouped_data->map(function ($group, $name) {
            $total_ingredients_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->ingredients->reduce(function ($ingredientCarry, $ingredient) {
                    return $ingredientCarry + $ingredient->pivot->points;
                }, 0);
            }, 0);

            $total_dishes_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->dishes->reduce(function ($ingredientCarry, $dish) {
                    return $ingredientCarry + $dish->pivot->points;
                }, 0);
            }, 0);

            $group_size = $group->count();

            $average_ingredients_points = $group_size > 0 ? $total_ingredients_points / $group_size : 0;
            $average_dishes_points = $group_size > 0 ? $total_dishes_points / $group_size : 0;

            return [
                'label' => $name,
                'number' => $group_size,
                'average' => $average_ingredients_points + $average_dishes_points > 0 ? round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2,
                    1) : 0,
                'x' => $group_size,
                'y' => $average_ingredients_points + $average_dishes_points > 0 ? round($average_ingredients_points + $average_dishes_points) : 0,
                'r' => $average_ingredients_points + $average_dishes_points > 0 ? round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2,
                    1) * 2 : 0,
            ];
        })->values()->toArray();

        return $result->toArray();

    }

    private function buildMealsByDayGraphData($data)
    {

        $grouped_meals = $data->groupBy(function ($item) {
            return Carbon::parse($item->created_at)->translatedFormat('l');
        })->sortKeysUsing(function ($day1, $day2) {
            $order = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];

            return array_search($day1, $order) <=> array_search($day2, $order);
        });

        $result = $grouped_meals->map(function ($group, $day) {
            $total_ingredients_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->ingredients->reduce(function ($ingredientCarry, $ingredient) {
                    return $ingredientCarry + $ingredient->pivot->points;
                }, 0);
            }, 0);

            $total_dishes_points = $group->reduce(function ($carry, $item) {
                return $carry + $item->dishes->reduce(function ($ingredientCarry, $dish) {
                    return $ingredientCarry + $dish->pivot->points;
                }, 0);
            }, 0);

            $group_size = $group->count();

            $average_ingredients_points = $group_size > 0 ? $total_ingredients_points / $group_size : 0;
            $average_dishes_points = $group_size > 0 ? $total_dishes_points / $group_size : 0;

            return [
                'label' => $day,
                'number' => $group_size,
                'average' => $average_ingredients_points + $average_dishes_points > 0 ? round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2,
                    1) : 0,
                'x' => $group_size,
                'y' => $average_ingredients_points + $average_dishes_points > 0 ? round($average_ingredients_points + $average_dishes_points) : 0,
                'r' => $average_ingredients_points + $average_dishes_points > 0 ? round(ceil(($average_ingredients_points + $average_dishes_points) * 2) / 2,
                    1) * 2 : 0,
            ];
        })->values();

        return $result->toArray();

    }

    private function buildIngredientsCards($data)
    {

        $ingredients = $data->pluck('ingredients')
            ->flatten()
            ->filter(function ($ingredient) {
                return $ingredient->name !== 'bonus';
            })
            ->map(function ($ingredient) {
                return [
                    'id' => $ingredient->id,
                    'name' => $ingredient->name,
                ];
            });

        $dishes_ingredients = Cache::remember('user_'.Auth::user()->id.'_bilan_dishes_ingredients', 60,
            function () use ($data) {
                return $data->pluck('dishes.*.ingredients')->flatten()->select('id', 'name');
            });

        $all_ingredients = $ingredients->merge($dishes_ingredients->toArray());

        $grouped_ingredients = $all_ingredients->groupBy('id')
            ->map(function ($group) {
                return [
                    'name' => $group->first()['name'],
                    'count' => $group->count(),
                ];
            })->sortByDesc('count')->take(10);

        return $grouped_ingredients->toArray();

    }

    private function buildActivitiesFollowUpGraphData($data)
    {

        $titles = [
            'yoga' => trans('messages.yoga'),
            'workout' => trans('messages.workout'),
            'running' => trans('messages.running'),
            'hiking' => trans('messages.hiking'),
        ];

        $grouped_activities = $data->groupBy('activity')->mapWithKeys(function ($group, $activity) use ($titles) {
            $title = $titles[$activity] ?? $activity;

            return [$title => $group];
        });

        $data = $grouped_activities->map(function ($group, $title) {
            return ['value' => $group->count(), 'color' => '#FFCE56', 'label' => $title];
        })->values();

        $labels = $grouped_activities->map(function ($group, $title) {
            return [$title];
        })->flatten();

        $colors = [
            '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF', '#FF9F40', '#FFD740', '#FF6384', '#36A2EB',
            '#FFCE56',
        ];

        return [
            'data' => $data->toArray(), 'labels' => $labels->toArray(),
            'label' => trans('messages.activities_follow_up_label'),
            'colors' => $colors,
        ];

    }

    private function buildActivitiesSummaryCardData($data)
    {

        $currentYear = now()->year;
        $previousYear = now()->subYear()->year;
        $previousMonth = now()->subMonth()->month;

        // Current year
        $grouped_activities_by_year = $data->groupBy('activity')->map(function ($group) use ($currentYear) {
            $currentYearActivities = $group->filter(function ($item) use ($currentYear) {
                return Carbon::parse($item->date)->year === $currentYear;
            });

            return [
                'count' => $currentYearActivities->count(),
            ];
        });

        $total_count_current_year = $grouped_activities_by_year->sum('count');

        $current_year_without_current_month = [
            'total' => $total_count_current_year,
            'activities' => $grouped_activities_by_year->toArray(),
            'legend' => trans('messages.current_year'),
            'key' => 2,
            'icon' => 'date_range',
        ];

        // Previous year

        $grouped_activities_previous_year = $data->groupBy('activity')->map(function ($group) use ($previousYear) {

            $previousYearActivities = $group->filter(function ($item) use ($previousYear) {
                return Carbon::parse($item->date)->year === $previousYear;
            });

            return [
                'count' => $previousYearActivities->count(),
            ];
        });

        $previous_year = [
            'total' => $grouped_activities_previous_year->sum('count'),
            'activities' => $grouped_activities_previous_year->toArray(),
            'key' => 0,
            'legend' => trans('messages.previous_year'),
            'icon' => 'calendar_clock',
        ];

        // Previous month

        $grouped_activities_previous_month = $data->groupBy('activity')->map(function ($group) use (
            $previousMonth,
            $currentYear
        ) {

            $previousYearActivities = $group->filter(function ($item) use ($previousMonth, $currentYear) {
                return Carbon::parse($item->date)->month === $previousMonth && Carbon::parse($item->date)->year === $currentYear;
            });

            return [
                'count' => $previousYearActivities->count(),
            ];
        });

        $previous_month = [
            'total' => $grouped_activities_previous_month->sum('count'),
            'activities' => $grouped_activities_previous_month->toArray(),
            'legend' => trans('messages.previous_month'),
            'key' => 1,
            'icon' => 'event_repeat',
        ];

        return [
            'previous_year' => $previous_year,
            'current_year_without_current_month' => $current_year_without_current_month,
            'previous_month' => $previous_month,
        ];

    }

    private function buildWeightsSummaryCardData($data)
    {

        $currentYear = now()->year;
        $previousYear = now()->subYear()->year;
        $previousMonth = now()->subMonth()->month;

        // Sum of weighing values for current year without the current month
        $total_weighing_year = $data->filter(function ($item) use ($currentYear) {
            $itemDate = Carbon::parse($item->date);

            return $itemDate->year === $currentYear;
        })->avg('weighing');

        // Sum of weighing values for the last year
        $total_weighing_last_year = $data->filter(function ($item) use ($previousYear) {
            return Carbon::parse($item->date)->year === $previousYear;
        })->avg('weighing');

        // Sum of weighing values for the previous month
        $total_weighing_previous_month = $data->filter(function ($item) use ($previousMonth, $currentYear) {
            return Carbon::parse($item->date)->month === $previousMonth && Carbon::parse($item->date)->year === $currentYear;
        })->avg('weighing');

        return [
            'weighing_last_year' => [
                'value' => $total_weighing_last_year > 0 ? round($total_weighing_last_year, 1) : 0,
                'key' => 0,
                'legend' => trans('messages.previous_year'),
                'icon' => 'calendar_clock',
            ],
            'weighing_current_year' => [
                'value' => $total_weighing_year > 0 ? round($total_weighing_year,
                    1) : 0,
                'legend' => trans('messages.current_year'),
                'key' => 1,
                'icon' => 'date_range',
            ],
            'weighing_previous_month' => [
                'value' => $total_weighing_previous_month > 0 ? round($total_weighing_previous_month, 1) : 0,
                'legend' => trans('messages.previous_month'),
                'key' => 2,
                'icon' => 'event_repeat',
            ],
        ];

    }

    private function buildActivitiesMonthsAverageCardData($data)
    {

        // Group activities by month and year
        $grouped_activities = $data->groupBy(function ($item) {
            return Carbon::parse($item->date)->format('Y-m');
        })->sortKeysDesc();

        // Exclude the current month
        $currentYearMonth = now()->format('Y-m');
        $filtered_activities = $grouped_activities->except($currentYearMonth);

        // Calculate total activities for each month
        $monthly_totals = $filtered_activities->map(function ($group) {
            return $group->count();
        })->groupBy(function ($value, $key) {
            return Carbon::parse($key)->year;
        }, true);

        // Calculate the average number of activities per month
        $average_activities_per_month = $monthly_totals->flatten()->avg();

        return [
            'average_activities_per_month_without_current' => $average_activities_per_month ? round(ceil(($average_activities_per_month) * 2) / 2,
                1) : 0,
            'monthly_totals_without_current' => $monthly_totals,
        ];

    }

    private function buildWeightsMonthsAverageCardData($data)
    {

        // Group activities by month and year
        $grouped_weights = $data->groupBy(function ($item) {
            return Carbon::parse($item->date)->format('Y-m');
        });

        // Exclude the current year and month
        $currentYearMonth = now()->format('Y-m');
        $filtered_weights = $grouped_weights->except($currentYearMonth);

        // Calculate total activities for each month
        $monthly_totals = $filtered_weights->map(function ($group) {
            return round($group->avg('weighing'), 1);
        })->groupBy(function ($value, $key) {
            return Carbon::parse($key)->year;
        }, true)->sortKeysDesc();

        // Calculate the average number of weights per month, ignoring 0 values
        $average_weights_per_month = $monthly_totals->map(function ($value) {
            return round(collect($value)->avg(), 1);
        })->avg();

        return [
            'average_weights_per_month_without_current' => $average_weights_per_month,
            'monthly_totals_weights_without_current' => $monthly_totals,
        ];

    }

    private function buildActivitiesCurrentMonthTendanceCardData($data, $avg)
    {

        $currentYearMonth = now()->format('Y-m');

        $current_month = $data->filter(function ($item) use ($currentYearMonth) {
            return Carbon::parse($item->date)->format('Y-m') === $currentYearMonth;
        });

        $current_month_total = $current_month->count();

        $grouped_activities = $current_month->groupBy('activity')->map(function ($group) {
            return $group->count();
        });

        if (! $current_month_total) {
            $tendance = 'null';
        } else {
            $tendance = $current_month_total > $avg ? 'up' : ($current_month_total < $avg ? 'down' : 'equal');
        }

        return [
            'current_month_total' => $current_month_total,
            'grouped_activities' => $grouped_activities,
            'tendance' => $tendance,
        ];
    }

    private function buildWeightsCurrentMonthTendanceCardData($data, $avg)
    {
        $currentYearMonth = now()->format('Y-m');

        $current_month = $data->filter(function ($item) use ($currentYearMonth) {
            return Carbon::parse($item->date)->format('Y-m') === $currentYearMonth;
        });

        $current_month_total = $current_month->avg('weighing');

        if (! $current_month_total) {
            $tendance = 'null';
        } else {
            $tendance = $current_month_total > $avg ? 'up' : ($current_month_total < $avg ? 'down' : 'equal');
        }

        return [
            'current_month_total' => $current_month_total,
            'tendance' => $tendance,
        ];
    }

    public function buildCyclesFollowUpGraphData($data)
    {

        $result = $data->map(function ($item) {
            return [
                'x' => Carbon::parse($item->date)->format('Y-m-d'),
                'y' => $item->duration,
            ];
        });

        return $result->toArray();

    }

    private function buildWeightsFollowUpGraphData($data)
    {

        $result = $data->map(function ($item) {
            return [
                'label' => trans('messages.weights_follow_up_label'),
                'x' => Carbon::parse($item->date)->format('Y-m-d'),
                'y' => $item->weighing,
            ];
        })->values();

        return $result->toArray();
    }

    public function render()
    {
        return view('livewire.bilan.bilan');
    }
}
