<?php

namespace App\Livewire\Modals;

use Livewire\Attributes\On;
use Livewire\Component;

class OpenFoodFacts extends Component
{
    public array $openFoodFactsData = [];

    public bool $isOpen = false;

    #[On('open-food-facts')]
    public function updateModal($data)
    {
        $this->openFoodFactsData = $data;
        $this->isOpen = true;
    }

    public function render()
    {
        return view('livewire.modals.open-food-facts');
    }
}
