<?php

namespace App\Livewire\Modals;

use App\Models\Meal;
use App\Models\User;
use App\Traits\UserActions;
use Carbon\Carbon;
use Livewire\Attributes\On;
use Livewire\Component;

class ShareMealWith extends Component
{
    use UserActions;

    public bool $isOpen = false;

    public $meal;

    public array $data;

    public int $shared_meal_with = 0;

    public bool $darkMode = false;

    public array $shareWith = [];

    #[On('share-meal')]
    public function updateModal($data)
    {
        $this->meal = $data;
        $this->shareWith = $this->shareWithList();
        $this->isOpen = true;
    }

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark' ? true : false;
    }

    public function save()
    {

        // Check if meal exist before sharing
        if ($this->shared_meal_with) {
            $user = User::find($this->shared_meal_with);
            if ($user) {
                $mealExists = $user->meals()
                    ->whereDate('created_at',
                        Carbon::createFromFormat('Y-m-d', Carbon::parse($this->meal['created_at'])->format('Y-m-d')))
                    ->where('name', $this->meal['name'])
                    ->exists();
                if (! $mealExists) {
                    $this->shared_meal_with = 0;
                    $mealID = $this->meal['id'];
                    $this->meal = [];
                    $this->isOpen = false;
                    $this->redirectRoute('meal.share',
                        ['meal' => $mealID, 'user' => $user->id, 'shared_by' => auth()->id()]);
                }
            }
        }

    }

    public function render()
    {
        return view('livewire.modals.share-meal-with');
    }
}
