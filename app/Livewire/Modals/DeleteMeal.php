<?php

namespace App\Livewire\Modals;

use Livewire\Attributes\On;
use Livewire\Component;

class DeleteMeal extends Component
{
    public bool $isOpen = false;

    public bool $darkMode = false;

    public int $meal_id = 0;

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark' ? true : false;
    }

    #[On('delete-meal')]
    public function mealDelete($id)
    {
        $this->meal_id = $id;
        $this->isOpen = true;
    }

    public function delete()
    {
        $this->isOpen = false;
        $id = $this->meal_id;
        $this->meal_id = 0;
    }

    public function render()
    {
        return view('livewire.modals.delete-meal');
    }
}
