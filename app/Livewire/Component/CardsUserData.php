<?php

namespace App\Livewire\Component;

use App\Traits\UserActions;
use Carbon\Carbon;
use Livewire\Attributes\On;
use Livewire\Component;

class CardsUserData extends Component
{
    use UserActions;

    public array $ringsData = [];

    public $user;

    public int $weeklyActivities = 0;

    public float $weeklyBonusPoints = 0;

    public function mount()
    {
        $this->user = auth()->user();
        $this->userRings();
        $this->weeklyActivities = $this->user->activities()->whereBetween('date',
            [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $this->weeklyBonusPoints = $this->user->bonus()->value('points') ?? 0;
    }

    public function userRings()
    {
        $start_date = Carbon::now()->startOfWeek();
        $end_date = Carbon::now()->endOfWeek();

        $user_daily_points = $this->user->dailyPoints()
            ->whereBetween('date', [$start_date, $end_date])
            ->get();

        $user_objectives = collect(auth()->user()->objectives()
            ->latest('created_at')
            ->first()?->daily_points);

        $user_objectives_max_daily_points = $user_objectives[0] ?? 0;

        for ($current_date = $start_date; $current_date->lte($end_date); $current_date->addDay()) {
            $formatted_date = $current_date->format('Y-m-d');
            $daily_points = collect($user_daily_points)->firstWhere('date', $formatted_date);

            $points = $daily_points ? $daily_points->points : 0;

            // Calculate $points as a percentage of $user_objectives_max_daily_points
            $value = ($user_objectives_max_daily_points > 0) ? ($points / $user_objectives_max_daily_points) * 100 : 0;
            $value = $value !== 0 ? round($value, 2) : 0;

            $this->ringsData['data'][] = [
                'day' => $current_date->format('l'),
                'day_fr' => $current_date->setTimezone('Europe/Paris')->isoFormat('dddd'),
                'date' => $current_date->format('Y-m-d'),
                'isToday' => $current_date->isToday(),
                'points' => $points,
                'value' => $value,
            ];
        }
    }

    #[On('echo-private:livewire.refresh.{user.id},RefreshLivewireComponent')]
    public function updateGauge($data)
    {
        if (! isset($data['meal'])) {
            return;
        }
        $date = Carbon::parse($data['meal']['created_at'])->format('Y-m-d');
        $points = $this->user->dailyPoints()->where('date', $date)->first()->points ?? 0;
        // Calculate $points as a percentage of $user_objectives_max_daily_points
        $user_objectives = collect(auth()->user()->objectives()
            ->latest('created_at')
            ->first()?->daily_points);

        $user_objectives_max_daily_points = $user_objectives[0] ?? 0;
        $value = ($user_objectives_max_daily_points > 0) ? ($points / $user_objectives_max_daily_points) * 100 : 0;
        $value = $value !== 0 ? round($value, 2) : 0;
        // Dispatch the event
        $this->dispatch('ring-and-bonus-updated', data: [
            'day' => Carbon::parse($date)->format('l'),
            'points' => $points,
            'value' => $value,
            'bonus' => $this->user->bonus()->value('points') ?? 0,
        ]);
    }

    public function render()
    {
        return view('livewire.component.cards-user-data');
    }
}
