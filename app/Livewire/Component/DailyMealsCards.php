<?php

namespace App\Livewire\Component;

use Carbon\Carbon;
use Livewire\Attributes\On;
use Livewire\Component;

class DailyMealsCards extends Component
{
    public $date;

    public $cards;

    public string $action = '';

    public string $cardsJson = '';

    public function mount(): void
    {
        // Check if there's a date in the session
        $this->date = session('selectedDate') ? session('selectedDate') : now()->translatedFormat('Y-m-d');
        $this->userMeals($this->date);

    }

    #[On('datepicker-update')]
    #[On('datepicker-update-button')]
    public function updateDailyCards($data)
    {
        $this->action = $data['action'];
        $this->date = Carbon::parse($data['date'])->format('Y-m-d');
        $this->userMeals($this->date);
        $this->dispatch('update-daily-meals-cards-class', data: ['action' => 'reveal', 'userAction' => $this->action]);
    }

    public function initialLoad()
    {
        $this->dispatch('update-daily-meals-cards-class', data: ['action' => 'init', 'date' => $this->date]);
    }

    public function userMealShare($meal)
    {
        $this->userMeals($this->date); // Display meal fix
        $this->dispatch('share-meal', $meal);
    }

    public function userMealDelete($mealID)
    {
        $this->userMeals($this->date); // Display meal fix
        $this->dispatch('delete-meal', $mealID);
    }

    public function userMeals($date)
    {

        $date = Carbon::parse($date)->format('Y-m-d');

        // Define the template meals
        $template_meals = [
            'breakfast' => [],
            'lunch' => [],
            'snack' => [],
            'dinner' => [],
            'bar' => [],
        ];

        $user_meals = auth()->user()->meals()->with('ingredients', 'dishes')
            ->whereDate('created_at', Carbon::createFromFormat('Y-m-d', $date))
            ->orderByRaw("FIELD(name, 'breakfast', 'lunch', 'snack', 'dinner', 'bar')")
            ->get()
            ->mapWithKeys(function ($meal) {
                return [$meal->name => $meal];
            });

        // Merge the user's meals with the template meals
        $this->cards = collect($template_meals)->map(function ($value, $key) use ($user_meals) {
            if ($user_meals->has($key)) {
                $meal = $user_meals->get($key);
                if ($meal->ingredients->isNotEmpty() || $meal->dishes->isNotEmpty()) {
                    $meal->ingredients = $meal->ingredients->reject(function ($ingredient) {
                        return $ingredient->name === 'bonus'; // Reject the ingredient if its name is 'bonus'
                    })->take(4);
                    $meal->dishes = $meal->dishes->unique('name')->take(2);
                }

                return $meal;
            } else {
                return $value;
            }
        })->all();

        $this->cardsJson = json_encode($this->cards);

    }

    public function render()
    {
        return view('livewire.component.daily-meals-cards');
    }
}
