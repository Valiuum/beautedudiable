<?php

namespace App\Livewire\Component;

use App\Traits\UserActions;
use Livewire\Attributes\On;
use Livewire\Component;

class ReminderMeals extends Component
{
    use UserActions;

    public array $missedMeals = [];

    public int|string $weekPoints = 0;

    public array $header = [];

    public $user;

    public function mount()
    {
        $this->user = auth()->user();
    }

    #[On('echo-private:livewire.refresh.{user.id},RefreshLivewireComponent')]
    public function updateMissingMeals($data)
    {
        $this->buildTabsAndWeekPoints();
    }

    public function buildTabsAndWeekPoints(): void
    {

        $pointsToReachObjective = $this->pointsToReachObjective();
        if (is_string($pointsToReachObjective)) {
            $this->weekPoints = $pointsToReachObjective;
        }

        if (! is_string($pointsToReachObjective)) {
            $this->weekPoints = $pointsToReachObjective;
        }

        $this->missedMeals = $this->missedMeals();

        if (! $this->missedMeals) {
            return;
        }

        $data = collect($this->missedMeals);
        $data->map(function ($item) {
            $this->header[$item['key']] = [
                'name' => trans('messages.'.ucfirst($item['slug'])), 'slug' => $item['slug'],
            ];
        });

    }

    public function render()
    {
        $this->buildTabsAndWeekPoints();

        return view('livewire.component.reminder-meals');
    }
}
