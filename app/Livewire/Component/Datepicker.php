<?php

namespace App\Livewire\Component;

use App\Traits\UserActions;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Livewire\Attributes\On;
use Livewire\Component;

class Datepicker extends Component
{
    use UserActions;

    public string $selectedDate = '';

    public string $selectedDateInternal = '';

    public bool $isHomePage = false;

    public bool $clearDate = false;

    public $darkMode = false;

    public $user;

    public function mount($clearDate = null): void
    {
        $this->user = auth()->user();
        $this->isHomePage = Route::currentRouteNamed('home');

        if ($clearDate) {
            $this->clearDate = true;
            $this->selectedDate = '';
        } else {

            // Check if there's a date in the session
            $dateFromSession = session('selectedDate');

            if ($dateFromSession) {
                // If there's a date in the session, use it
                $this->selectedDate = Carbon::parse($dateFromSession)->translatedFormat('l d M Y');
                $this->selectedDateInternal = Carbon::parse($dateFromSession)->translatedFormat('Y-m-d');
            } else {
                // If there's no date in the session, use the current date
                $this->selectedDate = now()->translatedFormat('l d M Y');
                $this->selectedDateInternal = now()->translatedFormat('Y-m-d');
            }

        }

    }

    #[On('dark-mode-update')]
    public function toggleDarkMode($state)
    {
        $this->darkMode = $state === 'is-dark';
    }

    #[On('datepicker-request-missing-meals')]
    public function missingUserMeals()
    {
        // Get the missing meals for the user
        $missed_meals = collect($this->missedMeals())->map(function ($item) {
            return [
                'date' => $item['date'],
                'meals' => collect($item['meals'])->keys()->toArray(),
            ];
        });
        // Dispatch the event
        $this->dispatch('datepicker-request-missing-meals-response', data: [$missed_meals]);
    }

    #[On('datepicker-update')]
    public function updatedSelectedDate($date): void
    {
        $this->selectedDate = Carbon::parse($date['date'])->translatedFormat('l d M Y');
        $this->selectedDateInternal = Carbon::parse($date['date'])->translatedFormat('Y-m-d');
        $this->updateDateSession();
    }

    private function updateDateSession(): void
    {
        // Store the selected date in the session
        session(['selectedDate' => $this->selectedDateInternal]);
    }

    #[On('echo-private:livewire.refresh.{user.id},RefreshLivewireComponent')]
    public function handleRefreshEvent($data)
    {
        $meal = $data['meal'];
        if ($meal) {
            $meal_date = Carbon::parse($meal['created_at']);
            if (Carbon::parse($this->selectedDateInternal)->isSameDay($meal_date)) {
                $this->updateCurrentDate('refresh', $meal_date);
            }
        }
    }

    #[On('swipe-event')]
    public function updateCurrentDate($action, $date = null): void
    {
        $currentDate = Carbon::parse($this->selectedDateInternal);
        switch ($action) {
            case 'increase':
                $this->selectedDateInternal = $currentDate->addDay()->format('Y-m-d');
                $this->selectedDate = Carbon::parse($this->selectedDateInternal)->translatedFormat('l d M Y');
                break;
            case 'decrease':
                $this->selectedDateInternal = $currentDate->subDay()->format('Y-m-d');
                $this->selectedDate = Carbon::parse($this->selectedDateInternal)->translatedFormat('l d M Y');
                break;
            case 'refresh':
                if ($date) {
                    $this->selectedDateInternal = $date->translatedFormat('Y-m-d');
                    $this->selectedDate = $date->translatedFormat('l d M Y');
                } else {
                    $this->selectedDateInternal = $currentDate->translatedFormat('Y-m-d');
                    $this->selectedDate = $currentDate->translatedFormat('l d M Y');
                }
        }
        $this->updateDateSession();
        $this->dispatch('datepicker-update-button', ['date' => $this->selectedDateInternal, 'action' => $action]);

    }

    public function render()
    {
        return view('livewire.component.datepicker');
    }
}
