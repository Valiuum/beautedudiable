<?php

namespace App\Livewire\Forms;

use App\Traits\UserActions;
use Illuminate\Validation\Rule;
use Livewire\Form;

class MealsForm extends Form
{
    use UserActions;

    public array $ingredients = [];

    public array $dishes = [];

    public string $date = '';

    public bool $warning = false;

    public string $new_date = '';

    public string $type = '';

    public int $shared_meal_with = 0;

    public int $meal_id = 0;

    public string $action = '';

    public float $user_bonus = 0;

    public float $meal_previous_points = 0;

    public array $meal_to_delete = [];

    public function rules(): array
    {
        return [
            'ingredients.*.id' => [
                'required', 'integer',
            ],
            'ingredients.*.name' => [
                'required', 'string',
            ],
            'date' => [
                'date_format:Y-m-d',
            ],
            'new_date' => [
                $this->action === 'duplicate' ? 'required' : 'nullable',
                'date_format:Y-m-d',
                'different:date',
            ],
            'shared_meal_with' => [
                'integer',
                Rule::in(collect($this->shareWithList())->keys()->push(0)->toArray()),
            ],
            'type' => [
                'nullable', 'string',
            ],
            'user_bonus' => [
                'numeric',
                'min:0',
                'max:'.(auth()->user()?->bonus->points ?? 0),
            ],
        ];
    }
}
