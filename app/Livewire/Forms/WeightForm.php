<?php

namespace App\Livewire\Forms;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Form;

class WeightForm extends Form
{
    public int $id = 0;

    public int $user_id = 0;

    public float $weighing = 0;

    public string $comment = '';

    public string $date = '';

    public function rules(): array
    {

        $dt = Carbon::now();
        $tomorrow = $dt->tomorrow()->format('d-m-Y');

        return [
            'weighing' => [
                'required',
                'numeric',
                'max:120',
                'min:40',
            ],
            'comment' => [
                'nullable',
                'string',
                'max:99',
            ],
            'date' => [
                'required',
                'date',
                'date_format:"Y-m-d"',
                'before:'.$tomorrow,
                Rule::unique('weights', 'date')->where(function ($query) {
                    return $query->where('user_id', Auth::id());
                })->ignore($this->id),
            ],
        ];
    }
}
