<?php

namespace App\Livewire\Forms;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Form;

class ActivityForm extends Form
{
    public int $id = 0;

    public int $user_id = 0;

    public string $activity = '';

    public string $comment = '';

    public string $date = '';

    public array $activities = [
        'running',
        'yoga',
        'hiking',
        'workout',
    ];

    public function rules(): array
    {

        $dt = Carbon::now();
        $tomorrow = $dt->tomorrow()->format('d-m-Y');

        return [
            'activity' => [
                'required',
                'string',
                Rule::in($this->activities),
                Rule::unique('activities', 'activity')->where(function ($query) {
                    $query->where('activity', '=', $this->activity)
                        ->where('id', '!=', $this->id)
                        ->where('user_id', '=', Auth::id())
                        ->where('date', Carbon::parse($this->date)->format('Y-m-d'));
                }),
            ],
            'comment' => [
                'nullable',
                'string',
                'max:99',
            ],
            'date' => [
                'required',
                'date',
                'date_format:"Y-m-d"',
                'before:'.$tomorrow,
            ],
        ];
    }
}
