<?php

namespace App\Livewire\Forms;

use Illuminate\Validation\Rule;
use Livewire\Form;

class IngredientForm extends Form
{
    public ?int $id;

    public string $name = '';

    public string $description = '';

    public float $points = 0;

    public float $quantity = 1;

    public string $unit = '';

    public float $energy = 0;

    public float $proteins = 0;

    public float $lipids = 0;

    public float $carbohydrates = 0;

    public float $fibers = 0;

    public string $open_food_facts_id = '';

    public array $units = [
        'g',
        'ml',
        'dl',
        'cl',
        'l',
        'kg',
        'unité',
        'cuillère à café',
        'cuillère à soupe',
        'verre',
        'bol',
        'tranche',
        'portion',
        'barquette',
        'pot',
        'bouteille',
        'canette',
        'sachet',
        'sandwich',
        'paquet',
        'boîte',
        'plaque',
        'filet',
        'brique',
        'pincée',
        'poignée',
        'branche',
        'feuille',
        'tige',
        'gousse',
    ];

    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
                'min:3',
                Rule::unique('ingredients', 'name')->ignore($this->id ?? null),
            ],
            'description' => [
                'nullable',
                'string',
                'max:255',
            ],
            'points' => [
                'required',
                'numeric',
                'min:0',
                'max:99',
            ],
            'quantity' => [
                'required',
                'numeric',
                'min:1',
                'max:900',
            ],
            'unit' => [
                'required',
                Rule::in($this->units),
            ],
            'energy' => [
                'required',
                'numeric',
                'min:0',
                'max:2500',
            ],
            'proteins' => [
                'required',
                'numeric',
                'min:0',
                'max:900',
            ],
            'lipids' => [
                'required',
                'numeric',
                'min:0',
                'max:900',
            ],
            'carbohydrates' => [
                'required',
                'numeric',
                'min:0',
                'max:900',
            ],
            'fibers' => [
                'required',
                'numeric',
                'min:0',
                'max:900',
            ],
            'open_food_facts_id' => [
                'nullable',
                'integer',
            ],
        ];
    }
}
