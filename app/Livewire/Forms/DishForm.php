<?php

namespace App\Livewire\Forms;

use App\Traits\UserActions;
use Livewire\Form;

class DishForm extends Form
{
    use UserActions;

    public array $ingredients = [];

    public string $date = '';

    public string $type = '';

    public string $name = '';

    public int $dish_id = 0;

    public string $action = '';

    public function rules(): array
    {
        return [
            'ingredients.*.id' => [
                'required', 'integer',
            ],
            'ingredients.*.name' => [
                'required', 'string',
            ],
            'name' => [
                'required', 'string',
            ],
            'type' => [
                'nullable', 'string',
            ],
        ];
    }
}
