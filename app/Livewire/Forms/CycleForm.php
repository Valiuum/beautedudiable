<?php

namespace App\Livewire\Forms;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Form;

class CycleForm extends Form
{
    public int $id = 0;

    public int $user_id = 0;

    public float $duration = 5;

    public string $comment = '';

    public string $date = '';

    public function rules(): array
    {

        $dt = Carbon::now();
        $tomorrow = $dt->tomorrow()->format('d-m-Y');

        if ($this->id === 0) {
            $last_cycle = Auth()->user()->cycles()->orderByDesc('date')->first();
            if ($last_cycle) {
                $date_end = Carbon::parse($last_cycle->date)->addDays($last_cycle->duration)->format('Y-m-d');
            }

            // 1er ajout
            if (! isset($date_end)) {
                $date_end = Carbon::now()->subMonth()->format('Y-m-d');
            }

            return [
                'duration' => [
                    'required',
                    'integer',
                    'min:1',
                    'max:8',
                ],
                'comment' => [
                    'nullable',
                    'string',
                    'max:99',
                ],
                'date' => [
                    'required',
                    'date',
                    'date_format:"Y-m-d"',
                    'after:'.$date_end,
                    Rule::unique('cycles', 'date')->where(function ($query) {
                        return $query->where('user_id', Auth::id());
                    })->ignore($this->id),
                ],
            ];

        } else {

            return [
                'duration' => [
                    'required',
                    'integer',
                    'min:1',
                    'max:8',
                ],
                'comment' => [
                    'nullable',
                    'string',
                    'max:99',
                ],
                'date' => [
                    'required',
                    'date',
                    'date_format:"Y-m-d"',
                    'before:'.$tomorrow,
                    Rule::unique('cycles', 'date')->where(function ($query) {
                        return $query->where('user_id', Auth::id());
                    })->ignore($this->id),
                ],
            ];

        }

    }
}
