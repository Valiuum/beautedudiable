<?php

namespace App\Events;

use App\Models\Activity;
use App\Models\Badge;
use App\Models\PointWeekly;
use App\Notifications\BadgeUnlockNotification;
use App\Notifications\WebPushNotification;
use Carbon\Carbon;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Badges
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    private Badge $badge;

    private static $lastTriggered = null;

    public function __construct(Badge $badge)
    {
        $this->badge = $badge;
    }

    public function onSavedActivity(Activity $activity): void
    {
        $user = $activity->user;
        $activity_count = $user->activities()->count();
        $badge = $this->badge->unlockActionFor($user, $activity->activity, $activity_count);
        if ($badge) {
            $user->notify(new BadgeUnlockNotification($badge));

            // WebPushNotification
            $preferences = $user->preferences ? collect(json_decode($user->preferences, true)) : collect();
            if (! is_null($preferences) && $preferences->get('notificationActivate') && $preferences->get('notificationBadges')) {
                $user->notify(new WebPushNotification('badge_unlocked'));
            }
        }
    }

    public function onSavedMeal(PointWeekly $pointWeekly): void
    {
        $currentTime = Carbon::now();

        // Check if the event was triggered within the last second
        if (self::$lastTriggered && $currentTime->diffInSeconds(self::$lastTriggered) < 3) {
            return;
        }

        self::$lastTriggered = $currentTime;

        $user = $pointWeekly->user;

        $start_date = Carbon::now()->startOfWeek()->subMonths('2')->format('Y-m-d');
        $end_date = Carbon::now()->subWeek()->startOfWeek()->format('Y-m-d');

        $success_weeks = $user->weeklyPoints()
            ->where('status', true)
            ->whereBetween('date', [
                $start_date,
                $end_date,
            ])
            ->count();

        if ($success_weeks) {
            $meals_count = $user->dailyPoints()
                ->whereBetween('date', [
                    $start_date,
                    $end_date,
                ])
                ->sum('meals');

            // 2 meals per day in a week = 14 meals
            if ((int) $meals_count >= (int) ($success_weeks * 14)) {
                $badge = $this->badge->unlockActionFor($user, 'week', $success_weeks);
                if ($badge) {
                    $user->notify(new BadgeUnlockNotification($badge));
                    // WebPushNotification
                    $preferences = $user->preferences ? collect(json_decode($user->preferences, true)) : collect();
                    if (! is_null($preferences) && $preferences->get('notificationActivate') && $preferences->get('notificationBadges')) {
                        $user->notify(new WebPushNotification('badge_unlocked'));
                    }
                }
            }

        }

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}
