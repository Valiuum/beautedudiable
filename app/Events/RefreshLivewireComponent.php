<?php

namespace App\Events;

use App\Models\Meal;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RefreshLivewireComponent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public User $user;

    public ?Meal $meal;

    public function __construct(private readonly ?int $meal_id, private readonly int $user_id)
    {
        $this->user = User::findOrFail($user_id);
        $this->meal = $meal_id ? Meal::find($meal_id) : null;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('livewire.refresh.'.$this->user->id);
    }
}
