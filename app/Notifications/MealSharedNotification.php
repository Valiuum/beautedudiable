<?php

namespace App\Notifications;

use App\Models\Meal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;

class MealSharedNotification extends Notification implements ShouldBroadcast, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, Queueable, SerializesModels;

    public Meal $meal;

    public User $user;

    /**
     * Create a new notification instance.
     */
    public function __construct(int $meal_id, int $user_id)
    {
        $this->meal = Meal::findOrFail($meal_id);
        $this->user = User::findOrFail($user_id);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'shared_by' => $this->user->name ?? 'Utilisateur inconnu',
            'name' => $this->meal->name,
            'date' => Carbon::parse($this->meal->created_at)->setTimezone('Europe/Paris')->isoFormat('LL'),
        ];
    }

    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'shared_by' => $this->user->name ?? 'Utilisateur inconnu',
            'name' => $this->meal->name ?? 'Nom menu inconnu',
            'date' => Carbon::parse($this->meal->created_at)->setTimezone('Europe/Paris')->isoFormat('LL'),
        ]);
    }
}
