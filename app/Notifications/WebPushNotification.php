<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class WebPushNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(private $action, private $userName = null) {}

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable): WebPushMessage
    {

        switch ($this->action) {
            case 'test':
                return (new WebPushMessage)
                    ->title(trans_choice('messages.notification_title', 1, ['username' => $notifiable->name]))
                    ->body(trans('messages.notification_body_test'))
                    ->icon(url('apple-touch-icon.png'));

            case 'reminder_meal':
                return (new WebPushMessage)
                    ->title(trans_choice('messages.notification_title', 1, ['username' => $notifiable->name]))
                    ->body(trans('messages.notification_body_reminder_meal'))
                    ->icon(url('apple-touch-icon.png'));

            case 'badge_unlocked':
                return (new WebPushMessage)
                    ->title(trans_choice('messages.notification_title', 1, ['username' => $notifiable->name]))
                    ->body(trans('messages.notification_body_badge_unlocked'))
                    ->icon(url('apple-touch-icon.png'));

            case 'shared_meal':
                return (new WebPushMessage)
                    ->title(trans_choice('messages.notification_title', 1, ['username' => $notifiable->name]))
                    ->body(trans_choice('messages.notification_body_shared_meal', 1, ['username' => $this->userName]))
                    ->icon(url('apple-touch-icon.png'));
            default:
                throw new \Exception('Unexpected value');
        }

    }
}
