<?php

namespace App\Notifications;

use App\Models\Badge;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class BadgeUnlockNotification extends Notification
{
    use Queueable;

    private Badge $badge;

    /**
     * Create a new notification instance.
     */
    public function __construct(Badge $badge)
    {
        $this->badge = $badge;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        $user = User::find($notifiable->id);

        return [
            'name' => $user->isWomen() && $this->badge->name_women ? $this->badge->name_women : $this->badge->name,
            'action' => $this->badge->action,
            'date' => Carbon::parse($this->badge->created_at)->setTimezone('Europe/Paris')->isoFormat('LL'),
        ];
    }
}
