<?php

namespace App\Listeners;

use App\Events\Badges;
use Illuminate\Events\Dispatcher;

class BadgeEventSubscriber
{
    public function subscribe(Dispatcher $events): array
    {

        $events->listen(
            'eloquent.saved: App\Models\Activity',
            [Badges::class, 'onSavedActivity']
        );

        $events->listen(
            'eloquent.saved: App\Models\PointWeekly',
            [Badges::class, 'onSavedMeal']
        );

        return [];
    }
}
