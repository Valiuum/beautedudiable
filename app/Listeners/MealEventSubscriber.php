<?php

namespace App\Listeners;

use App\Events\MealProcessed;
use App\Events\RefreshLivewireComponent;
use App\Models\Meal;
use App\Models\User;
use App\Notifications\MealSharedNotification;
use App\Notifications\WebPushNotification;
use App\Traits\UserActions;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;

class MealEventSubscriber implements ShouldQueue
{
    use UserActions;

    public function subscribe(Dispatcher $events): array
    {
        return [
            MealProcessed::class => 'MealProcessed',
        ];
    }

    public function MealProcessed($event): void
    {
        if ($event->data['action'] === 'delete') {
            RefreshLivewireComponent::dispatch(null, $event->data['meal_to_delete']['user_id']);

            return;
        }

        // Add current meal data for current user
        $this->addMeal($event);

        // Detect if the meal is shared
        if ($event->data['shared_meal_with']) {

            $user_share_with = User::find($event->data['shared_meal_with']);
            $meal_id = $event->data['meal_id'] ? $event->data['meal_id'] : $event->meal_id;
            $meal = Meal::find($meal_id);
            $date_share_with = $event->data['new_date'] !== '' ? Carbon::parse($event->data['new_date'])->format('Y-m-d') : Carbon::parse($meal->created_at)->format('Y-m-d');
            $share_meal_exists = $user_share_with->meals()->whereDate('created_at',
                Carbon::createFromFormat('Y-m-d', $date_share_with))
                ->where('name', $meal->name)
                ->exists();
            if (! $share_meal_exists) {
                $ingredients = collect($event->data['ingredients'])->reject(function ($ingredient) {
                    return $ingredient['name'] === 'bonus';
                })->values()->all();
                $dishes = collect($event->data['dishes']);
                $this->duplicateMeal($meal, $user_share_with, $date_share_with, $ingredients, $dishes);
                $this->addMeal($event, $event->data['shared_meal_with']);
                $user_share_with->notify(new MealSharedNotification($event->meal_id, $meal->user_id));
                // WebPushNotification
                $preferences = collect(json_decode($user_share_with->preferences));
                if (! is_null($preferences) && $preferences->get('notificationActivate') && $preferences->get('notificationSharedMeals')) {
                    $user_shared_by = User::find($meal->user_id);
                    $user_share_with->notify(new WebPushNotification('shared_meal', $user_shared_by->name));
                }
                MealSharedNotification::dispatch($event->meal_id, $meal->user_id);
                RefreshLivewireComponent::dispatch($meal->id, $user_share_with->id);
            }
        }

        $meal = Meal::find($event->meal_id);
        $user = User::find($meal->user_id);
        RefreshLivewireComponent::dispatch($meal->id, $user->id);

    }
}
