<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory(1)->create([
            'name' => 'Valentin',
            'email' => 'valentindufour@live.fr',
            'gender' => 'M', // 'M' or 'F'
            'role' => 'power',
            'password' => Hash::make('fakepassword'),
        ]);

        User::factory(1)->create([
            'name' => 'fille',
            'email' => 'fille@fille.fr',
            'gender' => 'F',
            'role' => 'power',
            'password' => Hash::make('fille'),
        ]);

        User::factory(1)->create([
            'name' => 'test',
            'email' => 'test@test.fr',
            'password' => Hash::make('test'),
        ]);
    }
}
