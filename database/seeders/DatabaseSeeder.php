<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Cycle;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Weight;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        // Users
        (new UserSeeder)->run();

        Weight::factory(80)->create();

        Activity::factory(100)->create();

        Cycle::factory(50)->create();

        // Badges
        (new BadgeSeeder)->run();

        // Ingredients
        (new IngredientSeeder)->run();

        // Meals
        (new MealSeeder)->run();
    }
}
