<?php

namespace Database\Seeders;

use App\Models\Dish;
use App\Models\Meal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $mealTypes = ['breakfast', 'lunch', 'dinner', 'snack', 'bar'];
        $dishNames = ['Pancakes', 'Poulet au citron', 'Cassoulet', 'Soupe de légume', 'Pates aux pesto'];
        $userList = User::pluck('id')->toArray();

        for ($i = 0; $i < 5; $i++) {

            $ingredient_un = rand(1, 50);
            $ingredient_deux = rand(1, 50);
            $ingredient_trois = rand(1, 50);

            $dish = new Dish;
            $dish->user_id = $userList[array_rand($userList)];
            $dish->name = $dishNames[array_rand($dishNames)];
            $dish->type = $mealTypes[array_rand($mealTypes)];
            $dish->save();
            $dish->ingredients()->sync([
                $ingredient_un => ['points' => rand(0, 23)],
                $ingredient_deux => ['points' => rand(0, 23)],
                $ingredient_trois => ['points' => rand(0, 23)],
            ]);
        }

        for ($i = 0; $i < 50; $i++) {
            $meal = new Meal;
            $meal->name = $mealTypes[array_rand($mealTypes)];
            $meal->user_id = $userList[array_rand($userList)];
            $meal->created_at = Carbon::now()->subDays(rand(4, 30));
            $meal->save();
            $meal->ingredients()->sync([
                rand(1, 50) => ['points' => rand(0, 23)],
                rand(1, 50) => ['points' => rand(0, 23)],
                rand(1, 50) => ['points' => rand(0, 23)],
            ]);

            $dish = Dish::find(rand(1, 5));
            $dish_meal = $dish->ingredients->map(function ($ingredient) use ($meal, $dish) {
                return [
                    'dish_id' => $dish->id,
                    'meal_id' => $meal->id,
                    'ingredient_id' => $ingredient->id,
                    'points' => rand(1, 23), // Assign random points
                ];
            })->toArray();

            $meal->dishes()->sync($dish_meal);
        }
    }
}
