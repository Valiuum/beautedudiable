<?php

namespace Database\Seeders;

use App\Models\Badge;
use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Badge::factory(1)->create([
            'display_order' => 1,
            'name' => 'Sportif du dimanche',
            'name_women' => 'Sportive du dimanche',
            'action' => 'sports',
            'action_count' => 50,
        ]);

        Badge::factory(1)->create([
            'display_order' => 1,
            'name' => 'Prouve moi que tu es une machine',
            'name_women' => '',
            'action' => 'sports',
            'action_count' => 100,
        ]);

        Badge::factory(1)->create([
            'display_order' => 1,
            'name' => 'Iron Man',
            'name_women' => 'Wonder Women',
            'action' => 'sports',
            'action_count' => 500,
        ]);

        Badge::factory(1)->create([
            'display_order' => 1,
            'name' => 'Spartiate',
            'name_women' => '',
            'action' => 'sports',
            'action_count' => 1000,
        ]);

        Badge::factory(1)->create([
            'display_order' => 1,
            'name' => 'Hercules',
            'name_women' => 'Athéna',
            'action' => 'sports',
            'action_count' => 5000,
        ]);

        // workout
        // running
        // hiking
        // yoga

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'Cul d\'acier',
            'name_women' => '',
            'action' => 'workout',
            'action_count' => 100,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'La vie s\'est musclée',
            'name_women' => '',
            'action' => 'workout',
            'action_count' => 200,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'Abdos Dips Pompes Barre',
            'name_women' => '',
            'action' => 'workout',
            'action_count' => 500,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'Cuisses de Lopez',
            'name_women' => '',
            'action' => 'workout',
            'action_count' => 1000,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'Biceps de Stallone',
            'name_women' => 'Big Biceps',
            'action' => 'workout',
            'action_count' => 1500,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'Abdos de Schwarzenegger',
            'name_women' => 'Abdos en béton',
            'action' => 'workout',
            'action_count' => 2000,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'On m\'appelle la buche',
            'name_women' => '',
            'action' => 'workout',
            'action_count' => 2500,
        ]);

        Badge::factory(1)->create([
            'display_order' => 2,
            'name' => 'Affuté comme une lame',
            'name_women' => 'Affutée comme une lame',
            'action' => 'workout',
            'action_count' => 3000,
        ]);

        Badge::factory(1)->create([
            'display_order' => 3,
            'name' => 'Joggeur du dimanche',
            'name_women' => 'Joggeuse du dimanche',
            'action' => 'running',
            'action_count' => 50,
        ]);

        Badge::factory(1)->create([
            'display_order' => 3,
            'name' => 'Triple galop',
            'name_women' => '',
            'action' => 'running',
            'action_count' => 100,
        ]);

        Badge::factory(1)->create([
            'display_order' => 3,
            'name' => 'Usain Bolt',
            'name_women' => 'Marie-José Pérec',
            'action' => 'running',
            'action_count' => 500,
        ]);

        Badge::factory(1)->create([
            'display_order' => 3,
            'name' => 'Flash',
            'name_women' => '',
            'action' => 'running',
            'action_count' => 1000,
        ]);

        Badge::factory(1)->create([
            'display_order' => 4,
            'name' => 'Randonneur du dimanche',
            'name_women' => 'Randonneuse du dimanche',
            'action' => 'hiking',
            'action_count' => 10,
        ]);

        Badge::factory(1)->create([
            'display_order' => 4,
            'name' => 'Mollets d\'acier',
            'name_women' => '',
            'action' => 'hiking',
            'action_count' => 50,
        ]);

        Badge::factory(1)->create([
            'display_order' => 4,
            'name' => 'Roi de la montagne',
            'name_women' => 'Reine de la montagne',
            'action' => 'hiking',
            'action_count' => 100,
        ]);

        Badge::factory(1)->create([
            'display_order' => 5,
            'name' => 'Semaine OKLM',
            'name_women' => '',
            'action' => 'week',
            'action_count' => 1,
        ]);

        Badge::factory(1)->create([
            'display_order' => 5,
            'name' => 'Petit appétit',
            'name_women' => '',
            'action' => 'week',
            'action_count' => 2,
        ]);

        Badge::factory(1)->create([
            'display_order' => 5,
            'name' => 'Semaine 5 étoiles',
            'name_women' => '',
            'action' => 'week',
            'action_count' => 5,
        ]);

        Badge::factory(1)->create([
            'display_order' => 5,
            'name' => 'As de la fourchette',
            'name_women' => '',
            'action' => 'week',
            'action_count' => 10,
        ]);

        Badge::factory(1)->create([
            'display_order' => 5,
            'name' => 'Cuistot d\'élite',
            'name_women' => '',
            'action' => 'week',
            'action_count' => 20,
        ]);

        Badge::factory(1)->create([
            'display_order' => 5,
            'name' => 'Papilles de chef',
            'name_women' => '',
            'action' => 'week',
            'action_count' => 50,
        ]);

    }
}
