<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Ingredient::factory(50)->create();

        // Ingredient Bonus
        Ingredient::factory(1)->create([
            'name' => 'bonus',
            'description' => 'ingredient bonus',
            'points' => 1,
            'quantity' => 1,
            'unit' => 'u',
            'energy' => 0,
            'proteins' => 0,
            'lipids' => 0,
            'carbohydrates' => 0,
            'fibers' => 0,
            'open_food_facts_id' => '',
        ]);
    }
}
