<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Activitie>
 */
class ActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $activities = ['running', 'yoga', 'hiking', 'workout'];
        $randomMActivity = $this->faker->randomElement($activities);

        return [
            'user_id' => $this->faker->numberBetween(1, 3),
            'activity' => $randomMActivity,
            'comment' => $this->faker->sentence(6),
            'date' => $this->faker->dateTimeBetween('2020-02-01', 'yesterday'),
        ];
    }
}
