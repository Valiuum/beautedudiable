<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cycle>
 */
class CycleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => 2,
            'duration' => $this->faker->numberBetween(4, 7),
            'comment' => $this->faker->sentence(6),
            'date' => $this->faker->dateTimeBetween('2020-02-01', 'yesterday'),
        ];
    }
}
