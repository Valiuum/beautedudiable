<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ingredient>
 */
class IngredientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $faker = \Faker\Factory::create();
        $faker->addProvider(new \FakerRestaurant\Provider\fr_FR\Restaurant($faker));

        $methods = ['beverageName', 'dairyName', 'vegetableName', 'fruitName', 'meatName', 'sauceName'];
        $random = $faker->randomElement($methods);
        $ingredient = $faker->$random();

        return [
            'name' => $ingredient,
            'description' => $this->faker->sentence(6, true),
            'points' => $this->faker->numberBetween(2, 100) / 2,
            'quantity' => $this->faker->numberBetween(1, 20),
            'unit' => $this->faker->randomElement(['g', 'kg', 'ml', 'cl', 'l']),
            'energy' => $this->faker->randomFloat(1, 0, 2500),
            'proteins' => $this->faker->randomFloat(1, 0, 900),
            'lipids' => $this->faker->randomFloat(1, 0, 900),
            'carbohydrates' => $this->faker->randomFloat(1, 0, 900),
            'fibers' => $this->faker->randomFloat(1, 0, 900),
            'open_food_facts_id' => $this->faker->unique()->sentence(1),
        ];
    }
}
