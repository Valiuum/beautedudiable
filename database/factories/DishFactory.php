<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\dish>
 */
class DishFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \FakerRestaurant\Provider\fr_FR\Restaurant($faker));

        $dishsTypes = ['breakfast', 'lunch', 'dinner', 'snack', 'bar'];
        $dishsNames = ['beverageName', 'dairyName', 'meatName', 'sauceName'];

        $dishsName = $faker->randomElement($dishsNames);
        $dishsType = $faker->randomElement($dishsTypes);

        $name = $faker->$dishsName();
        $type = $faker->$dishsType();

        return [
            'user_id' => $this->faker->numberBetween(1, 2),
            'name' => $name,
            'type' => $type,
        ];
    }
}
