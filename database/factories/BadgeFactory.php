<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Badge>
 */
class BadgeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'display_order' => fake()->numberBetween(1, 10),
            'name' => fake()->word,
            'name_women' => fake()->word,
            'action' => fake()->word,
            'action_count' => fake()->numberBetween(1, 10),
        ];
    }
}
