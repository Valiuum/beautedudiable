<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Weight>
 */
class WeightFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 3),
            'weighing' => $this->faker->randomFloat(1, 60, 85),
            'comment' => $this->faker->sentence(6),
            'date' => $this->faker->dateTimeBetween('2020-02-01', 'yesterday'),
        ];
    }
}
