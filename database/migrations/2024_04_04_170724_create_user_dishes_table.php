<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dishes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->constrained()
                ->onDelete('cascade');
            $table->string('name')->index();
            $table->string('type')->index();
            $table->timestamps();
        });

        // Add a full-text index
        DB::statement('ALTER TABLE dishes ADD FULLTEXT fulltext_index (name, type)');

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dishes');
        DB::statement('ALTER TABLE dishes DROP INDEX fulltext_index');
    }
};
