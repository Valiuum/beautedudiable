<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::table('users', function (Blueprint $table) {

            $table->string('gender', 1)
                ->nullable()
                ->after('email_verified_at');

            $table->string('role', 30)
                ->default('regular')
                ->after('gender');

            $table->json('preferences')
                ->nullable()
                ->after('role');

            $table->boolean('is_admin')
                ->default(false)
                ->after('preferences');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['gender', 'role', 'preferences', 'is_admin']);
        });
    }
};
