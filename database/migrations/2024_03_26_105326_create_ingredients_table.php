<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->longText('description')->nullable();
            $table->float('points');
            $table->float('quantity');
            $table->string('unit');
            $table->float('energy')->nullable();
            $table->float('proteins')->nullable();
            $table->float('lipids')->nullable();
            $table->float('carbohydrates')->nullable();
            $table->float('fibers')->nullable();
            $table->string('open_food_facts_id')->nullable();
            $table->timestamps();
        });

        // Add a full-text index
        DB::statement('ALTER TABLE ingredients ADD FULLTEXT fulltext_index (name, description)');
        // Change the table collation
        // DB::statement('ALTER TABLE ingredients CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ingredients');
        DB::statement('ALTER TABLE ingredients DROP INDEX fulltext_index');
        // DB::statement('ALTER TABLE ingredients CONVERT TO CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    }
};
